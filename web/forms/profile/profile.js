require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/text'
		}
	}
}),

require
(
	[
		  'forms/profile/abonent/c_abonent'
		, 'forms/profile/user/c_user'
		, 'forms/profile/tabs/c_tabs'
		, 'forms/profile/tab/c_tab'
		, 'forms/profile/eforms/c_eforms'
	],
	function (c_abonent, c_user, c_full, c_tab, c_eforms)
	{
		var c_profile =
		{
			  Abonent: c_abonent
			, User: c_user
			, Full: c_full
			, Tab: c_tab
			, Extensions: c_eforms
		};
		if (RegisterCpwProfileControllers)
			RegisterCpwProfileControllers(c_profile);
		return c_profile;
	}
);