define([
	  'forms/base/c_binded'
	, 'tpl!forms/profile/tabs/e_tabs.html'
	, 'forms/profile/abonent/c_abonent'
	, 'forms/profile/user/c_user'
	, 'forms/profile/tab/c_tab'
	, 'forms/fms/base/h_tabs'
	, 'forms/base/log'
],
function (binded_controller, tpl, c_abonent, c_user, c_tab, helper_tabs, GetLogger)
{
	return function (extensions)
	{
		var log = GetLogger('c_tabs');
		var controller = binded_controller(tpl, { extensions: extensions });

		var base_Render = controller.Render;
		controller.Render = function (id_form_div)
		{
			base_Render.call(this, id_form_div);

			this.c_Common = c_tab('', { Abonent: c_abonent, User: c_user });
			this.c_Common.SetFormContent(this.model);
			this.c_Common.Edit('#profile_common_tab_content');

			this.c_Extensions = {};
			for (var extension_key in extensions)
			{
				var extension = extensions[extension_key];
				var c_Extension = c_tab(extension_key, extension.controllers);
				this.c_Extensions[extension_key] = c_Extension;
				if (this.model && this.model.Extensions && this.model.Extensions[extension_key])
					c_Extension.SetFormContent(this.model.Extensions[extension_key]);
				c_Extension.Edit('#profile_' + extension_key + '_tab_content');
			}
			$('#profile-tabs').tabs();
			helper_tabs.bindSwitch($('#profile-tabs'), 'li', '#profile-tab');
		}

		controller.base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res = this.c_Common.GetFormContent();
			for (var extension_key in this.c_Extensions)
			{
				var c_Extension = this.c_Extensions[extension_key];
				if (!res.Extensions)
					res.Extensions = {};
				var econtent = c_Extension.GetFormContent();
				if ('string' === typeof econtent)
					econtent = JSON.parse(econtent);
				res.Extensions[extension_key] = econtent;
			}
			return res;
		};

		controller.Validate = function ()
		{
			for (var extension_key in this.c_Extensions)
			{
				var c_Extension = this.c_Extensions[extension_key];
				return c_Extension.Validate();
			}
		}

		return controller;
	}
});
