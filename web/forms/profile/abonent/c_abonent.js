define([
	  'forms/base/c_binded'
	, 'tpl!forms/profile/abonent/e_abonent.html'
	, 'forms/base/h_validate'
],
function (binded_controller, tpl, helper_validate)
{
	return function ()
	{
		var controller = binded_controller(tpl);

		var base_Render = controller.Render;
		controller.Render = function (id_form_div)
		{
			base_Render.call(this, id_form_div);
			$("#cpw_abonent_inn").keydown(helper_validate.ValidateSymbolsINN);
			$('#cpw_abonent_inn').keyup(helper_validate.OnValidateINN);
			$('#cpw_abonent_inn').focusout(helper_validate.OnValidateINN);
		}

		controller.Validate = function ()
		{
			var res = helper_validate.ValidateINN($("#cpw_abonent_inn"));
			return res ? null : "Неверно заполнены поля, подсвеченные красным цветом";
		}

		return controller;
	}
});
