define([
	  'forms/base/c_binded'
	, 'tpl!forms/profile/eforms/e_eforms.html'
],
function (binded_controller, tpl)
{
	return function ()
	{
		return binded_controller(tpl);
	}
});
