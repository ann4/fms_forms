define([
	  'forms/base/c_binded'
	, 'tpl!forms/profile/tab/e_tab.html'
	, 'tpl!forms/profile/tab/e_content.html'
	, 'forms/profile/abonent/c_abonent'
	, 'forms/profile/user/c_user'
	, 'forms/fms/base/h_tabs'
],
function (binded_controller, tpl, tpl_content, c_abonent, c_user, helper_tabs)
{
	return function (extension_key, controllers)
	{
		var template = tpl;
		if (!controllers.User)
		{
			template = tpl_content;
		}

		var controller = binded_controller(template, { extension_key: extension_key });

		var base_Render = controller.Render;
		controller.Render = function (id_form_div)
		{
			base_Render.call(this, id_form_div);

			if (controllers.Abonent)
			{
				this.c_Abonent = controllers.Abonent();
				if (this.model && this.model.Abonent)
					this.c_Abonent.SetFormContent(this.model.Abonent);
				this.c_Abonent.Edit('#' + extension_key + '_profile_abonent');
			}

			if (controllers.User)
			{
				this.c_User = controllers.User();
				if (this.model && this.model.User)
					this.c_User.SetFormContent(this.model.User);
				this.c_User.Edit('#' + extension_key + '_profile_user');
			}

			if (this.c_Abonent && this.c_User)
			{
				$('#profile-tabs-' + extension_key).tabs();
				helper_tabs.bindSwitch($('#profile-tabs-' + extension_key), 'li', '#profile-tab-' + extension_key);
			}
		}

		controller.base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res = {};
			if (this.c_Abonent)
			{
				var econtent = this.c_Abonent.GetFormContent();
				if ('string' === typeof econtent)
					econtent = JSON.parse(econtent);
				res.Abonent = econtent;
			}
			if (this.c_User)
			{
				var econtent = this.c_User.GetFormContent();
				if ('string' === typeof econtent)
					econtent = JSON.parse(econtent);
				res.User = econtent;
			}
			return res;
		};

		controller.Validate = function ()
		{
			return this.c_Abonent.Validate();
		}

		return controller;
	}
});
