define([
	  'forms/base/c_binded'
	, 'tpl!forms/profile/user/e_user.html'
],
function (binded_controller, tpl)
{
	return function ()
	{
		return binded_controller(tpl)
	}
});
