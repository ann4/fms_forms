define
(
	[
		  'forms/profile/abonent/c_abonent'
		, 'forms/profile/user/c_user'
		, 'forms/profile/tab/c_tab'
		, 'forms/fms/profile/c_abonent'
		, 'forms/profile/tabs/c_tabs'
		, 'forms/profile/eforms/c_eforms'
	],
	function (c_abonent, c_user, c_tab, c_fms_abonent, c_tabs, c_eforms)
	{
		var controls =
		{
			abonent: c_abonent
			, user: c_user
			, tab_common: function () { return c_tab('',   { Abonent: c_abonent,     User: c_user }); }
			, tabs_empty: function () { return c_tabs({}); }
			, tabs: function ()
			{
				return c_tabs
				({
					fms:
					{
						Title: 'ФМС РФ',
						controllers: { Abonent: c_fms_abonent }
					}
				});
			}
			, eforms: c_eforms
		};
		return controls;
	}
);