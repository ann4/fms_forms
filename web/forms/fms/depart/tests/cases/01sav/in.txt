wait_press_dlg_btn "Сообщение с веб-страницы" "ОК"

include ..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

execute_javascript_line "app.cpw_Now= function(){return new Date(2015, 6, 26, 17, 36, 0, 0 ); };"

wait_click_text "Вернуться без сохранения"
set_value_id form-content-text-area ""
set_value_id cpw-content-select "fms.profile_ok"
wait_click_text "Загрузить в профиль"
wait_click_text "Создать"

execute_javascript_stored_lines add_wbt_std_functions
execute_javascript_stored_lines add_select2_test_functions

wait_text "Адресный листок убытия"

play_stored_lines depart_fields_1
play_stored_lines depart_select2_fields_1
play_stored_lines depart_select2_focus_1

wait_click_text "Личные данные"
shot_check_png ..\..\shots\1sav.png
wait_click_text "Регистрация"
shot_check_png ..\..\shots\1sav_1.png
wait_click_text "Причина снятия с регистрационного учета"
shot_check_png ..\..\shots\1sav_2.png

wait_click_text "Сохранить содержимое формы"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\1sav.result.xml

exit