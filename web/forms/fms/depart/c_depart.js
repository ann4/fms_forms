﻿define([
	  'forms/fms/base/c_fms'
	, 'forms/fms/depart/x_depart'
	, 'forms/fms/base/town/c_town'
	, 'forms/fms/depart/m_depart'
	, 'forms/fms/base/town/h_town'
	, 'forms/base/h_names'
	, 'forms/fms/base/h_OfficialOrgan'
	, 'forms/base/h_times'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base//h_Select2'
	, 'forms/fms/base/h_tabs'
	, 'forms/base/h_validate'
	, 'tpl!forms/fms/depart/e_depart.html'
	, 'tpl!forms/fms/depart/v_depart.xaml'
	, 'tpl!forms/fms/depart/p_depart.html'
	, 'forms/base/log'
	, 'forms/fms/profile/h_abonent'
	, 'forms/fms/questionary/h_questionary'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/addr/c_addr'
	, 'forms/base/h_msgbox'
	, 'forms/fms/base/h_citizenship'
],
function 
(
	  BaseFormController
	, codec
	, controller_Town
	, model_depart
	, helper_Town
	, helper_Names
	, helper_OfficialOrgan
	, helper_Times
	, helper_DocumentType
	, helper_Select2
	, helper_tabs
	, helper_validate
	, layout_tpl
	, print_tpl
	, attrshtml_tpl
	, GetLogger
	, h_abonent
	, helper_Questionary
	, h_addr
	, c_addr
	, h_msgbox
	, h_citizenship
	)
{
	return function ()
	{
		var log = GetLogger('c_depart');

		var Select2Options = function (h_values)
		{
			return function (m)
			{
				return {
					placeholder: '',
					allowClear: true,
					query: function (q)
					{
						q.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), q.term) });
					}
				};
			}
		}

		var controller = BaseFormController(layout_tpl,
		{
			field_spec:
			{
				DocumentGivenBy: Select2Options({ GetValues: function () { return helper_Questionary.GetValues('v'); } })
				, DocumentGivenBy_code: Select2Options({ GetValues: function () { return helper_Questionary.GetValues('c'); } })
			}
		});

		controller.Sign = function () { }

		controller.UseProfile = function (profile)
		{
			if (!this.model)
				this.model = model_depart();
			if (profile && profile.Extensions && profile.Extensions.fms)
			{
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent)
				{
					var pAbonent = pfms.Abonent;
					pAbonent = h_abonent.SafeUpgradeAbonent(pAbonent);

					this.model.subdivision = h_abonent.SafeGet_dict_field(pAbonent, 'subdivision', this.model.subdivision);
					if (pAbonent.supplierInfo)
						this.model.supplierInfo = pAbonent.supplierInfo;

					if (pAbonent.AddressReal)
						this.model.RegAddress_variants = pAbonent.AddressReal;
					if (pAbonent.AddressReal && pAbonent.AddressReal.length == 1)
						this.model.RegAddress = pAbonent.AddressReal[0];

					var col = h_abonent.SafeGetDefaultColaborator(pAbonent);
					if (null != col)
					{
						if (col.LastName)
							this.model.fioComposing = col.LastName;

						if (col.FirstName)
							this.model.fioComposing = !this.model.fioComposing ? col.FirstName : this.model.fioComposing + ' ' + col.FirstName;

						if (col.MiddleName)
							this.model.fioComposing = !this.model.fioComposing ? col.MiddleName : this.model.fioComposing + ' ' + col.MiddleName;
					}
				}
				if (pfms.Forma)
				{
					this.ShowAttachmentsPanel = pfms.Forma.ShowAttachmentsPanel;
				}
			}
		}

		var UpdateAddressesLinksTextes = function ()
		{
			var null_text = 'Кликните, чтобы указать..';

			$('#cpw_form_Birthplace').text(!controller.model.Birthplace.text ? null_text : controller.model.Birthplace.text);
			$('#cpw_form_RegAddress').text(h_addr.SafePrepareReadableText(controller.model.RegAddress));
			$('#cpw_form_NewAddress').text(h_addr.SafePrepareReadableText(controller.model.NewAddress));
			$('#cpw_form_NewResidenceAddress').text(h_addr.SafePrepareReadableText(controller.model.NewResidenceAddress));

			$('#cpw_form_RegAddress').trigger('changeText');
		}

		var DataLoad = function (content)
		{
			$('#cpw_form_LastName').val(content.LastName);
			$('#cpw_form_FirstName').val(content.FirstName);
			$('#cpw_form_MiddleName').val(content.MiddleName);

			$('#cpw_form_LastName_latin').val(content.LastName_latin);
			$('#cpw_form_FirstName_latin').val(content.FirstName_latin);
			$('#cpw_form_MiddleName_latin').val(content.MiddleName_latin);

			$('#cpw_form_NewLastName').val(content.NewLastName);
			$('#cpw_form_NewFirstName').val(content.NewFirstName);
			$('#cpw_form_NewMiddleName').val(content.NewMiddleName);

			$('#cpw_form_NewLastName_latin').val(content.NewLastName_latin);
			$('#cpw_form_NewFirstName_latin').val(content.NewFirstName_latin);
			$('#cpw_form_NewMiddleName_latin').val(content.NewMiddleName_latin);

			$('#cpw_form_Birthday').val(!content.Birthday ? '' : content.Birthday);
			$('#cpw_form_NewBirthday').val(!content.NewBirthday ? '' : content.NewBirthday);
			$('#cpw_form_DocumentGivenDate').val(!content.DocumentGivenDate ? '' : content.DocumentGivenDate);
			$('#cpw_form_DateFrom').val(!content.DateFrom ? '' : content.DateFrom);
			$('#cpw_form_DateTill').val(!content.DateTill ? '' : content.DateTill);
			$('#cpw_form_OtherReason').val(!content.OtherReason ? '' : content.OtherReason);

			content.dateComposing = content.dateComposing ? content.dateComposing : helper_Times.nowDateUTC('r');
			content.NewResidenceAddressSameCityAddress ? $('#cpw_form_NewResidenceAddressSameCityAddress').attr('checked', 'checked') : '';

			UpdateAddressesLinksTextes(content);

			$('#cpw_form_DocumentSeries').val(content.DocumentSeries);
			$('#cpw_form_DocumentNumber').val(content.DocumentNumber);

			if (!content.DocumentGivenBy)
				$('#cpw_form_DocumentGivenBy').select2('data', { id: null, text: '' });

			controller.DataLoadSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content.Sex)
			controller.DataLoadSex('#cpw_form_NewSex_male', '#cpw_form_NewSex_female', content.NewSex)

			$('#cpw_form_Nationality').select2('data', content.Nationality);
			$('#cpw_form_DocumentType').select2('data', content.DocumentType);
		}

		var DataSave = function (content)
		{
			content.LastName = $.trim($('#cpw_form_LastName').val());
			content.FirstName = $.trim($('#cpw_form_FirstName').val());
			content.MiddleName = $.trim($('#cpw_form_MiddleName').val());
			content.NewLastName = $.trim($('#cpw_form_NewLastName').val());
			content.NewFirstName = $.trim($('#cpw_form_NewFirstName').val());
			content.NewMiddleName = $.trim($('#cpw_form_NewMiddleName').val());
			content.Birthday = $('#cpw_form_Birthday').val();
			content.NewBirthday = $('#cpw_form_NewBirthday').val();
			content.DocumentGivenDate = $('#cpw_form_DocumentGivenDate').val();
			content.DocumentSeries = $('#cpw_form_DocumentSeries').val().replace(/\s+/g, '');
			content.DocumentNumber = $('#cpw_form_DocumentNumber').val().replace(/\s+/g, '');
			content.DateFrom = $.trim($('#cpw_form_DateFrom').val());
			content.DateTill = $.trim($('#cpw_form_DateTill').val());
			content.OtherReason = $.trim($('#cpw_form_OtherReason').val());

			content.Sex = controller.DataSaveSex('#cpw_form_Sex_male', '#cpw_form_Sex_female');
			content.NewSex = controller.DataSaveSex('#cpw_form_NewSex_male', '#cpw_form_NewSex_female');

			content.Nationality = $('#cpw_form_Nationality').select2('data');
			content.DocumentType = $('#cpw_form_DocumentType').select2('data');

			content.NewResidenceAddressSameCityAddress = $('#cpw_form_NewResidenceAddressSameCityAddress').is(':checked');

			var txt_creation = helper_Times.unixDateTimeStamp();

			controller.SafeFixUids(content, txt_creation, ['number', 'uid', 'requestId',
				'person_uid', 'person_personId', 'Document_uid']);
		}

		var base_Render = controller.Render;
		controller.Render = function (form_div_selector)
		{
			base_Render.call(this, form_div_selector);

			var elLastName = $('#cpw_form_LastName');

			elLastName.change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_FirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_MiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_NewLastName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_NewFirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_NewMiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$('#cpw_form_Birthday').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_NewBirthday').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DocumentGivenDate').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DateFrom').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DateTill').datepicker(helper_Times.DatePickerOptions);

			$("#cpw_form_Birthday").change(controller.ValidateBirthday);
			$("#cpw_form_NewBirthday").change(helper_validate.OnConvertToDateFormat);
			$("#cpw_form_DocumentGivenDate").change(controller.ValidateBirthday);
			$("#cpw_form_DateFrom").change(controller.ValidateStayPeriod);
			$("#cpw_form_DateTill").change(controller.ValidateStayPeriod);

			$('#cpw_form_LastName').focusout(controller.OnValidateNamePerson);
			$('#cpw_form_FirstName').focusout(controller.OnValidateNamePerson);
			$('#cpw_form_MiddleName').focusout(controller.OnValidateNamePerson);
			$('#cpw_form_NewLastName').focusout(controller.OnValidateNamePerson);
			$('#cpw_form_NewFirstName').focusout(controller.OnValidateNamePerson);
			$('#cpw_form_NewMiddleName').focusout(controller.OnValidateNamePerson);
			$('#cpw_form_DocumentNumber').focusout(helper_validate.OnValidateIsNotEmpty);
			$('#cpw_form_DateFrom').focusout(controller.ValidateStayPeriod);
			$('#cpw_form_DateTill').focusout(controller.ValidateStayPeriod);
			$('#cpw_form_RegAddress').focusout(controller.ValidateAddress);

			$('#cpw_form_LastName').keyup(controller.OnValidateNamePerson);
			$('#cpw_form_FirstName').keyup(controller.OnValidateNamePerson);
			$('#cpw_form_MiddleName').keyup(controller.OnValidateNamePerson);
			$('#cpw_form_NewLastName').keyup(controller.OnValidateNamePerson);
			$('#cpw_form_NewFirstName').keyup(controller.OnValidateNamePerson);
			$('#cpw_form_NewMiddleName').keyup(controller.OnValidateNamePerson);
			$('#cpw_form_DocumentNumber').keyup(helper_validate.OnValidateIsNotEmpty);

			var select2_Nationality = $('#cpw_form_Nationality');
			select2_Nationality.select2({
				placeholder: '',
				allowClear: true,
				query: function (query)
				{	// query.term..
					query.callback({ results: helper_Select2.FindForSelect(h_citizenship.GetValues(), query.term) });
				}
			});

			var select2_DocumentType = $('#cpw_form_DocumentType');
			select2_DocumentType.select2({
				placeholder: '',
				allowClear: true,
				query: function (query)
				{
					query.callback({ results: helper_Select2.FindForSelect(helper_DocumentType.Values, query.term) });
				}
			});

			$('#cpw_form_DocumentGivenBy_code').on("select2-selecting", function (e)
			{
				var DocumentGivenBy = helper_Questionary.GetDocumentGivenBy_ByCode(e.object);
				$('#cpw_form_DocumentGivenBy').select2('data', DocumentGivenBy);
			});

			var content = this.model;
			var onUpdateAddressesLinksTextes = function () { UpdateAddressesLinksTextes(content); };
			var controller_Birthplace = controller_Town(content.Birthplace, onUpdateAddressesLinksTextes);

			$('#cpw_form_Birthplace').click(function (e) { controller_Birthplace.ShowModal(e); });

			$('#cpw_form_RegAddress').click(this.CreateOnClickAddressHandler(this.model, 'RegAddress', 'Место пребывания'));
			$('#cpw_form_NewAddress').click(this.CreateOnClickAddressHandler(this.model, 'NewAddress', 'Место жительства'));
			$('#cpw_form_NewResidenceAddress').click(this.NewAddress_Click);

			DataLoad(this.model);
			$('#form-tabs').tabs();
			helper_tabs.bindSwitch($('#form-tabs'), 'li', '#form-tab');
			$('#form-tab-1').on('tabIndexChanged', controller.ValidateTab1);
			$('#form-tab-2').on('tabIndexChanged', controller.ValidateTab2);
			$('#form-tab-3').on('tabIndexChanged', controller.ValidateTab3);

			$('#cpw_form_RegAddress').on('changeText', controller.ValidateAddress);

			$('.encoding_date').change(helper_Names.OnNameChange_NormalizeEncoding);

			elLastName.focus();
			elLastName.select();
		}

		controller.OnClickAddress = function (content, field_name, title)
		{
			var controller = c_addr();
			var field_value = content[field_name];
			if (null != field_value && (!field_value.Country || null == field_value.Country))
				field_value.Country = { id: 'RUS', text: "Россия" };
			controller.SetFormContent(field_value);
			var bname_Ok = 'Сохранить';
			h_msgbox.ShowModal({
				controller: controller
				, width: 750
				, height: 400
				, title: !title ? 'Адрес' : title
				, buttons: [bname_Ok, 'Отменить']
				, onclose: function (bname)
				{
					if (bname_Ok == bname)
					{
						content[field_name] = controller.GetFormContent();
						UpdateAddressesLinksTextes(content);
					}
				}
			});
		}

		controller.CreateOnClickAddressHandler = function (content, field_name, title)
		{
			return function (e)
			{
				e.preventDefault();
				controller.OnClickAddress(content, field_name, title);
			}
		}

		controller.NewAddress_Click = function (e)
		{
			e.preventDefault();
			controller.model.NewResidenceAddressSameCityAddress = $('#cpw_form_NewResidenceAddressSameCityAddress').is(':checked');
			if (controller.model.NewResidenceAddressSameCityAddress)
				helper_Town.CopyFromTo(controller.model.RegAddress, controller.model.NewResidenceAddress);
			controller.OnClickAddress(controller.model, 'NewResidenceAddress', 'Новое место жительства');
		}

		controller.CreateNew = function (form_div_selector)
		{
			if (!this.model)
				this.model = model_depart();
			this.Render(form_div_selector);

			$('#cpw_form_Nationality').select2('data', { id: 'RUS', text: "Россия" });
			$('#cpw_form_DocumentType').select2('data', { id: 103008, text: "Паспорт гражданина Российской Федерации" });

			this.BeforeEditValidation();
		};

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return attrshtml_tpl(this.model);
		};

		controller.Edit = function (form_div_selector)
		{
			this.Render(form_div_selector);
		};

		controller.BuildXamlView = function (form_div_selector)
		{
			log.Debug('BuildXamlView {');
			var RegAddress_parts = h_addr.PrepareAddressParts(this.model.RegAddress);
			var NewAddress_parts = h_addr.PrepareAddressParts(this.model.NewAddress);
			var NewResidenceAddress_parts = h_addr.PrepareAddressParts(this.model.NewResidenceAddress);
			var args = {
				form: this.model
				, helper_Town: helper_Town
				, helper_Times: helper_Times
				, helper_OfficialOrgan: helper_OfficialOrgan
				, RegAddress: RegAddress_parts
				, NewAddress: NewAddress_parts
				, NewResidenceAddress: NewResidenceAddress_parts
			};
			log.Debug('call print_tpl {');
			var content = print_tpl(args);
			log.Debug('call print_tpl }');
			log.Debug('BuildXamlView }');
			return content;
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			this.model = base_GetFormContent.call(this);
			log.Debug('GetFormContent() {');
			DataSave(this.model);
			log.Debug('GetFormContent() }');
			return this.model;
		};

		controller.SetFormContent = function (content)
		{
			this.model = content;

			if (content.DocumentGivenBy_code && !content.DocumentGivenBy_code.text && content.DocumentGivenBy_code.id)
			{
				var item = helper_Select2.FindById(helper_Questionary.GetValues('c'), content.DocumentGivenBy_code.id)
				if (item)
					content.DocumentGivenBy_code.text = item.text;
			}

			return null;
		};

		controller.BeforeEditValidation = function ()
		{
			if (this.model.subdivision.id == -1)
			{
				alert('Не заполнены обязательные поля в профиле');
			}
		}

		controller.OnValidateNamePerson = function (e)
		{
			if (e.keyCode == 9)
				return;
			var elem = $(e.target);
			var arrControlsRequire = ["cpw_form_MiddleName", "cpw_form_NewLastName", "cpw_form_NewFirstName", "cpw_form_NewMiddleName"];
			if (arrControlsRequire.indexOf(e.target.id) == -1)
			{
				var res = helper_validate.ValidateIsNotEmpty(elem) &&
						  helper_validate.IsValidName(elem);
			}
			else
			{
				helper_validate.IsValidName(elem);
			}
		};

		controller.ValidateBirthday = function (e)
		{
			if (e)
				helper_validate.ConvertToDateFormat($(e.target));
			return helper_validate.IsValidDateBeforeNow($('#cpw_form_Birthday')) &&
				   helper_validate.IsValidDateBeforeNow($('#cpw_form_DocumentGivenDate')) &&
				   (($('#cpw_form_Birthday').val() == "" || $('#cpw_form_DocumentGivenDate').val() == "") ||
				   helper_validate.IsValidPeriodDate($('#cpw_form_Birthday'), $('#cpw_form_DocumentGivenDate'), 'Дата выдачи документа не должна быть меньше даты рождения'));
		};

		controller.ValidateStayPeriod = function (e)
		{
			if (e)
				helper_validate.ConvertToDateFormat($(e.target));
			var res = helper_validate.ValidateIsNotEmpty($('#cpw_form_DateFrom'));
			res &= helper_validate.IsValidDate($('#cpw_form_DateFrom'));
			res &= helper_validate.ValidateIsNotEmpty($('#cpw_form_DateTill'));
			res &= helper_validate.IsValidDate($('#cpw_form_DateTill'));
			return res && helper_validate.IsValidDateBeforeNow($('#cpw_form_DateFrom')) &&
				   helper_validate.IsValidPeriodDate($('#cpw_form_DateFrom'), $('#cpw_form_DateTill'), 'Дата окончания регистрации не может быть меньше даты начала регистрации');
		};

		controller.ValidateAddress2 = function (address, idControl)
		{
			var address = controller.model == undefined ? null : controller.model.RegAddress;
			var idControl = 'cpw_form_RegAddress';
			var is_empty_address = h_addr.IsEmptyAddress(address);
			if (!is_empty_address)
			{
				$('#' + idControl + '_errors').hide();
			}
			else
			{
				$('#' + idControl + '_errors').show();
			}
		};

		controller.ValidateAddress = function (e)
		{
			var address = controller.model == undefined ? null : controller.model.RegAddress;
			var idControl = 'cpw_form_RegAddress';
			controller.ValidateAddress2(address, idControl)
		};

		controller.ValidateTab1 = function (e)
		{
			var res = helper_validate.ValidateIsNotEmpty($('#cpw_form_LastName')) && helper_validate.IsValidName($('#cpw_form_LastName'));
			res &= helper_validate.ValidateIsNotEmpty($('#cpw_form_FirstName')) && helper_validate.IsValidName($('#cpw_form_FirstName'));
			res &= $('#cpw_form_MiddleName').val() == "" || helper_validate.IsValidName($('#cpw_form_MiddleName'));
			res &= helper_validate.ValidateIsNotEmpty($('#cpw_form_Nationality'));
			res &= helper_validate.ValidateIsNotEmpty($('#cpw_form_DocumentType'));
			res &= helper_validate.ValidateIsNotEmpty($('#cpw_form_DocumentNumber'));
			res &= controller.ValidateBirthday();
			helper_validate.IsValidControlsOnTabPage(res, $('#title-tab-1'));
			return res;
		};

		controller.ValidateTab2 = function (e)
		{
			var res = controller.ValidateStayPeriod();
			controller.ValidateAddress2(controller.model == undefined ? null : controller.model.RegAddress, 'cpw_form_RegAddress');
			helper_validate.IsValidControlsOnTabPage(res, $('#title-tab-2'));
			return res;
		};

		controller.ValidateTab3 = function (e)
		{
			var res = helper_validate.IsValidDateBeforeNow($('#cpw_form_NewBirthday'));
			res &= $('#cpw_form_NewLastName').val() == "" || helper_validate.IsValidName($('#cpw_form_NewLastName'));
			res &= $('#cpw_form_NewFirstName').val() == "" || helper_validate.IsValidName($('#cpw_form_NewFirstName'));
			res &= $('#cpw_form_NewMiddleName').val() == "" || helper_validate.IsValidName($('#cpw_form_NewMiddleName'));
			helper_validate.IsValidControlsOnTabPage(res, $('#title-tab-3'));
			return res;
		};

		controller.Validate = function ()
		{
			var resTab1 = controller.ValidateTab1();
			var resTab2 = controller.ValidateTab2();
			var resTab3 = controller.ValidateTab3();
			var message = '';
			if (!(resTab1 && resTab2 && resTab3))
			{
				var message = 'Неверно заполнены поля, подсвеченные красным цветом на вкладках:';
				if (!resTab1)
					message += '\r\n    "' + $('#title-tab-1')[0].textContent + '"';
				if (!resTab2)
					message += '\r\n    "' + $('#title-tab-2')[0].textContent + '"';
				if (!resTab3)
					message += '\r\n    "' + $('#title-tab-3')[0].textContent + '"';
			}
			return message;
		};

		controller.UseCodec(codec());

		return controller;
	}
});