define([
	  'forms/fms/depart/c_depart'
	, 'forms/fms/depart/ff_depart'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'depart'
		, Title: 'Листок убытия. Форма7'
		, FileFormat: FileFormat
	};
	return form_spec;
});
