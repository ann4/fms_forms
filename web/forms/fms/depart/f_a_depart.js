define([
	  'forms/fms/depart/c_a_depart'
	, 'forms/fms/depart/ff_depart'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_depart'
		, Title: 'Листок убытия. Форма7'
		, FileFormat: FileFormat
	};
	return form_spec;
});
