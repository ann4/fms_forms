﻿define([
	  'forms/fms/base/x_fms'
	, 'forms/fms/depart/m_depart'
	, 'forms/fms/base/town/m_town'
	, 'txt!forms/fms/schemes/1.3.36/addrSheet.xsd'
	, 'txt!forms/fms/schemes/1.3.36/core.xsd'
	, 'txt!forms/fms/schemes/1.3.36/xmldsig-core-schema-without-dtd.xml'
	, 'forms/base/log'
	, 'forms/fms/base/addr/h_addr'
],
function (BaseCodec, model_depart, model_town, addrSheet, core, xmldsig, GetLogger, h_addr)
{
	var log = GetLogger('x_depart');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.tabs = ['', '  ', '    ', '      ', '        ', '          ', '            ', '              ', '                ', '                  '];
		res.x_fms_base.tabs = res.tabs;

		res.GetRootNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/addrSheet'; };

		res.Encode = function (m_depart)
		{
			var xml_string = '<?xml version="1.0" encoding="utf-8"?>' + this.FormatEOL();
			xml_string += '<addrSheet:addressSheetRoot' + this.FormatEOL();
			xml_string += ' xmlns="http://umms.fms.gov.ru/replication/core"' + this.FormatEOL();
			xml_string += ' xmlns:addrSheet="http://umms.fms.gov.ru/replication/addrSheet">' + this.FormatEOL();
			xml_string += this.FormatEOL();

			xml_string += this.OpenTagLine(1, 'addrSheet:departureAddressSheet');

			xml_string += this.OpenTagLine(2, 'addrSheet:personData');
			xml_string += this.StringifyField(3, "uid", m_depart.person_uid);
			xml_string += this.StringifyField(3, "personId", m_depart.person_personId);
			xml_string += this.StringifyField(3, "lastName", m_depart.LastName);
			xml_string += this.StringifyField(3, "firstName", m_depart.FirstName);
			xml_string += this.SafeStringifyField(3, "middleName", m_depart.MiddleName);
			xml_string += this.EncodeSex(3, m_depart.Sex);
			xml_string += this.SafeStringifyField(3, "birthDate", m_depart.Birthday);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_depart.Nationality, 'citizenship', 'citizenship');
			xml_string += this.EncodeBirthplace(3, m_depart.Birthplace);
			xml_string += this.CloseTagLine(2, 'addrSheet:personData');

			xml_string += this.OpenTagLine(2, 'addrSheet:document');
			xml_string += this.StringifyField(3, "uid", m_depart.Document_uid);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_depart.DocumentType, 'type', 'DocumentType');
			xml_string += this.SafeStringifyField(3, "series", m_depart.DocumentSeries);
			xml_string += this.StringifyField(3, "number", m_depart.DocumentNumber);
			if (null != m_depart.DocumentGivenBy && null != m_depart.DocumentGivenBy_code)
				xml_string += this.EncodeNamedDictionaryItem(3, { id: m_depart.DocumentGivenBy_code.id, text: m_depart.DocumentGivenBy.text }, 'authorityOrgan', 'OfficialOrgan');
			xml_string += this.SafeStringifyField(3, "issued", res.DateEncode(m_depart.DocumentGivenDate));
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_depart.Document_status, 'status', 'DocumentStatus');
			xml_string += this.CloseTagLine(2, 'addrSheet:document');

			if ((m_depart.NewLastName && m_depart.NewLastName != '') && (m_depart.NewFirstName && m_depart.NewFirstName != ''))
			{
				xml_string += this.OpenTagLine(2, 'addrSheet:changePersonData');
				xml_string += this.StringifyField(3, "uid", m_depart.person_uid);
				xml_string += this.StringifyField(3, "personId", m_depart.person_personId);
				xml_string += this.StringifyField(3, "lastName", m_depart.NewLastName);
				xml_string += this.StringifyField(3, "firstName", m_depart.NewFirstName);
				xml_string += this.SafeStringifyField(3, "middleName", m_depart.NewMiddleName);
				xml_string += this.EncodeSex(3, m_depart.NewSex);
				xml_string += this.SafeStringifyField(3, "birthDate", m_depart.NewBirthday);
				xml_string += this.CloseTagLine(2, 'addrSheet:changePersonData');
			}

			xml_string += this.SafeStringifyField(2, "addrSheet:otherRegistrationReason", m_depart.OtherReason);

			if (m_depart.dateComposing != null || m_depart.fioComposing != null)
			{
				xml_string += this.OpenTagLine(2, 'addrSheet:composingResponsibleData');
				xml_string += this.StringifyField(3, "addrSheet:date", res.DateEncode(m_depart.dateComposing));
				xml_string += this.StringifyField(3, "addrSheet:fio", m_depart.fioComposing);
				xml_string += this.CloseTagLine(2, 'addrSheet:composingResponsibleData');
			}

			xml_string += this.OpenTagLine(2, 'addrSheet:stayingRegistrationData');
			xml_string += this.StringifyField(3, "dateFrom", res.DateEncode(m_depart.DateFrom));
			xml_string += this.StringifyField(3, "dateTo", res.DateEncode(m_depart.DateTill));

			if (m_depart.RegAddress && null!=m_depart.RegAddress)
				xml_string += this.EncodeAddress(3, 'address', m_depart.RegAddress);

			xml_string += this.CloseTagLine(2, 'addrSheet:stayingRegistrationData');

			xml_string += this.SafeEncodeNamedDictionaryItem(2, m_depart.subdivision, 'addrSheet:territorialRegOrgan', 'officialOrgan.fms');

			if (!h_addr.IsEmptyAddress(m_depart.NewResidenceAddress))
			{
				if (m_depart.NewResidenceAddressSameCityAddress)
					xml_string += this.EncodeAddress(2, 'addrSheet:previousAddressSameCityAddress', m_depart.NewResidenceAddress);
				else
					xml_string += this.EncodeAddress(2, 'addrSheet:previousAddress', m_depart.NewResidenceAddress);
			}

			if (!h_addr.IsEmptyAddress(m_depart.NewAddress))
				xml_string += this.EncodeAddress(2, 'addrSheet:newAddress', m_depart.NewAddress);

			xml_string += this.CloseTagLine(1, 'addrSheet:departureAddressSheet');

			xml_string += this.FormatEOL();
			xml_string += '</addrSheet:addressSheetRoot>';
			return xml_string;
		};


		res.DecodeNewPersonData = function (node, m_arrival)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "lastName": m_arrival.NewLastName = child.text; break;
					case "firstName": m_arrival.NewFirstName = child.text; break;
					case "middleName": m_arrival.NewMiddleName = child.text; break;
					case "birthDate": m_arrival.NewBirthday = child.text; break;
					case "gender": m_arrival.NewSex = self.DecodeSex(child); break;
					case 'citizenship': m_arrival.NewNationality = self.DecodeDictionaryItem(child, 'citizenship'); break;
					case 'birthPlace': m_arrival.NewBirthplace = self.DecodeBirthPlace(child); break;
				}
			});
		}

		res.DecodeStayingRegistration = function (node, m_depart)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "dateFrom": m_depart.DateFrom = res.DateDecode(child.text); break;
					case "dateTo": m_depart.DateTill = res.DateDecode(child.text); break;
					case "address": m_depart.RegAddress= self.DecodeAddressAddress(child); break;
				}
			});
		}

		res.DecodeResponsibleData = function (node, m_depart)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "addrSheet:date": m_depart.dateComposing = res.DateDecode(child.text); break;
					case "addrSheet:fio": m_depart.fioComposing = child.text; break;
				}
			});
		}

		res.DecodeDepartureAddressSheet = function (node, m_depart)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "addrSheet:territorialRegOrgan": m_depart.subdivision = self.DecodeDictionaryItem(child, 'officialOrgan.fms'); break;
					case "addrSheet:personData": self.DecodePersonData(child, m_depart); break;
					case "addrSheet:document": self.DecodePersonDataDocument_Document(child, m_depart); break;
					case "addrSheet:changePersonData": self.DecodeNewPersonData(child, m_depart); break;
					case "addrSheet:otherRegistrationReason": m_depart.OtherReason = child.text; break;
					case "addrSheet:composingResponsibleData": self.DecodeResponsibleData(child, m_depart); break;
					case "addrSheet:stayingRegistrationData": self.DecodeStayingRegistration(child, m_depart); break;
					case "addrSheet:previousAddress": m_depart.NewResidenceAddress= self.DecodeAddressAddress(child); break;
					case "addrSheet:previousAddressSameCityAddress":
						m_depart.NewResidenceAddressSameCityAddress = true;
						m_depart.NewResidenceAddress= self.DecodeAddressAddress(child);
						break;
					case "addrSheet:newAddress": m_depart.NewAddress= self.DecodeAddressAddress(child); break;
				}
			});
		};

		res.DecodeXmlDocument = function (doc)
		{
			var m_depart = model_depart();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "addrSheet:departureAddressSheet": this.DecodeDepartureAddressSheet(child, m_depart); break;
				}
			}
			return m_depart;
		};

		res.GetScheme = function ()
		{
			var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");

			xs.add("http://www.w3.org/2000/09/xmldsig#", res.LoadXsd(xmldsig));
			xs.add("http://umms.fms.gov.ru/replication/core", res.LoadXsd(core));
			xs.add("http://umms.fms.gov.ru/replication/addrSheet", res.LoadXsd(addrSheet));

			return xs;
		};

		log.Debug('Create }');
		return res;
	}
});