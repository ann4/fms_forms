require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/text'
		}
	}
}),

require
(
	[
		  'forms/fms/profile/c_abonent'
		, 'forms/fms/profile/readable/c_read_abonent'
		, 'forms/fms/migrationn/f_migrationn'
		, 'forms/fms/migrationn/f_a_migrationn'
		, 'forms/fms/migrationn_z/f_mig_unreg'
		, 'forms/fms/migrationn_z/f_a_mig_unreg'
		, 'forms/fms/questionary/f_questionary'
		, 'forms/fms/questionary/f_a_questionary'
		, 'forms/fms/arrival/f_arrival'
		, 'forms/fms/arrival/f_a_arrival'
		, 'forms/fms/depart/f_depart'
		, 'forms/fms/depart/f_a_depart'
		, 'forms/fms/response/f_response'
		, 'forms/fms/migrationn_c/f_mig_rereg'
		, 'forms/fms/migrationn_c/f_a_mig_rereg'
		, 'forms/fms/protocol/f_protocol'
		, 'forms/fms/1ksr/f_1ksr'
		, 'forms/fms/register/f_register'
		, 'forms/fms/bygeography/f_bygeography'
		, 'forms/fms/form2g/f_form2g'
	],
	function (profile_Abonent_controller, profile_Abonent_readable)
	{
		var extension =
		{
			Title: 'ФМС РФ'
			, key: 'fms'
			, forms: {}
			, profile:
				{
					Abonent: profile_Abonent_controller,
					AbonentReadable: profile_Abonent_readable
				}
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 1; i < forms.length; i++)
		{
			var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);