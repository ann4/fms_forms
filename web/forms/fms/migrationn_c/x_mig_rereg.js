define([
	  'forms/fms/base/x_fms'
	, 'forms/fms/migrationn_c/m_mig_rereg'
	, 'forms/fms/base/town/m_town'
	, 'forms/base/h_times'
	, 'forms/base/log'
],
function (BaseCodec, create_m_mig_rereg, m_town, h_times, GetLogger)
{
	var log = GetLogger('x_mig_rereg');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.GetRootNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/migration/staying'; };
		res.GetEditNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/migration/staying/case-edit'; };

		res.tabs = ['', '    ', '        ', '            ', '                ', '                    ', '                        ', '                            ', '                                ', '                                    '];

		res.Encode = function (m_mig_rereg)
		{
			log.Debug('Encode() {');
			var xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + this.FormatEOL();
			xml_string += '<ns6:caseEdit ' + this.FormatEOL();
			xml_string += '  xmlns:ds="http://www.w3.org/2000/09/xmldsig#"' + this.FormatEOL();
			xml_string += '  xmlns="http://umms.fms.gov.ru/replication/core"' + this.FormatEOL();
			xml_string += '  xmlns:ns1="http://umms.fms.gov.ru/replication/core"' + this.FormatEOL();
			xml_string += '  xmlns:ns2="http://umms.fms.gov.ru/replication/core/correction"' + this.FormatEOL();
			xml_string += '  xmlns:ns3="http://umms.fms.gov.ru/replication/foreign-citizen-core"' + this.FormatEOL();
			xml_string += '  xmlns:ns4="http://umms.fms.gov.ru/replication/migration"' + this.FormatEOL();
			xml_string += '  xmlns:ns5="http://umms.fms.gov.ru/replication/hotel"' + this.FormatEOL();
			xml_string += '  xmlns:ns6="http://umms.fms.gov.ru/replication/migration/staying/case-edit"' + this.FormatEOL();
			xml_string += '  xmlns:ns7="http://umms.fms.gov.ru/replication/hotel/form5"' + this.FormatEOL();
			xml_string += '  xmlns:ns8="http://umms.fms.gov.ru/replication/migration/staying/unreg"' + this.FormatEOL();
			xml_string += '  xmlns:ns9="http://umms.fms.gov.ru/replication/migration/staying"' + this.FormatEOL();
			xml_string += '  xmlns:ns10="http://umms.fms.gov.ru/replication/invitation-application"' + this.FormatEOL();
			xml_string += '  xmlns:ns11="http://umms.fms.gov.ru/replication/payment"' + this.FormatEOL();
			xml_string += '  xmlns:ns12="http://umms.fms.gov.ru/hotel/hotel-response"' + this.FormatEOL();
			xml_string += '  schemaVersion="1.0">' + this.FormatEOL();

			xml_string = this.EncodeCoreCaseFields(1, m_mig_rereg, xml_string);

			xml_string += this.FormatTabs(1) + '<ns6:declarant schemaVersion="1.0">' + this.FormatEOL();
			xml_string += this.StringifyField(2, "uid", m_mig_rereg.declarant_uid);
			xml_string += this.StringifyField(2, "ns2:supplierInfo", m_mig_rereg.declarant_supplierInfo);
			xml_string += this.OpenTagLine(2, 'ns2:employee');
			xml_string += this.StringifyField(3, "ummsId", m_mig_rereg.declarant_employee.ummsId);
			xml_string += this.CloseTagLine(2, 'ns2:employee');
			xml_string += this.StringifyField(2, "ns2:date", m_mig_rereg.declarant_date);
			xml_string += this.StringifyField(2, "ns2:reason", m_mig_rereg.declarant_reason);

			xml_string += this.FormatTabs(2) + '<ns2:personDataDocument>' + this.FormatEOL();
			xml_string += this.FormatTabs(3) + '<person>' + this.FormatEOL();
			xml_string += this.StringifyField(4, "uid", m_mig_rereg.person_uid);
			xml_string += this.StringifyField(4, "personId", m_mig_rereg.person_personId);
			xml_string += this.StringifyField(4, "lastName", m_mig_rereg.LastName);
			xml_string += this.SafeStringifyField(4, "lastNameLat", m_mig_rereg.LastName_latin);
			xml_string += this.StringifyField(4, "firstName", m_mig_rereg.FirstName);
			xml_string += this.SafeStringifyField(4, "firstNameLat", m_mig_rereg.FirstName_latin);
			if (m_mig_rereg.MiddleName)
			{
				xml_string += this.StringifyField(4, "middleName", m_mig_rereg.MiddleName);
				xml_string += this.SafeStringifyField(4, "middleNameLat", m_mig_rereg.MiddleName_latin);
			}
			xml_string += this.EncodeSex(4, m_mig_rereg.Sex);
			xml_string += this.SafeStringifyField(4, "birthDate", m_mig_rereg.Birthday);
			xml_string += this.SafeEncodeNamedDictionaryItem(4, m_mig_rereg.Nationality, 'citizenship', 'Citizenship');
			xml_string += this.EncodeBirthplace(4, m_mig_rereg.Birthplace);
			xml_string += this.FormatTabs(3) + '</person>' + this.FormatEOL();

			xml_string += this.FormatTabs(3) + '<document>' + this.FormatEOL();
			xml_string += this.StringifyField(4, "uid", m_mig_rereg.Document_uid);
			xml_string += this.SafeEncodeNamedDictionaryItem(4, m_mig_rereg.DocumentType, 'type', 'DocumentType');
			xml_string += this.StringifyField(4, "series", m_mig_rereg.DocumentSeries);
			xml_string += this.StringifyField(4, "number", m_mig_rereg.DocumentNumber);
			xml_string += this.StringifyField(4, "authority", m_mig_rereg.authority);
			xml_string += this.SafeStringifyField(4, "issued", res.DateEncode(m_mig_rereg.DocumentGivenDate));
			xml_string += this.SafeStringifyField(4, "validFrom", res.DateEncode(m_mig_rereg.DocumentValidFrom));
			if (m_mig_rereg.DocumentFinishDate)
				xml_string += this.SafeStringifyField(4, "validTo", res.DateEncode(m_mig_rereg.DocumentFinishDate));
			xml_string += this.SafeEncodeNamedDictionaryItem(4, m_mig_rereg.Document_DocumentStatus, 'status', 'DocumentStatus');
			xml_string += this.FormatTabs(3) + '</document>' + this.FormatEOL();

			xml_string += this.StringifyField(3, "entered", '1.102.2' == this.SchemaVersion ? false : m_mig_rereg.Document_entered, '����, ����������� �� ��, ������� �� ������ ���� � ��������');
			xml_string += this.FormatTabs(2) + '</ns2:personDataDocument>' + this.FormatEOL();

			xml_string += this.FormatTabs(1) + '</ns6:declarant>' + this.FormatEOL();

			xml_string += this.SafeDocResidenceEncode(1, m_mig_rereg, 'ns6');

			xml_string += this.SafeMigrationCardEncode(1, m_mig_rereg, 'ns6');

			if (m_mig_rereg.Profession && m_mig_rereg.Profession.id && !m_mig_rereg.Profession.text)
			{
				xml_string += this.SafeEncodeNamedDictionaryItem(1, m_mig_rereg.Profession, 'ns6:profession', 'Profession');
			}
			else if (m_mig_rereg.Profession && m_mig_rereg.Profession.text)
			{
				xml_string += this.SafeStringifyField(1, 'ns6:profession', m_mig_rereg.Profession.text);
			}

			xml_string += this.SafeEncodeNamedDictionaryItem(1, m_mig_rereg.entrancePurpose, 'ns6:entrancePurpose', 'VisitPurpose', '���� ������');

			xml_string += this.EncodeStayPeriod(1, m_mig_rereg, 'ns6');

			xml_string += this.StringifyField(1, "ns6:notificationReceived", res.DateEncode(m_mig_rereg.notificationReceived));

			if (m_mig_rereg.PreAddress && null != m_mig_rereg.PreAddress)
				xml_string += this.EncodeRussianAddress(1, m_mig_rereg.PreAddress.russianAddress, 'ns6:arrivalFromPlaceAddress');

			xml_string += '</ns6:caseEdit>' + this.FormatEOL();
			log.Debug('Encode() }');
			return xml_string;
		};

		res.DecodeDeclarant = function (m_mig_rereg, node)
		{
			log.Debug('DecodeDeclarant {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (self.GetTagNameWithoutNamespace(child))
				{
					case 'uid': m_mig_rereg.declarant_uid = child.text; break;
					case 'supplierInfo': m_mig_rereg.declarant_supplierInfo = child.text; break;
					case 'employee': m_mig_rereg.declarant_employee = self.DecodeEmployee(child); break;
					case 'date': m_mig_rereg.declarant_date = child.text; break;
					case 'reason': m_mig_rereg.declarant_reason = child.text; break;
					case 'personDataDocument': self.DecodePersonDataDocument(child, m_mig_rereg); break;
				}
			});
			log.Debug('DecodeDeclarant }');
		}

		res.DecodeXmlDocument = function (doc)
		{
			log.Debug('DecodeXmlDocument {');
			var m_mig_rereg = create_m_mig_rereg();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (this.GetTagNameWithoutNamespace(child))
				{
					case "uid": m_mig_rereg.uid = child.text; break;
					case "requestId": m_mig_rereg.requestId = child.text; break;
					case "supplierInfo": m_mig_rereg.supplierInfo = child.text; break;
					case "subdivision": m_mig_rereg.subdivision = this.DecodeDictionaryItem(child, 'officialOrgan'); break;
					case "employee": m_mig_rereg.employee = this.DecodeEmployee(child); break;
					case "date": m_mig_rereg.date = child.text; break;
					case "comments": m_mig_rereg.comment = child.text; break;
					case "number": m_mig_rereg.number = child.text; break;

					case "declarant": this.DecodeDeclarant(m_mig_rereg, child); break;

					case "entrancePurpose": m_mig_rereg.entrancePurpose = this.DecodeDictionaryItem(child, 'VisitPurpose'); break;
					case "stayPeriod": this.DecodeStayPeriod(child, m_mig_rereg); break;
					case "notificationReceived": m_mig_rereg.notificationReceived = res.DateDecode(child.text); break;

					case "docResidence": this.DocResidenceDecode(child, m_mig_rereg); break;
					case "migrationCard": this.DecodeMigrationCardData(child, m_mig_rereg); break;
					case "profession": m_mig_rereg.Profession = this.DecodeProfession(child); break;

					case "arrivalFromPlaceAddress":
						m_mig_rereg.PreAddress =
						{
							Country: { id: 'RUS', text: '������' }
							, russianAddress: this.DecodeAddressAddressRussianAddress(child)
						};
						log.Debug(JSON.stringify(m_mig_rereg.PreAddress));
						break;
				}
			}
			log.Debug('DecodeXmlDocument }');
			return m_mig_rereg;
		};

		log.Debug('Create }');
		return res;
	}
});
