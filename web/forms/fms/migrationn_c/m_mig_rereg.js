define([
	  'forms/fms/base/town/m_town'
]
, function (TownAddress)
{
	return function ()
	{
		var res =
		{
			  uid: null
			, requestId: ''
			, supplierInfo: 'Не указано в профиле!'
			, subdivision: { id: -1, text: 'Не указано в профиле!' } // {id:'', text:''}
			, employee: { ummsId: null }
			, declarant_employee: { ummsId: null }
			, date: null // 'ДД.ММ.ГГГГ'

			, Sex: { id: '', text: '' } // М|Ж

			, Birthplace: TownAddress()

			, Visa_DocumentStatus: { id: 102877, text: 'Действительный'} // {id:'',text:''}
			, Visa_identifier: null

			, MigrationCard_uid: null
			, MigrationCard_series: ''
			, MigrationCard_number: ''
			, MigrationCard_dateFrom: null  // 'ДД.ММ.ГГГГ'
			, MigrationCard_dateTo: null  // 'ДД.ММ.ГГГГ'
			, MigrationCard_entranceDate: null  // 'ДД.ММ.ГГГГ'
			, MigrationCard_EntranceCheckpoint: null  // {id:'',text:''}
			, MigrationCard_visitPurpose: null  // {id:'',text:''}
			, MigrationCard_kpp: null  // {id:'',text:''}
		};
		return res;
	};
});