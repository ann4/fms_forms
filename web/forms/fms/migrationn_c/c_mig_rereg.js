define([
	  'forms/fms/base/c_fms'
	, 'forms/fms/migrationn_c/x_mig_rereg'
	, 'forms/fms/migrationn_c/m_mig_rereg'
	, 'forms/base/log'
	, 'tpl!forms/fms/migrationn_c/v_mig_rereg.xaml'
	, 'tpl!forms/fms/migrationn_c/e_mig_rereg.html'
	, 'forms/fms/profile/h_abonent'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/town/c_town'
	, 'forms/fms/base/h_visaType'
	, 'forms/fms/base/h_entryGoal'
	, 'forms/fms/base/h_arrivals'
	, 'forms/base/h_times'
	, 'forms/fms/base/h_visaCategory'
	, 'forms/fms/base/h_visaMultiplicity'
	, 'forms/fms/base/h_MigKpp'
	, 'forms/base/h_names'
	, 'forms/fms/base/h_profession'
	, 'forms/fms/base/h_validate_n'
	, 'tpl!forms/fms/migrationn_c/p_mig_rereg.html'
	, 'forms/fms/base/addr/c_addr_modal'
	, 'forms/fms/base/addr/h_addr'
],
function 
(
	  BaseFormController
	, codec
	, create_m_mig_rereg
	, GetLogger
	, print_tpl
	, layout_tpl
	, h_abonent
	, helper_DocumentType
	, helper_Select2
	, h_citizenship
	, controller_Town
	, helper_VisaType
	, helper_entryGoal
	, helper_Arrivals
	, helper_Times
	, helper_visaCategory
	, helper_visaMultiplicity
	, helper_MigKpp
	, helper_Names
	, helper_Profession
	, helper_validate
	, attrshtml_tpl
	, c_addr_modal
	, h_addr
	)
{
	return function ()
	{
		var log = GetLogger('c_mig_rereg');
		log.Debug('Create {');

		var Select2Options = function (h_values)
		{
			return function (m, field_name)
			{
				if (m[field_name] && !m[field_name].text && m[field_name].id)
				{
					var item = helper_Select2.FindById(h_values.GetValues(), m[field_name].id)
					if (item)
						m[field_name].text = item.text;
				}
				return {
					placeholder: ''
					, allowClear: true
					, query: function (q)
					{
						q.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), q.term) });
					}
				};
			}
		}

		var Select2OptionsWithNewValues = function (h_values)
		{
			return function (m, field_name)
			{
				if (m[field_name] && !m[field_name].text && m[field_name].id)
				{
					var item = helper_Select2.FindById(h_values.GetValues(), m[field_name].id)
					if (item)
						m[field_name].text = item.text;
				}
				return {
					placeholder: ''
					, allowClear: true
					, query: function (q)
					{
						q.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), q.term) });
					}
					, createSearchChoice: function (term, data)
					{
						if ($(data).filter(function () { return this.text.localeCompare(term) === 0 }).length === 0)
							return { id: term, text: term };
					}
				};
			}
		}

		var AddressTextFunc = function (item)
		{
			return !(!item || null == item || !item.text || null == item.text || '' == item.text)
					? item.text : 'Кликните, чтобы указать..';
		}

		var AddressTextFunc2 = function (item)
		{
			return h_addr.SafePrepareReadableText(item, 'Кликните, чтобы указать..');
		}

		var controller = BaseFormController(layout_tpl, {
			field_spec:
			{
				DocumentType: Select2Options(helper_DocumentType)
				, Nationality: Select2Options(h_citizenship)
				, Visa_type: Select2Options(helper_VisaType)
				, entrancePurpose: Select2Options(helper_Arrivals)
				, Visa_category: Select2Options(helper_visaCategory)
				, Visa_multiplicity: Select2Options(helper_visaMultiplicity)
				, Visa_visitPurpose: Select2Options(helper_entryGoal)
				, MigrationCard_kpp: Select2Options(helper_MigKpp)
				, MigrationCard_visitPurpose: Select2Options(helper_Arrivals)
				, Profession: Select2OptionsWithNewValues(helper_Profession)
				, Birthplace:
					{
						text: AddressTextFunc
						, controller: function (item, onAfterSave) { return controller_Town(item, onAfterSave); }
					}
				, PreAddress: function (model)
				{
					var res = { text: AddressTextFunc2, controller: c_addr_modal(model, 'PreAddress', 'Прежний адрес в РФ') };
					return res;
				}
			}
		});

		controller.SafeCreateNewContent = function ()
		{
			if (!this.model)
				this.model = create_m_mig_rereg();
		}

		controller.UseProfile = function (profile)
		{
			h_abonent.CheckResponsibleOfficiers(profile);
			this.SafeCreateNewContent();
			if (profile && profile.Abonent)
			{
				if (!this.model.Hotel_organization_name || '' == this.model.Hotel_organization_name)
				{
					if (profile.Abonent.ShortName)
					{
						this.model.Hotel_organization_name = profile.Abonent.ShortName;
					}
					else if (profile.Abonent.Name)
					{
						this.model.Hotel_organization_name = profile.Abonent.Name;
					}
				}
				if (!this.model.Hotel_organization_inn || '' == this.model.Hotel_organization_inn)
				{
					if (profile.Abonent.Inn)
						this.model.Hotel_organization_inn = profile.Abonent.Inn;
				}
				if (profile.Abonent.phone)
				{
					this.model.Hotel_organization_phone = profile.Abonent.phone;
					this.model.Phone = profile.Abonent.phone;
				}
			}
			if (profile && profile.Extensions && profile.Extensions.fms)
			{
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent)
				{
					var pAbonent = pfms.Abonent;
					pAbonent = h_abonent.SafeUpgradeAbonent(pAbonent);

					this.model.subdivision = h_abonent.SafeGet_dict_field(pAbonent, 'subdivision', this.model.subdivision);
					this.model.sono = h_abonent.SafeGet_dict_field(pAbonent, 'sono', this.model.sono);

					if (pAbonent.supplierInfo)
						this.model.supplierInfo = pAbonent.supplierInfo;

					if (pAbonent.AddressesOfPlacement && pAbonent.AddressesOfPlacement.length > 0)
					{
						this.model.Hotel_address_variants = pAbonent.AddressesOfPlacement;
						var defAddressesOfPlacement = h_abonent.SafeGetDefaultAddressesOfPlacement(pAbonent);
						if (defAddressesOfPlacement && null != defAddressesOfPlacement)
							this.model.Address = defAddressesOfPlacement;
					}
					if (pAbonent.AddressRealAbonent)
						this.model.Address_Company = pAbonent.AddressRealAbonent;

					if (pAbonent.employee_ummsId)
						this.model.employee.ummsId = pAbonent.employee_ummsId;
					if (pAbonent.employee_ummsId)
						this.model.declarant_employee.ummsId = pAbonent.employee_ummsId;

					var col = h_abonent.SafeGetDefaultColaborator(pAbonent);
					if (null != col)
					{
						if (col.user_document_serie)
							this.model.Organization_Document_series = col.user_document_serie;
						if (col.user_document_number)
							this.model.Organization_Document_number = col.user_document_number;
						if (col.user_document_dateFrom)
							this.model.Organization_Document_issued = col.user_document_dateFrom;
						if (col.Birthday)
							this.model.Organization_Person_birthDate = col.Birthday;
						if (col.FirstName)
							this.model.Organization_Person_firstName = col.FirstName;
						if (col.LastName)
							this.model.Organization_Person_lastName = col.LastName;
						if (col.MiddleName)
							this.model.Organization_Person_middleName = col.MiddleName;

						if ('M' == col.Sex)
							this.model.Organization_Person_gender = { id: 'M', text: 'Мужской' };
						else if ('F' == col.Sex)
							this.model.Organization_Person_gender = { id: 'F', text: 'Женский' };

						if (col.AddressLegal && null != col.AddressLegal)
							this.model.Address_CompanyN = col.AddressLegal;

						if (col.Phone)
							this.model.Phone = col.Phone;
					}

					if (pAbonent.OrganizationPhone)
						this.model.Hotel_organization_phone = pAbonent.OrganizationPhone;
				}
				if (pfms.Forma)
				{
					this.ShowAttachmentsPanel = pfms.Forma.ShowAttachmentsPanel;
				}
			}
		}

		controller.BuildXamlView = function ()
		{
			log.Debug("BuildXamlView {");
			var model = print_tpl({
				form: this.model
			});
			log.Debug("BuildXamlView }");
			return model;
		};

		controller.CreateNew = function (id_form_div)
		{
			this.SafeCreateNewContent();
			this.Render(id_form_div);

			$('#cpw_form_DocumentType').select2('data', { id: 103012, text: "Иностранный паспорт" });
		};

		var DataLoad = function (content)
		{
			controller.DataLoadSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content.Sex);
		}

		var DataSave = function (content)
		{
			content.Sex = controller.DataSaveSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content);

			if (!content.date)
				content.date = helper_Times.nowLocalISOStringDateTime();

			if (!content.declarant_date)
				content.declarant_date = content.date;

			if (!content.Document_DocumentStatus)
				content.Document_DocumentStatus = { id: 102877, text: 'Действительный' };

			if (!content.Document_entered)
				content.Document_entered = true;

			if (!content.Visa_validFrom && content.Visa_issued)
				content.Visa_validFrom = content.Visa_issued;

			if (!content.Visa_entered && content.Document_entered)
				content.Visa_entered = content.Document_entered;

			var txt_creation = helper_Times.unixDateTimeStamp();

			controller.SafeFixUids(content, txt_creation, ['number', 'uid', 'requestId',
				'Hotel_organization_uid', 'person_uid', 'person_personId', 'Document_uid']);
		}

		var base_Render = controller.Render;
		controller.Render = function (id_form_div)
		{
			base_Render.call(this, id_form_div);

			$('#cpw_form_LastName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_FirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_MiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$('.input-datepicker').datepicker(helper_Times.DatePickerOptions);
			$('.input-datepicker-birthday').datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'dd.mm.yy'
			});
			$('.input-datepicker').focusout(helper_validate.OnConvertToDateFormat);
			$('.input-datepicker-birthday').focusout(helper_validate.OnConvertToDateFormat);

			$('.encoding_date').change(helper_Names.OnNameChange_NormalizeEncoding);

			DataLoad(this.model);
		}

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return attrshtml_tpl(this.model);
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			if (this.SignedContent)
			{
				return this.SignedContent;
			}
			else
			{
				this.model = base_GetFormContent.call(this);
				log.Debug('GetFormContent() {');
				DataSave(this.model);
				log.Debug('GetFormContent() }');
				return this.model;
			}
		};

		controller.Sign = function ()
		{
			var content = this.GetFormContent();
			if (null != content)
			{
				var wax = CPW_Singleton();
				if (wax)
				{
					var pos = this.model.PositionForSignature;
					if (null == pos)
					{
						this.SignedContent = content;
					}
					else
					{
						var signature = wax.SignXml(content);
						this.SignedContent = content.substring(0, pos)
							+ signature
							+ content.substring(pos, content.length);
					}
				}
			}
		};

		controller.UseCodec(codec());

		log.Debug('Create }');
		return controller;
	}
});
