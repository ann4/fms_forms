﻿define([
	  'forms/fms/attachments/c_attachments'
	, 'forms/fms/migrationn_c/c_mig_rereg'
	, 'forms/fms/migrationn_c/ff_mig_rereg'
	, 'forms/base/log'
],
function (BaseController, BaseFormController, formSpec, GetLogger)
{
	return function()
	{
		var log = GetLogger('c_migrationn');

		var controller = BaseController(BaseFormController, formSpec);

		controller.SetStayPeriod = function (period) { }
		controller.GetStayPeriod = function () { return null; }

		controller.GetBody = function () {
			var body = 'Направляем документ "Корректировка о прибытии ИГ или ЛБГ".';
			if ($('#cpw_form_LastName').val() != '') {
				body += '\r\nПрибывший  гражданин: ' + $('#cpw_form_LastName').val();
				if ($('#cpw_form_FirstName').val() != '')
					body += ' ' + $('#cpw_form_FirstName').val().substr(0, 1) + '.';
				if ($('#cpw_form_MiddleName').val() != '')
					body += '' + $('#cpw_form_MiddleName').val().substr(0, 1) + '.';
				var birthDate = $('#cpw_form_Birthday').val();
				if (birthDate) {
					var birthDateParts = birthDate.split('.');
					if (birthDateParts && birthDateParts.length == 3)
						body += ', ' + birthDateParts[2] + ' г.р.';
				}
			}
			return body;
		}

		return controller;
	}
});