define([
	  'forms/fms/migrationn_c/c_a_mig_rereg'
	, 'forms/fms/migrationn_c/ff_mig_rereg'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_migrationn_c'
		, Title: 'Корректировка уведомления о прибытии ИГ'
		, FileFormat: FileFormat
	};
	return form_spec;
});
