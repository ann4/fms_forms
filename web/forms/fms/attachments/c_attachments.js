﻿define([
	  'forms/fms/attachments/x_attachments'
	, 'tpl!forms/fms/attachments/attachments.html'
	, 'tpl!forms/fms/attachments/attachment.html'
	, 'tpl!forms/fms/attachments/note.html'
	, 'forms/base/log'
	, 'forms/base/h_file'
	, 'forms/base/h_times'
	, 'forms/fms/base//h_Select2'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_dictionary'
	, 'forms/fms/base/upload/h_upload'
	, 'forms/fms/guides/dict_officialorgan_fms.profile.csv'
],
function (codec
	, attachments_tpl
	, attachment_tpl
	, note_tpl
	, GetLogger
	, helper_File
	, helper_times
	, helper_Select2
	, helper_DocumentType
	, helper_msgbox
	, helper_dictionary
	, helper_upload
	, dict_officialorgan_fms_profile)
{
	return function (BaseFormController, formSpec)
	{
		var log = GetLogger('c_attachments');
		var controller = BaseFormController();

		var form = formSpec;

		controller.ShowAttachmentsPanel = false;

		controller.result_Data =
		{
			subject: '',
			body: '',
			attachments: [],
			note: '',
			stay_period: null
		}


		var baseform_BuildHtmlViewForProfiledData = controller.BuildHtmlViewForProfiledData;
		controller.BuildHtmlViewForProfiledData = function ()
		{
			this.RenderFormAttachments();
			return baseform_BuildHtmlViewForProfiledData.call(controller);
		};

		var baseform_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var attachments = [];
			var content = baseform_GetFormContent();
			var name = helper_File.FMSPrepareFileName(form.FilePrefix, helper_times.safeDateTime());
			attachments.push
			({
				Name: name,
				FileName: name + '.' + form.FileExtension,
				FilePath: name + '.' + form.FileExtension,
				FileSize: helper_File.GetSizeOfUtf8(content),
				Description: '',
				Format: '',
				Content: content,
				Note: form.Description,
				AttachmentType: 'f'
			});
			var scans = this.CollectAttachments();
			this.result_Data.attachments = attachments.concat(scans);
			this.result_Data.body = this.GetBody();
			this.result_Data.stay_period = this.GetStayPeriod();
			if (scans.length > 0)
				this.result_Data.body += '\r\nК документу приложены скан-копии документов в количестве ' + scans.length + ' шт.';
			if ($('.scan-note').val() && $('.scan-note').val() != '')
			{
				this.result_Data.body += '\r\n' + $('.scan-note').val();
				this.result_Data.note += '\r\n' + $('.scan-note').val();
			}
			return this.result_Data;
		};

		var baseform_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (form_content)
		{
			var content = 'object' == (typeof form_content) ? form_content : JSON.parse(form_content);
			var _content = '';
			var indexStartAttach = 0;
			var hasAttachTypeF = false;
			if (content.attachments && content.attachments.length >= 1) {
				for (var i = 0; i < content.attachments.length; i++) {
					if ('f' == content.attachments[i].AttachmentType) {
						_content = content.attachments[i].Content;
						indexStartAttach++;
						hasAttachTypeF = true;
					}
					else if ('p' == content.attachments[i].AttachmentType) {
						indexStartAttach++;
					}
				}
			}
			var res = null;
			if (hasAttachTypeF)
			    res = baseform_SetFormContent.call(controller, _content);
			if (content.stay_period && null != content.stay_period)
				this.SetStayPeriod(content.stay_period);
			this.SetAttachments(content.attachments.slice(indexStartAttach), content.note);
			return res;
		};

		var baseform_CopyFormContent = controller.CopyFormContent;
		controller.CopyFormContent = function (form_content)
		{
			var content = 'object' == (typeof form_content) ? form_content : JSON.parse(form_content);
			baseform_CopyFormContent.call(controller, !content.attachments || content.attachments.length < 1 ? '' : content.attachments[0].Content);
			this.SetAttachments(content.attachments.slice(1), '');
		}

		var baseform_CreateNew = controller.CreateNew;
		controller.CreateNew = function (id_form_div)
		{
			baseform_CreateNew.call(controller, id_form_div);
			this.RenderFormAttachments();
		};

		var base_Edit = controller.Edit;
		controller.Edit = function (form_div_selector)
		{
			base_Edit.call(this, form_div_selector);
			this.RenderFormAttachments();
		};

		var base_Sign = controller.Sign;
		controller.Sign = function ()
		{
			var content = this.GetFormContent();
			if (null != content && null != content.attachments.length > 0)
			{
				var wax = CPW_Singleton();
				if (wax)
				{
					var pos = !this.model && this.content ? this.content.PositionForSignature : this.model.PositionForSignature;
					if (null != pos)
					{
						var form_content = content.attachments[0].Content;
						var signature = wax.SignXml(form_content);
						var signedContent = form_content.substring(0, pos)
							+ signature
							+ form_content.substring(pos, form_content.length);
						content.attachments[0].Content = signedContent;
					}
					this.SignedContent = content;
				}
			}
		};

		var RenderSelect2 = function (select2_element, h_values)
		{
			select2_element.select2({
				placeholder: '',
				allowClear: true,
				query: function (query) {
					query.callback({ results: helper_Select2.FindForSelect(!controller.GetDocumentTypeValues ? h_values.GetValues() : controller.GetDocumentTypeValues(false), query.term) });
				}
			});
		}

		controller.RenderFormAttachments = function ()
		{
			if (this.ShowAttachmentsPanel && (!$('#scan-list') || $('#scan-list').length == 0))
			{
				var countChild = $('.cpw-forms-attachments').children().length;
				if ($('.cpw-forms-attachments').children()[countChild - 1].localName != "hr")
					$('.cpw-forms-attachments').append('<hr/>');
				$('.cpw-forms-attachments').append(attachments_tpl());
				$('#add-scans').click(function (e) { OnAddScans(e); });
				LoadAttachments(this.attachments, this.note);
			}
		}

		var RenderSelectDocumentType = function(scanType, current_block)
		{
			var types = current_block && null != current_block ? current_block.find('.scan_DocumentType') : $('.scan_DocumentType');
			if (types.length > 0)
			{
				RenderSelect2(types.last(), helper_DocumentType);
				types.last().select2('data', scanType);
			}
		}

		var LoadAttachments = function (attachments, note)
		{
			if (attachments)
			{
				var availableTypes = controller.GetDocumentTypeValues(true);
				for (var i = 0; i < attachments.length; i++)
				{
					var attachment = attachments[i];
					LoadAttachment(attachment, availableTypes);
				}
				ShowAddDocumentTypeButton();
				$('#scan-list .remove-doc').on('click', function (e) { OnDeleteAttachmentClick(e); });
				$('#scan-list .scan-doc').on('click', function (e) { OnShowScreenshotClick(e); });
				$('#scan-list .scan_DocumentType').on('change', function (e) { ValidateAttachments(null); });
				$('#scan-list #add-type').click(function (e) { OnAddTypeDocument(e); });
				if (attachments.length > 0)
					AddNoteFormScans(note);
			}
		}

		var LoadAttachment = function (attachment, availableTypes)
		{
			var descriptions = [];
			if (null != availableTypes && 0 < availableTypes.length)
				GetDescriptions(attachment.Description, availableTypes, descriptions);
			AppendAttachmentToHtml(attachment, descriptions);
		}

		var GetDescriptions = function (attachDescriptions, availableTypes, descriptions)
		{
			if ('[object Array]' == Object.prototype.toString.call(attachDescriptions))
			{
				for (var i = 0; i < attachDescriptions.length; i++)
				{
					GetDescriptions(attachDescriptions[i], availableTypes, descriptions);
				}
			}
			else if ('object' == (typeof attachDescriptions))
			{
				descriptions.push(attachDescriptions);
			}
			else
			{
				var descriptionIndex = FindAttachmentType(availableTypes, attachDescriptions);
				if (-1 != descriptionIndex)
				{
					descriptions.push(availableTypes[descriptionIndex]);
					availableTypes.splice(descriptionIndex, 1);
				}
				else
				{
					var subDescriptions = SafeGetDecriptions(attachDescriptions, availableTypes);
					GetDescriptions(subDescriptions, availableTypes, descriptions);
				}
			}
		}

		var SafeGetDecriptions = function (sdescription, availableTypes)
		{
			var descriptions = sdescription.split(',');
			if (!descriptions || 1 >= descriptions.length)
			{
				return null;
			}
			else
			{
				var result = [];
				for (var i = 0; i < descriptions.length; i++)
				{
					var description = $.trim(descriptions[i]);
					if ("" == description)
						continue;
					if (description[0].toUpperCase() == description[0] || 0 == result.length)
						result.push(description);
					else
						result[result.length - 1] = result[result.length - 1] + ',' + descriptions[i];
				}
				return result;
			}
		}

		var AppendAttachmentToHtml = function (attachment, descriptions)
		{
			var htmlScans = PrepAttachmentHtml(attachment.Name, attachment.FileName, attachment.FileSize, attachment.Content);
			$('#scans').append(htmlScans);
			RenderSelectDocumentType(0 == descriptions.length ? null : descriptions[0]);
			var docs = $('#scans').find('.doc-wrap');
			var last_comment = docs.find('.body_comment').last()
			for (var i = 1; i < descriptions.length; i++)
			{
				if (docs && null != docs)
				{
					AddTypeDocument(last_comment);
				}
				RenderSelectDocumentType(descriptions[i]);
			}
		}

		var FindAttachmentType = function (availableTypes, description)
		{
			for (var j = 0; j < availableTypes.length; j++)
			{
				if (description == availableTypes[j].text)
					return j;
			}
			return -1;
		}

		var OnAddScans = function (e)
		{
			e.preventDefault();
			var wax = CPW_Singleton();
			if (wax)
				var scans = wax.LoadScans('Файлы изображений|*.jpg;*.jpeg;*.png;*.bmp;*.tif;*.tiff', '{Formats: "jpeg", MaxSizeBytes:500000, ResolutionDPI:{min:200,max:300}, colors: "Gray"}');
			if (scans)
			{
				for (var i = 0; i < scans.length; i++)
				{
					var scan = scans[i];
					var shortName = scan.path.split('\\');
					shortName = shortName[shortName.length - 1];
					var htmlScans = PrepAttachmentHtml(shortName, scan.path, scan.size, scan.content);
					$('#scans').append(htmlScans);
					RenderSelectDocumentType(controller.GetDefaultDocumentType ? controller.GetDefaultDocumentType() : { id: 103008, text: "Паспорт гражданина Российской Федерации" });
				}
				ShowAddDocumentTypeButton();
				$('#scan-list .remove-doc').on('click', function (e) { OnDeleteAttachmentClick(e); });
				$('#scan-list .scan-doc').on('click', function (e) { OnShowScreenshotClick(e); });
				$('#scan-list .scan_DocumentType').on('change', function (e) { ValidateAttachments(null); });
				$('#scan-list #add-type').click(function (e) { OnAddTypeDocument(e); });
				if (scans.length > 0)
					AddNoteFormScans('');
			}
			controller.DoValidateAttachments(null);
		}

		var AddNoteFormScans = function(note)
		{
			note = !note ? '' : note;
			if ($('#scans-notes').html() == "")
			{
				$('#scans-notes').html(note_tpl({ note: note }));
			}
		}

		var PrepAttachmentHtml = function (name, filepath, size, content)
		{
			return attachment_tpl({ name: name, filepath: filepath, size: size, sizetext: helper_File.GetNumberingBytes(size), content: content });
		}

		var OnDeleteAttachmentClick = function (e)
		{
			e.preventDefault();
			$(e.target).closest('.doc-wrap').remove();
			if ($('#scans-notes').html() != "" && $('#scans .doc-wrap').length == 0)
			{
				$('#scans-notes').html('');
			}
			controller.DoValidateAttachments(null);
		}

		var ShowAddDocumentTypeButton = function ()
		{
			var types = !controller.GetDocumentTypeValues ? helper_DocumentType.GetValues() : controller.GetDocumentTypeValues(false);
			if (types && 1 < types.length)
				$('.column_add_comment').removeClass('hidden');
			else if (!$('.column_add_comment').hasClass('hidden'))
				$('.column_add_comment').addClass('hidden');
		}

		var OnAddTypeDocument = function (e)
		{
			e.preventDefault();
			AddTypeDocument($(e.target).closest('.doc-wrap .body_comment'));
		}

		var AddTypeDocument = function (docs)
		{
			if (docs.length && 0 < docs.length)
			{
				var coments = docs.find('.coment');
				if (coments && 0 < coments.length)
				{
					var currentHtml = '<div class="inline_data_block">';
					currentHtml += '	<div class="column_comment scan_DocumentType" id="cpw_form_scan_DocumentType"></div>';
					currentHtml += '	<a href="#" id="delete-type" class="column_delete_comment">Удалить</a>';
					currentHtml += '</div>';
					coments.append(currentHtml);
					var docType = controller.GetDefaultDocumentType ? controller.GetDefaultDocumentType() : { id: 103008, text: "Паспорт гражданина Российской Федерации" };
					RenderSelectDocumentType(docType, docs);
					$('.column_delete_comment').on('click', function (e) { OnDeleteTypeDocument(e); });
				}
			}
		}

		var OnDeleteTypeDocument = function (e)
		{
			e.preventDefault();
			$(e.target).closest('.inline_data_block').remove();
		}

		var OnShowScreenshotClick = function (e)
		{
			e.preventDefault();
			var docWrap = $(e.target).closest('.doc-wrap');
			if (docWrap)
			{
				var content = docWrap.find('.content').html();
				$('#modal-dialog-scan').dialog({
					width: 600,
					height: 600,
					resizable: true,
					modal: true,
					title: docWrap.find('.file-name').html(),
					buttons: {
						'Закрыть': function () {
							$(this).dialog('close');
						}
					}
				});
				$('#modal-dialog-scan-content').html('<div class="attach-block"><img style="max-width:500px;" src="data:image/png;base64,' + content + '"/></div>');
			}
		}

		controller.CollectAttachments = function()
		{
			var attachments= [];
			$('.doc-wrap').each(function (counter)
			{
				if ($(this).data('virtual') !== 1)
				{
					var size= $(this).find('.size').html().toString();
					var filename = $(this).find('.file').html();
					var name= $(this).find('.file-name').html();
					var content= $(this).find('.content').html();
					var description = controller.CollectDescription($(this));
					attachments.push
					({
						Name: name,
						FileName: name,
						FilePath: name,
						FileSize: size,
						Description: description,
						Format: 'text/binary',
						Content: content,
						Note: '',
						AttachmentType: 's'
					});
				}
			});
			return attachments;
		}

		controller.CollectDescription = function (self)
		{
			var description = [];
			self.find('.inline_data_block').each(function (counter) {
				var descript = $(this).find('#s2id_cpw_form_scan_DocumentType').select2('data');
				if (descript && null != descript && descript.id)
				{
					descript.find = false;
					description.push(descript);
				}
			});
			return description;
		}

		controller.SetAttachments = function (attachments, note)
		{
			this.attachments = attachments;
			this.note = note;
			return null;
		}

		var base_Validate = controller.Validate;
		controller.Validate = function ()
		{
			var problem_constraints = base_Validate.call(this);
			return controller.DoValidateAttachments(problem_constraints);
		}

		controller.DoValidateAttachments = function (problem_constraints)
		{
			var model_subdivision = controller.content ? controller.content.subdivision : controller.model.subdivision;
			if (controller.ShowAttachmentsPanel && model_subdivision)
			{
				var itemSubdivision = helper_dictionary.FindFirstByField(dict_officialorgan_fms_profile, 'ID', model_subdivision.id)
				var did_after = false;
				if (itemSubdivision)
				{
					var p= itemSubdivision.row.CODE.substring(0, 2);
					if ("18" == p || "31"==p)
					{
						did_after = true;
						problem_constraints = ValidateAttachments(problem_constraints);
					}
				}
				problem_constraints = ValidateAttachmentDescription(problem_constraints);
				if (!did_after)
					AfterValidation(true);
			}
			return problem_constraints;
		}

		var ValidateAttachments = function(res_txt)
		{
			var types = controller.GetDocumentTypeValues(true);
			var attachments = controller.CollectAttachments();
			var check_constraint_result = true;
			for(var i = 0; i < types.length; i++)
			{
				var findAttach = false;
				for(var j = 0; j < attachments.length; j++)
				{
					findAttach = CheckDescription(attachments[j], types[i]);
					if (findAttach)
						break;
				}
				if (!findAttach)
				{
					check_constraint_result &= findAttach;
					res_txt = BuildValidationTextAttach(res_txt, types[i]);
				}
			}
			AfterValidation(check_constraint_result);
			return res_txt;
		}

		var ValidateAttachmentDescription = function(res_txt)
		{
			var types = controller.GetDocumentTypeValues(true);
			var attachments = controller.CollectAttachments();
			var check_constraint_result = true;
			for(var i = 0; i < attachments.length; i++)
			{
				var findType = CheckDescriptionType(attachments[i].Description, types);
				if (!findType)
				{
					check_constraint_result &= findType;
					res_txt = BuildValidationTextType(res_txt, attachments[i]);
				}
			}
			return res_txt;
		}

		var CheckDescription = function (attachment, type)
		{
			if ('[object Array]' == Object.prototype.toString.call(attachment.Description))
			{
				for (var i = 0; i < attachment.Description.length; i++)
				{
					if (type.id == attachment.Description[i].id && !attachment.Description[i].find)
					{
						attachment.Description[i].find = true;
						return true;
					}
				}
			}
			else
			{
				if (attachment.Description && type.id == attachment.Description.id && !attachment.Description[i].find)
				{
					attachment.Description[i].find = true;
					return true;
				}
			}
			return false;
		}

		var CheckDescriptionType = function (descriptions, types)
		{
			var findType = true;
			if ('[object Array]' == Object.prototype.toString.call(descriptions))
			{
				for (var i = 0; i < descriptions.length; i++)
				{
					findType &= CheckType(descriptions[i], types);
				}
			}
			else
			{
				findType &= CheckType(descriptions, types);
			}
			return findType;
		}

		var CheckType = function (description, types)
		{
			for (var i = 0; i < types.length; i++) {
			    if (types[i].id == description.id)
				{
					return true;
				}
			}
			return false;
		}

		var BuildValidationTextAttach = function (res_txt, type)
		{
			if (null == res_txt)
			{
				res_txt = [];
			}
			var message = 'Не прикреплена скан-копия документа "' + type.text + '"';
			message += (!type.series || type.series == '') && (!type.number || type.number == '') ? '' : ',';
			message += !type.series || type.series == '' ? '' : ' серия ' + type.series;
			message += !type.number || type.number == '' ? '' : ' №' + type.number;
			if ('string' == typeof res_txt)
			{
				res_txt = res_txt == null ? '' : res_txt + '\r\n';
				res_txt += message;
			}
			else if ('[object Array]' == Object.prototype.toString.call(res_txt))
			{
				res_txt.push({ check_constraint_result: false, description: message });
			}
			return res_txt;
		}

		var BuildValidationTextType = function (res_txt, attachment)
		{
			if (null == res_txt)
			{
				res_txt = [];
			}
			var message = 'Нет документа, соответствующего скан-копии документа  "' + attachment.FileName + '"';
			if ('string' == typeof res_txt)
			{
				res_txt = res_txt == null ? '' : res_txt + '\r\n';
				res_txt += message;
			}
			else if ('[object Array]' == Object.prototype.toString.call(res_txt))
			{
				res_txt.push({ check_constraint_result: false, description: message });
			}
			return res_txt;
		}

		var AfterValidation = function(check_constraint_result)
		{
			if (!check_constraint_result && !$('#add-scans').parent().parent().hasClass('invalid'))
			{
				$('#add-scans').parent().parent().addClass('invalid');
				$('#add-scans').parent().parent().find('.field-hint').click(ShowMessageForField);
			}
			else if (check_constraint_result && $('#add-scans').parent().parent().hasClass('invalid'))
			{
				$('#add-scans').parent().parent().removeClass('invalid');
			}
		}

		var ShowMessageForField = function ()
		{
			var validation_result = controller.DoValidateAttachments(null);
			if (validation_result && validation_result.length > 0)
			{
				var descriptions = [];
				for(var i = 0; i < validation_result.length; i++)
				{
					descriptions.push(validation_result[i].description);
				}
				helper_msgbox.ShowModal({
					title: 'Пояснения для поля'
					, html: '<ul><li>' + descriptions.join('</li><li>') + '</li></ul>'
					, width: 600
				});
			}
		}

		controller.AfterClearDocument = function(idDocument) {
			$('.doc-wrap .inline_data_block').each(function (counter) {
				var document = $(this).find('#s2id_cpw_form_scan_DocumentType').select2('data');
				if (document && document.id == idDocument) {
					$(this).closest('.doc-wrap').remove();
				}
			});
		};

		controller.CheckDocumentNotEmpty = function (idDocument) {
			var result = false;
			$('.doc-wrap .inline_data_block').each(function (counter) {
				var document = $(this).find('#s2id_cpw_form_scan_DocumentType').select2('data');
				if (document && document.id == idDocument) {
					result = true
				}
			});
			return result;
		}

		controller.UseCodec(codec());

		return controller;
	}
});