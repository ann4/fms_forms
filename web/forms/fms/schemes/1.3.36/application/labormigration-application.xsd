<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	11.10.2013 Я. Савельева UMMS-22565:
		- убрана обязательность элемента validFrom в базовом заявлении
	25.06.2014 Р. Исбаров UMMS-22760:
		- уведомления и мед. справки перенесены из базового типа application в тип workPermitApplication
		  (уведомления и мед. справки могут быть только в заявлении о выдаче разрешения на работу и не могут быть в
		  заявлении о выдаче патента)
	11.12.2014 Р. Исбаров UMMS-25533:
		- в комплексный базовый тип application возвращены мед. справки, т.к. снова появилась возможность
		  передачи мед. справок в патенте
	16.12.2014 Р. Исбаров UMMS-25639:
		- изменено пространство имен с replication на application
	30.03.2015 Я. Савельева UMMS-27028:
		- удален комплексный тип notification
	19.05.2015 Р. Исбаров UMMS-27718:
		- добавлен комплексный тип laborDocument
	12.01.2016 Р. Исбаров UMMS-33033:
		- удален элемент disease в комплексном типе healthCertificate, вместо него добавлен флаг diseased
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:payment="http://umms.fms.gov.ru/replication/payment"
        xmlns:lm="http://umms.fms.gov.ru/application/labormigration"
        targetNamespace="http://umms.fms.gov.ru/application/labormigration">

	<import namespace="http://umms.fms.gov.ru/replication/core" />
	<import namespace="http://umms.fms.gov.ru/replication/payment" schemaLocation="../payment.xsd"/>

	<complexType name="application" abstract="true">
		<annotation>
			<documentation>Базовое заявление для дел физического лица модуля Трудовая миграция</documentation>
		</annotation>
		<sequence>
			<element name="fillingDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата подачи заявления</documentation>
				</annotation>
			</element>
			<!-- labormigration.TypeOfRecipient (ru.gov.fms.umms.domain.dictionary.labormigration.TypeOfRecipient) -->
			<element name="declarantType" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Кем оформляется</documentation>
				</annotation>
			</element>
			<element name="applicant" type="umms:personDocument" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Информация о заявителе</documentation>
				</annotation>
			</element>
			<element name="representative" type="umms:personDocument" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Информация о представителе</documentation>
				</annotation>
			</element>
			<element name="healthCertificate" type="lm:healthCertificate" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Медицинские справки</documentation>
				</annotation>
			</element>
			<element name="paymentDocument" type="payment:paymentDocument" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Квитанции</documentation>
				</annotation>
			</element>
			<element name="receivingPartyContact" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Особые отметки</documentation>
				</annotation>
			</element>
			<element name="validFrom" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Срок действия с</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="healthCertificate">
		<annotation>
			<documentation>Медицинская справка</documentation>
		</annotation>
		<sequence>
			<!-- labormigration.HealthCertificateType (ru.gov.fms.umms.domain.dictionary.labormigration.HealthCertificateType) -->
			<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Тип медицинской справки</documentation>
				</annotation>
			</element>
			<element name="number" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Номер справки</documentation>
				</annotation>
			</element>
			<element name="issueDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата выдачи справки в медицинском учреждении</documentation>
				</annotation>
			</element>
			<element name="receivingDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата подачи справки заявителем</documentation>
				</annotation>
			</element>
			<element name="medicalInstitution" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Медицинское учреждение, выдавшее справку</documentation>
				</annotation>
			</element>
			<element name="diseased" type="boolean" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Признак наличия заболеваний (true - заболевания выявлены, false - не выявлены)
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="laborDocument">
		<annotation>
			<documentation>
				РНР или патент.
				Для передачи данных РНР или патента нам недостаточно базового типа umms:document из core.xsd,
				т.к. помимо серии и номера бланка, в РНР или патенте есть еще серия и номер документа.
				Данный тип расширяет umms:document и добавляет элементы для передачи серии и номера документа.
				Серия и номер бланка передаются в составе базового типа umms:document.
				Типом данного документа является значение из справочника с кодом "labormigration.DocumentType".
			</documentation>
		</annotation>
		<complexContent>
			<extension base="umms:document">
				<sequence>
					<element name="documentSeries" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Серия документа (серия бланка передается в базовом типе)</documentation>
						</annotation>
					</element>
					<element name="documentNumber" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Номер документа (номер бланка передается в базовом типе)</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

</schema>
