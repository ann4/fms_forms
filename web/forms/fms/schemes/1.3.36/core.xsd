<?xml version="1.0" encoding="UTF-8" ?>

<!--
История изменений:
	28.08.2013 Р. Исбаров UMMS-17686:
		- добавлены дополнительные элементы в тип organization
	06.09.2013 Р. Исбаров UMMS-17811:
		- добавлен дополнительный элемент inn в тип personData
	14.10.2013 Р. Исбаров UMMS-18370:
		- добавлен дополнительный элемент requestId в тип case
	13.11.2013 А. Ефимчук UMMS-18944:
		- добавлен дополнительный элемент Signature для реализации электронной подписи
	15.12.2014 Р. Исбаров UMMS-25639:
		- добавлен тип innData для передачи данных об ИНН, дате и органе выдачи
	25.12.2014 Я. Савельева UMMS-25770:
		- добавлен тип hexBinary для ограничения размера изображения до 500 Кб
	20.03.2015 Р. Исбаров UMMS-26925:
		- размерность типа hexBinary увеличена до 512 Кб
	13.04.2015 Я. Савельева UMMS-27365:
		- добавлен дополнительный элемент ummsNumber в тип case
	19.05.2015 Р. Исбаров UMMS-23017:
		- добавлены дополнительные элементы additionalActivityKind и headOrganization в тип organization
	19.05.2015 Р. Исбаров UMMS-27718:
		- добавлены типы foreignOrganization, individualOrganization и organizationData
	20.05.2015 Р. Исбаров UMMS-27767:
		- изменено описание типа uid, а также элементов date, number, ummsNumber типа case
	19.11.2015 Р. Исбаров UMMS-32751:
		- добавлен тип personPreviousName
	12.01.2016 Р. Исбаров UMMS-34875:
		- добавлена возможность передачи двух электронных подписей в составе типа case
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
        targetNamespace="http://umms.fms.gov.ru/replication/core">

	<import namespace="http://www.w3.org/2000/09/xmldsig#"
			schemaLocation="http://www.w3.org/TR/2002/REC-xmldsig-core-20020212/xmldsig-core-schema.xsd"/>

	<simpleType name="string">
		<annotation>
			<documentation>Строка, содержащая любые символы</documentation>
		</annotation>
		<restriction base="string">
			<whiteSpace value="collapse"/>
		</restriction>
	</simpleType>

	<simpleType name="hexBinary">
		<annotation>
			<documentation>Содержимое изображения</documentation>
		</annotation>
		<restriction base="hexBinary">
			<maxLength value="524288"/>
		</restriction>
	</simpleType>

	<simpleType name="value">
		<annotation>
			<documentation>Тип предназначен для передачи непустого строкового значения</documentation>
		</annotation>
		<restriction base="umms:string">
			<minLength value="1"/>
		</restriction>
	</simpleType>

	<simpleType name="uid">
		<annotation>
			<documentation>Уникальный идентификатор в системе поставщика данных</documentation>
		</annotation>
		<restriction base="umms:string">
			<pattern value=".+"/>
		</restriction>
	</simpleType>

	<simpleType name="inn">
		<annotation>
			<documentation>ИНН организации или частного лица</documentation>
		</annotation>
		<restriction base="umms:string">
			<pattern value="\d{10}"/>
			<pattern value="\d{12}"/>
			<pattern value="\d{14}"/>
		</restriction>
	</simpleType>

	<simpleType name="inn_o">
		<annotation>
			<documentation>ИНН организации</documentation>
		</annotation>
		<restriction base="umms:inn">
			<pattern value="\d{10}"/>
		</restriction>
	</simpleType>

	<simpleType name="inn_p">
		<annotation>
			<documentation>ИНН частного лица</documentation>
		</annotation>
		<restriction base="umms:inn">
			<pattern value="\d{12}"/>
		</restriction>
	</simpleType>

	<simpleType name="inn_i">
		<annotation>
			<documentation>ИНН ИПБЮЛ</documentation>
		</annotation>
		<restriction base="umms:inn">
			<pattern value="\d{14}"/>
		</restriction>
	</simpleType>

	<simpleType name="ummsId">
		<annotation>
			<documentation>Идентификатор</documentation>
		</annotation>
		<restriction base="umms:value">
			<pattern value="[\-]?\d{1,12}"/>
		</restriction>
	</simpleType>

	<simpleType name="birthDate">
		<annotation>
			<documentation>
				Дата рождения в формате DD.MM.YYYY. Выделена в отдельный тип для переопределения
				возможности указать отсутствующий день и/или месяц рождения, например: 00.00.1979.
			</documentation>
		</annotation>
		<restriction base="umms:string">
			<pattern value="\d{2}\.\d{2}\.\d{4}"/>
		</restriction>
	</simpleType>

	<simpleType name="class">
		<annotation>
			<documentation>Тип, описывающий полное имя класса (FQN)</documentation>
		</annotation>
		<restriction base="umms:value">
			<pattern value="[a-z][\.a-z0-9]+[A-Z][A-Za-z0-9]+"/>
		</restriction>
	</simpleType>

	<complexType name="dictionary">
		<annotation>
			<documentation>Тип для передачи справочного значения</documentation>
		</annotation>
		<sequence>
			<element name="type" type="umms:value" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Код справочника в ППО "Территория"</documentation>
				</annotation>
			</element>
			<element name="element" type="umms:value" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Код элемента справочника в ППО "Территория"</documentation>
				</annotation>
			</element>
			<element name="value" type="umms:value" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Значение справочника в ППО "Территория" (не используется при обработке)
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="user">
		<annotation>
			<documentation>Тип, описывающий пользователя</documentation>
		</annotation>
		<choice minOccurs="1" maxOccurs="1">
			<element name="ummsId" type="umms:ummsId">
				<annotation>
					<documentation>Идентификатор пользователя в ППО "Территория"</documentation>
				</annotation>
			</element>
			<element name="name" type="umms:value">
				<annotation>
					<documentation>Строка, идентифицирующая пользователя (ФИО / логин)</documentation>
				</annotation>
			</element>
		</choice>
	</complexType>

	<complexType name="entityBase">
		<annotation>
			<documentation>Общий предок типов</documentation>
		</annotation>
		<sequence>
			<element name="uid" type="umms:uid" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Уникальный идентификатор в системе поставщика данных</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="period">
		<annotation>
			<documentation>Тип, описывающий период времени</documentation>
		</annotation>
		<sequence>
			<element name="dateFrom" type="date" minOccurs="1" maxOccurs="1"/>
			<element name="dateTo" type="date" minOccurs="0" maxOccurs="1"/>
		</sequence>
	</complexType>

	<complexType name="russianAddress">
		<annotation>
			<documentation>Тип, описывающий российский адрес</documentation>
		</annotation>
		<sequence>
			<choice minOccurs="1" maxOccurs="1">
				<!-- addressObject (ru.gov.fms.umms.domain.dictionary.address.AddressObject) -->
				<element name="addressObject" type="umms:dictionary">
					<annotation>
						<documentation>Адресный объект (код ФИАС)</documentation>
					</annotation>
				</element>
				<element name="addressObjectString" type="umms:value">
					<annotation>
						<documentation>
							Строковое представление адреса, записанное в формате:
							Регион$Район$Город$Населенный Пункт$Городской район$Улица
						</documentation>
					</annotation>
				</element>
				<element name="addressObjectId" type="umms:dictionary">
					<annotation>
						<documentation>
							Адресный объект (идентификатор ППО "Территория", используется для исторических адресов,
							отсутствующих в ФИАС)
						</documentation>
					</annotation>
				</element>
			</choice>
			<element name="housing" minOccurs="0" maxOccurs="8">
				<annotation>
					<documentation>Помещение</documentation>
				</annotation>
				<complexType>
					<annotation>
						<documentation>Тип, описывающий помещение</documentation>
					</annotation>
					<sequence>
						<!-- addressObjectType (ru.gov.fms.umms.domain.dictionary.address.AddressObjectType) -->
						<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
							<annotation>
								<documentation>Тип адресного объекта</documentation>
							</annotation>
						</element>
						<element name="value" type="umms:value">
							<annotation>
								<documentation>Значение адресного объекта</documentation>
							</annotation>
						</element>
					</sequence>
				</complexType>
			</element>
		</sequence>
	</complexType>

	<complexType name="foreignAddress">
		<annotation>
			<documentation>Тип, описывающий иностранный адрес</documentation>
		</annotation>
		<sequence>
			<!-- Country (ru.gov.fms.umms.domain.dictionary.address.Country) -->
			<element name="country" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Страна (код альфа-3 ОКСМ)</documentation>
				</annotation>
			</element>
			<element name="unstructured" type="umms:value" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Неструктурированный адрес для иностранных и исторических значений</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="address">
		<annotation>
			<documentation>Тип, описывающий адрес</documentation>
		</annotation>
		<sequence>
			<choice minOccurs="1" maxOccurs="1">
				<element name="russianAddress" type="umms:russianAddress">
					<annotation>
						<documentation>Российский адрес</documentation>
					</annotation>
				</element>
				<element name="foreignAddress" type="umms:foreignAddress">
					<annotation>
						<documentation>Иностранный адрес</documentation>
					</annotation>
				</element>
			</choice>
		</sequence>
	</complexType>

	<complexType name="contactInfo">
		<annotation>
			<documentation>Контактная информация</documentation>
		</annotation>
		<sequence>
			<element name="address" type="umms:address" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Адрес</documentation>
				</annotation>
			</element>
			<element name="phone" type="umms:string" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Телефон</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="case" abstract="true">
		<complexContent mixed="false">
			<annotation>
				<documentation>Тип, содержащий в себе базовые атрибуты любого дела</documentation>
			</annotation>
			<extension base="umms:entityBase">
				<sequence>
					<element ref="ds:Signature" minOccurs="0" maxOccurs="2"/>
					<element name="requestId" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								Уникальный идентификатор запроса, по которому сформирован ответ
							</documentation>
						</annotation>
					</element>
					<element name="supplierInfo" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Уникальный идентификатор системы, передающей данные, получаемый в ППО "Территория"
							</documentation>
						</annotation>
					</element>
					<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
					<element name="subdivision" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Территориальный орган/структурное подразделение
								(значение из справочника "Официальные органы")
							</documentation>
						</annotation>
					</element>
					<element name="employee" type="umms:user" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Оператор, внесший дело</documentation>
						</annotation>
					</element>
					<element name="date" type="dateTime" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Дата ввода заявления в систему поставщика данных</documentation>
						</annotation>
					</element>
					<element name="number" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								Номер заявления в системе поставщика данных
								(элемент заполняется только для заявлений, для дел не заполняется)
							</documentation>
						</annotation>
					</element>
					<element name="ummsNumber" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								Номер дела
								(необязательный, используется только в случае дополнительного указания со стороны ФМС)
							</documentation>
						</annotation>
					</element>
					<element name="photo" type="umms:hexBinary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Фотография</documentation>
						</annotation>
					</element>
					<element name="comments" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Комментарии</documentation>
						</annotation>
					</element>
					<element name="documentPhoto" type="umms:documentPhoto" minOccurs="0" maxOccurs="unbounded">
						<annotation>
							<documentation>Фотографии документов, участвующих в деле</documentation>
						</annotation>
					</element>
				</sequence>
				<attribute name="schemaVersion" type="decimal" use="required" fixed="1.0"/>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="personData">
		<complexContent mixed="false">
			<annotation>
				<documentation>Тип, содержащий персональные данные физического лица</documentation>
			</annotation>
			<extension base="umms:entityBase">
				<sequence>
					<element name="personId" type="umms:uid" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Идентификатор физического лица, связанного с персональными данными
							</documentation>
						</annotation>
					</element>
					<!-- В случае отсутствия записывается значение "[отсутствует]" -->
					<element name="lastName" type="umms:value" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Фамилия</documentation>
						</annotation>
					</element>
					<element name="lastNameLat" type="umms:value" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Фамилия (латиницей)</documentation>
						</annotation>
					</element>
					<!-- В случае отсутствия записывается значение "[отсутствует]" -->
					<element name="firstName" type="umms:value" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Имя</documentation>
						</annotation>
					</element>
					<element name="firstNameLat" type="umms:value" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Имя (латиницей)</documentation>
						</annotation>
					</element>
					<element name="middleName" type="umms:value" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Отчество</documentation>
						</annotation>
					</element>
					<element name="middleNameLat" type="umms:value" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Отчество (латиницей)</documentation>
						</annotation>
					</element>
					<!-- Gender (ru.gov.fms.umms.domain.dictionary.Gender) -->
					<element name="gender" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Пол</documentation>
						</annotation>
					</element>
					<element name="birthDate" type="umms:birthDate" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата рождения</documentation>
						</annotation>
					</element>
					<!-- citizenship (ru.gov.fms.umms.domain.dictionary.Citizenship) -->
					<element name="citizenship" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Гражданство, подданство (код ОКСМ)</documentation>
						</annotation>
					</element>
					<element name="birthPlace" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Место рождения</documentation>
						</annotation>
						<complexType>
							<sequence>
								<!-- Country (ru.gov.fms.umms.domain.dictionary.address.Country) -->
								<element name="country" type="umms:dictionary" minOccurs="0" maxOccurs="1">
									<annotation>
										<documentation>Государство (код альфа-3 ОКСМ)</documentation>
									</annotation>
								</element>
								<element name="place" type="umms:string" minOccurs="0" maxOccurs="1">
									<annotation>
										<documentation>Основная часть или регион</documentation>
									</annotation>
								</element>
								<element name="place2" type="umms:string" minOccurs="0" maxOccurs="1">
									<annotation>
										<documentation>Если заполнено - район</documentation>
									</annotation>
								</element>
								<element name="place3" type="umms:string" minOccurs="0" maxOccurs="1">
									<annotation>
										<documentation>Если заполнено - город</documentation>
									</annotation>
								</element>
								<element name="place4" type="umms:string" minOccurs="0" maxOccurs="1">
									<annotation>
										<documentation>Если заполнено - населенный пункт</documentation>
									</annotation>
								</element>
							</sequence>
						</complexType>
					</element>
					<element name="inn" type="umms:inn_p" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>ИНН</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="document">
		<complexContent mixed="false">
			<annotation>
				<documentation>Тип, описывающий любой документ</documentation>
			</annotation>
			<extension base="umms:entityBase">
				<sequence>
					<!-- DocumentType (ru.gov.fms.umms.domain.dictionary.DocumentType) -->
					<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Тип документа (справочное значение)</documentation>
						</annotation>
					</element>
					<element name="series" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Серия</documentation>
						</annotation>
					</element>
					<element name="number" type="umms:value" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Номер</documentation>
						</annotation>
					</element>
					<choice minOccurs="0" maxOccurs="1">
						<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
						<element name="authorityOrgan" type="umms:dictionary">
							<annotation>
								<documentation>
									Кем выдан. Значение из справочника "Официальные органы" для документов,
									выдаваемых ФМС
								</documentation>
							</annotation>
						</element>
						<element name="authority" type="umms:string">
							<annotation>
								<documentation>
									Кем выдан. Неструктурированная информация для любых типов документов,
									отличных от выдаваемых ФМС
								</documentation>
							</annotation>
						</element>
					</choice>
					<element name="issued" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата выдачи</documentation>
						</annotation>
					</element>
					<element name="validFrom" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Срок действия c</documentation>
						</annotation>
					</element>
					<element name="validTo" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Срок действия по</documentation>
						</annotation>
					</element>
					<!-- DocumentStatus (ru.gov.fms.umms.domain.dictionary.DocumentStatusType) -->
					<element name="status" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Статус документа (значение из справочника "Статус документа")
							</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="personDocument">
		<annotation>
			<documentation>Тип, описывающий персональные данные физ. лица и его ДУЛ</documentation>
		</annotation>
		<sequence>
			<element name="person" type="umms:personData" minOccurs="1" maxOccurs="1"/>
			<element name="document" type="umms:document" minOccurs="1" maxOccurs="1"/>
			<element name="entered" type="boolean" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Флаг, указывающий на то, вписано ли данное физ. лицо в документ
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="personPreviousName">
		<annotation>
			<documentation>Предыдущие ФИО физ. лица</documentation>
		</annotation>
		<sequence>
			<element name="previousPersonData" type="umms:personData" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Предыдущие ФИО</documentation>
				</annotation>
			</element>
			<element name="changeDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата изменения ФИО</documentation>
				</annotation>
			</element>
			<element name="changePlace" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Место изменения ФИО</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<!-- FIXME: необходимо переименовать в russianOrganization. -->
	<complexType name="organization">
		<annotation>
			<documentation>Тип, описывающий российское юридическое лицо</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:entityBase">
				<sequence>
					<element name="inn" type="umms:inn" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>ИНН</documentation>
						</annotation>
					</element>
					<element name="name" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Полное наименование</documentation>
						</annotation>
					</element>
					<!-- FIXME: необходимо указывать российский адрес. -->
					<!-- FIXME: необходимо переименовать в actualAddress. -->
					<element name="address" type="umms:organizationAddress" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Фактический адрес организации</documentation>
						</annotation>
					</element>
					<!-- FIXME: необходимо указывать российский адрес. -->
					<element name="legalAddress" type="umms:organizationAddress" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Юридический адрес организации</documentation>
						</annotation>
					</element>
					<element name="shortName" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Краткое наименование</documentation>
						</annotation>
					</element>
					<element name="ogrn" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>ОГРН</documentation>
						</annotation>
					</element>
					<element name="okpo" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>ОКПО</documentation>
						</annotation>
					</element>
					<element name="kpp" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>КПП</documentation>
						</annotation>
					</element>
					<element name="okonh" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>ОКОНХ</documentation>
						</annotation>
					</element>
					<element name="registerDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата регистрации юр. лица в налоговом органе</documentation>
						</annotation>
					</element>
					<element name="registerNumber" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Номер регистрационного свидетельства</documentation>
						</annotation>
					</element>
					<element name="registerAuthority" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Орган гос. регистрации</documentation>
						</annotation>
					</element>
					<element name="pboul" type="boolean" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Предприниматель без образования юр. лица</documentation>
						</annotation>
					</element>
					<!-- ActivityKind (ru.gov.fms.umms.domain.dictionary.ActivityKind) -->
					<element name="activityKind" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Основной вид деятельности</documentation>
						</annotation>
					</element>
					<!-- ActivityKind (ru.gov.fms.umms.domain.dictionary.ActivityKind) -->
					<element name="additionalActivityKind" type="umms:dictionary" minOccurs="0" maxOccurs="unbounded">
						<annotation>
							<documentation>Дополнительный вид деятельности</documentation>
						</annotation>
					</element>
					<!-- CapitalKind (ru.gov.fms.umms.domain.dictionary.CapitalKind) -->
					<element name="capitalKind" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Вид уставного капитала</documentation>
						</annotation>
					</element>
					<!-- Ownership (ru.gov.fms.umms.domain.dictionary.Ownership) -->
					<element name="ownership" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Форма собственности</documentation>
						</annotation>
					</element>
					<!-- OPF (ru.gov.fms.umms.domain.dictionary.OPF) -->
					<element name="opf" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Организационно-правовая форма</documentation>
						</annotation>
					</element>
					<!-- Opf2t (ru.gov.fms.umms.domain.dictionary.Opf2t) -->
					<element name="opf2t" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Организационно-правовая форма (для справки 2Т)</documentation>
						</annotation>
					</element>
					<element name="headOrganization" type="boolean" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Признак головной организации</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="foreignOrganization">
		<annotation>
			<documentation>Тип, описывающий иностранное юридическое лицо</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:entityBase">
				<sequence>
					<element name="foreignName" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Наименование иностранного юр. лица</documentation>
						</annotation>
					</element>
					<element name="foreignRegistrationNumber" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Иностранный регистрационный номер юр. лица</documentation>
						</annotation>
					</element>
					<element name="foreignLegalAddress" type="umms:foreignAddress" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Иностранный юридический адрес юр. лица</documentation>
						</annotation>
					</element>
					<element name="affiliateInn" type="umms:inn" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>ИНН филиала (представительства)</documentation>
						</annotation>
					</element>
					<element name="affiliateFullName" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Полное наименование филиала (представительства)</documentation>
						</annotation>
					</element>
					<element name="affiliateShortName" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Краткое наименование филиала (представительства)</documentation>
						</annotation>
					</element>
					<element name="affiliateActualAddress" type="umms:russianAddress" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Фактический адрес филиала (представительства)</documentation>
						</annotation>
					</element>
					<element name="affiliateKpp" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>КПП филиала (представительства)</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="individualOrganization">
		<annotation>
			<documentation>Тип, описывающий физическое лицо - работодателя</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:entityBase">
				<sequence>
					<element name="employer" type="umms:personDocument" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Установочные данные и документ работодателя - физ. лица</documentation>
						</annotation>
					</element>
					<element name="inn" type="umms:inn_p" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>ИНН</documentation>
						</annotation>
					</element>
					<element name="name" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Полное наименование</documentation>
						</annotation>
					</element>
					<element name="actualAddress" type="umms:russianAddress" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Фактический адрес</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<!-- FIXME: необходимо переименовать в organization. -->
	<complexType name="organizationData">
		<annotation>
			<documentation>Тип, описывающий юридическое лицо</documentation>
		</annotation>
		<sequence>
			<choice minOccurs="1" maxOccurs="1">
				<element name="russianOrganization" type="umms:organization">
					<annotation>
						<documentation>Российское юридическое лицо</documentation>
					</annotation>
				</element>
				<element name="foreignOrganization" type="umms:foreignOrganization">
					<annotation>
						<documentation>Иностранное юридическое лицо</documentation>
					</annotation>
				</element>
				<element name="individualOrganization" type="umms:individualOrganization">
					<annotation>
						<documentation>Физическое лицо - работодатель</documentation>
					</annotation>
				</element>
			</choice>
		</sequence>
	</complexType>

	<complexType name="livingRegistration">
		<annotation>
			<documentation>Регистрация по месту жительства</documentation>
		</annotation>
		<sequence>
			<element name="dateFrom" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Регистрация с</documentation>
				</annotation>
			</element>
			<element name="dateTo" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Регистрация по</documentation>
				</annotation>
			</element>
			<element name="address" type="umms:address" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Адрес</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="stayingRegistration">
		<annotation>
			<documentation>Регистрация по месту пребывания</documentation>
		</annotation>
		<sequence>
			<element name="dateFrom" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Регистрация с</documentation>
				</annotation>
			</element>
			<element name="dateTo" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Регистрация по</documentation>
				</annotation>
			</element>
			<element name="address" type="umms:address" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Адрес</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="organizationAddress">
		<annotation>
			<documentation>Адрес организации</documentation>
		</annotation>
		<sequence>
			<element name="address" type="umms:address" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Российский или иностранный адрес организации</documentation>
				</annotation>
			</element>
			<element name="dateFrom" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата начала действия</documentation>
				</annotation>
			</element>
			<element name="dateTo" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата окончания действия</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="documentPhoto">
		<annotation>
			<documentation>Изображение документа</documentation>
		</annotation>
		<sequence>
			<element name="documentUID" type="umms:uid" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Уникальный идентификатор документа, фотография которого передается</documentation>
				</annotation>
			</element>
			<!-- DocumentType (ru.gov.fms.umms.domain.dictionary.DocumentType) -->
			<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Тип документа (справочное значение)</documentation>
				</annotation>
			</element>
			<element name="documentPhotoContent" type="umms:hexBinary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Содержимое изображения документа</documentation>
				</annotation>
			</element>
			<element name="documentPhotoFormat" type="umms:imageMIMEType" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Формат изображения документа</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<simpleType name="imageMIMEType">
		<annotation>
			<documentation>MIME-тип загружаемых изображений</documentation>
		</annotation>
		<restriction base="umms:string">
			<enumeration value="image/jpeg">
				<annotation>
					<documentation>MIME-тип image/jpeg</documentation>
				</annotation>
			</enumeration>
			<enumeration value="image/png">
				<annotation>
					<documentation>MIME-тип image/png</documentation>
				</annotation>
			</enumeration>
			<enumeration value="image/tiff">
				<annotation>
					<documentation>MIME-тип image/tiff</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>

	<simpleType name="systemCaseId">
		<annotation>
			<documentation>Системный (внутренний) составной идентификатор дела</documentation>
		</annotation>
		<restriction base="umms:value">
			<pattern value="\d{1,}[\-]\d{1,}"/>
		</restriction>
	</simpleType>

	<complexType name="innData">
		<annotation>
			<documentation>Данные об ИНН, дате и органе выдачи</documentation>
		</annotation>
		<sequence>
			<element name="inn" type="umms:inn" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>ИНН</documentation>
				</annotation>
			</element>
			<element name="issued" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата выдачи</documentation>
				</annotation>
			</element>
			<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
			<element name="authorityOrgan" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Кем выдан. Значение из справочника "Официальные органы ФНС".</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

</schema>
