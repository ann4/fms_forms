<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	09.09.2013 Р. Исбаров UMMS-17751:
		- в блоке "Информация о трудовой деятельности" поле "Дата начала работы" сделано необязательным.
	19.11.2015 Р. Исбаров UMMS-32751:
		- добавлен тип residencePermit
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:fcc="http://umms.fms.gov.ru/replication/foreign-citizen-core"
        targetNamespace="http://umms.fms.gov.ru/replication/foreign-citizen-core">

	<import namespace="http://umms.fms.gov.ru/replication/core" schemaLocation="core.xsd"/>

	<complexType name="migrationCard">
		<annotation>
			<documentation>Миграционная карта</documentation>
		</annotation>
		<complexContent>
			<extension base="umms:entityBase">
				<sequence>
					<element name="series" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								Серия. Сделана необязательным реквизитом ввиду
								наличия карт по соглашению с Республикой Беларусь
							</documentation>
						</annotation>
					</element>
					<element name="number" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Номер</documentation>
						</annotation>
					</element>
					<element name="stayPeriod" type="umms:period" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Заявленный срок пребывания</documentation>
						</annotation>
					</element>
					<element name="entranceDate" type="date" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Дата въезда в РФ</documentation>
						</annotation>
					</element>
					<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
					<element name="entranceCheckpoint" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>КПП въезда (значение из справочника "КПП")</documentation>
						</annotation>
					</element>
					<element name="departureDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								Дата выезда из РФ. Сделана необязательной.
								При подаче уведомления дата выезда неизвестна.
							</documentation>
						</annotation>
					</element>
					<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
					<element name="departureCheckpoint" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>При подаче уведомления КПП выезда неизвестен</documentation>
						</annotation>
					</element>
					<!-- VisitPurpose (ru.gov.fms.umms.domain.dictionary.migration.VisitPurpose) -->
					<element name="visitPurpose" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								Цель въезда.
								Добавлена для сопоставления цели, указанной в уведомлении, с целью в миграционной карте.
								Для выявления адм. нарушителей ст. 26 пункт 2 114-ФЗ.
							</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="visa">
		<annotation>
			<documentation>Виза</documentation>
		</annotation>
		<complexContent>
			<extension base="umms:document">
				<sequence>
					<!-- VisaCategory (ru.gov.fms.umms.domain.dictionary.VisaCategory) -->
					<element name="category" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Категория визы - значение из справочника "Категории визы"</documentation>
						</annotation>
					</element>
					<element name="stayDuration" type="integer" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Разрешенный срок пребывания</documentation>
						</annotation>
					</element>
					<!-- VisaMultiplicity (ru.gov.fms.umms.domain.dictionary.VisaMultiplicity) -->
					<element name="multiplicity" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Кратность визы - значение из справочника "Кратность визы"</documentation>
						</annotation>
					</element>
					<element name="identitifier" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Идентификатор визы</documentation>
						</annotation>
					</element>
					<element name="invitationNumber" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Номер приглашения</documentation>
						</annotation>
					</element>
					<!-- EntryGoal (ru.gov.fms.umms.domain.dictionary.EntryGoal) -->
					<element name="visitPurpose" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Цель поездки - значение из справочника "Цель поездки"</documentation>
						</annotation>
					</element>
					<element name="prolongation" type="fcc:prolongationBase" minOccurs="0" maxOccurs="unbounded">
						<annotation>
							<documentation>Продление визы</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="residencePermit">
		<annotation>
			<documentation>Вид на жительство</documentation>
		</annotation>
		<complexContent>
			<extension base="umms:document">
				<sequence>
					<element name="decisionNumber" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Номер решения</documentation>
						</annotation>
					</element>
					<element name="decisionDate" type="date" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Дата решения</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="prolongationBase">
		<annotation>
			<documentation>Тип, описывающий любое продление</documentation>
		</annotation>
		<sequence>
			<element name="decision" type="date">
				<annotation>
					<documentation>Дата решения</documentation>
				</annotation>
			</element>
			<element name="extendedFrom" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Продлена c</documentation>
				</annotation>
			</element>
			<element name="extendedTo" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Продлена до</documentation>
				</annotation>
			</element>
			<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
			<element name="subdivision" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Орган, выполнивший продление - значение из справочника "Официальные органы"
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="employmentInfo">
		<annotation>
			<documentation>Информация о трудовой деятельности</documentation>
		</annotation>
		<sequence>
			<!-- WorkType (ru.gov.fms.umms.domain.dictionary.WorkType) -->
			<element name="workType" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Тип трудовой деятельности</documentation>
				</annotation>
			</element>
			<element name="workPlace" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Место работы (учебы)</documentation>
				</annotation>
			</element>
			<element name="position" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Должность</documentation>
				</annotation>
			</element>
			<element name="startDate" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата начала работы (учебы)</documentation>
				</annotation>
			</element>
			<element name="endDate" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата окончания работы (учебы)</documentation>
				</annotation>
			</element>
			<element name="address" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Адрес места работы (учебы)</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="relative">
		<annotation>
			<documentation>Информация о родственнике</documentation>
		</annotation>
		<sequence>
			<!-- RelationType (ru.gov.fms.umms.domain.dictionary.RelationType) -->
			<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Степень родства</documentation>
				</annotation>
			</element>
			<choice minOccurs="1" maxOccurs="1">
				<element name="personData" type="umms:personData">
					<annotation>
						<documentation>Персональные данные физ. лица без ДУЛ</documentation>
					</annotation>
				</element>
				<element name="personDataDocument" type="umms:personDocument">
					<annotation>
						<documentation>Персональные данные физ. лица c ДУЛ</documentation>
					</annotation>
				</element>
			</choice>
			<element name="relationAddress" type="umms:address" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Адрес родственника</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

</schema>
