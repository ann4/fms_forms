<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	03.02.2014 Р. Исбаров UMMS-20101:
		- добавлен дополнительный элемент additionalApplicationReason в тип case
	25.04.2014 Р. Исбаров UMMS-21901:
		- изменен тип элемента spouseBirthDate с date на umms:birthDate
-->

<!-- TODO: Добавить сведения о детях вписанных в паспорт -->
<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:rpissueapp="http://umms.fms.gov.ru/application/rpissueapp"
        xmlns:payment="http://umms.fms.gov.ru/replication/payment"
        targetNamespace="http://umms.fms.gov.ru/application/rpissueapp">

	<import namespace="http://umms.fms.gov.ru/replication/core"/>
	<import namespace="http://umms.fms.gov.ru/replication/payment" schemaLocation="payment.xsd"/>

	<complexType name="case">
		<annotation>
			<documentation>Дело о выдаче/замене паспорта гражданина России</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:case">
				<sequence>
					<element name="application" type="rpissueapp:application" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Заявление на выдачу / замену паспорта гражданина России</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="application">
		<annotation>
			<documentation>Заявление на выдачу / замену паспорта гражданина России</documentation>
		</annotation>
		<sequence>
			<!-- RPApplicationReason (ru.gov.fms.umms.domain.dictionary.RPApplicationReason) -->
			<element name="applicationReason" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Причина открытия дела</documentation>
				</annotation>
			</element>
			<!-- RPApplicationReason (ru.gov.fms.umms.domain.dictionary.RPApplicationReason) -->
			<element name="additionalApplicationReason" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Вторая (дополнительная) причина открытия дела</documentation>
				</annotation>
			</element>
			<element name="appDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата приема документов</documentation>
				</annotation>
			</element>
			<element name="applicant" type="umms:personDocument" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Данные заявителя</documentation>
				</annotation>
			</element>
			<element name="familyInfo" type="rpissueapp:familyInfo" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Сведения о семейном положении</documentation>
				</annotation>
			</element>
			<element name="newApplicantData" type="umms:personData" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Новые установочные данные заявителя, заполняются в случае замены паспорта по смене УД
					</documentation>
				</annotation>
			</element>
			<!-- applicationPlace (ru.gov.fms.umms.domain.dictionary.ApplicationPlace) -->
			<element name="applicationPlace" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Место обращения</documentation>
				</annotation>
			</element>
			<element name="livingRegistration" type="rpissueapp:livingRegAddress" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Место жительства (регистрации)</documentation>
				</annotation>
			</element>
			<element name="stayingRegistration" type="umms:stayingRegistration" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Место пребывания (регистрации)</documentation>
				</annotation>
			</element>
			<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
			<element name="officialOrgan" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Налоговый орган</documentation>
				</annotation>
			</element>
			<element name="citizenshipInfo" type="rpissueapp:citizenshipInfo" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Сведения о предыдущем гражданстве</documentation>
				</annotation>
			</element>
			<element name="paymentDocument" type="payment:paymentDocument" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Платёжные документы</documentation>
				</annotation>
			</element>
			<element name="children" type="rpissueapp:children" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Сведения о детях, вписанных в паспорт</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="children">
		<annotation>
			<documentation>Сведения о детях, вписанных в паспорт</documentation>
		</annotation>
		<choice minOccurs="1" maxOccurs="1">
			<element name="personData" type="umms:personData">
				<annotation>
					<documentation>Персональные данные ребенка без ДУЛ</documentation>
				</annotation>
			</element>
			<element name="personDataDocument" type="umms:personDocument">
				<annotation>
					<documentation>Персональные данные ребенка c ДУЛ</documentation>
				</annotation>
			</element>
		</choice>
	</complexType>

	<complexType name="livingRegAddress">
		<annotation>
			<documentation>Тип регистрации по месту жительства</documentation>
		</annotation>
		<sequence>
			<element name="livingRegistration" type="umms:livingRegistration" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Место жительства (регистрации)</documentation>
				</annotation>
			</element>
			<!-- officialOrgan (ru.gov.fms.umms.domain.dictionary.OfficialOrgan) -->
			<element name="regOffice" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Орган регистрационного учета</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="citizenshipInfo">
		<annotation>
			<documentation>Сведения о предыдущем гражданстве</documentation>
		</annotation>
		<sequence>
			<!-- citizenship (ru.gov.fms.umms.domain.dictionary.Citizenship) -->
			<element name="prevCitizenship" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Прежнее гражданство</documentation>
				</annotation>
			</element>
			<element name="rusCitizenshipDate" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата принятия гражданства РФ</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="familyInfo">
		<annotation>
			<documentation>Сведения о семье</documentation>
		</annotation>
		<sequence>
			<element name="motherLastname" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>ФИО матери</documentation>
				</annotation>
			</element>
			<element name="fatherLastname" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>ФИО отца</documentation>
				</annotation>
			</element>
			<!-- maritalStatus (ru.gov.fms.umms.domain.dictionary.MaritalStatus) -->
			<element name="maritalStatus" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Семейное положение</documentation>
				</annotation>
			</element>
			<element name="spouseOrganName" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Наименование органа регистрации/расторжения брака</documentation>
				</annotation>
			</element>
			<element name="spouseActNum" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Номер акта регистрации/расторжения брака</documentation>
				</annotation>
			</element>
			<element name="spouseRegistrationDate" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата акта регистрации брака / факта расторжения брака</documentation>
				</annotation>
			</element>
			<element name="spouseUnregistrationDate" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата акта расторжения брака</documentation>
				</annotation>
			</element>
			<element name="spouseFirstName" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Имя супруга/супруги</documentation>
				</annotation>
			</element>
			<element name="spouseLastName" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Фамилия супруга/супруги</documentation>
				</annotation>
			</element>
			<element name="spouseMiddleName" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Отчество супруга/супруги</documentation>
				</annotation>
			</element>
			<element name="spouseBirthDate" type="umms:birthDate" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата рождения супруга/супруги</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<element name="case" type="rpissueapp:case">
		<annotation>
			<documentation>Дело о выдаче/замене паспорта гражданина России</documentation>
		</annotation>
	</element>

</schema>
