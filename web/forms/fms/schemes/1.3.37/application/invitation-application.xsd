<?xml version="1.0" encoding="UTF-8"?>

<!--
История изменений:
	04.06.2013 А. Мазур:
		- элемент petitionDate сделан необязательным, при загрузке преддела в это поле будет подставлена системная дата с возможностью корректировки сотрудником ФМС
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:fcc="http://umms.fms.gov.ru/replication/foreign-citizen-core"
        xmlns:payment="http://umms.fms.gov.ru/replication/payment"
        xmlns:invitationAppl="http://umms.fms.gov.ru/replication/invitation-application"
        targetNamespace="http://umms.fms.gov.ru/replication/invitation-application">

	<import namespace="http://umms.fms.gov.ru/replication/foreign-citizen-core" schemaLocation="../foreign-citizen-core.xsd"/>
	<import namespace="http://umms.fms.gov.ru/replication/core" />
	<import namespace="http://umms.fms.gov.ru/replication/payment" schemaLocation="../payment.xsd"/>

	<complexType name="applicationCase">
		<annotation>
			<documentation>Заявление по приглашению</documentation>
		</annotation>
		<complexContent>
			<extension base="umms:case">
				<sequence>
					<element name="application" type="invitationAppl:invitationApplication" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Дата приема анкеты</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="invitationApplication">
		<annotation>
			<documentation>Заявление по приглашению</documentation>
		</annotation>
		<sequence>
			<element name="invitedForeignCitizen" type="umms:personDocument" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>УД и ДУЛ приглашаемого иностранного гражданина</documentation>
				</annotation>
			</element>
			<element name="invitedForeignCitizenAddress" type="umms:foreignAddress" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Адрес места проживания ИГ за рубежом</documentation>
				</annotation>
			</element>
			<element name="petitionDate" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата ходатайства</documentation>
				</annotation>
			</element>
			<element name="urgency" type="boolean" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Срочное оформление</documentation>
				</annotation>
			</element>
			<element name="entryInfo" type="invitationAppl:entryInfo" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Информация о въезде</documentation>
				</annotation>
			</element>
			<element name="host" type="invitationAppl:host" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Приглашающая сторона</documentation>
				</annotation>
			</element>
			<!-- addressObject (ru.gov.fms.umms.domain.dictionary.address.AddressObject) -->
			<element name="route" type="umms:dictionary" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Маршрут</documentation>
				</annotation>
			</element>
			<element name="paymentDocument" type="payment:paymentDocument" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Квитанции</documentation>
				</annotation>
			</element>
			<element name="relative" minOccurs="0" maxOccurs="unbounded" type="fcc:relative">
				<annotation>
					<documentation>Лица, вписанные в ходатайство</documentation>
				</annotation>
			</element>
			<element name="additionalInfo" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дополнительная информация</documentation>
				</annotation>
			</element>
			<element name="invitedExpectationAddress" type="umms:address" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Адрес предполагаемого пребывания</documentation>
				</annotation>
			</element>
			<element name="employmentInfo" type="fcc:employmentInfo" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Информация о трудовой деятельности</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="entryInfo">
		<annotation>
			<documentation>Информация о въезде</documentation>
		</annotation>
		<sequence>
			<!-- VisaCategory (ru.gov.fms.umms.domain.dictionary.VisaCategory) -->
			<element name="visaCategory" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Категория и вид визы</documentation>
				</annotation>
			</element>
			<!-- EntryGoal (ru.gov.fms.umms.domain.dictionary.EntryGoal) -->
			<element name="entryGoal" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Цель поездки</documentation>
				</annotation>
			</element>
			<!-- VisaMultiplicity (ru.gov.fms.umms.domain.dictionary.VisaMultiplicity) -->
			<element name="multiplicity" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Кратность визы</documentation>
				</annotation>
			</element>
			<element name="entryFrom" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Въезд с</documentation>
				</annotation>
			</element>
			<element name="entryTo" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Въезд по</documentation>
				</annotation>
			</element>
			<!-- Country (ru.gov.fms.umms.domain.dictionary.address.AddressObjectType) -->
			<element name="visaCountry" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Страна получения визы</documentation>
				</annotation>
			</element>
			<!-- VisaIssuedCity (ru.gov.fms.umms.domain.dictionary.visa.VisaIssuedCity) -->
			<element name="visaCity" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Город получения визы</documentation>
				</annotation>
			</element>
			<element name="validTo" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Приглашение действительно до</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="host">
		<annotation>
			<documentation>
				Тип, описывающий приглашающую сторону - физическое лицо или организацию.
				Определяется по присутствию элемента organization.
			</documentation>
		</annotation>
		<sequence>
			<element name="personDataDocument" type="umms:personDocument" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Персональные данные и ДУЛ физ. лица или представителя организации</documentation>
				</annotation>
			</element>
			<element name="personRegistrationAddress" type="umms:address" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Адрес регистрации приглашающего лица</documentation>
				</annotation>
			</element>
			<element name="employmentInfo" type="fcc:employmentInfo" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>
						Информация о трудовой деятельности физ. лица или представителя организации
					</documentation>
				</annotation>
			</element>
			<element name="organization" type="umms:organization" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>
						Если в качестве принимающей стороны выступает организация,
						в реквизите указываются сведения о ней
					</documentation>
				</annotation>
			</element>
			<element name="contactInfo" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Контактная информация приглашающего физ. лица</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<element name="case" type="invitationAppl:applicationCase">
		<annotation>
			<documentation>Заявление по приглашению</documentation>
		</annotation>
	</element>

</schema>
