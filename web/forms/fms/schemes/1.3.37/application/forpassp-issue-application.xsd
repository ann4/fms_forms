<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	07.10.2015 Я. Савельева UMMS-31366:
		- в комплексный тип application добавлены элементы children, previousApplicantData,
		  employmentInfo, HavingInformationAccess, HavingObligations.
		  Добавлен новый комплексный тип HavingRestrictions.
	08.10.2015 Я. Cавельева UMMS-31592:
		- изменен тип элемента previousApplicantData на новый комплексный тип OtherApplicantData
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:fcc="http://umms.fms.gov.ru/replication/foreign-citizen-core"
        xmlns:payment="http://umms.fms.gov.ru/replication/payment"
        xmlns:fpissueapp="http://umms.fms.gov.ru/replication/fpissueapp"
        targetNamespace="http://umms.fms.gov.ru/replication/fpissueapp">

	<import namespace="http://umms.fms.gov.ru/replication/foreign-citizen-core" schemaLocation="../foreign-citizen-core.xsd"/>
	<import namespace="http://umms.fms.gov.ru/replication/core" />
	<import namespace="http://umms.fms.gov.ru/replication/payment" schemaLocation="../payment.xsd"/>

	<complexType name="case">
		<annotation>
			<documentation>Проект дела о выдаче/замене загран. паспорта гражданина России</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:case">
				<sequence>
					<element name="application" type="fpissueapp:application" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Заявление на выдачу / замену загран. паспорта гражданина России
							</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="application">
		<annotation>
			<documentation>Заявление на выдачу / замену загран. паспорта гражданина России</documentation>
		</annotation>
		<sequence>
			<!-- Категория заявителя: "Военнослужащий ВС РФ", "Сотрудник правоохранительных органов" -->
			<!-- fp.applicantCategory (ru.gov.fms.umms.domain.dictionary.foreignpassport.ApplicantCategory) -->
			<element name="applicantCategory" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Категория заявителя</documentation>
				</annotation>
			</element>
			<element name="appDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата приема документов</documentation>
				</annotation>
			</element>
			<element name="applicant" type="umms:personDocument" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Данные заявителя</documentation>
				</annotation>
			</element>
			<!-- applicationPlace (ru.gov.fms.umms.domain.dictionary.ApplicationPlace) -->
			<element name="applicationPlace" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Место обращения</documentation>
				</annotation>
			</element>
			<element name="livingRegistration" type="umms:livingRegistration" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Место жительства (регистрации)</documentation>
				</annotation>
			</element>
			<element name="stayingRegistration" type="umms:stayingRegistration" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Место пребывания (регистрации)</documentation>
				</annotation>
			</element>
			<element name="currentForeignPassport" type="umms:document" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Действующий загранпаспорт</documentation>
				</annotation>
			</element>
			<element name="secondForeignPassport" type="umms:document" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Второй загранпаспорт, выданный в дополнение к имеющемуся</documentation>
				</annotation>
			</element>
			<element name="legalRepresentative" type="umms:personDocument" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Сведения о законном представителе</documentation>
				</annotation>
			</element>
			<element name="legalRepresentativeAddress" type="umms:address" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Адрес законного представителя</documentation>
				</annotation>
			</element>
			<element name="contactInfo" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Контактная информация</documentation>
				</annotation>
			</element>
			<element name="paymentDocument" type="payment:paymentDocument" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Платёжные документы</documentation>
				</annotation>
			</element>
			<element name="children" type="umms:personData" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Сведения о детях, вписанных в паспорт</documentation>
				</annotation>
			</element>
			<element name="previousApplicantData" type="fpissueapp:OtherApplicantData" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Предыдущие установочные данные заявителя</documentation>
				</annotation>
			</element>
			<element name="employmentInfo" type="fcc:employmentInfo" minOccurs="0" maxOccurs="unbounded">
				<annotation>
					<documentation>Информация о трудовой деятельности</documentation>
				</annotation>
			</element>
			<element name="HavingInformationAccess" type="fpissueapp:HavingRestrictions" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Наличие допуска к сведениям особой важности либо секретным сведениям, отнесенным к гос.тайне</documentation>
				</annotation>
			</element>
			<element name="HavingObligations" type="fpissueapp:HavingRestrictions" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Наличие договорных, контрактных обязательств, препятствующих выезду заграницу</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="HavingRestrictions">
		<annotation>
			<documentation>Наличие ограничений для выезда</documentation>
		</annotation>
		<sequence>
			<element name="OrganizationName" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Организация</documentation>
				</annotation>
			</element>
			<element name="Year" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Год</documentation>
				</annotation>
				<simpleType>
					<restriction base="umms:string">
						<pattern value="\d{4}"/>
					</restriction>
				</simpleType>
			</element>
		</sequence>
	</complexType>

	<complexType name="OtherApplicantData">
		<annotation>
			<documentation>Другие установочные данные заявителя</documentation>
		</annotation>
		<sequence>
			<element name="ApplicantData" type="umms:personData" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Установочные данные</documentation>
				</annotation>
			</element>
			<element name="Place" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Место изменения</documentation>
				</annotation>
			</element>
			<element name="Date" type="umms:birthDate" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата изменения</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<element name="case" type="fpissueapp:case">
		<annotation>
			<documentation>Дело о выдаче/замене загран. паспорта гражданина России</documentation>
		</annotation>
	</element>

</schema>
