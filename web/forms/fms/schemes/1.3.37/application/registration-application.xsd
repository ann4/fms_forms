<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	20.06.2013 А. Мазур UMMS-16202:
		- изменено название типа и элемента registrationPlaceInfo на renterInfo
	08.06.2015 Р. Исбаров UMMS-27961:
		- изменены пространства имен и префиксы пространств имен
		- переименованы типы registrationCase в regCase, unregistrationCase в unregCase
		- добавлены типы regCaseDecision, militaryStatement, lsu
		- добавлен элемент receptionDate в тип case
-->

<schema xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:ra="http://umms.fms.gov.ru/application/registration"
        targetNamespace="http://umms.fms.gov.ru/application/registration"
        elementFormDefault="qualified">

	<import namespace="http://umms.fms.gov.ru/replication/core" />

	<complexType name="case" abstract="true">
		<annotation>
			<documentation>
				Абстрактный тип, содержащий в себе базовые атрибуты любого дела или проекта дела по
				регистрационному учету
			</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:case">
				<sequence>
					<element name="receptionDate" type="date" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Дата подачи заявления</documentation>
						</annotation>
					</element>
					<choice minOccurs="1" maxOccurs="1">
						<element name="declarant" type="umms:personDocument">
							<annotation>
								<documentation>Сведения о заявителе</documentation>
							</annotation>
						</element>
						<element name="declarantVagabond" type="umms:personData">
							<annotation>
								<documentation>Сведения о заявителе (БОМЖ)</documentation>
							</annotation>
						</element>
					</choice>
					<element name="legalRepresentative" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Законный представитель</documentation>
						</annotation>
						<complexType>
							<sequence>
								<!-- RelationType (ru.gov.fms.umms.domain.dictionary.RelationType) -->
								<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
									<annotation>
										<documentation>Тип законного представителя (мать/отец/...)</documentation>
									</annotation>
								</element>
								<element name="legalRepresentative" type="umms:personDocument" minOccurs="1" maxOccurs="1">
									<annotation>
										<documentation>Сведения о законном представителе</documentation>
									</annotation>
								</element>
							</sequence>
						</complexType>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="regCase" abstract="true">
		<annotation>
			<documentation>
				Абстрактный тип, содержащий в себе базовые атрибуты любого дела или проекта дела по
				постановке на регистрационный учет
			</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="ra:case">
				<sequence>
					<element name="previousRegistrationAddress" type="umms:livingRegistration" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Адрес места жительства (откуда прибыл)</documentation>
						</annotation>
					</element>
					<element name="renterInfo" type="ra:renterInfo" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Сведения о предоставителе жилого помещения</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="unregCase" abstract="true">
		<annotation>
			<documentation>
				Абстрактный тип, содержащий в себе базовые атрибуты любого дела или проекта дела по
				снятию с регистрационного учета
			</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="ra:case">
				<sequence>
					<element name="unregistrationDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата окончания регистрации</documentation>
						</annotation>
					</element>
					<element name="newAddress" type="ra:newAddress" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Адрес нового места жительства (Выбыл в)</documentation>
						</annotation>
					</element>
					<!-- reg.deregistrationCause (ru.gov.fms.umms.domain.dictionary.registration.DeregistrationCause) -->
					<element name="deregistrationCause" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Причина снятия с регистрационного учета</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<!-- TODO: Перевести на dictionary. -->
	<simpleType name="renterType">
		<annotation>
			<documentation>Тип предоставителя жилого помещения (физическое лицо, организация)</documentation>
		</annotation>
		<restriction base="umms:string">
			<enumeration value="PersonRenter">
				<annotation>
					<documentation>Физическое лицо</documentation>
				</annotation>
			</enumeration>
			<enumeration value="OrganizationRenter">
				<annotation>
					<documentation>Организация</documentation>
				</annotation>
			</enumeration>
			<enumeration value="StateOrganizationRenter">
				<annotation>
					<documentation>Государственное или муниципальное предприятие</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>

	<complexType name="renterInfo">
		<annotation>
			<documentation>Сведения о предоставителе жилого помещения</documentation>
		</annotation>
		<sequence>
			<element name="renterType" minOccurs="1" maxOccurs="1" type="ra:renterType">
				<annotation>
					<documentation>Тип предоставителя жилого помещения</documentation>
				</annotation>
			</element>
			<choice minOccurs="1" maxOccurs="1">
				<element name="renterData" type="umms:personDocument">
					<annotation>
						<documentation>Сведения о предоставителе жилого помещения</documentation>
					</annotation>
				</element>
				<element name="renterDataWithoutDoc" type="umms:personData">
					<annotation>
						<documentation>Сведения о предоставителе жилого помещения без ДУЛ</documentation>
					</annotation>
				</element>
			</choice>
			<element name="organization" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Название организации, предоставляющей жилое помещение</documentation>
				</annotation>
			</element>
			<element name="renterDecision" type="ra:renterDecision" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Основание для предоставления жилого помещения</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="renterDecision">
		<annotation>
			<documentation>Основание для предоставления жилого помещения</documentation>
		</annotation>
		<sequence>
			<!-- DwellingProvidingBasis (ru.gov.fms.umms.domain.dictionary.migration.DwellingProvidingBasis) -->
			<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Вид документа-основания для предоставления ЖП</documentation>
				</annotation>
			</element>
			<element name="number" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Номер документа</documentation>
				</annotation>
			</element>
			<element name="date" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата документа</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="newAddress">
		<annotation>
			<documentation>Адрес нового места жительства</documentation>
		</annotation>
		<sequence>
			<element name="dateFrom" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Регистрация с</documentation>
				</annotation>
			</element>
			<element name="dateTo" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Регистрация по</documentation>
				</annotation>
			</element>
			<element name="address" type="umms:address" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Адрес</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="regCaseDecision">
		<annotation>
			<documentation>Принятое решение</documentation>
		</annotation>
		<sequence>
			<element name="decisionBasis" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Основание для решения
						(код справочника зависит от типа учета)
					</documentation>
				</annotation>
			</element>
			<element name="decision" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Принятое решение</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="militaryStatement">
		<annotation>
			<documentation>Данные о постановке на воинский учет или снятии с воинского учета</documentation>
		</annotation>
		<sequence>
			<element name="militaryRegistrationDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата вручения направления на постановку/снятие с воинского учета</documentation>
				</annotation>
			</element>
			<element name="militaryCommissariat" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Военный комиссариат</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="lsu">
		<annotation>
			<documentation>Сведения из листка статистического учета</documentation>
		</annotation>
		<sequence>
			<element name="compositionReason" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Причина составления листка статистического учета
						(код справочника зависит от типа учета)
					</documentation>
				</annotation>
			</element>
			<!-- Citizenship (ru.gov.fms.umms.domain.dictionary.Citizenship) -->
			<element name="secondCitizenship" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Государство второго гражданства (код альфа-3 ОКСМ)</documentation>
				</annotation>
			</element>
			<!-- Citizenship (ru.gov.fms.umms.domain.dictionary.Citizenship) -->
			<element name="previousCitizenship" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Государство предыдущего гражданства (код альфа-3 ОКСМ)</documentation>
				</annotation>
			</element>
			<element name="previousLivingAddressDateFrom" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата начала проживания по последнему месту жительства</documentation>
				</annotation>
			</element>
			<!-- reg.relocationReason (ru.gov.fms.umms.domain.dictionary.registration.RelocationReason) -->
			<element name="relocationReason" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Основное обстоятельство, вызвавшее необходимость переселения</documentation>
				</annotation>
			</element>
			<!-- reg.activity (ru.gov.fms.umms.domain.dictionary.registration.Activity) -->
			<element name="activity" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Занятие по последнему месту жительства</documentation>
				</annotation>
			</element>
			<!-- reg.activityStatus (ru.gov.fms.umms.domain.dictionary.registration.ActivityStatus) -->
			<element name="activityStatus" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Статус в занятости</documentation>
				</annotation>
			</element>
			<!-- reg.socialProvisionType (ru.gov.fms.umms.domain.dictionary.registration.SocialProvisionType) -->
			<element name="socialProvisionType" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Вид социального обеспечения по последнему месту жительства</documentation>
				</annotation>
			</element>
			<!-- reg.educationLevel (ru.gov.fms.umms.domain.dictionary.registration.EducationLevel) -->
			<element name="educationLevel" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Образование</documentation>
				</annotation>
			</element>
			<!-- reg.maritalStatus (ru.gov.fms.umms.domain.dictionary.MaritalStatus) -->
			<element name="maritalStatus" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Состояние в браке</documentation>
				</annotation>
			</element>
			<!-- reg.familyRelocationType (ru.gov.fms.umms.domain.dictionary.registration.FamilyRelocationType) -->
			<element name="familyRelocationType" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Если до переселения проживал с семьей, то прибыл</documentation>
				</annotation>
			</element>
			<!-- YesNoType (ru.gov.fms.umms.domain.dictionary.YesNoType) -->
			<element name="familyRelocationState" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Признак проживания части членов семьи по новому месту жительства</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

</schema>
