<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	20.05.2015 Р. Исбаров UMMS-27767:
		- изменено описание элемента stateProgramMember типа case
	08.12.2015 Р. Исбаров UMMS-31900:
		- изменен код справочника для элемента stateProgramMember в комплексном типе case
	16.02.2015 Р. Исбаров UMMS-35389:
		- добавлен элемент prolongationReason в тип case
		- удален элемент firstArrival из типа case
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:fcc="http://umms.fms.gov.ru/replication/foreign-citizen-core"
        xmlns:mig="http://umms.fms.gov.ru/replication/migration"
        xmlns:stay="http://umms.fms.gov.ru/replication/migration/staying"
        targetNamespace="http://umms.fms.gov.ru/replication/migration/staying">

	<import namespace="http://umms.fms.gov.ru/replication/core" />
	<import namespace="http://umms.fms.gov.ru/replication/foreign-citizen-core" schemaLocation="foreign-citizen-core.xsd"/>
	<import namespace="http://umms.fms.gov.ru/replication/migration" schemaLocation="migration.xsd"/>

	<complexType name="case">
		<annotation>
			<documentation>Дело о постановке на миграционный учет по месту пребывания</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="mig:case">
				<sequence>
					<element name="notificationNumber" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								№ уведомления в формате отметка/код_уфмс/год/порядковый_номер, где:
									* отметка - код из справочника "Код, определяющий орган (организацию), проставивший отметку о прибытии"
									* код_уфмс - код подразделения УФМС
									* год - 2 цифры года
									* порядковый_номер - 6 цифр номера регистрации
							</documentation>
						</annotation>
						<simpleType>
							<restriction base="umms:string">
								<pattern value="\d{2}\/[\d\-]+\/\d{2}\/\d{6}"/>
							</restriction>
						</simpleType>
					</element>
					<!-- NoticeFrom (ru.gov.fms.umms.domain.dictionary.migration.NoticeFrom) -->
					<element name="noticeFrom" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Значение из справочника "Код, определяющий орган (организацию),
								проставивший отметку о прибытии"
							</documentation>
						</annotation>
					</element>
					<!-- ProlongationReason (ru.gov.fms.umms.domain.dictionary.migration.ProlongationReason) -->
					<element name="prolongationReason" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Причина продления срока временного пребывания ИГ (ЛБГ) в РФ (если постановка
								первоначальная (не продление), то указывается значение "Нет" из справочника)
							</documentation>
						</annotation>
					</element>
					<!-- mig.specialStatus (ru.gov.fms.umms.domain.dictionary.SpecialStatus) -->
					<element name="stateProgramMember" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Категория заявителя:
								- Нет
								- Высококвалифицированный специалист
								- Участник Государственной программы
								- Член семьи высококвалифицированного специалиста
								- Член семьи участника Государственной программы
							</documentation>
						</annotation>
					</element>
					<element name="host" type="stay:hostContactInfo">
						<annotation>
							<documentation>Принимающая сторона</documentation>
						</annotation>
					</element>
					<!-- VisitPurpose (ru.gov.fms.umms.domain.dictionary.migration.VisitPurpose) -->
					<element name="entrancePurpose" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Цель въезда - значение из справочника "Цель въезда"</documentation>
						</annotation>
					</element>
					<element name="migrationCard" type="fcc:migrationCard" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Миграционная карта</documentation>
						</annotation>
					</element>
					<element name="profession" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Профессия ИГ</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="hostContactInfo">
		<annotation>
			<documentation>
				Тип, описывающий принимающую сторону - физическое лицо c адресом или организацию
				(определяется по присутствию элемента organization)
			</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="mig:host">
				<sequence>
					<element name="personDataDocument" type="umms:personDocument">
						<annotation>
							<documentation>Персональные данные и ДУЛ физ. лица</documentation>
						</annotation>
					</element>
					<element name="contactInfo" type="umms:contactInfo" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Контактная информация ответственного лица: адрес и телефон</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<element name="case" type="stay:case">
		<annotation>
			<documentation>Дело о постановке на миграционный учет по месту пребывания</documentation>
		</annotation>
	</element>

</schema>
