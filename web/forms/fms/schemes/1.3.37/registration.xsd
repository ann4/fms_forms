<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	20.06.2013 А. Мазур UMMS-16202:
		- изменено название типа и элемента registrationPlaceInfo на renterInfo
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:reg="http://umms.fms.gov.ru/replication/registration"
        targetNamespace="http://umms.fms.gov.ru/replication/registration">

	<import namespace="http://umms.fms.gov.ru/replication/core" />

	<complexType name="case" abstract="true">
		<annotation>
			<documentation>
				Абстрактный тип, содержащий в себе базовые атрибуты любого дела или
				проекта дела по регистрационному учету
			</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:case">
				<sequence>
					<choice minOccurs="1" maxOccurs="1">
						<element name="declarant" type="umms:personDocument">
							<annotation>
								<documentation>Сведения о заявителе</documentation>
							</annotation>
						</element>
						<element name="declarantVagabond" type="umms:personData">
							<annotation>
								<documentation>Сведения о заявителе (БОМЖ)</documentation>
							</annotation>
						</element>
					</choice>
					<element name="legalRepresentative" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Законный представитель</documentation>
						</annotation>
						<complexType>
							<sequence>
								<!-- RelationType (ru.gov.fms.umms.domain.dictionary.RelationType) -->
								<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
									<annotation>
										<documentation>Тип законного представителя (мать/отец/...)</documentation>
									</annotation>
								</element>
								<element name="legalRepresentative" type="umms:personDocument" minOccurs="1" maxOccurs="1">
									<annotation>
										<documentation>Сведения о законном представителе</documentation>
									</annotation>
								</element>
							</sequence>
						</complexType>
					</element>
					<element name="militaryRegistration" type="reg:military" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Сведения о воинском учете</documentation>
						</annotation>
					</element>
					<element name="decision" type="reg:decision" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Данные о принятом решении</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="registrationCase" abstract="true">
		<annotation>
			<documentation>
				Абстрактный тип, содержащий в себе базовые атрибуты любого дела или
				проекта дела по постановке на регистрационный учет
			</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="reg:case">
				<sequence>
					<element name="previousRegistrationAddress" type="umms:livingRegistration" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Адрес места жительства (откуда прибыл)</documentation>
						</annotation>
					</element>
					<element name="renterInfo" type="reg:renterInfo" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Сведения о предоставителе жилого помещения</documentation>
						</annotation>
					</element>
					<!-- Используются для регистрации в органах местной администрации. -->
					<element name="localAdministrationOrgan" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Наименование органа местной администрации</documentation>
						</annotation>
					</element>
					<element name="alpCreate" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Составил АЛП (ФИО)</documentation>
						</annotation>
					</element>
					<element name="alpRegistration" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Зарегистрировал АЛП (ФИО)</documentation>
						</annotation>
					</element>
					<element name="alpCreateDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата составления АЛП</documentation>
						</annotation>
					</element>
					<element name="alpRegistrationDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата регистрации АЛП</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="unregistrationCase">
		<annotation>
			<documentation>
				Абстрактный тип, содержащий в себе базовые атрибуты любого дела или
				проекта дела по снятию с регистрационного учета
			</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="reg:case">
				<sequence>
					<element name="unregistrationDate" type="date" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Дата окончания регистрации</documentation>
						</annotation>
					</element>
					<element name="newAddress" type="reg:newAddress" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Адрес нового места жительства (Выбыл в)</documentation>
						</annotation>
					</element>
					<element name="militaryUnregistration" type="reg:military" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Сведения о снятии с воинского учета</documentation>
						</annotation>
					</element>
					<!-- Используются для снятия с регистрации в органах местной администрации. -->
					<element name="aluCreate" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Составил АЛУ (ФИО)</documentation>
						</annotation>
					</element>
					<element name="aluCreateDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата составления АЛУ</documentation>
						</annotation>
					</element>
					<element name="aluUnregistrationPerformer" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Снятие с рег. учета: ФИО</documentation>
						</annotation>
					</element>
					<element name="unregistrationPerformDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата оформления снятия с учета</documentation>
						</annotation>
					</element>
					<!-- reg.deregistrationCause (ru.gov.fms.umms.domain.dictionary.registration.DeregistrationCause) -->
					<element name="deregistrationCause" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Причина снятия с регистрационного учета</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="military">
		<annotation>
			<documentation>Сведения о воинском учете</documentation>
		</annotation>
		<sequence>
			<element name="directionDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата вручения направления</documentation>
				</annotation>
			</element>
			<element name="directioNumber" type="umms:value" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Номер направления</documentation>
				</annotation>
			</element>
			<element name="militaryComissariatId" type="umms:ummsId" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Идентификатор военного комиссариата в ППО "Территория"
						для постановки или снятия с воинского учета
					</documentation>
				</annotation>
			</element>
			<element name="militaryRegistrationEmployee" type="umms:user" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>
						Сведения о сотруднике, поставившем / снявшем с воинского учета
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<!-- TODO: Перевести на dictionary. -->
	<simpleType name="renterType">
		<annotation>
			<documentation>
				Тип предоставителя жилого помещения (физическое лицо, организация, государство)
			</documentation>
		</annotation>
		<restriction base="umms:string">
			<enumeration value="PersonRenter">
				<annotation>
					<documentation>Физическое лицо</documentation>
				</annotation>
			</enumeration>
			<enumeration value="OrganizationRenter">
				<annotation>
					<documentation>Организация</documentation>
				</annotation>
			</enumeration>
			<enumeration value="StateOrganizationRenter">
				<annotation>
					<documentation>Государственное или муниципальное предприятие</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>

	<!-- TODO: Перевести на dictionary. -->
	<simpleType name="unregistrationApplicantType">
		<annotation>
			<documentation>
				Снятие с рег. учета: заявителя, несовершеннолетнего ребенка заявителя, иного гражданина
			</documentation>
		</annotation>
		<restriction base="umms:string">
			<enumeration value="byApplicant">
				<annotation>
					<documentation>Заявителя</documentation>
				</annotation>
			</enumeration>
			<enumeration value="byChildren">
				<annotation>
					<documentation>Несовершеннолетнего ребенка заявителя</documentation>
				</annotation>
			</enumeration>
			<enumeration value="byOtherPerson">
				<annotation>
					<documentation>Иного гражданина</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>

	<complexType name="decision">
		<annotation>
			<documentation>Данные принятого решения о постановке / снятии с учета</documentation>
		</annotation>
		<sequence>
			<element name="applicationDate" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата подачи заявления</documentation>
				</annotation>
			</element>
			<!-- reg.decisionBasis (ru.gov.fms.umms.domain.dictionary.registration.RegistrationDecisionType) -->
			<element name="decisionCause" type="umms:dictionary" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Основание для решения</documentation>
				</annotation>
			</element>
			<element name="decisionDate" type="date" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Дата принятия решения (дата подписи заявления начальником ТП)</documentation>
				</annotation>
			</element>
			<element name="decisionOfficialEmployee" type="umms:user" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Должностное лицо, принявшее решение</documentation>
				</annotation>
			</element>
			<element name="applicationOfficialEmployee" type="umms:user" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Должностное лицо, принявшее заявление</documentation>
				</annotation>
			</element>
			<element name="certificateNumber" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Номер свидетельства</documentation>
				</annotation>
			</element>
			<element name="certificateDuplicate" type="boolean" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Выдан дубликат свидетельства о регистрации</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="renterInfo">
		<annotation>
			<documentation>Сведения о предоставителе жилого помещения</documentation>
		</annotation>
		<sequence>
			<element name="renterType" type="reg:renterType" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Тип предоставителя жилого помещения</documentation>
				</annotation>
			</element>
			<choice minOccurs="1" maxOccurs="1">
				<element name="renterData" type="umms:personDocument">
					<annotation>
						<documentation>Сведения о предоставителе жилого помещения</documentation>
					</annotation>
				</element>
				<element name="renterDataWithoutDoc" type="umms:personData">
					<annotation>
						<documentation>Сведения о предоставителе жилого помещения без ДУЛ</documentation>
					</annotation>
				</element>
			</choice>
			<element name="organization" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Название организации, предоставляющей жилое помещение</documentation>
				</annotation>
			</element>
			<element name="renterDecision" type="reg:renterDecision" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Основание для предоставления жилого помещения</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="renterDecision">
		<annotation>
			<documentation>Основание для предоставления жилого помещения</documentation>
		</annotation>
		<sequence>
			<!-- DwellingProvidingBasis (ru.gov.fms.umms.domain.dictionary.migration.DwellingProvidingBasis) -->
			<element name="type" type="umms:dictionary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Вид документа-основания для предоставления ЖП</documentation>
				</annotation>
			</element>
			<element name="number" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Номер документа</documentation>
				</annotation>
			</element>
			<element name="date" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Дата документа</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="newAddress">
		<annotation>
			<documentation>Адрес нового места жительства</documentation>
		</annotation>
		<sequence>
			<element name="dateFrom" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Регистрация с</documentation>
				</annotation>
			</element>
			<element name="dateTo" type="date" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Регистрация по</documentation>
				</annotation>
			</element>
			<element name="address" type="umms:address" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Адрес</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

</schema>
