<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	14.10.2013 Р. Исбаров UMMS-18370:
		- добавлен дополнительный элемент requestId в тип hotelResponse
	23.09.2014 Я. Савельева UMMS-24568:
		- добавлен дополнительный элемент externalCaseId в типы correctResponse и errorResponse, изменено описание
		  элемента externalSystemId
	05.04.2016 Р. Исбаров UMMS-36921:
		- изменен тип элемента notificationNumber с umms:string на mig:notificationNumber
	05.05.2016 Р. Исбаров UMMS-37706:
		- изменен тип элемента fileData в типе xmlFile с xsd:string на xsd:hexBinary
	06.05.2016 Р. Исбаров UMMS-37554:
		- изменен тип элемента notificationNumber с mig:notificationNumber на umms:migrationNotificationNumber
		- добавлен элемент Signature в тип hotelResponse для передачи данных электронной подписи в ответе
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:hotelResponse="http://umms.fms.gov.ru/hotel/hotel-response"
        targetNamespace="http://umms.fms.gov.ru/hotel/hotel-response">

	<import namespace="http://umms.fms.gov.ru/replication/core"/>
	<import namespace="http://www.w3.org/2000/09/xmldsig#"
	        schemaLocation="http://www.w3.org/TR/2002/REC-xmldsig-core-20020212/xmldsig-core-schema.xsd"/>

	<complexType name="hotelResponse">
		<annotation>
			<documentation>
				Тип, описывающий ответ во внешнюю систему о загрузке данных из гостиничной системы в ППО "Территория"
			</documentation>
		</annotation>
		<sequence>
			<element ref="ds:Signature" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Электронная подпись документа</documentation>
				</annotation>
			</element>
			<element name="requestId" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Уникальный идентификатор запроса, по которому сформирован ответ</documentation>
				</annotation>
			</element>
			<element name="entityType" type="hotelResponse:entityType" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Тип загружаемых данных</documentation>
				</annotation>
			</element>
			<choice minOccurs="1" maxOccurs="1">
				<element name="success" type="hotelResponse:correctResponse">
					<annotation>
						<documentation>Успешная загрузка</documentation>
					</annotation>
				</element>
				<element name="error" type="hotelResponse:errorResponse">
					<annotation>
						<documentation>Ошибка загрузки дела</documentation>
					</annotation>
				</element>
			</choice>
		</sequence>
		<attribute name="schemaVersion" type="decimal" use="required" fixed="1.0"/>
	</complexType>

	<simpleType name="entityType">
		<annotation>
			<documentation>Тип загружаемых данных</documentation>
		</annotation>
		<restriction base="umms:string">
			<enumeration value="Hotel">
				<annotation>
					<documentation>Сведения о гостинице</documentation>
				</annotation>
			</enumeration>
			<enumeration value="MigCase">
				<annotation>
					<documentation>Сведения о постановке на миг. учет в гостинице</documentation>
				</annotation>
			</enumeration>
			<enumeration value="MigCaseEdit">
				<annotation>
					<documentation>Редактирование сведений о постановке на миг. учет в гостинице</documentation>
				</annotation>
			</enumeration>
			<enumeration value="UnregMigCase">
				<annotation>
					<documentation>Сведения о снятии с миг. учета в гостинице</documentation>
				</annotation>
			</enumeration>
			<enumeration value="Form5">
				<annotation>
					<documentation>Форма № 5</documentation>
				</annotation>
			</enumeration>
			<enumeration value="Unknown">
				<annotation>
					<documentation>Неизвестно, используется при ошибке чтения XML-файла</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>

	<complexType name="correctResponse">
		<annotation>
			<documentation>
				Тип, описывающий ответ во внешнюю систему о корректной загрузке данных из
				гостиничной системы в ППО "Территория"
			</documentation>
		</annotation>
		<sequence>
			<element name="notificationNumber" type="umms:migrationNotificationNumber" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Номер уведомления о постановке на учёт по месту пребывания</documentation>
				</annotation>
			</element>
			<element name="externalSystemId" type="umms:value" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Уникальный идентификатор внешней системы</documentation>
				</annotation>
			</element>
			<element name="externalCaseId" type="umms:value" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Уникальный идентификатор дела во внешней системе</documentation>
				</annotation>
			</element>
			<element name="ummsId" type="umms:value" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Уникальный идентификатор дела в системе ППО "Территория"</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="errorResponse">
		<annotation>
			<documentation>
				Тип, описывающий ответ во внешнюю систему об ошибочной загрузке данных из
				гостиничной системы в ППО "Территория"
			</documentation>
		</annotation>
		<sequence>
			<choice minOccurs="1" maxOccurs="1">
				<sequence>
					<element name="externalSystemId" type="umms:value">
						<annotation>
							<documentation>Уникальный идентификатор внешней системы</documentation>
						</annotation>
					</element>
					<element name="externalCaseId" type="umms:value">
						<annotation>
							<documentation>Уникальный идентификатор дела во внешней системе</documentation>
						</annotation>
					</element>
				</sequence>
				<element name="xml" type="hotelResponse:xmlFile">
					<annotation>
						<documentation>Содержимое необработанного XML-файла</documentation>
					</annotation>
				</element>
			</choice>
			<element name="errorMsg" type="umms:value" minOccurs="1" maxOccurs="unbounded">
				<annotation>
					<documentation>Ошибка загрузки дела</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="xmlFile">
		<annotation>
			<documentation>Тип, описывающий непрочитанный XML-файл</documentation>
		</annotation>
		<sequence>
			<element name="fileName" type="umms:value" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Имя файла во внешней системе</documentation>
				</annotation>
			</element>
			<element name="fileData" type="xsd:hexBinary" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Содержимое необработанного XML-файла</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<element name="response" type="hotelResponse:hotelResponse">
		<annotation>
			<documentation>Ответ внешней системе</documentation>
		</annotation>
	</element>

</schema>
