<?xml version="1.0" encoding="utf-8"?>

<!--
История изменений:
	18.09.2014 Р. Исбаров UMMS-24501:
		- добавлен обязательный элемент oktmo в комплексный тип departmentInformation
		- убрана обязательность элемента okato в комплексном типе departmentInformation
	04.02.2015 Р. Исбаров UMMS-26104:
		- добавлен обязательный элемент paymentNameGroup в комплексный тип paymentDocument
		- убрана обязательность элементов paymentName и kbk в комплексном типе paymentDocument
	21.06.2016 Р. Исбаров UMMS-38527:
		- тип элемента image изменен с umms:hexBinary на umms:binary
-->

<schema elementFormDefault="qualified"
        xmlns="http://www.w3.org/2001/XMLSchema"
        xmlns:umms="http://umms.fms.gov.ru/replication/core"
        xmlns:payment="http://umms.fms.gov.ru/replication/payment"
        targetNamespace="http://umms.fms.gov.ru/replication/payment">

	<import namespace="http://umms.fms.gov.ru/replication/core"/>

	<complexType name="paymentDocument">
		<annotation>
			<documentation>Платёжный документ</documentation>
		</annotation>
		<complexContent mixed="false">
			<extension base="umms:entityBase">
				<sequence>
					<element name="docNo" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Номер платежного документа</documentation>
						</annotation>
					</element>
					<!-- paymentdocument.PaymentDocumentType (ru.gov.fms.umms.domain.dictionary.paymentdocument.PaymentDocumentType) -->
					<element name="orderType" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Тип платежного документа (Платежная квитанция; Платежное поручение)
							</documentation>
						</annotation>
					</element>
					<!-- paymentdocument.PaymentChargeType (ru.gov.fms.umms.domain.dictionary.paymentdocument.PaymentChargeType) -->
					<element name="chargeType" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>
								Типы производимых начислений доходов в бюджет (Штраф или налог/госпошлина)
							</documentation>
						</annotation>
					</element>
					<element name="okato" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Код ОКАТО, специфичный для данного платежа</documentation>
						</annotation>
					</element>
					<element name="departInformation" type="payment:departmentInformation" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Информация о подразделении</documentation>
						</annotation>
					</element>
					<!-- PaymentNameGroup (ru.gov.fms.umms.domain.dictionary.PaymentNameGroup) -->
					<element name="paymentNameGroup" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Группа наименований платежей</documentation>
						</annotation>
					</element>
					<!-- PaymentName (ru.gov.fms.umms.domain.dictionary.PaymentName) -->
					<element name="paymentName" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Наименование платежа</documentation>
						</annotation>
					</element>
					<element name="sum" type="umms:string" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Сумма платежа</documentation>
						</annotation>
					</element>
					<element name="creationDate" type="date" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Дата составления (оформления) документа</documentation>
						</annotation>
					</element>
					<element name="paymentDate" type="date" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Дата платежа</documentation>
						</annotation>
					</element>
					<!-- ChargeDeliveryType (ru.gov.fms.umms.domain.dictionary.incomeadmin.ChargeDeliveryType) -->
					<element name="chargeDeliveryType" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>
								Способ расчетов между банком (филиалом) плательщика и банком (филиалом)
								получателя платежа
							</documentation>
						</annotation>
					</element>
					<!-- BCCode (ru.gov.fms.umms.domain.dictionary.BCCode) -->
					<element name="kbk" type="umms:dictionary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Код бюджетной классификации налогов и сборов</documentation>
						</annotation>
					</element>
					<!-- paymentdocument.PaymentDocumentStatus (ru.gov.fms.umms.domain.dictionary.paymentdocument.PaymentDocumentStatus) -->
					<element name="status" type="umms:dictionary" minOccurs="1" maxOccurs="1">
						<annotation>
							<documentation>Статус платежного документа</documentation>
						</annotation>
					</element>
					<element name="imageType" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Тип изображения</documentation>
						</annotation>
					</element>
					<element name="image" type="umms:binary" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Изображение платежного документа</documentation>
						</annotation>
					</element>
					<element name="contractorBank" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Наименование банка плательщика</documentation>
						</annotation>
					</element>
					<element name="contractorInn" type="umms:inn" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>ИНН банка плательщика</documentation>
						</annotation>
					</element>
					<element name="contractorAccountNumber" type="umms:string" minOccurs="0" maxOccurs="1">
						<annotation>
							<documentation>Номер счёта банка плательщика</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>

	<complexType name="departmentInformation">
		<annotation>
			<documentation>Информация о подразделении</documentation>
		</annotation>
		<sequence>
			<element name="inn" type="umms:inn" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>ИНН</documentation>
				</annotation>
			</element>
			<element name="kpp" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>КПП</documentation>
				</annotation>
			</element>
			<element name="okato" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>ОКАТО</documentation>
				</annotation>
			</element>
			<element name="oktmo" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>ОКТМО</documentation>
				</annotation>
			</element>
			<element name="ogrn" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>ОГРН</documentation>
				</annotation>
			</element>
			<element name="account" type="payment:bankAccount" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Информация о банковском счёте</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

	<complexType name="bankAccount">
		<annotation>
			<documentation>Банковский счёт</documentation>
		</annotation>
		<sequence>
			<element name="bankName" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Название банка</documentation>
				</annotation>
			</element>
			<element name="accountNumber" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Номер счёта</documentation>
				</annotation>
			</element>
			<element name="bik" type="umms:string" minOccurs="1" maxOccurs="1">
				<annotation>
					<documentation>Банковский идентификационный код (БИК)</documentation>
				</annotation>
			</element>
			<element name="corrAccount" type="umms:string" minOccurs="0" maxOccurs="1">
				<annotation>
					<documentation>Корреспондентский счёт</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>

</schema>
