define([],function ()
{
	return function ()
	{
		var res =
		{
			ReportingPeriodMonthFrom: null,
			ReportingPeriodMonthTill: null,
			ReportingPeriodYear: null,
			CountByArea: {},
			CountAll: null,
			IDCurrentRegion: null,
			Form: 'FMS-A'
		};
		return res;
	};
});