define([],
function (CreateController) {
	var file_format =
	{
	    FilePrefix: 'Report_by_geography'
		, FileExtension: 'xls'
		, Encoding: 65001
		, Description: 'ФМС. Отчет о географии прибытия граждан РФ.'
	};
	return file_format;
});
