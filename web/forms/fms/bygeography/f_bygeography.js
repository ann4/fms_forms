define(['forms/fms/bygeography/c_bygeography', 'forms/fms/bygeography/ff_bygeography'],
function (CreateController, FileFormat) {
	var form_spec =
	{
		CreateController: CreateController
		, key: 'bygeography'
		, Title: 'Отчет о географии прибытия граждан РФ'
		, FileFormat: FileFormat
	};
	return form_spec;
});
