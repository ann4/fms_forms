define([
	  'forms/fms/base/c_reporting'
	, 'forms/base/log'
	, 'forms/fms/bygeography/m_bygeography'
	, 'forms/fms/bygeography/x_bygeography'
	, 'forms/fms/bygeography/ff_bygeography'
	, 'tpl!forms/fms/bygeography/e_bygeography.html'
	, 'tpl!forms/fms/bygeography/v_bygeography.xml'
    , 'forms/fms/base/h_Select2'
	, 'forms/base/h_times'
    , 'forms/fms/profile/h_abonent'
    , 'forms/base/h_dictionary'
    , 'forms/fms/guides/dict_officialorgan_fms.profile.csv'
],
function (binded_controller, GetLogger, model_report, codec, form_spec, layout_tpl, print_tpl, h_select2, h_times, h_abonent, h_dictionary, dict_officialorgan_fms_profile)
{
	return function ()
	{
	    var log = GetLogger('c_bygeography');

	    var range = function (start, end) {
	        var res = [];
	        for (var i = start; i <= end; i++) {
	            res.push({ id: i.toString(), text: i.toString() });
	        }
	        return res;
	    };

	    var select2Options = function (h_values) {
	        return function (m) {
	            return {
	                placeholder: '',
	                allowClear: true,
	                query: function (q) {
	                    q.callback({ results: h_select2.FindForSelect(h_values, q.term) });
	                }
	            };
	        }
	    };

	    var lastDayOfMonth = function (y, m) {
	        return new Date(y, m, 0).getDate();
	    }

	    var listData = null;
	    var regions = null;

	    var controller = binded_controller(layout_tpl, form_spec, {
		    extension_key: 'bygeography'
            , field_spec:
			{
			    ReportingPeriodYear: select2Options(range((new Date).getFullYear() - 5, (new Date).getFullYear()))
                , ReportingPeriodMonthFrom: select2Options([
					{ id: 1, text: 'января' },
					{ id: 2, text: 'февраля' },
					{ id: 3, text: 'марта' },
					{ id: 4, text: 'апреля' },
                    { id: 5, text: 'мая' },
					{ id: 6, text: 'июня' },
					{ id: 7, text: 'июля' },
					{ id: 8, text: 'августа' },
                    { id: 9, text: 'сентября' },
					{ id: 10, text: 'октября' },
					{ id: 11, text: 'ноября' },
					{ id: 12, text: 'декабря' }
                ])
				, ReportingPeriodMonthTill: select2Options([
					{ id: 1, text: 'январь' },
					{ id: 2, text: 'февраль' },
					{ id: 3, text: 'март' },
					{ id: 4, text: 'апрель' },
                    { id: 5, text: 'май' },
					{ id: 6, text: 'июнь' },
					{ id: 7, text: 'июль' },
					{ id: 8, text: 'август' },
                    { id: 9, text: 'сентябрь' },
					{ id: 10, text: 'октябрь' },
					{ id: 11, text: 'ноябрь' },
					{ id: 12, text: 'декабрь' }
				])
			}
		});

		controller.SafeCreateNewContent = function () {
			if (!this.model)
				this.model = model_report();
		};

		controller.UseProfile = function (profile) {
		    log.Debug('UseProfile {');
		    this.SafeCreateNewContent();
		    if (profile && profile.Extensions && profile.Extensions.fms) {
		        var pfms = profile.Extensions.fms;
		        if (pfms.Abonent) {
		            var pAbonent = pfms.Abonent;
		            pAbonent = h_abonent.SafeUpgradeAbonent(pAbonent);
		            var subdivision = h_abonent.SafeGet_dict_field(pAbonent, 'subdivision', null);
		            var itemSubdivision = h_dictionary.FindFirstByField(dict_officialorgan_fms_profile, 'ID', subdivision.id)
		            if (itemSubdivision) {
		                this.model.IDCurrentRegion = itemSubdivision.row.CODE.substring(0, 2);
		            }
		        }
		    }
		    log.Debug('UseProfile }');
		};

		var base_Render = controller.Render;
		controller.Render = function (id_form_div) {
		    base_Render.call(this, id_form_div);
		    $('#cpw_form_ReportingPeriodMonthFrom').focus();
		};

		controller.SetFormContent = function (content) {
			log.Debug('SetFormContent {');
			this.SafeCreateNewContent();
			this.model = content;
			if (this.model && this.model.ReportingPeriodFrom) {
			    this.model.ReportingPeriodFromDate = controller.parseDate(this.model.ReportingPeriodFrom);
			}
			if (this.model && this.model.ReportingPeriodTill) {
			    this.model.ReportingPeriodTillDate = controller.parseDate(this.model.ReportingPeriodTill);
			}
			log.Debug('SetFormContent }');
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
		    this.model = base_GetFormContent.call(this);
		    if (this.model && this.model.ReportingPeriodMonthFrom && this.model.ReportingPeriodMonthTill && this.model.ReportingPeriodYear) {
		        this.model.ReportingPeriodFrom = "01." + (this.model.ReportingPeriodMonthFrom.id > 9 ? '' : '0') + this.model.ReportingPeriodMonthFrom.id + "." + this.model.ReportingPeriodYear.id;
		        var dayTill = lastDayOfMonth(this.model.ReportingPeriodYear.id, this.model.ReportingPeriodMonthTill.id);
		        this.model.ReportingPeriodTill = dayTill + "." + (this.model.ReportingPeriodMonthTill.id > 9 ? '' : '0') + this.model.ReportingPeriodMonthTill.id + "." + this.model.ReportingPeriodYear.id;
		    }
		    this.model.Form = 'FMS-A';
		    this.model.CountByArea = {};
		    this.model.CountAll = null;
		    listData = [];
		    regions = [];
			return this.model;
		};

		var checkByDates = function (data, startDate, finishDate, model) {
			var start = controller.parseDate(data.DateStart);
			if (null == start) {
				controller.writeWarningToLog("  Не указана дата прибытия!");
				return false;
			}
			return start >= startDate && start <= finishDate;
		};

		function isEqual(field1, field2) {
		    if (!field1 || !field2) {
		        return field1 == field2;
		    }
		    if ('string' == typeof field1 && 'string' == typeof field2) {
		        return field1 == field2
		    }
		    if ('object' == typeof field1 && 'object' == typeof field2) {
		        return field1.id == field2.id;
		    }
		    return false;
		};

		function checkExtensionResidence(data) {
		    for (var i = 0; i < listData.length; i++) {
		        var item = listData[i];
		        if (isEqual(item.LastName, data.LastName) && isEqual(item.FirstName, data.FirstName) && isEqual(item.MiddleName, data.MiddleName) &&
                    isEqual(item.Gender, data.Gender) && isEqual(item.BirthDate, data.BirthDate) && isEqual(item.Citizenship, data.Citizenship) &&
                    isEqual(item.DocumentPersonalType, data.DocumentPersonalType) &&
                    isEqual(item.DocumentPersonalSerial, data.DocumentPersonalSerial) && isEqual(item.DocumentPersonalNumber, data.DocumentPersonalNumber) &&
                    isEqual(item.DocumentResidenceType, data.DocumentResidenceType) &&
                    isEqual(item.DocumentResidenceSeries, data.DocumentResidenceSeries) && isEqual(item.DocumentResidenceNumber, data.DocumentResidenceNumber) &&
                    isEqual(item.DateFinish, data.DateStart)) {
		            listData[i].DateFinish = data.DateFinish;
		            return true;
		        }
		    }
		    return false;
		};

		var generateListMonth = function (monthFrom, monthTill) {
		    var list = {};
		    if (monthFrom <= 1 && 1 <= monthTill) list[1] = { text: "январь", count: 0 };
		    if (monthFrom <= 2 && 2 <= monthTill) list[2] = { text: "февраль", count: 0 };
		    if (monthFrom <= 3 && 3 <= monthTill) list[3] = { text: "март", count: 0 };
		    if (monthFrom <= 4 && 4 <= monthTill) list[4] = { text: "апрель", count: 0 };
		    if (monthFrom <= 5 && 5 <= monthTill) list[5] = { text: "май", count: 0 };
		    if (monthFrom <= 6 && 6 <= monthTill) list[6] = { text: "июнь", count: 0 };
		    if (monthFrom <= 7 && 7 <= monthTill) list[7] = { text: "июль", count: 0 };
		    if (monthFrom <= 8 && 8 <= monthTill) list[8] = { text: "август", count: 0 };
		    if (monthFrom <= 9 && 9 <= monthTill) list[9] = { text: "сентябрь", count: 0 };
		    if (monthFrom <= 10 && 10 <= monthTill) list[10] = { text: "октябрь", count: 0 };
		    if (monthFrom <= 11 && 11 <= monthTill) list[11] = { text: "ноябрь", count: 0 };
		    if (monthFrom <= 12 && 12 <= monthTill) list[12] = { text: "декабрь", count: 0 };
		    return list;
		}

		controller.ProcessingData = function (data) {
			log.Debug('ProcessingData {');
			if (!this.model || !this.model.ReportingPeriodFrom || !this.model.ReportingPeriodTill) {
				controller.writeWarningToLog("  Не заданы параметры проверки!");
				return null;
			}
			this.model.ReportingPeriodFromDate = controller.parseDate(this.model.ReportingPeriodFrom);
			this.model.ReportingPeriodTillDate = controller.parseDate(this.model.ReportingPeriodTill);
			if (this.model.CountAll == null) {
			    this.model.CountAll = generateListMonth(this.model.ReportingPeriodMonthFrom.id, this.model.ReportingPeriodMonthTill.id);
			}
			var result = checkByDates(data, this.model.ReportingPeriodFromDate, this.model.ReportingPeriodTillDate, this.model);
			if (!result) {
				controller.writeWarningToLog("  Дата заселения не соответствует указанному периоду!");
				return false;
			}
			if (!listData) {
			    listData = [];
			}
			if (!regions) {
			    regions = [];
			}
			result = !checkExtensionResidence(data)
			if (!result) {
			    controller.writeWarningToLog("  Это повторный заезд");
			    return false;
			} else {
			    listData.push(data);
			    var start = controller.parseDate(data.DateStart);
			    var month = start.getMonth() + 1;

			    var region = data.RegAddressRegion;
			    if (!this.model.CountByArea[region])
			    {
			        regions.push(region);
			        this.model.CountByArea[region] = generateListMonth(this.model.ReportingPeriodMonthFrom.id, this.model.ReportingPeriodMonthTill.id);
			    }
			    if (region.substring(0, 2) == this.model.IDCurrentRegion) {
			        controller.writeWarningToLog("  Зарегистрирован в том же регионе, в котором находится гостиница. Не будет учитываться при построении отчета.");
			    } else {
			        this.model.CountByArea[region][month] = { text: (this.model.CountByArea[region][month]).text, count: (this.model.CountByArea[region][month]).count + 1 };
			        this.model.CountAll[month] = { text: (this.model.CountAll[month]).text, count: (this.model.CountAll[month]).count + 1 };
			    }
			}
			log.Debug('ProcessingData }');
			return result;
		};

		var sortElements = function (data1, data2) {
			var val1 = data1;
			var val2 = data2;
			if (val1 > val2) { return 1; }
			if (val1 <= val2) { return -1; }
		};

		var base_BuildXamlView = controller.BuildXamlView;
		controller.BuildXamlView = function () {
			var content = base_BuildXamlView.call(this);
			if (regions && regions.length > 0) {
			    regions.sort(sortElements);
			}
			content.Content = print_tpl({
			    form: this.model,
			    regions: !regions ? regions = [] : regions,
				date: h_times.nowLocalISOStringDateTime()
			});
			return content;
		};

		controller.UseCodec(codec());

		return controller;
	}
});
