define([
	  'forms/fms/base/x_fms_csv'
	, 'forms/fms/migrationn/m_migrationn'
	, 'forms/base/log'
	, 'forms/fms/uploading/csv.sample.kontur'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_docTypeMigrationn'
	, 'forms/fms/base/h_docTypeMigrationnR'
	, 'forms/fms/base/h_visaType'
	, 'forms/fms/base/h_visaCategory'
	, 'forms/fms/base/h_visaMultiplicity'
	, 'forms/fms/base/h_MigKpp'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/h_country'
	, 'forms/fms/base/h_entryGoal'
	, 'forms/fms/base/h_arrivals'
	, 'forms/fms/base/h_profession'
	, 'forms/fms/base/h_StateProgram'
	, 'forms/base/h_times'
],
function (
	  BaseCodec
	, model_migration
	, GetLogger
	, csvFormat
	, helper_DocumentType
	, helper_DocumentTypeMig
	, helper_DocumentTypeMigR
	, helper_VisaType
	, helper_visaCategory
	, helper_visaMultiplicity
	, helper_MigKpp
	, helper_citizenship
	, helper_country
	, helper_entryGoal
	, helper_Arrivals
	, helper_Profession
	, helper_StateProgram
	, h_times
) {
	var log = GetLogger('x_csv_migration');
	return function () {
		log.Debug('Create {');
		var res = BaseCodec();

		var base_Decode = res.Decode;
		res.Decode = function (data_string) {
			var data = data_string.split(';');
			return base_Decode.call(this, data);
		};

		res.DecodeRelation = function (data, m_migrationn) {
			log.Debug('DecodeRelation {');
			m_migrationn.Document_entered = 'false';
			if (!this.isNullOrEmpty(this.GetValue(data, csvFormat.RelationLastName)) && !this.isNullOrEmpty(this.GetValue(data, csvFormat.RelationFirstName))) {
				m_migrationn.LastName_Relation = this.GetValue(data, csvFormat.RelationLastName);
				m_migrationn.LastName_Relation_latin = this.GetValue(data, csvFormat.RelationLastNameLat);
				m_migrationn.FirstName_Relation = this.GetValue(data, csvFormat.RelationFirstName);
				m_migrationn.FirstName_Relation_latin = this.GetValue(data, csvFormat.RelationFirstNameLat);
				m_migrationn.MiddleName_Relation = this.GetValue(data, csvFormat.RelationMiddleName);
				m_migrationn.MiddleName_Relation_latin = this.GetValue(data, csvFormat.RelationMiddleNameLat);
				m_migrationn.Sex_Relation = this.DecodeSex(data[csvFormat.RelationSex], m_migrationn.MiddleName_Relation);
				m_migrationn.Birthday_Relation = this.GetValue(data, csvFormat.RelationBirthDay);
				m_migrationn.Birthplace_Relation = {
					Country: this.GetDictionaryValue(data, csvFormat.RelationBirthCountry, helper_country),
					Region: this.GetValue(data, csvFormat.RelationBirthRegion),
					Area: this.GetValue(data, csvFormat.RelationBirthArea),
					City: this.GetValue(data, csvFormat.RelationBirthCity),
					Town: this.GetValue(data, csvFormat.RelationBirthPlace)
				};
				m_migrationn.Nationality_Relation = this.DecodeNationality(data, csvFormat.RelationCitizenship);
				m_migrationn.DocumentType_Relation = this.GetDictionaryValue(data, csvFormat.RelationDocumentType, helper_DocumentTypeMigR);
				m_migrationn.DocumentSeries_Relation = this.GetValue(data, csvFormat.RelationDocumentSeries);
				m_migrationn.DocumentNumber_Relation = this.GetValue(data, csvFormat.RelationDocumentNumber);
				m_migrationn.DocumentGivenDate_Relation = this.GetValue(data, csvFormat.RelationDocumentIssued);
				m_migrationn.DocumentFinishDate_Relation = this.GetValue(data, csvFormat.RelationDocumentValidTo);
				m_migrationn.Document_entered = true;
			}
			log.Debug('DecodeRelation }');
		};

		res.DecodeVisaDocument = function (data, m_migrationn) {
			log.Debug('DecodeVisaDocument {');
			m_migrationn.Visa_entered = m_migrationn.Document_entered;
			m_migrationn.Visa_type = this.GetDictionaryValue(data, csvFormat.VisaType, helper_VisaType);
			m_migrationn.Visa_series = this.GetValue(data, csvFormat.VisaSeries);
			m_migrationn.Visa_number = this.GetValue(data, csvFormat.VisaNumber);
			m_migrationn.Visa_issued = this.GetValue(data, csvFormat.VisaIssued);
			m_migrationn.Visa_validFrom = this.GetValue(data, csvFormat.VisaFrom);
			m_migrationn.Visa_validTo = this.GetValue(data, csvFormat.VisaTo);
			m_migrationn.Visa_category = this.GetDictionaryValue(data, csvFormat.VisaCategory, helper_visaCategory);
			m_migrationn.Visa_multiplicity = this.GetDictionaryValue(data, csvFormat.VisaMultiplisity, helper_visaMultiplicity);
			m_migrationn.Visa_identifier = this.GetValue(data, csvFormat.VisaIdentifier);
			m_migrationn.Visa_visitPurpose = this.GetDictionaryValue(data, csvFormat.VisaVisitPurpose, helper_entryGoal);
			m_migrationn.LivingDocDecisionNumber = this.GetValue(data, csvFormat.DecisionNumber);
			m_migrationn.LivingDocDecisionDate = this.GetValue(data, csvFormat.DecisionDate);
			m_migrationn.RvpVisa_validTo = this.GetValue(data, csvFormat.VisaValidTo);
			var documentGivenBy = this.DecodeDocumentGivenBy(data, csvFormat.DecisionGivenBy);
			if ('string' == typeof documentGivenBy) {
				m_migrationn.RvpDocumentGivenBy_text = documentGivenBy;
				m_migrationn.RvpDocumentGivenBy_from_dict = false;
			} else {
				m_migrationn.RvpDocumentGivenBy = documentGivenBy;
				m_migrationn.RvpDocumentGivenBy_from_dict = true;
			}
			log.Debug('DecodeVisaDocument }');
		};

		res.DecodeMigrationCard = function (data, m_migrationn) {
			log.Debug('DecodeMigrationCard {');
			m_migrationn.MigrationCard_series = this.GetValue(data, csvFormat.MigrationCardSeries);
			m_migrationn.MigrationCard_number = this.GetValue(data, csvFormat.MigrationCardNumber);
			m_migrationn.MigrationCard_dateFrom = this.GetValue(data, csvFormat.MigrationCardDateFrom);
			m_migrationn.MigrationCard_dateTo = this.GetValue(data, csvFormat.MigrationCardDateTo);
			m_migrationn.MigrationCard_kpp = this.GetDictionaryValue(data, csvFormat.MigrationCardKPP, helper_MigKpp);
			m_migrationn.MigrationCard_visitPurpose = this.GetDictionaryValue(data, csvFormat.EntrancePurpose, helper_Arrivals);
			log.Debug('DecodeMigrationCard }');
		};

		res.DecodeDocumentMig = function (data, model, csvFormat) {
			log.Debug('DecodeDocument {');
			model.DocumentType = this.GetDictionaryValue(data, csvFormat.Document, helper_DocumentTypeMig);
			model.DocumentSeries = this.GetValue(data, csvFormat.DocumentSeries);
			model.DocumentNumber = this.GetValue(data, csvFormat.DocumentNumber);
			model.DocumentGivenDate = this.GetValue(data, csvFormat.DocumentIssued);
			log.Debug('DecodeDocument }');
		};

		var base_DecodeKonturCsvDocument = res.DecodeKonturCsvDocument;
		res.DecodeKonturCsvDocument = function (data) {
			log.Debug('DecodeKonturCsvDocument {');
			if (data[0] != '1') {
				var err = { parseException: true, readablemsg: 'Неверный формат' };
				log.Debug('throw parsing error..');
				log.Debug('Неверный формат');
				throw err;
			}
			var m_migrationn = model_migration();
			this.DecodePerson(data, m_migrationn, csvFormat);
			m_migrationn.LastName_latin = this.GetValue(data, csvFormat.LastNameLat);
			m_migrationn.FirstName_latin = this.GetValue(data, csvFormat.FirstNameLat);
			m_migrationn.MiddleName_latin = this.GetValue(data, csvFormat.MiddleNameLat);
			this.DecodeBirthPlace(data, m_migrationn, csvFormat);
			this.DecodeDocumentMig(data, m_migrationn, csvFormat);
			m_migrationn.DocumentFinishDate = this.GetValue(data, csvFormat.DocumentValidTo);
			this.DecodeRelation(data, m_migrationn);
			this.DecodeVisaDocument(data, m_migrationn);
			this.DecodeMigrationCard(data, m_migrationn);
			m_migrationn.StateProgram = this.GetDictionaryValue(data, csvFormat.StateProgramm, helper_StateProgram);
			m_migrationn.Profession = this.GetDictionaryValue(data, csvFormat.Profession, helper_Profession);
			m_migrationn.entrancePurpose = this.GetDictionaryValue(data, csvFormat.EntrancePurpose, helper_Arrivals);
			m_migrationn.stayPeriod_dateFrom = this.GetValue(data, csvFormat.DateFrom);
			m_migrationn.notificationReceived = h_times.stringifyDateUTC('d', h_times.ParseRussianDate(m_migrationn.stayPeriod_dateFrom));
			m_migrationn.stayPeriod_dateTo = this.GetValue(data, csvFormat.DateTo);
			log.Debug('DecodeKonturCsvDocument }');
			return m_migrationn;
		};

		log.Debug('Create }');
		return res;
	}
});
