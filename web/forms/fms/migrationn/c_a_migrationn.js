﻿define([
	  'forms/fms/attachments/c_attachments'
	, 'forms/fms/migrationn/c_migrationn'
	, 'forms/fms/migrationn/ff_migrationn'
	, 'forms/fms/base//h_Select2'
	, 'forms/base/log'
],
function (BaseController, BaseFormController, formSpec, helper_Select2, GetLogger)
{
	return function()
	{
		var log = GetLogger('c_migrationn');

		var controller = BaseController(BaseFormController, formSpec);

		controller.SetStayPeriod = function (period) {
			controller.model.local_stayPeriod_dateFrom = period.from;
		}

		controller.GetStayPeriod = function () {
			return {
				from: $('#cpw_form_stayPeriod_dateFrom').val(),
				to: $('#cpw_form_stayPeriod_dateTo').val()
			};
			return;
		}

		controller.GetBody = function () {
			var body = 'Направляем документ "Уведомление о прибытии ИГ или ЛБГ".';
			if ($('#cpw_form_LastName').val() != '') {
				body += '\r\nПрибывший  гражданин: ' + $('#cpw_form_LastName').val();
				if ($('#cpw_form_FirstName').val() != '')
					body += ' ' + $('#cpw_form_FirstName').val().substr(0, 1) + '.';
				if ($('#cpw_form_MiddleName').val() != '')
					body += '' + $('#cpw_form_MiddleName').val().substr(0, 1) + '.';
				var birthDate = $('#cpw_form_Birthday').val();
				if (birthDate) {
					var birthDateParts = birthDate.split('.');
					if (birthDateParts && birthDateParts.length == 3)
						body += ', ' + birthDateParts[2] + ' г.р.';
				}
			}
			return body;
		}

		controller.GetDefaultDocumentType = function()
		{
			var DocumentType = $('#cpw_form_DocumentType').select2('data');
			var additionalText = '';
			if ('checked' == $('#cpw_form_Document_entered').attr('checked')) {
				additionalText = CheckDiffDatesMore14($('#cpw_form_Birthday').val(), $('#cpw_form_notificationReceived').val()) ?
					" (на ребенка)" :
					" (на прибывшего)";
			}
			return !DocumentType ? { id: 103009, text: "Национальный заграничный паспорт" + additionalText} : DocumentType;
		}

		var CheckDiffDatesMore14 = function (date1, date2) {
			var dateCurrent1 = new Date(date1.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			var dateCurrentYear1 = dateCurrent1.getFullYear();
			var dateCurrent2 = new Date(date2.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			var dateCurrentYear2 = dateCurrent2.getFullYear();
			var dateDiff = dateCurrentYear2 - dateCurrentYear1;
			if (dateDiff > 0 && dateDiff < 14) {
				return false;
			}
			else if (dateDiff > 14 || dateDiff < 0) {
				return true;
			}
			else {
				dateCurrent1.setYear(dateCurrentYear1 + 14);
				return dateCurrent2 >= dateCurrent1;
			}
		}

		controller.GetDocumentTypeValues = function(withDuplicate)
		{
			types = [];
			var additionalText = '';
			if ('checked' == $('#cpw_form_Document_entered').attr('checked')) {
				additionalText = CheckDiffDatesMore14($('#cpw_form_Birthday').val(), $('#cpw_form_notificationReceived').val()) ?
					" (на прибывшего)" :
					" (на ребенка)";
			}
			var DocumentType = $('#cpw_form_DocumentType').select2('data');
			var textDocumentType = !DocumentType ? null : DocumentType.text + additionalText;
			if (DocumentType && (withDuplicate || !ContainsTypeByIdAndText(DocumentType.id, textDocumentType, types))) {
				types.push({
					id: DocumentType.id,
					text: textDocumentType,
					series: $('#cpw_form_DocumentSeries').val(),
					number: $('#cpw_form_DocumentNumber').val()
				});
			}

			var DocumentType_Relation = $('#cpw_form_DocumentType_Relation').select2('data');
			var textDocumentType_Relation = !DocumentType_Relation ? null : DocumentType_Relation.text + ' (на отв. лицо)';
			if (DocumentType_Relation && (withDuplicate || !ContainsTypeByIdAndText(DocumentType_Relation.id, textDocumentType_Relation, types))) {
				types.push({
					id: DocumentType_Relation.id,
					text: textDocumentType_Relation,
					series: $('#cpw_form_DocumentSeries_Relation').val(),
					number: $('#cpw_form_DocumentNumber_Relation').val()
				});
			}

			var Visa_type = $('#cpw_form_Visa_type').select2('data');
			var textVisa_type = !Visa_type ? null : Visa_type.text + additionalText;
			if (Visa_type && (withDuplicate || !ContainsTypeByIdAndText(Visa_type.id, textVisa_type, types))) {
				types.push({
					id: Visa_type.id,
					text: textVisa_type,
					series: $('#cpw_form_Visa_series').val(),
					number: $('#cpw_form_Visa_number').val()
				});
			}

			var MigrationCard_series = $('#cpw_form_MigrationCard_series').val();
			var MigrationCard_number = $('#cpw_form_MigrationCard_number').val();
			var textMigrationCard = "Миграционная карта" + additionalText;
			if ((MigrationCard_series != '' || MigrationCard_number != '') && (withDuplicate || !ContainsTypeByIdAndText(103022, textMigrationCard, types))) {
				types.push({
					id: 103022,
					text: textMigrationCard,
					series: MigrationCard_series,
					number: MigrationCard_number
				});
			}

			return types;
		}

		var baseform_AfterAddedDocument = controller.AfterAddedDocument;
		controller.AfterAddedDocument = function()
		{
			var types = !controller.GetDocumentTypeValues ? helper_DocumentType.GetValues() : controller.GetDocumentTypeValues(false);
			if (types && 1 < types.length)
				$('.column_add_comment').removeClass('hidden');
			else if (!$('.column_add_comment').hasClass('hidden'))
				$('.column_add_comment').addClass('hidden');
		}

		var ContainsTypeByIdAndText = function (id, text, types)
		{
			for (var i = 0; i < types.length; i++)
			{
				if (types[i].id == id && types[i].text == text)
					return true;
			}
			return false;
		}

		return controller;
	}
});