define([
	  'forms/fms/base/c_fms'
	, 'forms/fms/migrationn/codec_migrationn'
	, 'forms/fms/base/town/c_town'
	, 'forms/fms/migrationn/m_migrationn'
	, 'forms/base/h_names'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_docTypeMigrationn'
	, 'forms/fms/base/h_docTypeMigrationnR'
	, 'forms/base/h_times'
	, 'forms/fms/base/town/h_town'
	, 'forms/fms/base/upload/c_upload'
	, 'forms/fms/base/upload/h_upload'
	, 'forms/fms/base/h_arrivals'
	, 'forms/fms/base/h_profession'
	, 'forms/fms/base/h_visaType'
	, 'forms/fms/base/h_visaCategory'
	, 'forms/fms/base/h_visaMultiplicity'
	, 'forms/fms/base/h_MigKpp'
	, 'forms/fms/base/h_StateProgram'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/h_InputMask'
	, 'forms/fms/base/h_tabs'
	, 'forms/fms/base/h_validate_n'
	, 'tpl!forms/fms/migrationn/v_migrationn.xaml'
	, 'tpl!forms/fms/migrationn/e_migrationn.html'
	, 'tpl!forms/fms/migrationn/p_migrationn.html'
	, 'forms/base/log'
	, 'forms/fms/base/h_entryGoal'
	, 'forms/fms/profile/h_abonent'
	, 'forms/base/h_dictionary'
	, 'forms/fms/guides/dict_officialorgan_fns.csv'
	, 'forms/fms/guides/dict_officialorgan_fms.csv'
	, 'forms/base/h_msgbox'
	, 'forms/fms/base/russ_addr/c_russ_addr'
	, 'forms/fms/base/addr/c_addr'
	, 'forms/fms/base/addr_variants_with_document/c_addr_variants_with_document'
	, 'forms/fms/base/addr/c_addr_modal'
	, 'forms/fms/base/russ_addr_fias/x_fias'
	, 'forms/fms/base/addr/x_addr'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/russ_addr_fias/h_fias'
	, 'forms/fms/questionary/h_questionary'
	, 'forms/fms/base/h_citizenship'
	, 'forms/base/h_constraints'
	, 'forms/fms/migrationn/r_migrationn'
	, 'forms/base/codec.tpl.xaml'
],
function 
(
	  BaseFormController
	, codec
	, controller_Town
	, model_MigrationNotify
	, helper_Names
	, helper_DocumentType
	, helper_DocumentTypeMig
	, helper_DocumentTypeMigR
	, helper_Times
	, helper_Town
	, controller_UploadFile
	, helper_Upload
	, helper_Arrivals
	, helper_Profession
	, helper_VisaType
	, helper_visaCategory
	, helper_visaMultiplicity
	, helper_MigKpp
	, helper_StateProgram
	, helper_Select2
	, helper_InputMask
	, helper_Tabs
	, helper_validate
	, print_tpl
	, layout_tpl
	, attrshtml_tpl
	, GetLogger
	, helper_entryGoal
	, h_abonent
	, h_dictionary
	, fns
	, fms
	, h_msgbox
	, c_russ_addr
	, c_addr
	, c_addr_variants
	, c_addr_modal
	, x_fias
	, x_addr
	, h_addr
	, h_fias
	, h_questionary
	, h_citizenship
	, h_constraints
	, r_migrationn
	, codec_tpl_xaml
	)
{
	return function ()
	{
		var log = GetLogger('c_migrationn');
		log.Debug('Create {');

		var Select2Options = function (h_values)
		{
			return function (m)
			{
				return {
					placeholder: '',
					allowClear: true,
					query: function (query) {
						query.callback({ results: helper_Select2.FindForSelect(h_values(), query.term) });
					}
				};
			}
		}

		var Select2OptionsWithNewValue = function (h_values)
		{
			return function (m)
			{
				return {
					placeholder: '',
					allowClear: true,
					query: function (query)
					{
						query.callback({ results: helper_Select2.FindForSelect(h_values(), query.term) });
					},
					createSearchChoice: function (term, data)
					{
						if ($(data).filter(function () { return this.text.localeCompare(term) === 0 }).length === 0)
							return { id: term, text: term };
					}
				}
			}
		}

		var AddressTextFunc = function (item)
		{
			return !(!item || null == item || !item.text || null == item.text || '' == item.text)
					? item.text : 'Кликните, чтобы указать..';
		}

		var AddressTextFunc2 = function (item)
		{
			return h_addr.SafePrepareReadableText(item, 'Кликните, чтобы указать..');
		}

		var СreateCreateControllerVariants = function (model, field_name, field_name_document, title, onlyfias, default_fias)
		{
			var createController = function (item, onAfterSave)
			{
				var res =
				{
					ShowModal: function (e)
					{
						e.preventDefault();
						var controller = c_addr_variants(model.Hotel_address_variants);
						var field_value = model[field_name];
						if (onlyfias) {
							if (!field_value)
								field_value = {};
							if (!field_value.russianAddress)
								field_value.russianAddress = {};
							field_value.russianAddress.housing_types = true;
							if (!field_value.russianAddress.onlyfias)
							{
								field_value.russianAddress.variant_fias = true;
								field_value.russianAddress.onlyfias = true;
							}
							field_value.document = !model[field_name_document] ? '' : model[field_name_document];
						}
						if (!default_fias) {
							if (!field_value)
								field_value = {};
							if (!field_value.russianAddress)
								field_value.russianAddress = {};
							field_value.russianAddress.fias_version = h_fias.GetFiasVersionBySchemaVersion(model);
						}
						if (null != field_value)
							controller.SetFormContent(field_value);
						var bname_Ok = 'Сохранить';
						h_msgbox.ShowModal({
							controller: controller
							, width: 750
							, height: 495
							, title: !title ? 'Адрес' : title
							, buttons: [bname_Ok, 'Отменить']
							, onclose: function (bname)
							{
								if (bname_Ok == bname)
								{
									var content = controller.GetFormContent();
									model[field_name] =
									{
										Country: { id: 'RUS', text: "Россия" }
										, russianAddress: content && content.russianAddress ? content.russianAddress : {}
									};
									model[field_name_document] = content && content.document ? content.document : '';
									onAfterSave();
								}
							}
						});
					}
				};
				return res;
			};
			return createController;
		}

		var controller = BaseFormController(layout_tpl,
		{
			  constraints: r_migrationn()
			, field_spec:
			{
				DocumentType: Select2Options(helper_DocumentTypeMig.GetValues)
				, DocumentType_Relation: Select2Options(helper_DocumentTypeMigR.GetValues)
				, DocumentType_Company: Select2Options(helper_DocumentType.GetValues)
				, Nationality: Select2Options(h_citizenship.GetValues)
				, Nationality_Relation: Select2Options(h_citizenship.GetValues)
				, Organization_Person_nationality: Select2Options(h_citizenship.GetValues)
				, Visa_type: Select2Options(helper_VisaType.GetValues)
				, Visa_multiplicity: Select2Options(helper_visaMultiplicity.GetValues)
				, Visa_category: Select2Options(helper_visaCategory.GetValues)
				, Visa_visitPurpose: Select2Options(helper_entryGoal.GetValues)
				, MigrationCard_visitPurpose: Select2Options(helper_Arrivals.GetValues)
				, entrancePurpose: Select2Options(helper_Arrivals.GetValues)
				, Profession: Select2OptionsWithNewValue(helper_Profession.GetValues)
				, StateProgram: Select2Options(helper_StateProgram.GetValues)
				, MigrationCard_kpp: Select2Options(helper_MigKpp.GetValues)
				, Birthplace: {
					text: AddressTextFunc
					, controller: function (item, onAfterSave) { return controller_Town(item, onAfterSave); }
				}
				, Birthplace_Relation: {
					text: AddressTextFunc
					, controller: function (item, onAfterSave) { return controller_Town(item, onAfterSave); }
				}
				, Organization_Person_birthPlace: {
					text: AddressTextFunc
					, controller: function (item, onAfterSave) { return controller_Town(item, onAfterSave); }
				}
				, Address: function (model) {
					var createController = СreateCreateControllerVariants(model, 'Address', 'Hotel_Document', 'Место пребывания', true, false);
					var res = { text: AddressTextFunc2, controller: createController };
					return res;
				}
				, Address_Company: function (model) {
					var res = { text: AddressTextFunc2, controller: c_addr_modal(model, 'Address_Company', 'Фактический адрес', true, false) };
					return res;
				}
				, Address_CompanyN: function (model) {
					var res = { text: AddressTextFunc2, controller: c_addr_modal(model, 'Address_CompanyN', 'Адрес прописки', true, false) };
					return res;
				}
				, PreAddress: function (model) {
					var res = { text: AddressTextFunc2, controller: c_addr_modal(model, 'PreAddress', 'Прежний адрес РФ', false, false) };
					return res;
				}
			}
		});

		controller.SafeCreateNewContent = function ()
		{
			if (!this.model)
				this.model = model_MigrationNotify();
		}

		controller.UseProfile = function (profile)
		{
			log.Debug('UseProfile {');
			h_abonent.CheckResponsibleOfficiers(profile);
			this.SafeCreateNewContent();

			if (profile && profile.Abonent)
			{
				if (!this.model.Hotel_organization_name || '' == this.model.Hotel_organization_name)
				{
					if (profile.Abonent.ShortName)
					{
						this.model.Hotel_organization_name = profile.Abonent.ShortName;
					}
					else if (profile.Abonent.Name)
					{
						this.model.Hotel_organization_name = profile.Abonent.Name;
					}
				}
				if (!this.model.Hotel_organization_inn || '' == this.model.Hotel_organization_inn)
				{
					if (profile.Abonent.Inn)
						this.model.Hotel_organization_inn = profile.Abonent.Inn;
				}
				if (profile.Abonent.phone)
				{
					this.model.Hotel_organization_phone = profile.Abonent.phone;
					this.model.Phone = profile.Abonent.phone;
				}
			}
			if (profile && profile.Extensions && profile.Extensions.fms)
			{
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent)
				{
					var pAbonent = pfms.Abonent;
					pAbonent = h_abonent.SafeUpgradeAbonent(pAbonent);
					this.model.subdivision = h_abonent.SafeGet_dict_field(pAbonent, 'subdivision', this.model.subdivision);
					this.model.sono = h_abonent.SafeGet_dict_field(pAbonent, 'sono', this.model.sono);

					if (pAbonent.supplierInfo)
						this.model.supplierInfo = pAbonent.supplierInfo;

					if (pAbonent.AddressesOfPlacement && pAbonent.AddressesOfPlacement.length > 0)
					{
						this.model.Hotel_address_variants = pAbonent.AddressesOfPlacement;
						var defAddressesOfPlacement = h_abonent.SafeGetDefaultAddressesOfPlacement(pAbonent);
						if (defAddressesOfPlacement && null != defAddressesOfPlacement)
						{
							log.Debug('defAddressesOfPlacement {');
							this.model.Address = h_addr.ConvertFromOldFormat(defAddressesOfPlacement);
							this.model.Hotel_Document = !defAddressesOfPlacement.document ? '' : defAddressesOfPlacement.document;
							log.Debug('defAddressesOfPlacement }');
						}
					}
					if (pAbonent.AddressRealAbonent)
					{
						log.Debug('AddressRealAbonent {');
						this.model.Address_Company = h_addr.ConvertFromOldFormat(pAbonent.AddressRealAbonent);
						log.Debug('AddressRealAbonent }');
					}

					if (pAbonent.employee_ummsId)
						this.model.employee.ummsId = pAbonent.employee_ummsId;

					if (pAbonent.hotel_id)
						this.model.hotel_id = pAbonent.hotel_id;

					var col = h_abonent.SafeGetDefaultColaborator(pAbonent);
					if (null != col)
					{
						if (col.user_document_serie)
							this.model.Organization_Document_series = col.user_document_serie;
						if (col.user_document_number)
							this.model.Organization_Document_number = col.user_document_number;
						if (col.user_document_dateFrom)
							this.model.Organization_Document_issued = col.user_document_dateFrom;
						if (col.user_document_dateTo)
							this.model.Organization_Document_validTo = col.user_document_dateTo;
						if (col.user_document_type)
							this.model.DocumentType_Company = col.user_document_type;

						if (col.user_document_givenby) {
							this.model.Organization_Document_authorityOrgan = h_abonent.SafeGet_dict_field(col, 'user_document_givenby', col.user_document_givenby);
							var item = h_dictionary.FindById(fms, this.model.Organization_Document_authorityOrgan.id)
							if (item) {
								this.model.Organization_Document_authorityOrgan.row = item.row;
							}
							this.model.Organization_Document_authorityOrgan_check = true;
						}
						if (col.user_document_givenby_text) {
							this.model.Organization_Document_authorityOrgan_text = col.user_document_givenby_text;
							this.model.Organization_Document_authorityOrgan_check = false;
						}

						if (col.Nationality)
							this.model.Organization_Person_nationality = col.Nationality;
						if (col.Birthplace)
						{
							log.Debug('Birthplace {');
							this.model.Organization_Person_birthPlace = col.Birthplace;
							log.Debug('Birthplace }');
						}
						if (col.Birthday)
							this.model.Organization_Person_birthDate = col.Birthday;
						if (col.FirstName)
							this.model.Organization_Person_firstName = col.FirstName;
						if (col.LastName)
							this.model.Organization_Person_lastName = col.LastName;
						if (col.MiddleName)
							this.model.Organization_Person_middleName = col.MiddleName;

						if (col.FirstName_latin)
							this.model.Organization_Person_firstName_latin = col.FirstName_latin;
						if (col.LastName_latin)
							this.model.Organization_Person_lastName_latin = col.LastName_latin;
						if (col.MiddleName_latin)
							this.model.Organization_Person_middleName_latin = col.MiddleName_latin;

						if ('M' == col.Sex)
							this.model.Organization_Person_gender = { id: 'M', text: 'Мужской' };
						else if ('F' == col.Sex)
							this.model.Organization_Person_gender = { id: 'F', text: 'Женский' };

						if (col.AddressLegal && null != col.AddressLegal)
						{
							log.Debug('AddressLegal {');
							this.model.Address_CompanyN = h_addr.ConvertFromOldFormat(col.AddressLegal);
							log.Debug('AddressLegal }');
						}

						if (col.Phone)
							this.model.Phone = col.Phone;
					}

					if (pAbonent.OrganizationPhone)
						this.model.Hotel_organization_phone = pAbonent.OrganizationPhone;

					controller.ConvertOldFiasAddressesToNew();
				}
				if (pfms.Forma)
				{
					this.ShowAttachmentsPanel = pfms.Forma.ShowAttachmentsPanel;
				}
				UpdateFieldsForSchema_1_102_2(getSchemaVersionByModel(this));
			}
			log.Debug('UseProfile }');
		}

		var formatResult = function(item)
		{
			return '<div class="cpw-fms-official-organ"><div class="code">код:' +
				item.row.CODE +'</div><div class="name">' +
				item.row.NAME + '</div></div>';
		}

		var formatSelection= function(item)
		{
			if (!item.row)
			{
				return item.text;
			}
			else
			{
				return '<div class="cpw-fms-official-organ" title="' +
					item.row.NAME + '"><div class="name">' +
					item.row.NAME + '</div><div class="code">код:<br/>' +
					item.row.CODE + '</div></div>';
			}
		}

		var UpdateAddressesLinksTextes = function (content)
		{
			var null_text = 'Кликните, чтобы указать..';
			try
			{
				$('#cpw_form_Birthplace').text(!content.Birthplace.text ? null_text : content.Birthplace.text);
				$('#cpw_form_Birthplace_Relation').text(!content.Birthplace_Relation.text ? null_text : content.Birthplace_Relation.text);
				$('#cpw_form_Organization_Person_birthPlace').text(!content.Organization_Person_birthPlace.text ? null_text : content.Organization_Person_birthPlace.text);

				$('#cpw_form_Address').text(h_addr.SafePrepareReadableText(content.Address, null_text));
				$('#cpw_form_Address_Company').text(h_addr.SafePrepareReadableText(content.Address_Company));
				$('#cpw_form_Address_CompanyN').text(h_addr.SafePrepareReadableText(content.Address_CompanyN));

				$('#cpw_form_PreAddress').text(h_addr.SafePrepareReadableText(content.PreAddress));

				$('#cpw_form_Address').trigger('changeAddressText');
				$('#cpw_form_Address_Company').trigger('changeAddressText');
				$('#cpw_form_Address_CompanyN').trigger('changeAddressText');
			}
			catch (err)
			{
				log.Error(err);
			}
		}

		var UpdateUploadsLinksTextes = function ()
		{
			var null_text = 'Кликните чтобы выбрать файл';

			var images_doc = controller.content.DocumentScan;
			var d = 0;
			var v = 0;
			if (images_doc.length > 0)
			{
				$('#cpw_form_Upload').empty();
				$('#cpw_form_UploadVisa').empty();
				for (var i = 0; i < images_doc.length; i++)
				{
					if (images_doc[i] && images_doc[i].uid == controller.content.Document_uid)
					{
						$('#cpw_form_Upload').append('<img width="40" src="' + images_doc[i].src + '"/>'); d++;
					} else if (images_doc[i] && images_doc[i].uid == controller.content.Visa_uid)
					{
						$('#cpw_form_UploadVisa').append('<img width="40" src="' + images_doc[i].src + '"/>'); v++;
					}
				}
			}
			if (d == 0)
				$('#cpw_form_Upload').text(null_text);
			else if (v == 0)
				$('#cpw_form_UploadVisa').text(null_text);
		}

		controller.PrepareRepresentativeLinesToPrint = function ()
		{
			log.Debug("PrepareRepresentativeLinesToPrint");
			if (null == this.model.Document_entered || 'false' == this.model.Document_entered)
			{
				return '';
			}
			else
			{
				var name = '';
				if (this.model.LastName_Relation)
					name += this.model.LastName_Relation;
				if (this.model.FirstName_Relation)
					name += ' ' + this.model.FirstName_Relation;
				if (this.model.MiddleName_Relation)
					name += ' ' + this.model.MiddleName_Relation;
				if (name.length > 38)
				{
					name = name.substring(0, 37);
				}
				var document = 'ДОКУМ';
				if (this.model.DocumentSeries_Relation)
					document += ' ' + this.model.DocumentSeries_Relation;
				if (this.model.DocumentNumber_Relation)
					document += ' №' + this.model.DocumentNumber_Relation;
				return name + ',' + document;
			}
		}

		controller.PrepareAddresssCompanyLinesToPrint = function ()
		{
			log.Debug("PrepareAddresssCompanyLinesToPrint {");
			if (!this.model.Address_Company)
			{
				log.Debug("PrepareAddresssCompanyLinesToPrint } empty");
				return ['', '', ''];
			}
			else
			{
				var fixed_text = h_addr.PrepareReadableText(this.model.Address_Company);
				fixed_text = fixed_text.replace('Россия, ', '');
				fixed_text = fixed_text.replace('д. ', 'д.');
				fixed_text = fixed_text.replace('кв. ', 'кв.');
				fixed_text = fixed_text.replace('корп. ', 'корп.');
				fixed_text = fixed_text.replace('г Москва, г Москва, ', 'г Москва, ');
				if (fixed_text.length <= 34)
				{
					log.Debug("PrepareAddresssCompanyLinesToPrint } 1");
					return [fixed_text, '', ''];
				}
				else
				{
					var line1 = fixed_text.substring(0, 34);
					var line2 = fixed_text.substring(34, 63);
					var line3 = fixed_text.substring(63, fixed_text.length);
					log.Debug("PrepareAddresssCompanyLinesToPrint } 2");
					return [line1, line2, line3];
				}
			}
		}

		controller.PrepareAddresssToPrint = function (address)
		{
			log.Debug("PrepareAddresssToPrint");
			return h_addr.PrepareAddressParts(address);
		}

		controller.PreparePreAddressText = function ()
		{
			log.Debug("PreparePreAddressText");
			return (!this.model.PreAddress ||
					!this.model.PreAddress.russianAddress ||
					null == this.model.PreAddress.Country)
					? '' : h_addr.PrepareReadableText(this.model.PreAddress)
		}

		controller.PrepareDateOfEntryInfoCountry = function ()
		{
			if (null == this.model.MigrationCard_number || '' == this.model.MigrationCard_number)
			{
				return this.model.stayPeriod_dateFrom;
			}
			else
			{
				return this.model.MigrationCard_dateFrom;
			}
		}

		controller.PrepareHotelDocumentLinesToPrint = function ()
		{
			var result = null;
			var lineLength = 40;
			var text = this.model.Hotel_Document.toUpperCase();
			if (text.length <= lineLength)
			{
				result = [text, '', ''];
			}
			else if (text.length <= 2 * lineLength)
			{
				result = [text.substring(0, lineLength), text.substring(lineLength), ''];
			}
			else
			{
				result = [text.substring(0, lineLength), text.substring(lineLength, 2 * lineLength), text.substring(lineLength)];
			}
			return result;
		}

		controller.ConvertOldFiasAddressesToNew = function () {
			var self = this;
			if (this.model.Address && null != this.model.Address) {
			h_fias.ConvertOldFiasToNew(this.model.Address, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.Address = res;
					self.model.Address.text = h_addr.PrepareReadableText(self.model.Address);
					$('#cpw_form_Address').text(h_addr.SafePrepareReadableText(self.model.Address));
				});
			}
			if (this.model.Address_Company && null != this.model.Address_Company) {
				h_fias.ConvertOldFiasToNew(this.model.Address_Company, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.Address_Company = res;
					self.model.Address_Company.text = h_addr.PrepareReadableText(self.model.Address_Company);
					$('#cpw_form_Address_Company').text(h_addr.SafePrepareReadableText(self.model.Address_Company));
				});
			}
			if (this.model.Address_CompanyN && null != this.model.Address_CompanyN) {
				h_fias.ConvertOldFiasToNew(this.model.Address_CompanyN, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.Address_CompanyN = res;
					self.model.Address_CompanyN.text = h_addr.PrepareReadableText(self.model.Address_CompanyN);
					$('#cpw_form_Address_CompanyN').text(h_addr.SafePrepareReadableText(self.model.Address_CompanyN));
				});
			}
			if (this.model.PreAddress && null != this.model.PreAddress) {
				h_fias.ConvertOldFiasToNew(this.model.PreAddress, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.PreAddress = res;
					self.model.PreAddress.text = h_addr.PrepareReadableText(self.model.PreAddress);
					$('#cpw_form_PreAddress').text(h_addr.SafePrepareReadableText(self.model.PreAddress));
				});
			}
		}

		controller.BuildXamlView = function ()
		{
			log.Debug("BuildXamlView {");
			var SymbolsLine= function(symbols)
			{
				var res = '';
				for (var i = 0; i < symbols.length; i++) {
					res += '<TableCell><Paragraph><Label Style="{StaticResource SymbolPlacer}" >' + symbols.charAt(i).toUpperCase() + '</Label></Paragraph></TableCell>\r\n                             ';
				}
				return res;
			};
			var SymbolsLineLen= function(symbols,len)
			{
				var fixed_symbols= symbols && symbols.length ? symbols : " ";
				while (fixed_symbols.length < len)
					fixed_symbols+= " ";
				fixed_symbols= fixed_symbols.substring(0,len);
				return SymbolsLine(fixed_symbols);
			};
			var SafeTrunk= function(str,max_length)
			{
				return !str ? '' : str.length<=max_length ? str : str.substr(0,max_length);
			};
			var FixPhoneNumber=function(str)
			{
				return null==str ? '' : SafeTrunk(str.replace(/\D/g,''),10);
			};
			var SubSymbolsLineLen= function(symbols,index,len)
			{
				var fixed_symbols= symbols && symbols.length ? symbols : " ";
				if (fixed_symbols.length > index + len)
				{
					fixed_symbols = fixed_symbols.substr(index, len);
				}
				else if (fixed_symbols.length > index)
				{
					fixed_symbols = fixed_symbols.substr(index, fixed_symbols.length - index);
				}
				else if (fixed_symbols.length <= index)
				{
					fixed_symbols = " ";
				}
				return SymbolsLineLen(fixed_symbols,len);
			};
			var SymbolsLineDateLen= function(symbols,index,len)
			{
				var fixed_symbols= symbols && symbols.length ? symbols.split('.')[index] : " ";
				return SymbolsLineLen(fixed_symbols,len);
			};
			var SymbolsLineDateLen_day_month= function(symbols,index,len)
			{
				var fixed_symbols= symbols && symbols.length ? symbols.split('.')[index] : " ";
				if ('00'==fixed_symbols)
					fixed_symbols= '';
				return SymbolsLineLen(fixed_symbols,len);
			};
			var SymbolsLineTextLen= function(symbols,len)
			{
				var fixed_symbols= symbols && symbols.text ? symbols.text : " ";
				return SymbolsLineLen(fixed_symbols, len);
			};
			var DocumentTypeSymbolsLineTextLen= function(symbols,len)
			{
				var fixed_symbols= helper_DocumentType.GetShortName(symbols);
				return SymbolsLineLen(fixed_symbols,len);
			};
			var DocumentTypeMigSymbolsLineTextLen= function(symbols,len)
			{
				var fixed_symbols= helper_DocumentTypeMig.GetShortName(symbols);
				return SymbolsLineLen(fixed_symbols,len);
			};
			var HousingTypeLine= function(housing_type)
			{
				return housing_type && housing_type.text && housing_type.text.length ? housing_type.text.toUpperCase() : "";
			};
			var CityOrOther= function(addr)
			{
				var res='';
				if (addr.Город && null!=addr.Город)
					res+= addr.Город;
				if (addr.НаселенныйПункт && null!=addr.НаселенныйПункт)
				{
					if (''!=res)
						res+= ' ';
					res+= addr.НаселенныйПункт;
				}
				return res;
			};
			var addDot= function(txt)
			{
				var parts= txt.split(' ');
				if (0 < parts.length && 0 < parts[0].length)
				{
					parts[0]+= '.';
				}
				return parts.join(' ');
			};
			var CityOrOtherToPrint= function(addr)
			{
				var res='';
				if (addr.Город && null!=addr.Город)
					res+= addDot(addr.Город);
				if (addr.НаселенныйПункт && null!=addr.НаселенныйПункт)
				{
					if (''!=res)
						res+= ', ';
					res+= addDot(addr.НаселенныйПункт);
				}
				return res;
			};
			var TerritoryWithSreeet= function(addr)
			{
				var res='';
				if (addr.Улица && null!=addr.Улица)
					res+= addr.Улица;
				if (addr.ГородскойРайон && null!=addr.ГородскойРайон && ''!=addr.ГородскойРайон)
				{
					if (''!=res)
						res+= ' (';
					res+= addr.ГородскойРайон + ')';
				}
				return res;
			};
			var CityOrTown= function(addr)
			{
				var res='';
				if (addr.City && null!=addr.City)
					res+= addr.City;
				if (addr.Town && null!=addr.Town)
				{
					if (''!=res)
						res+= ' ';
					res+= addr.Town;
				}
				return res;
			};
			var obj_skeleton = {
				form: this.model
				, RepresentativeLines: this.PrepareRepresentativeLinesToPrint()
				, Address_Company: this.PrepareAddresssCompanyLinesToPrint()
				, Address_CompanyN: this.PrepareAddresssToPrint(this.model.Address_CompanyN)
				, Address: this.PrepareAddresssToPrint(this.model.Address)
				, txtPreAddress: this.PreparePreAddressText()
				, Hotel_Document: this.PrepareHotelDocumentLinesToPrint()
				, dateOfEntryInfoCountry: this.PrepareDateOfEntryInfoCountry()
				, SymbolsLine: SymbolsLine
				, SymbolsLineLen: SymbolsLineLen
				, SafeTrunk: SafeTrunk
				, FixPhoneNumber: FixPhoneNumber
				, SubSymbolsLineLen: SubSymbolsLineLen
				, SymbolsLineDateLen: SymbolsLineDateLen
				, SymbolsLineDateLen_day_month: SymbolsLineDateLen_day_month
				, SymbolsLineTextLen: SymbolsLineTextLen
				, DocumentTypeSymbolsLineTextLen: DocumentTypeSymbolsLineTextLen
				, DocumentTypeMigSymbolsLineTextLen: DocumentTypeMigSymbolsLineTextLen
				, HousingTypeLine: HousingTypeLine
				, CityOrOther: CityOrOther
				, addDot: addDot
				, CityOrOtherToPrint: CityOrOtherToPrint
				, TerritoryWithSreeet: TerritoryWithSreeet
				, CityOrTown: CityOrTown
			};
			log.Debug("use template");
			var content = print_tpl(obj_skeleton);
			var codec = codec_tpl_xaml();
			var result = codec.Encode(content);
			log.Debug("BuildXamlView }");
			return result;
		};


		var DataLoad = function (content)
		{
			var dt = helper_Times.ParseXsdDate(content.notificationReceived);
			if (null != dt)
				$('#cpw_form_notificationReceived').val($.datepicker.formatDate("dd.mm.yy", dt));

			if (null == content.Document_entered || 'false' == content.Document_entered)
			{
				$('#form-tabs').tabs('disable', '#form-tab-3');
				$('#cpw_form_Document_entered').attr('checked', null);
			}
			else
			{
				$('#cpw_form_Document_entered').attr('checked', 'checked');
				$('#form-tabs').tabs('enable', '#form-tab-3');
			}

			controller.DataLoadSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content.Sex);
			controller.DataLoadSex('#cpw_form_Sex_Relation_male', '#cpw_form_Sex_Relation_female', content.Sex_Relation);
			controller.DataLoadSex('#cpw_form_Organization_Person_gender_male', '#cpw_form_Organization_Person_gender_female', content.Organization_Person_gender);

			controller.UpdateDocResidenceTypeVisibility(!content.Visa_type ? null : content.Visa_type.id);

			var visible = $('#cpw_form_RvpDocumentGivenBy').is(':visible');
			$('#cpw_form_RvpDocumentGivenBy').select2('data', content.RvpDocumentGivenBy);
			if (!visible)
				$('#s2id_cpw_form_RvpDocumentGivenBy').hide();
			$('#cpw_form_RvpDocumentGivenBy_check').attr('checked', content.RvpDocumentGivenBy_from_dict ? 'checked' : null).change();
			$('#cpw_form_RvpDocumentGivenBy_text').val(content.RvpDocumentGivenBy_text);

			var org_doc_visible = $('#cpw_form_Organization_Document_authorityOrgan').is(':visible');
			$('#cpw_form_Organization_Document_authorityOrgan').select2('data', content.Organization_Document_authorityOrgan);
			if (!org_doc_visible)
				$('#s2id_cpw_form_Organization_Document_authorityOrgan').hide();
			$('#cpw_form_Organization_Document_authorityOrgan_check').attr('checked', content.Organization_Document_authorityOrgan_check ? 'checked' : null).change();
			$('#cpw_form_Organization_Document_authorityOrgan_text').val(content.Organization_Document_authorityOrgan_text);

			$('#cpw_form_Nationality').select2('data', content.Nationality);
			$('#cpw_form_Nationality_Relation').select2('data', content.Nationality_Relation);
			$('#cpw_form_Organization_Person_nationality').select2('data', content.Organization_Person_nationality);
			$('#cpw_form_DocumentType').select2('data', content.DocumentType);
			$('#cpw_form_DocumentType_Company').select2('data', content.DocumentType_Company);
			$('#cpw_form_DocumentType_Relation').select2('data', content.DocumentType_Relation);
			$('#cpw_form_Visa_type').select2('data', content.Visa_type);
			$('#cpw_form_Visa_category').select2('data', content.Visa_category);
			$('#cpw_form_Visa_multiplicity').select2('data', content.Visa_multiplicity);
			$('#cpw_form_Visa_visitPurpose').select2('data', content.Visa_visitPurpose);
			$('#cpw_form_MigrationCard_visitPurpose').select2('data', content.MigrationCard_visitPurpose);
			$('#cpw_form_MigrationCard_kpp').select2('data', content.MigrationCard_kpp);
			$('#cpw_form_Profession').select2('data', content.Profession);
			$('#cpw_form_StateProgram').select2('data', content.StateProgram);
			$('#cpw_form_entrancePurpose').select2('data', content.entrancePurpose);
			$('#cpw_form_RvpDocumentGivenBy').select2('data', content.RvpDocumentGivenBy);
			if (null != $('#cpw_form_LastName').val() && '' == $('#cpw_form_LastName_latin').val()) {
				$('#cpw_form_LastName').change();
			}
			if (null != $('#cpw_form_FirstName').val() && '' == $('#cpw_form_FirstName_latin').val()) {
				$('#cpw_form_FirstName').change();
			}
			if (null != $('#cpw_form_MiddleName').val() && '' == $('#cpw_form_MiddleName_latin').val()) {
				$('#cpw_form_MiddleName').change();
			}
			if (null != $('#cpw_form_LastName_Relation').val() && '' == $('#cpw_form_LastName_Relation_latin').val()) {
				$('#cpw_form_LastName_Relation').change();
			}
			if (null != $('#cpw_form_FirstName_Relation').val() && '' == $('#cpw_form_FirstName_Relation_latin').val()) {
				$('#cpw_form_FirstName_Relation').change();
			}
			if (null != $('#cpw_form_MiddleName_Relation').val() && '' == $('#cpw_form_MiddleName_Relation_latin').val()) {
				$('#cpw_form_MiddleName_Relation').change();
			}
			if ('1.102.2' == $('#cpw_form_schemaVersion').val()) {
				if (content.local_stayPeriod_dateFrom && content.stayPeriod_dateFrom != content.local_stayPeriod_dateFrom) {
					content.stayPeriod_dateFrom = content.local_stayPeriod_dateFrom;
					$('#cpw_form_stayPeriod_dateFrom').val(content.stayPeriod_dateFrom);
				}
			}
			controller.ClearFieldsMigrationForDocResidenceType(controller);
			controller.ConvertOldFiasAddressesToNew();
		}

		var DataSave = function (content)
		{
			content.notificationReceived = $.datepicker.formatDate("yy-mm-dd", $('#cpw_form_notificationReceived').datepicker("getDate"));

			content.Document_entered = $('#cpw_form_Document_entered').is(':checked') ? 'true' : 'false';

			content.Sex = controller.DataSaveSex('#cpw_form_Sex_male', '#cpw_form_Sex_female');
			content.Sex_Relation = controller.DataSaveSex('#cpw_form_Sex_Relation_male', '#cpw_form_Sex_Relation_female');
			content.Organization_Person_gender = controller.DataSaveSex('#cpw_form_Organization_Person_gender_male', '#cpw_form_Organization_Person_gender_female');

			content.RvpDocumentGivenBy = $('#cpw_form_RvpDocumentGivenBy').select2('data');
			content.RvpDocumentGivenBy_from_dict = ('checked' == $('#cpw_form_RvpDocumentGivenBy_check').attr('checked'));
			content.RvpDocumentGivenBy_text = $('#cpw_form_RvpDocumentGivenBy_text').val();

			content.Organization_Document_authorityOrgan = $('#cpw_form_Organization_Document_authorityOrgan').select2('data');
			content.Organization_Document_authorityOrgan_check = ('checked' == $('#cpw_form_Organization_Document_authorityOrgan_check').attr('checked'));
			content.Organization_Document_authorityOrgan_text = $('#cpw_form_Organization_Document_authorityOrgan_text').val();

			if (!content.date)
				content.date = helper_Times.nowLocalISOStringDateTime();
			if (!content.notificationReceived)
				content.notificationReceived = $.datepicker.formatDate("yy-mm-dd", $('#cpw_form_stayPeriod_dateFrom').datepicker("getDate"));

			if (!content.Visa_entered && content.Document_entered)
				content.Visa_entered = content.Document_entered;

			if (!content.Company_DocumentStatus)
				content.Company_DocumentStatus = { id: 102877, text: 'Действительный' };

			if (!content.Organization_Document_entered)
				content.Organization_Document_entered = true;

			if (!content.headOrganization)
				content.headOrganization = "true";

			var txt_creation = helper_Times.unixDateTimeStamp() + '';

			controller.SafeFixUids(content, txt_creation, ['number', 'uid', 'requestId',
			'person_uid', 'person_personId', 'DocumentRelation_uid', 'Document_uid',
			'Visa_uid', 'MigrationCard_uid', 'Hotel_organization_uid', 'Organization_Person_uid',
			'Organization_Document_uid', 'Organization_Person_personId']);

			if (!content.notificationNumber || null == content.notificationNumber)
			{
				var t = helper_Times.safeDateTime();
				var year = (t.getFullYear() + '').substring(2, 4);
				var tt = txt_creation.substring(txt_creation.length - 6, txt_creation.length);
				content.notificationNumber = '02/' + content.hotel_id + '/' + year + '/' + tt;
			}

			if ('1.102.2' == $('#cpw_form_schemaVersion').val()) {
				content.local_stayPeriod_dateFrom = content.stayPeriod_dateFrom;
				if (content.MigrationCard_dateFrom && content.MigrationCard_dateFrom != content.stayPeriod_dateFrom) {
					content.stayPeriod_dateFrom = content.MigrationCard_dateFrom;
				}
			}
		}

		var old_USSR_countries=
			{
				"RUS": "Россия"
				,"UKR":"Украина"
				,"BLR":"Белоруссия"

				,"LVA":"Латвия"
				,"LTU":"Литва"
				,"EST":"Эстония"

				,"GEO":"Грузия"
				,"ARM":"Армения"
				,"AZE":"Республика Азербайджан"

				,"KGZ":"Кыргызстан"
				,"KAZ":"Республика Казахстан"
				,"TKM":"Туркменистан"

				,"TJK":"Республика Таджикистан"
				,"UZB":"Республика Узбекистан"

				,"MDA":"Республика Молдова"
			};


		var base_Render = controller.Render;
		controller.Render = function (form_div_selector)
		{
			base_Render.call(this, form_div_selector);
			log.Debug('Render {');

			$('.migration-card-info').hide();

			UpdateFieldsForSchema_1_102_2(getSchemaVersionByModel(this));

			$('#cpw_form_LastName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_FirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_MiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$('#cpw_form_LastName_Relation').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_FirstName_Relation').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_MiddleName_Relation').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$('#cpw_form_Organization_Person_lastName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_Organization_Person_firstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_Organization_Person_middleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$("#cpw_form_Hotel_organization_inn").keydown(helper_validate.ValidateSymbolsINN);

			$('#cpw_form_RvpDocumentGivenBy').select2({
				placeholder: ''
				, allowClear: true
				, dropdownCssClass: "bigdrop"
				, query: function (q) { q.callback({results: h_dictionary.FindByNameOrCode(fms, q.term) }); }
				, escapeMarkup: function (m) { return m; }
				, formatResult: formatResult
				, formatSelection: formatSelection
			});

			$('#cpw_form_RvpDocumentGivenBy_check').change(function (e)
			{
				e.preventDefault();
				var checked= $(this).attr('checked');
				if ('checked'==checked)
				{
					$('#s2id_cpw_form_RvpDocumentGivenBy').show();
					$('#cpw_form_RvpDocumentGivenBy_text').hide();
				}
				else
				{
					$('#s2id_cpw_form_RvpDocumentGivenBy').hide();
					$('#cpw_form_RvpDocumentGivenBy_text').show();
				}
			});

			$('#cpw_form_Organization_Document_authorityOrgan').select2({
				placeholder: ''
				, allowClear: true
				, dropdownCssClass: "bigdrop"
				, query: function (q) { q.callback({ results: h_dictionary.FindByNameOrCode(fms, q.term) }); }
				, escapeMarkup: function (m) { return m; }
				, formatResult: formatResult
				, formatSelection: formatSelection
			});

			$('#cpw_form_Organization_Document_authorityOrgan_check').change(function (e) {
				e.preventDefault();
				var checked = $(this).attr('checked');
				if ('checked' == checked) {
					$('#s2id_cpw_form_Organization_Document_authorityOrgan').show();
					$('#cpw_form_Organization_Document_authorityOrgan_text').hide();
				}
				else {
					$('#s2id_cpw_form_Organization_Document_authorityOrgan').hide();
					$('#cpw_form_Organization_Document_authorityOrgan_text').show();
				}
			});

			$('#cpw_form_Document_entered').change(function (e)
			{
				e.preventDefault();
				var checked = 'checked' == $(this).attr('checked');
				$('#form-tabs').tabs(checked ? 'enable' : 'disable', '#form-tab-3');
			});

			var content = this.model;

			/*var controller_Upload = controller_UploadFile(content.DocumentScan, 'Загрузка файла', UpdateUploadsLinksTextes, this.model.Document_uid);
			var controller_UploadVisa = controller_UploadFile(content.DocumentScan, 'Загрузка файла', UpdateUploadsLinksTextes, this.model.Visa_uid);
			$('#cpw_form_Upload').click(controller_Upload.ShowModal);
			$('#cpw_form_UploadVisa').click(controller_UploadVisa.ShowModal);*/

			$('.input-datepicker').datepicker(helper_Times.DatePickerOptions);
			$('.input-datepicker-birthday').datepicker(helper_Times.DatePickerOptionsPrevious);
			$('.input-datepicker').change(helper_validate.OnChangeDateInput_FixFormat);
			$('.input-datepicker-birthday').change(helper_validate.OnChangeDateInput_FixFormat);

			$('#cpw_form_stayPeriod_dateFrom').on('change', function (e) {
				var text_stayPeriod_dateFrom = $('#cpw_form_stayPeriod_dateFrom').val();
				$('#cpw_form_notificationReceived').val(text_stayPeriod_dateFrom);
			});

			var self = this;
			$('#cpw_form_Visa_type').on('select2-selecting', function (evt) {
				self.ClearFieldsForDocResidenceType($('#cpw_form_Visa_type').val());
			});
			$('#cpw_form_Visa_type').on('select2-selected', function (evt) {
				self.ClearFieldsMigrationForDocResidenceType(self);
			});
			$('#cpw_form_Visa_type').on('change', function (e) {
				e.preventDefault();
				self.UpdateDocResidenceTypeVisibility($('#cpw_form_Visa_type').val());
				self.AfterAddedDocument();
			});

			$('#cpw_form_DocumentType_Relation').on('change', function (e)
			{
				e.preventDefault();
				self.AfterAddedDocument();
			});

			$('#cpw_form_MigrationCard_series').on('change', function (e)
			{
				e.preventDefault();
				self.AfterAddedDocument();
			});

			$('#cpw_form_MigrationCard_number').on('change', function (e)
			{
				e.preventDefault();
				self.AfterAddedDocument();
			});

			var set_birthplace_country_by_citizenship= function(birthplace,country,birthday_sel)
			{
				if (birthplace && (!birthplace.Country || birthplace.Country == "") &&
					country && country.id && "L-B-G"!=country.id) // Лицо без гражданства..
				{
					if ("N-G-L" == country.id) // Негражданин Латвии
						country = { id: "LVA", text: "Латвия" };
					if ("N-G-E" == country.id) // Негражданин Эстонии
						country = { id: "EST", text:"Эстония" };
					if (old_USSR_countries[country.id])
					{
						var old_txtdt = $(birthday_sel).val();
						if ('' != old_txtdt)
						{
							var dt = helper_Times.ParseRussianDate(old_txtdt);
							var bound_dt = new Date(1991, 11, 26);
							if (dt < bound_dt)
							{
								country = { id: 'SUN', text: 'СССР' };
							}
						}
					}
					birthplace.Country = country;
				}
			};

			$('#cpw_form_Nationality').on('change', function (e)
			{
				set_birthplace_country_by_citizenship(content.Birthplace, $(this).select2('data'), '#cpw_form_Birthday');
			});
			$('#cpw_form_Nationality_Relation').on('change', function (e)
			{
				set_birthplace_country_by_citizenship(content.Birthplace_Relation, $(this).select2('data'), '#cpw_form_Birthday_Relation');
			});
			var self = this;
			$('#cpw_form_Organization_Person_nationality').on('change', function (e)
			{
				var nationality = $(this).select2('data');
				if (content.Organization_Person_birthPlace && (!content.Organization_Person_birthPlace.Country || content.Organization_Person_birthPlace.Country == ""))
				{
					content.Organization_Person_birthPlace.Country = nationality;
					content.Organization_Person_birthPlace.text = helper_Town.ToReadableString(content.Organization_Person_birthPlace);
					$('#cpw_form_Organization_Person_birthPlace').text(!content.Organization_Person_birthPlace.text ? null_text : content.Organization_Person_birthPlace.text);
				}
				self.OnUpdate_Organization_Person_nationality(nationality);
			});
			self.OnUpdate_Organization_Person_nationality(content.Organization_Person_nationality);

			$('.encoding_date').change(helper_Names.OnNameChange_NormalizeEncoding);

			helper_InputMask.BindDefaultMasks();

			$('input').first().focus();

			$('#form-tabs').tabs();
			helper_Tabs.bindSwitch($('#form-tabs'), 'li', '#form-tab');

			DataLoad(this.model);
			log.Debug('Render }');
		}

		var getSchemaVersionByModel = function (self) {
			var methodGetSchemaVersion = self.UsedCodecInfo.Codec.codec_xml && self.UsedCodecInfo.Codec.codec_xml.GetSchemaVersionByModel ?
				self.UsedCodecInfo.Codec.codec_xml.GetSchemaVersionByModel :
				self.UsedCodecInfo.previous.CodecInfo.Codec.codec_xml.GetSchemaVersionByModel;
			return methodGetSchemaVersion(self.model);
		}

		var UpdateFieldsForSchema_1_102_2 = function (schemaVersion) {
			if ('1.102.2' == schemaVersion) {
				$('div.org-doc-givenby').show();
				$('#cpw_form_schemaVersion').val(schemaVersion);
			} else {
				$('div.org-doc-givenby').hide();
			}
		}

		controller.AfterAddedDocument = function() { }
		controller.AfterClearDocument = function (idDocument) { }
		controller.CheckDocumentNotEmpty = function (idDocument) {}

		var ShowModalDialogConfirm = function (message, delay, callback) {
			var timerId = setInterval(function () {
				if ($('#cpw-msgbox').dialog('isOpen') == true) {
					return;
				}
				clearInterval(timerId);
				h_msgbox.ShowModal({
					title: 'Предупреждение'
						, html: '<pre style="width: 700px; white-space: pre-wrap;">' + message + '</pre>'
						, width: 750
						, buttons: ['Да, удалить', 'Нет, не удалять']
						, onclose: function (bname) {
							if ('Да, удалить' == bname) {
								callback();
							}
						}
				});
			}, delay);
		}

		var CheckDocResidenceTypeFields = function (id_docResidence_Type)
		{
			if ('139356' == id_docResidence_Type)
			{
				return '' != $('#cpw_form_Visa_series').val() || '' != $('#cpw_form_Visa_number').val() || '' != $('#cpw_form_Visa_identifier').val()
					|| '' != $('#cpw_form_Visa_issued').val() || '' != $('#cpw_form_Visa_validFrom').val() || '' != $('#cpw_form_Visa_validTo').val()
					|| '' != $('#cpw_form_Visa_category').val() || '' != $('#cpw_form_Visa_visitPurpose').val() || '' != $('#cpw_form_Visa_multiplicity').val();
			}
			if ('139372' == id_docResidence_Type)
			{
				return '' != $('#cpw_form_Visa_series').val() || '' != $('#cpw_form_Visa_number').val()
					|| '' != $('#cpw_form_Visa_issued').val() || '' != $('#cpw_form_Visa_validTo').val();
			}
			if ('139373' == id_docResidence_Type || '135717' == id_docResidence_Type)
			{
				return '' != $('#cpw_form_LivingDocDecisionNumber').val() || '' != $('#cpw_form_LivingDocDecisionDate').val()
					|| '' != $('#cpw_form_RvpVisa_validTo').val() || '' != $('#cpw_form_RvpDocumentGivenBy').val()
					|| '' != $('#cpw_form_RvpDocumentGivenBy_text').val();
			}
			if ('135709' == id_docResidence_Type || '135710' == id_docResidence_Type || '139387' == id_docResidence_Type)
			{
				return '' != $('#cpw_form_Visa_series').val() || '' != $('#cpw_form_Visa_number').val()
					|| '' != $('#cpw_form_Visa_issued').val() || '' != $('#cpw_form_Visa_validTo').val()
					|| '' != $('#cpw_form_LivingDocDecisionNumber').val() || '' != $('#cpw_form_LivingDocDecisionDate').val()
					|| '' != $('#cpw_form_RvpDocumentGivenBy').val() || '' != $('#cpw_form_RvpDocumentGivenBy_text').val();
			}
		}

		controller.ClearFieldsForDocResidenceType = function (id_docResidence_Type)
		{
			var getTypeText = function (idType)
			{
				var item = helper_Select2.FindById(helper_VisaType.GetValues(), idType)
				if (item)
					return item.text;
				return null;
			}
			var docResidence_Text = getTypeText(id_docResidence_Type);
			var needClear = CheckDocResidenceTypeFields(id_docResidence_Type);
			if (!needClear)
				return;

			var message = '';
			if ('139373' == id_docResidence_Type || '135717' == id_docResidence_Type)
			{
				message = 'Вы хотите удалить все сведения о документе "' + docResidence_Text + '"?';
			}
			else if (docResidence_Text != null)
			{
				message = 'Вы хотите удалить все сведения о документе "' + docResidence_Text + '"';
				if ('' != $('#cpw_form_Visa_series').val())
					message += ', серия ' + $('#cpw_form_Visa_series').val();
				if ('' != $('#cpw_form_Visa_number').val())
					message += ' №' + $('#cpw_form_Visa_number').val();
				message += '?';
			}

			if ('' != message) {
				ShowModalDialogConfirm(message, 0, function() {
					if ('139356' == id_docResidence_Type) {
						$('div.doc-residence-type-common input').val('');
						$('div.doc-residence-type-visa input').val('');
						$('#cpw_form_Visa_category').select2('data', null);
						$('#cpw_form_Visa_visitPurpose').select2('data', null);
						$('#cpw_form_Visa_multiplicity').select2('data', null);
					}
					if ('139372' == id_docResidence_Type) {
						$('div.doc-residence-type-common input').val('');
					}
					if ('139373' == id_docResidence_Type || '135717' == id_docResidence_Type) {
						$('div.doc-residence-type-rvp input').val('');
						$('div.doc-residence-type-rvp textarea').val('');
						$('#cpw_form_RvpDocumentGivenBy').select2('data', null);
					}
					if ('135709' == id_docResidence_Type || '135710' == id_docResidence_Type || '139387' == id_docResidence_Type) {
						$('div.doc-residence-type-common input').val('');
						$('div.doc-residence-type-vnz input').val('');
						$('div.doc-residence-type-vnz textarea').val('');
						$('#cpw_form_RvpDocumentGivenBy').select2('data', null);
					}
				});
			}
		}

		var ReadonlyMigrationFields = function ()
		{
			$('#cpw_form_MigrationCard_series').closest('.row').hide()
			$('#cpw_form_MigrationCard_kpp').closest('.row').hide();
			$('.migration-card-info').show();
		}

		var UnReadonlyMigrationFields = function ()
		{
			$('#cpw_form_MigrationCard_series').closest('.row').show();
			$('#cpw_form_MigrationCard_kpp').closest('.row').show();
			$('.migration-card-info').hide();
		}

		controller.ClearFieldsMigrationForDocResidenceType = function (self)
		{
			var getTypeText = function (idType)
			{
				var item = helper_Select2.FindById(helper_VisaType.GetValues(), idType);
				return item ? item.text : null;
			}
			var id_docResidence_Type = $('#cpw_form_Visa_type').val();
			var docResidence_Text = getTypeText(id_docResidence_Type);

			var migrationNotEmpty = '' != $('#cpw_form_MigrationCard_series').val() || '' != $('#cpw_form_MigrationCard_number').val()
				|| '' != $('#cpw_form_MigrationCard_dateFrom').val() || '' != $('#cpw_form_MigrationCard_dateTo').val()
				|| null != $('#cpw_form_MigrationCard_kpp').select2('data') || null != $('#cpw_form_MigrationCard_visitPurpose').select2('data')
				|| self.CheckDocumentNotEmpty(helper_DocumentTypeMig.migrationnType);
			var documentTypes = helper_DocumentTypeMig.forbiddenVisaTypeMigrationn;

			if (migrationNotEmpty && "" != id_docResidence_Type && -1 < documentTypes.indexOf(id_docResidence_Type))
			{
				var message = 'В случае если документом, разрешающим пребывание (проживание) гражданина на территории РФ, является "' +
					docResidence_Text + '", миграционная карта не требуется.<br><br>Удалить ранее введенные сведения о миграционной карте?';
				ShowModalDialogConfirm(message, 10, function() {
					$('#cpw_form_MigrationCard_series').val('').change();
					$('#cpw_form_MigrationCard_number').val('').change();
					$('#cpw_form_MigrationCard_dateFrom').val('').change();
					$('#cpw_form_MigrationCard_dateTo').val('').change();
					$('#cpw_form_MigrationCard_kpp').select2('data', null).change();
					$('#cpw_form_MigrationCard_visitPurpose').select2('data', null).change();
					ReadonlyMigrationFields();
					self.AfterClearDocument(helper_DocumentTypeMig.migrationnType);
				});
				return true;
			}
			if (!migrationNotEmpty && "" != id_docResidence_Type && -1 < documentTypes.indexOf(id_docResidence_Type))
			{
				ReadonlyMigrationFields();
			}
			else
			{
				UnReadonlyMigrationFields();
			}
			return false;
		}

		controller.UpdateDocResidenceTypeVisibility = function (id_docResidence_Type) {
			if ('139356' == id_docResidence_Type) // виза..
			{
				$('div.doc-residence-type-visa').show();
			}
			else
			{
				$('div.doc-residence-type-visa').hide();
			}
			
			if ('139373' == id_docResidence_Type || // Разрешение на временное проживание ИГ
				'135717' == id_docResidence_Type) // Разрешение на временное проживание ЛБГ
			{
				$('div.doc-residence-type-common').hide();
				$('div.doc-residence-type-rvp').show();
			}
			else
			{
				$('div.doc-residence-type-rvp').hide();
				$('div.doc-residence-type-common').show();
			}

			if ('135709' == id_docResidence_Type || // Вид на жительство ИГ
				'135710' == id_docResidence_Type || // Вид на жительство ЛБГ
				'139387' == id_docResidence_Type)   // Вид на жительство ЛБГ (с биометрией)
			{
				$('div.doc-residence-type-rvp').hide();
				$('div.doc-residence-type-common').show();
				$('div.doc-residence-type-vnz').show();
			}
		}

		controller.OnUpdate_Organization_Person_nationality = function (nationality)
		{
			if (!nationality || !nationality.id || 'RUS' == nationality.id)
			{
				$('.foreign-organization-person').addClass('invisible');
			}
			else
			{
				$('.foreign-organization-person').removeClass('invisible');
				$('#cpw_form_Organization_Person_lastName').change();
				$('#cpw_form_Organization_Person_firstName').change();
				$('#cpw_form_Organization_Person_middleName').change();
			}
		}

		var RemoveHadFocus = function ()
		{
			$('#cpw_form_Nationality').parent().removeClass('had_focus');
			$('#cpw_form_DocumentType').parent().removeClass('had_focus');
			$('#cpw_form_DocumentGivenBy_check').parent().parent().removeClass('had_focus');
		}

		controller.CreateNew = function (form_div_selector)
		{
			this.SafeCreateNewContent();

			if (!this.model.notificationReceived)
				this.model.notificationReceived = helper_Times.nowDateUTC('d');

			if (!this.model.DocumentType_Company)
				this.model.DocumentType_Company = { id: 103008, text: "Паспорт гражданина Российской Федерации" };

			this.Render(form_div_selector);

			if (!this.model.DocumentType)
				$('#cpw_form_DocumentType').select2('data', { id: 103009, text: "Национальный заграничный паспорт" });

			if (!this.model.StateProgram)
				$('#cpw_form_StateProgram').select2('data', { id: 1019, text: "Нет" });

			RemoveHadFocus();
			this.BeforeEditValidation();
		};

		controller.Edit = function (form_div_selector)
		{
			this.Render(form_div_selector);
			RemoveHadFocus();
			this.BeforeEditValidation();
			this.Validate();
			waitClearMigration = false;
		};

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return attrshtml_tpl({ content: this.model, helper_Times: helper_Times });
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			if (this.SignedContent)
			{
				return this.SignedContent;
			}
			else
			{
				this.model = base_GetFormContent.call(this);
				log.Debug('GetFormContent() {');
				DataSave(this.model);
				log.Debug('GetFormContent() }');
				return this.model;
			}
		};

		controller.GetShortFormContent = function()
		{
			log.Debug("GetShortFormContent {");
			var sortContent = {
				LastName: this.model.LastName,
				FirstName: this.model.FirstName,
				MiddleName: this.model.MiddleName,
				LastName_latin: this.model.LastName_latin,
				FirstName_latin: this.model.FirstName_latin,
				MiddleName_latin: this.model.MiddleName_latin,
				Gender: this.model.Sex,
				BirthDate: this.model.Birthday,
				Citizenship: this.model.Nationality,
				DocumentPersonalType: this.model.DocumentType,
				DocumentPersonalSerial: this.model.DocumentSeries,
				DocumentPersonalNumber: this.model.DocumentNumber,
				DateStart: this.model.stayPeriod_dateFrom,
				DateFinish: this.model.stayPeriod_dateTo,
				DocumentResidenceType: this.model.Visa_type,
				DocumentResidenceSeries: this.model.Visa_series,
				DocumentResidenceNumber: this.model.Visa_number,
				VisitPurpose: (this.model.Visa_visitPurpose ? this.model.Visa_visitPurpose :
					(this.model.MigrationCard_visitPurpose ? this.model.MigrationCard_visitPurpose : this.model.entrancePurpose))
			};
			log.Debug("GetShortFormContent }");
			return sortContent;
		}

		controller.Sign = function ()
		{
			var content = this.GetFormContent();
			if (null != content)
			{
				var wax = CPW_Singleton();
				if (wax)
				{
					var pos = this.model.PositionForSignature;
					if (null == pos)
					{
						this.SignedContent = content;
					}
					else
					{
						var signature = wax.SignXml(content);
						this.SignedContent = content.substring(0, pos)
							+ signature
							+ content.substring(pos, content.length);
					}
				}
			}
		};

		controller.SetFormContent = function (content)
		{
			if (content.Nationality && !content.Nationality.text && content.Nationality.id)
			{
				var item = helper_Select2.FindById(h_citizenship.GetValues(), content.Nationality.id)
				if (item)
					content.Nationality.text = item.text;
			}
			if (content.Nationality_Relation && !content.Nationality_Relation.text && content.Nationality_Relation.id)
			{
				var item = helper_Select2.FindById(h_citizenship.GetValues(), content.Nationality_Relation.id)
				if (item)
					content.Nationality_Relation.text = item.text;
			}
			if (content.Organization_Person_nationality && !content.Organization_Person_nationality.text && content.Organization_Person_nationality.id)
			{
				var item = helper_Select2.FindById(h_citizenship.GetValues(), content.Organization_Person_nationality.id)
				if (item)
					content.Organization_Person_nationality.text = item.text;
			}
			if (content.DocumentType && !content.DocumentType.text && content.DocumentType.id)
			{
				var item = helper_Select2.FindById(helper_DocumentType.GetValues(), content.DocumentType.id)
				if (item)
					content.DocumentType.text = item.text;
			}
			if (content.DocumentType_Company && !content.DocumentType_Company.text && content.DocumentType_Company.id)
			{
				var item = helper_Select2.FindById(helper_DocumentType.GetValues(), content.DocumentType_Company.id)
				if (item)
					content.DocumentType_Company.text = item.text;
			}
			if (content.DocumentType_Relation && !content.DocumentType_Relation.text && content.DocumentType_Relation.id)
			{
				var item = helper_Select2.FindById(helper_DocumentType.GetValues(), content.DocumentType_Relation.id)
				if (item)
					content.DocumentType_Relation.text = item.text;
			}
			if (content.Visa_type && !content.Visa_type.text && content.Visa_type.id)
			{
				var item = helper_Select2.FindById(helper_VisaType.GetValues(), content.Visa_type.id)
				if (item)
					content.Visa_type.text = item.text;
			}
			if (content.Visa_category && !content.Visa_category.text && content.Visa_category.id)
			{
				var item = helper_Select2.FindById(helper_visaCategory.GetValues(), content.Visa_category.id)
				if (item)
					content.Visa_category.text = item.text;
			}
			if (content.Visa_multiplicity && !content.Visa_multiplicity.text && content.Visa_multiplicity.id)
			{
				var item = helper_Select2.FindById(helper_visaMultiplicity.GetValues(), content.Visa_multiplicity.id)
				if (item)
					content.Visa_multiplicity.text = item.text;
			}
			if (content.Visa_visitPurpose && !content.Visa_visitPurpose.text && content.Visa_visitPurpose.id)
			{
				var item = helper_Select2.FindById(helper_entryGoal.GetValues(), content.Visa_visitPurpose.id)
				if (item)
					content.Visa_visitPurpose.text = item.text;
			}
			if (content.MigrationCard_visitPurpose && !content.MigrationCard_visitPurpose.text && content.MigrationCard_visitPurpose.id)
			{
				var item = helper_Select2.FindById(helper_Arrivals.GetValues(), content.MigrationCard_visitPurpose.id)
				if (item)
					content.MigrationCard_visitPurpose.text = item.text;
			}
			if (content.MigrationCard_kpp && !content.MigrationCard_kpp.text && content.MigrationCard_kpp.id)
			{
				var item = helper_Select2.FindById(helper_MigKpp.GetValues(), content.MigrationCard_kpp.id)
				if (item)
					content.MigrationCard_kpp.text = item.text;
			}
			if (content.Profession && !content.Profession.text && content.Profession.id)
			{
				var item = helper_Select2.FindById(helper_Profession.GetValues(), content.Profession.id)
				if (item)
					content.Profession.text = item.text;
			}
			if (content.StateProgram && !content.StateProgram.text && content.StateProgram.id)
			{
				var item = helper_Select2.FindById(helper_StateProgram.GetValues(), content.StateProgram.id)
				if (item)
					content.StateProgram.text = item.text;
			}
			if (content.entrancePurpose && !content.entrancePurpose.text && content.entrancePurpose.id)
			{
				var item = helper_Select2.FindById(helper_Arrivals.GetValues(), content.entrancePurpose.id)
				if (item)
					content.entrancePurpose.text = item.text;
			}
			if (content.RvpDocumentGivenBy && !content.RvpDocumentGivenBy.row && content.RvpDocumentGivenBy.id)
			{
				var item = h_dictionary.FindById(fms, content.RvpDocumentGivenBy.id)
				if (item)
					content.RvpDocumentGivenBy.row = item.row;
			}
			if (content.subdivision && !content.subdivision.text && content.subdivision.id)
			{
				var item = h_dictionary.FindFirstByField(fms, 'ID', content.subdivision.id)
				if (item)
					content.subdivision.text = item.row.NAME;
			}
			if (content.sono && !content.sono.text && content.sono.id)
			{
				var item = h_dictionary.FindFirstByField(fns, 'ID', content.sono.id)
				if (item)
					content.sono.text = item.row.NAME;
			}
			if (content.Organization_Document_authorityOrgan && !content.Organization_Document_authorityOrgan.row && content.Organization_Document_authorityOrgan.id) {
				var item = h_dictionary.FindById(fms, content.Organization_Document_authorityOrgan.id)
				if (item) {
					content.Organization_Document_authorityOrgan.row = item.row;
				}
			}
			this.model = content;
			return null;
		};

		controller.CopyFormContent = function (form_content)
		{
			if (form_content) {
				var dateNow = new Date();
				this.model.LastName = form_content.LastName;
				this.model.LastName_latin = form_content.LastName_latin;
				this.model.FirstName = form_content.FirstName;
				this.model.FirstName_latin = form_content.FirstName_latin;
				this.model.MiddleName = form_content.MiddleName;
				this.model.Sex = form_content.Sex;
				this.model.Birthday = form_content.Birthday;
				this.model.Birthplace = form_content.Birthplace;
				this.model.Nationality = form_content.Nationality;
				if (h_constraints.check_PeriodDateNow(form_content.DocumentValidFrom, form_content.DocumentFinishDate))
				{
					this.model.DocumentType = form_content.DocumentType;
					this.model.DocumentSeries = form_content.DocumentSeries;
					this.model.DocumentNumber = form_content.DocumentNumber;
					this.model.DocumentGivenDate = form_content.DocumentGivenDate;
					this.model.DocumentValidFrom = form_content.DocumentValidFrom;
					this.model.DocumentFinishDate = form_content.DocumentFinishDate;
				}

				this.model.Document_entered = form_content.Document_entered;

				this.model.LastName_Relation = form_content.LastName_Relation;
				this.model.LastName_Relation_latin = form_content.LastName_Relation_latin;
				this.model.FirstName_Relation = form_content.FirstName_Relation;
				this.model.FirstName_Relation_latin = form_content.FirstName_Relation_latin;
				this.model.MiddleName_Relation = form_content.MiddleName_Relation;
				this.model.Sex_Relation = form_content.Sex_Relation;
				this.model.Birthday_Relation = form_content.Birthday_Relation;
				this.model.Birthplace_Relation = form_content.Birthplace_Relation;
				this.model.Nationality_Relation = form_content.Nationality_Relation;
				if (h_constraints.check_PeriodDateNow(form_content.DocumentGivenDate_Relation, form_content.DocumentFinishDate_Relation))
				{
					this.model.DocumentType_Relation = form_content.DocumentType_Relation;
					this.model.DocumentSeries_Relation = form_content.DocumentSeries_Relation;
					this.model.DocumentNumber_Relation = form_content.DocumentNumber_Relation;
					this.model.DocumentGivenDate_Relation = form_content.DocumentGivenDate_Relation;
					this.model.DocumentFinishDate_Relation = form_content.DocumentFinishDate_Relation;
				}

				this.model.Profession = form_content.Profession;
				this.model.StateProgram = form_content.StateProgram;
				this.model.stayPeriod_dateFrom = form_content.stayPeriod_dateFrom;
				this.model.stayPeriod_dateTo = form_content.stayPeriod_dateTo;
				this.model.local_stayPeriod_dateFrom = form_content.local_stayPeriod_dateFrom;
				this.model.entrancePurpose = form_content.entrancePurpose;
				this.model.notificationReceived = form_content.notificationReceived;
				this.model.PreAddress = form_content.PreAddress;

				if (h_constraints.check_PeriodDateNow(form_content.Visa_validFrom, form_content.Visa_validTo))
				{
					this.model.Visa_type = form_content.Visa_type;
					this.model.Visa_series = form_content.Visa_series;
					this.model.Visa_number = form_content.Visa_number;
					this.model.Visa_issued = form_content.Visa_issued;
					this.model.Visa_validFrom = form_content.Visa_validFrom;
					this.model.Visa_validTo = form_content.Visa_validTo;
					this.model.Visa_category = form_content.Visa_category;
					this.model.Visa_multiplicity = form_content.Visa_multiplicity;
					this.model.Visa_identifier = form_content.Visa_identifier;
					this.model.Visa_visitPurpose = form_content.Visa_visitPurpose;
				}

				this.model.LivingDocDecisionNumber = form_content.LivingDocDecisionNumber;
				this.model.LivingDocDecisionDate = form_content.LivingDocDecisionDate;
				this.model.RvpVisa_validTo = form_content.RvpVisa_validTo;
				this.model.RvpDocumentGivenBy = form_content.RvpDocumentGivenBy;
				this.model.RvpDocumentGivenBy_from_dict = form_content.RvpDocumentGivenBy_from_dict;
				this.model.RvpDocumentGivenBy_text = form_content.RvpDocumentGivenBy_text;

				if (h_constraints.check_PeriodDateNow(form_content.MigrationCard_dateFrom, form_content.MigrationCard_dateTo))
				{
					this.model.MigrationCard_series = form_content.MigrationCard_series;
					this.model.MigrationCard_number = form_content.MigrationCard_number;
					this.model.MigrationCard_dateFrom = form_content.MigrationCard_dateFrom;
					this.model.MigrationCard_dateTo = form_content.MigrationCard_dateTo;
					this.model.MigrationCard_kpp = form_content.MigrationCard_kpp;
					this.model.MigrationCard_visitPurpose = form_content.MigrationCard_visitPurpose;
				}
			}
		}

		controller.PrepareValidationRuleForProfileFields = function ()
		{
			var content = this.model;
			var rule = {
				handler: helper_validate.RequiredValues,
				attributes:
				[
					{ element: content.supplierInfo, title: 'Идентификатор поставщика' },
					{ element: content.employee.ummsId, title: 'Идентификатор пользователя' },
					{ element: content.subdivision, title: 'Территориальный орган / структурное подразделение ФМС' },
					{ element: content.sono, title: 'Налоговый орган' }
				]
			};
			return rule;
		}

		var TryToFindValidationErrors = function (rules)
		{
			if (rules)
			{
				var res_validation_text = null;
				for (var i = 0; i < rules.length; i++)
				{
					var rule = rules[i];
					if (rule && rule.attributes)
					{
						for (var j = 0; j < rule.attributes.length; j++)
						{
							var attribute = rule.attributes[j];
							var element =
								('object' != typeof attribute) ? attribute :
								attribute.selector ? $(attribute.selector) :
								attribute.element;
							var title;
							if ('object' != typeof attribute)
							{
								title = 'непоименованный элемент';
							}
							else
							{
								try
								{
									title = attribute.title ? attribute.title : element.attr('id');
								}
								catch (ex)
								{
									title = 'непоименованный элемент';
								}
							}
							if (!rule.handler || !rule.handler(element))
								res_validation_text = controller.PrepareValidationElementText(res_validation_text, title);
						}
					}
				}
			}
			return res_validation_text;
		}

		controller.TryToFindValidationErrorsAddresses = function ()
		{
			var res_validation_text = '';
			if (!this.model.Address || !this.model.Address.Country) {
				res_validation_text += 'Адрес места пребывания не указан в профиле!';
			}
			if (!this.model.Address_Company || !this.model.Address_Company.Country) {
				res_validation_text += '' == res_validation_text ? '' : '\r\n'
				res_validation_text += 'Фактический адрес не указан в профиле!';
			}
			if (!this.model.Address_CompanyN || !this.model.Address_CompanyN.Country) {
				res_validation_text += '' == res_validation_text ? '' : '\r\n'
				res_validation_text += 'Адрес прописки сотрудника не указан в профиле!';
			}
			if (!this.model.subdivision || !h_addr.only_free_address_by_substitusion_id(this.model.subdivision.id))
			{
				if (this.model.Address && this.model.Address.russianAddress && !this.model.Address.russianAddress.variant_fias)
				{
					this.model.Address = null;
					res_validation_text += '' == res_validation_text ? '' : '\r\n'
					res_validation_text += 'Адрес места пребывания необходимо заполнить по справочнику ФИАС!';
				}
				if (this.model.Address_Company && this.model.Address_Company.russianAddress && !this.model.Address_Company.russianAddress.variant_fias)
				{
					this.model.Address_Company = null;
					res_validation_text += '' == res_validation_text ? '' : '\r\n'
					res_validation_text += 'Фактический адрес необходимо заполнить по справочнику ФИАС!';
				}
				if (this.model.Address_CompanyN && this.model.Address_CompanyN.russianAddress && !this.model.Address_CompanyN.russianAddress.variant_fias)
				{
					this.model.Address_CompanyN = null;
					res_validation_text += '' == res_validation_text ? '' : '\r\n'
					res_validation_text += 'Адрес прописки сотрудника необходимо заполнить по справочнику ФИАС!';
				}
			}
			UpdateAddressesLinksTextes(this.model);
			return res_validation_text;
		}

		controller.BeforeEditValidation = function ()
		{
			var rules = [this.PrepareValidationRuleForProfileFields()];
			var res_txt = '';
			var errors = TryToFindValidationErrors(rules);
			if (null != errors)
				res_txt = helper_validate.ProfileErrorText + ':\r\n\r\n     ' + errors;
			errors = this.TryToFindValidationErrorsAddresses();
			if ('' != errors)
				res_txt += '' == res_txt ? errors : '\r\n\r\n' + errors;
			if (null != res_txt && '' != res_txt)
			{
				h_msgbox.ShowModal({
					title: "Недостаточно заполненный профиль для заполнения формы!"
					, html: '<pre>' + res_txt + '</pre>'
					, width: 800
				});
			}
		}

		controller.PrepareValidationElementText = function (txt, txt_to_add)
		{
			if (null == txt)
			{
				return txt_to_add;
			}
			else
			{
				return txt + ',\r\n     ' + txt_to_add;
			}
		}

		var waitClearMigration = false;
		controller.Validate = function ()
		{
			if (this.binding.CurrentCollectionIndex >= 0)
			{
				return null;
			}
			else
			{
				if (!waitClearMigration)
				{
					waitClearMigration = this.ClearFieldsMigrationForDocResidenceType(this);
					if (waitClearMigration)
					{
						return 'wait_while_open_dialogs';
					}
				}
				waitClearMigration = false;
				return this.binding.Validate(this.binding.form_div_selector);
			}
		};

		controller.UseCodec(codec());

		log.Debug('Create }');
		return controller;
	}
});
