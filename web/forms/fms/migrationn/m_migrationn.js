define([
	  'forms/fms/base/town/m_town'
],function (TownAddress)
{
	return function ()
	{
		var res =
		{
			// нередактируемые атрибуты {
			uid: null,
			requestId: null,
			supplierInfo: null,
			subdivision: null, // {id:'', text:''}
			sono: null, // {id:'', text:''},
			authority: '',
			employee: { ummsId: null },
			date: null, // 'ДД.ММ.ГГГГ' // Дата ввода в систему
			notificationReceived: null, // 'ДД.ММ.ГГГГ'
			// нередактируемые атрибуты }

			RvpDocumentGivenBy_from_dict : false,

			FirstName : '',
			LastName : '',
			MiddleName : '',

			FirstName_latin : '',
			LastName_latin : '',
			MiddleName_latin: '',

			person_uid: null, // Уникальный идентификатор
			person_personId: null, // Идентификатор физического лица, связанного с персональными данными
			number: null,

			DocumentRelation_uid: null,
			RelationType: '',
			DocumentType_Relation: null,
			DocumentStatus_Relation: {id: 102877, text: 'Действительный'},
			DocumentSeries_Relation: '',
			DocumentNumber_Relation: '',
			DocumentGivenDate_Relation: '',
			DocumentFinishDate_Relation: '',

			Sex_Relation: { id: '', text: '' }, // М|Ж
			Birthplace_Relation: TownAddress(),
			Nationality_Relation: null, // {id:'',text:''}

			Sex: { id: '', text: '' }, // М|Ж
			Birthday: null, // 'ДД.ММ.ГГГГ'
			Nationality: null, // {id:'',text:''}
			Birthplace: TownAddress(),
			PersonPhone: '', // телефон ИГ или ЛБГ
			Phone: '', //телефон принимающей стороны

			DocumentType: null, // {id:'',text:''}
			DocumentSeries: '',
			DocumentNumber: '',
			Document_issued: null, // 'ДД.ММ.ГГГГ'
			DocumentGivenDate: null, // 'ДД.ММ.ГГГГ'
			DocumentValidFrom: null, // 'ДД.ММ.ГГГГ'
			DocumentFinishDate: null, // 'ДД.ММ.ГГГГ'
			DocumentGivenBy_code: null, // {id:'',text:''}
			DocumentGivenBy: null, // {id:'',text:''}
			Document_DocumentStatus: {id: 102877, text: 'Действительный'}, // {id:'',text:''}
			Document_uid: null,
			Document_entered: null,

			Visa_uid: null,
			Visa_type: null, // {id:'',text:''}
			Visa_series: '',
			Visa_number: '',
			Visa_issued: null, // 'ДД.ММ.ГГГГ'
			Visa_validFrom: null, // 'ДД.ММ.ГГГГ'
			Visa_validTo: null, // 'ДД.ММ.ГГГГ'
			Visa_category: null, // {id:'',text:''}
			Visa_multiplicity: null, // {id:'',text:''}
			Visa_identifier: null,
			Visa_visitPurpose: null, // {id:'',text:''}
			Visa_DocumentStatus: {id: 102877, text: 'Действительный'}, // {id:'',text:''}
			Visa_entered: null,

			MigrationCard_uid: null,
			MigrationCard_series: '', 
			MigrationCard_number: '', 
			MigrationCard_dateFrom: null,  // 'ДД.ММ.ГГГГ'
			MigrationCard_dateTo: null,  // 'ДД.ММ.ГГГГ'
			MigrationCard_entranceDate: null,  // 'ДД.ММ.ГГГГ'
			MigrationCard_EntranceCheckpoint: null,  // {id:'',text:''}
			MigrationCard_visitPurpose: null,  // {id:'',text:''}
			MigrationCard_kpp: null,  // {id:'',text:''}

			profession: null,  // {id:'',text:''}

			noticeFrom: { id: "02" }, // {id:'',text:''}
			hotel_id: '780-100',
			notificationNumber: null, //'02/780-100/14/000038',
			firstArrival: false, //boolean
			stateProgramMember: null, // {id:'',text:''}
			entrancePurpose: null, // {id:'',text:''}

			stayPlace: null,

			AppartmentNumber: '',
			AppartmentNumber_Company: '',
			AppartmentNumber_CompanyN: '',
			stayPeriod_dateFrom: null, // 'ДД.ММ.ГГГГ'
			stayPeriod_dateTo: null,  // 'ДД.ММ.ГГГГ'
			local_stayPeriod_dateFrom: null, // 'ДД.ММ.ГГГГ'

			Hotel_organization_uid: null,
			Hotel_organization_inn: null,
			Hotel_organization_phone: null,
			Hotel_organization_name: null,
			Hotel_address_variants: [],
			Hotel_Document: '',
			Organization_Person_uid: null,
			Organization_Person_personId: null,
			Organization_Person_lastName: null,
			Organization_Person_firstName: null,
			Organization_Person_middleName: null,
			Organization_Person_gender: { id: '', text: '' }, // М|F
			Organization_Person_nationality: null, // {id:'',text:''}
			Organization_Person_birthDate: null,
			Organization_Person_birthPlace: TownAddress(),
			Organization_Document_uid: null,
			Organization_Document_series: null,
			Organization_Document_number: null,
			Organization_Document_authorityOrgan: null,
			Organization_Document_authorityOrgan_text: null,
			Organization_Document_authorityOrgan_check: true,
			Organization_Document_issued: null,
			Organization_Document_validFrom: null,
			Organization_Document_validTo: null,
			DocumentType_Company: null, // {id:'',text:''}
			Organization_Document_DocumentStatus: {id: 102877, text: 'Действительный'},
			Organization_Document_entered: null,

			DocumentScan: [],
			DocumentScanDoc: [],
			DocumentScanVisa: []
		};
		res.Birthplace.Country = null;
		return res;
	};
});