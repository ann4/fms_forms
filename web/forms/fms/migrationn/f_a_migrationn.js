define([
	  'forms/fms/migrationn/c_a_migrationn'
	, 'forms/fms/migrationn/ff_migrationn'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_migrationn'
		, Title: 'Прибытие ИГ или ЛБГ'
		, FileFormat: FileFormat
	};
	return form_spec;
});
