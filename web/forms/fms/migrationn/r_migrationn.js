﻿define([
	'forms/base/h_constraints'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_docTypeMigrationn'
	, 'forms/fms/base/h_docTypeMigrationnR'
	, 'forms/fms/base/h_visaType'
	, 'forms/fms/base/h_visaCategory'
	, 'forms/fms/base/h_visaMultiplicity'
	, 'forms/fms/base/h_entryGoal'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/h_arrivals'
	, 'forms/fms/base/h_StateProgram'
	, 'forms/fms/base/h_MigKpp'
	, 'forms/base/h_times'
],
function (h_constraints
	, h_addr
	, h_DocumentType
	, h_DocumentTypeMig
	, h_DocumentTypeMigR
	, h_VisaType
	, h_VisaCategory
	, h_VisaMultiplicity
	, h_entryGoal
	, h_citizenship
	, h_arrivals
	, h_StateProgram
	, h_MigKpp
	, h_times
	)
{
	return function () {
		var documentTypes = h_DocumentType.GetValues();
		var constraints =
		[
			  h_constraints.for_Field('LastName', h_constraints.NotEmpty())
			, h_constraints.for_Field('LastName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('FirstName', h_constraints.NotEmpty())
			, h_constraints.for_Field('FirstName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('MiddleName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('LastName_latin', h_constraints.recommend_NotEmpty('Не указана фамилия гражданина (латиницей)'))
			, h_constraints.for_Field('LastName_latin', h_constraints.recommend_Latin())
			, h_constraints.for_Field('FirstName_latin', h_constraints.recommend_NotEmpty('Не указано имя гражданина (латиницей)'))
			, h_constraints.for_Field('FirstName_latin', h_constraints.recommend_Latin())
			, h_constraints.for_Field('MiddleName_latin', h_constraints.recommend_Latin())
			, { fields: ['MiddleName', 'MiddleName_latin']
				, marks: ['MiddleName_latin']
				, check: function (name, item) {
					return h_constraints.check_NotEmpty(name) ? h_constraints.recommend_check_NotEmpty(item) : true;
				}
				, description: function (element, field) {
					return 'Не указано отчество гражданина (латиницей)';
				}
			}
			, h_constraints.for_Field('Birthday', h_constraints.Date())
			, { fields: ['Birthday']
				, check: h_constraints.recommend_check_BeforeDateNow
				, description: function (element, field) { return 'Недопустимая дата рождения'; }
			}
			, h_constraints.for_Field('Birthday', h_constraints.recommend_NotEmpty('Не указана дата рождения'))
			, { fields: ['Birthday', 'DocumentType']
				, check: h_constraints.recommend_check_DocumentTypePassport
				, description: function (element, field) { return 'Паспорт РФ не выдается гражданам, не достигшим 14 лет'; }
			}
			, { fields: ['Birthday', 'DocumentType']
				, check: h_constraints.recommend_check_DocumentTypeNotPassport
				, description: function (element, field) { return 'Документом, удостоверяющим личность гражданина, достигшего 14 лет, является паспорт'; }
			}
			, { fields: ['Sex']
				, check: function (item) { return '' == item ? 'recommended' : true; }
				, description: function (element, field) { return 'Не указан пол гражданина'; }
			}
			, { fields: ['Sex', 'MiddleName']
				, check: h_constraints.recommend_check_SexByMiddleName
				, description: h_constraints.description_SexByMiddleName
			}
			, h_constraints.for_Field('Nationality', h_constraints.NotEmpty('Не указано гражданство'))
			, { fields: ['Nationality']
				, check: function (item) { return !(h_constraints.check_NotEmpty(item) && item.id == 'RUS'); }
				, description: function (element, field) { return 'Недопустимое гражданство при прибытии иностранного гражданина'; }
			}
			, { fields: ['Nationality']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_citizenship.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Birthplace']
				, check: function (item) { return !(!item || null == item || !item.text || null == item.text || '' == item.text) ? true : 'recommended'; }
				, description: function (element, field) { return 'Не указано государство рождения'; }
			}
			, { fields: ['Birthday', 'Birthplace']
				, check: h_constraints.recommend_check_CountrySun
				, description: function (element, field) { return 'Государство рождения указано неверно. До 06.02.1992 года государство рождения - СССР'; }
			}
			, h_constraints.for_Field('DocumentType', h_constraints.NotEmpty())
			, { fields: ['DocumentType']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_DocumentTypeMig.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['DocumentType']
				, check: function (type) {
					return !(h_constraints.check_NotEmpty(type) && (h_DocumentTypeMig.russianValuesMigrationn).indexOf(type.id) > -1);
				}
				, description: function (element, field) {
					return 'Иностранный гражданин не может иметь документ, удостоверяющий личность гражданина РФ';
				}
			}
			, { fields: ['DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentType']
				, check: function (type, docSeries, docNumber) {
					return !(h_constraints.check_NotEmpty(type) && 
						((h_DocumentTypeMig.forbiddenValuesMigrationn).indexOf(type.id) > -1 || (h_DocumentTypeMig.notViewValuesMigration).indexOf(type.id) > -1));
				}
				, description: function (element, ltype, ldocSeries, ldocNumber, type, docSeries, docNumber) {
					return 'Выбранный тип документа (' + type.text +
						('' == docSeries ? '' : (' серия ' + docSeries)) + 
						' №' + docNumber + ') не является документом, удостоверяющим личность.';
				}
			}
			, { fields: ['DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentType']
				, check: function (type, docSeries, docNumber) {
					return h_constraints.check_NotEmpty(type) && (h_DocumentTypeMig.limitedValuesMigrationn).indexOf(type.id) > -1 ? 'recommended' : true;
				}
				, description: function (element, ltype, ldocSeries, ldocNumber, type, docSeries, docNumber) {
					return 'Выбранный тип документа (' + type.text +
						('' == docSeries ? '' : (' серия ' + docSeries)) +
						' №' + docNumber + ') , как правило, не является документом, удостоверяющим личность. ' +
						'Сверьте указанный тип документа с предъявленным гражданином при заселении.';
				}
			}
			, { fields: ['DocumentType', 'Nationality']
				, check: function (type, nationality) {
					var needCountry = "BLR, UKR, KAZ, ARM, KGZ, ABH, OST";
					return h_constraints.check_NotEmpty(type) && h_constraints.check_NotEmpty(nationality)
						&& type.id == '103012' && needCountry.indexOf(nationality.id) == -1 ? 'recommended' : true;
				}
				, description: function (element, field) {
					return 'Документ "Иностранный паспорт" в Российской Федерации может удостоверять личность граждан только следующих государств: ' +
						'Беларусь, Украина, Казахстан, Армения, Киргизия, Абхазия, Южная Осетия';
				}
			}
			, { fields: ['DocumentNumber']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Поле "Номер документа" не может быть пустым'; }
			}
			, h_constraints.for_Field('DocumentGivenDate', h_constraints.Date())
			, h_constraints.for_Field('DocumentFinishDate', h_constraints.Date())
			, { fields: ['DocumentGivenDate']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Дата выдачи документа, удостоверяющего личность не может быть пустой'; }
			}
			, { fields: ['Birthday', 'DocumentGivenDate', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['Birthday', 'DocumentGivenDate']
				, check: function (birthday, dateDocumentGiven, docType, dateDocumentSeries, dateDocumentNumber) {
					return h_constraints.check_PeriodDate(birthday, dateDocumentGiven);
				}
				, description: function (element, lbirthday, ldateDocumentGiven, ldtype, ldseries, ldnumber, birthday, dateDocumentGiven, dtype, dseries, dnumber) {
					var message = 'Дата выдачи документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					message += ' не может быть меньше даты рождения';
					return message;
				}
			}
			, { fields: ['DocumentType', 'DocumentFinishDate']
				, marks: ['DocumentFinishDate']
				, check: function (type, date) {
					return h_constraints.check_NotEmpty(type) && '103012' == type.id ? true : h_constraints.recommend_check_NotEmpty(date);
				}
				, description: function (element, type, date) { return 'Не указан срок действия документа, удостоверяющего личность'; }
			}

			, { fields: ['DocumentGivenDate', 'DocumentFinishDate']
				, check: h_constraints.check_SrictPeriodDate
				, description: function (element, dateFrom, dateTo) { return 'Срок действия документа удостоверяющего личность должен быть больше даты его выдачи'; }
			}
			, { fields: ['Birthday', 'notificationReceived', 'Document_entered']
				, marks: ['Birthday', 'Document_entered']
				, check: function (birthday, date, doc_entered) {
					return h_constraints.check_NotEmpty(birthday) && h_constraints.check_NotEmpty(date) && 
						   !doc_entered && !h_constraints.check_DiffDatesMore14(birthday, date) ? 'recommended' : true;
				}
				, description: function (element, birthday, date) {
					return 'Данные блока "Законный представитель" обязательны к заполнению в случае, когда заявитель на момент подачи заявления не достиг возраста 14 лет';
				}
			}
			, { fields: ['Document_entered', 'LastName_Relation']
				, marks: ['LastName_Relation']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Фамилия законного представителя не может быть пустой'; }
			}
			, { fields: ['Document_entered', 'LastName_Relation']
				, marks: ['LastName_Relation']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.recommend_check_Ciryllic(item) : true; }
				, description: function (element, field) { return 'Фамилия законного представителя содержит недопустимые символы'; }
			}
			, { fields: ['Document_entered', 'FirstName_Relation']
				, marks: ['FirstName_Relation']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Имя законного представителя не может быть пустым'; }
			}
			, { fields: ['Document_entered', 'FirstName_Relation']
				, marks: ['FirstName_Relation']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.recommend_check_Ciryllic(item) : true; }
				, description: function (element, field) { return 'Имя законного представителя содержит недопустимые символы'; }
			}
			, { fields: ['Document_entered', 'MiddleName_Relation']
				, marks: ['MiddleName_Relation']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.recommend_check_Ciryllic(item) : true; }
				, description: function (element, field) { return 'Отчество законного представителя содержит недопустимые символы'; }
			}
			, { fields: ['Document_entered', 'LastName_Relation_latin']
				, marks: ['LastName_Relation_latin']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.recommend_check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Не указана фамилия законного представителя (латиницей)'; }
			}
			, { fields: ['Document_entered', 'FirstName_Relation_latin']
				, marks: ['FirstName_Relation_latin']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.recommend_check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Не указано имя законного представителя (латиницей)'; }
			}
			, { fields: ['Document_entered', 'MiddleName', 'MiddleName_Relation_latin']
				, marks: ['MiddleName_Relation_latin']
				, check: function (doc_entered, name, item) { return doc_entered && h_constraints.check_NotEmpty(name) ? h_constraints.recommend_check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Не указано отчество законного представителя (латиницей)'; }
			}
			, { fields: ['Document_entered', 'LastName_Relation_latin']
				, marks: ['LastName_Relation_latin']
				, check: function (doc_entered, item) { return doc_entered && h_constraints.check_NotEmpty(item) ? h_constraints.recommend_check_Latin(item) : true; }
				, description: function (element, field) { return 'Фамилия законного представителя (латиницей) содержит недопустимые символы'; }
			}
			, { fields: ['Document_entered', 'FirstName_Relation_latin']
				, marks: ['FirstName_Relation_latin']
				, check: function (doc_entered, item) { return doc_entered && h_constraints.check_NotEmpty(item) ? h_constraints.recommend_check_Latin(item) : true; }
				, description: function (element, field) { return 'Имя законного представителя (латиницей) содержит недопустимые символы'; }
			}
			, { fields: ['Document_entered', 'MiddleName_Relation_latin']
				, marks: ['MiddleName_Relation_latin']
				, check: function (doc_entered, item) { return doc_entered && h_constraints.check_NotEmpty(item) ? h_constraints.recommend_check_Latin(item) : true; }
				, description: function (element, field) { return 'Отчество законного представителя (латиницей) содержит недопустимые символы'; }
			}
			, h_constraints.for_Field('Birthday_Relation', h_constraints.Date())
			, { fields: ['Document_entered', 'Birthday_Relation']
				, marks: ['Birthday_Relation']
				, check: function (doc_entered, date) { return doc_entered ? h_constraints.recommend_check_BeforeDateNow(date) : true; }
				, description: function (element, field) { return 'Недопустимая дата рождения законного представителя'; }
			}
			, { fields: ['Document_entered', 'Birthday_Relation']
				, marks: ['Birthday_Relation']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.recommend_check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Не указана дата рождения законного представителя'; }
			}
			, { fields: ['DocumentType_Relation']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_DocumentTypeMigR.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Document_entered', 'Birthday_Relation', 'DocumentType_Relation']
				, marks: ['Birthday_Relation', 'DocumentType_Relation']
				, check: function (doc_entered, date, type) { return doc_entered ? h_constraints.recommend_check_DocumentTypePassport(date, type) : true; }
				, description: function (element, field) { return 'Паспорт РФ не выдается гражданам, не достигшим 14 лет'; }
			}
			, { fields: ['Document_entered', 'Birthday_Relation', 'DocumentType_Relation']
				, marks: ['Birthday_Relation', 'DocumentType_Relation']
				, check: function (doc_entered, date, type) { return doc_entered ? h_constraints.recommend_check_DocumentTypeNotPassport(date, type) : true; }
				, description: function (element, field) { return 'Документом, удостоверяющим личность гражданина, достигшего 14 лет, является паспорт'; }
			}
			, { fields: ['Document_entered', 'Sex_Relation']
				, marks: ['Sex_Relation']
				, check: function (doc_entered, item) { return doc_entered && h_constraints.check_Empty(item) ? 'recommended' : true; }
				, description: function (element, field) { return 'Не указан пол законного представителя'; }
			}
			, { fields: ['Document_entered', 'Sex_Relation', 'MiddleName_Relation']
				, marks: ['Sex_Relation', 'MiddleName_Relation']
				, check: function (doc_entered, sex, middle) { return doc_entered ? h_constraints.recommend_check_SexByMiddleName(sex, middle) : true; }
				, description: h_constraints.description_SexByMiddleName
			}
			, { fields: ['Document_entered', 'Nationality_Relation']
				, marks: ['Nationality_Relation']
				, check: function (doc_entered, item) { return doc_entered ? h_constraints.recommend_check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Не указано гражданство законного представителя'; }
			}
			, { fields: ['Nationality_Relation']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_citizenship.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Document_entered', 'Birthplace_Relation']
				, marks: ['Birthplace_Relation']
				, check: function (doc_entered, item) { return !doc_entered || !(!item || null == item || !item.text || null == item.text || '' == item.text) ? true : 'recommended'; }
				, description: function (element, field) { return 'Не указано государство рождения законного представителя'; }
			}
			, { fields: ['Document_entered', 'Birthday_Relation', 'Birthplace_Relation']
				, marks: ['Birthday_Relation', 'Birthplace_Relation']
				, check: function (doc_entered, date, item) { return doc_entered ? h_constraints.recommend_check_CountrySun(date, item) : true; }
				, description: function (element, field) { return 'Государство рождения законного представителя указано неверно. До 06.02.1992 года государство рождения - СССР'; }
			}
			, { fields: ['Document_entered', 'DocumentType_Relation']
				, marks: ['DocumentType_Relation']
				, check: function (doc_entered, type) { return doc_entered ? h_constraints.check_NotEmpty(type) : true; }
				, description: function (element, field) { return 'Документ, удостоверяющий личность законного представителя, не может быть пустым'; }
			}
			, { fields: ['Document_entered', 'DocumentType_Relation']
				, marks: ['DocumentType_Relation']
				, check: function (doc_entered, type) {
					return !(doc_entered && h_constraints.check_NotEmpty(type) && (h_DocumentTypeMigR.russianValuesMigrationn).indexOf(type.id) > -1);
				}
				, description: function (element, field) { return 'Иностранный гражданин не может иметь документ, удостоверяющий личность гражданина РФ'; }
			}
			, {
				fields: ['Document_entered', 'DocumentType_Relation']
				, marks: ['DocumentType_Relation']
				, check: function (doc_entered, type) {
					return !(doc_entered && h_constraints.check_NotEmpty(type) && (h_DocumentTypeMigR.posibleRussianValuesMigrationn).indexOf(type.id) > -1) ? true : 'recommended';
				}
				, description: function (element, field) {
					return 'Данный тип документа предназначен только для граждан Российской Федерации. Убедитесь, что лицо, сопровождающее ребенка, не является иностранным гражданином.';
				}
			}
			, { fields: ['Document_entered', 'DocumentType_Relation', 'DocumentSeries_Relation', 'DocumentNumber_Relation']
				, marks: ['DocumentType_Relation']
				, check: function (type, docSeries, docNumber) {
					return !(h_constraints.check_NotEmpty(type) && 
					((h_DocumentTypeMigR.forbiddenValuesMigrationn).indexOf(type.id) > -1  || (h_DocumentTypeMig.notViewValuesMigration).indexOf(type.id) > -1));
				}
				, description: function (element, ltype, ldocSeries, ldocNumber, type, docSeries, docNumber) {
					return 'Выбранный тип документа (' + type.text +
						('' == docSeries ? '' : (' серия ' + docSeries)) +
						' №' + docNumber + ') не является документом, удостоверяющим личность.';
				}
			}
			, { fields: ['Document_entered', 'DocumentType_Relation', 'DocumentSeries_Relation', 'DocumentNumber_Relation']
				, marks: ['DocumentType_Relation']
				, check: function (type, docSeries, docNumber) {
					return h_constraints.check_NotEmpty(type) && (h_DocumentTypeMigR.limitedValuesMigrationn).indexOf(type.id) > -1 ? 'recommended' : true;
				}
				, description: function (element, ltype, ldocSeries, ldocNumber, type, docSeries, docNumber) {
					return 'Выбранный тип документа (' + type.text +
						('' == docSeries ? '' : (' серия ' + docSeries)) +
						' №' + docNumber + ') , как правило, не является документом, удостоверяющим личность. ' +
						'Сверьте указанный тип документа с предъявленным гражданином при заселении.';
				}
			}
			, { fields: ['Document_entered', 'DocumentType_Relation', 'Nationality_Relation']
				, marks: ['DocumentType_Relation', 'Nationality_Relation']
				, check: function (doc_entered, type, nationality) {
					var needCountry = "BLR, UKR, KAZ, ARM, KGZ, ABH, OST";
					return doc_entered && h_constraints.check_NotEmpty(type) && h_constraints.check_NotEmpty(nationality)
						&& type.id == '103012' && needCountry.indexOf(nationality.id) == -1 ? 'recommended' : true;
				}
				, description: function (element, field) {
					return 'Документ "Иностранный паспорт" в Российской Федерации может удостоверять личность граждан только следующих государств: ' +
						'Беларусь, Украина, Казахстан, Армения, Киргизия, Абхазия, Южная Осетия';
				}
			}
			, { fields: ['Document_entered', 'DocumentNumber_Relation']
				, marks: ['DocumentNumber_Relation']
				, check: function (doc_entered, number) { return doc_entered ? h_constraints.check_NotEmpty(number) : true; }
				, description: function (element, field) { return 'Номер документа законного представителя не может быть пустым'; }
			}
			, h_constraints.for_Field('DocumentGivenDate_Relation', h_constraints.Date())
			, h_constraints.for_Field('DocumentFinishDate_Relation', h_constraints.Date())
			, { fields: ['Document_entered', 'DocumentGivenDate_Relation']
				, marks: ['DocumentGivenDate_Relation']
				, check: function (doc_entered, date) { return doc_entered ? h_constraints.check_NotEmpty(date) : true; }
				, description: function (element, field) { return 'Дата выдачи документа, удостоверяющего личность законного представителя не может быть пустой'; }
			}
			, { fields: ['Document_entered', 'DocumentFinishDate_Relation']
				, marks: ['DocumentFinishDate_Relation']
				, check: function (doc_entered, date) { return doc_entered ? h_constraints.recommend_check_NotEmpty(date) : true; }
				, description: function (element, field) { return 'Не указан срок действия документа, удостоверяющего личность законного представителя'; }
			}
			, { fields: ['Document_entered', 'DocumentGivenDate_Relation', 'DocumentFinishDate_Relation']
				, check: function (doc_entered, dateFrom, dateTo) { return doc_entered ? h_constraints.check_SrictPeriodDate(dateFrom, dateTo) : true; }
				, description: function (element, dateFrom, dateTo) {
					return 'Срок действия документа удостоверяющего личность законного представителя должен быть больше даты его выдачи';
				}
			}
			, { fields: ['Document_entered', 'Birthday_Relation', 'DocumentGivenDate_Relation', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['Birthday_Relation', 'DocumentGivenDate_Relation']
				, check: function (doc_entered, birthday, dateDocumentGiven, docType, dateDocumentSeries, dateDocumentNumber) {
					return doc_entered ? h_constraints.check_PeriodDate(birthday, dateDocumentGiven) : true;
				}
				, description: function (element, ldocEntered, lbirthday, ldateGiven, ldocType, ldocSeries, ldocNumber, docEntered, birthday, dateGiven, dtype, dseries, dnumber) {
					var message = 'Дата выдачи документа законного представителя ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					message += ' не может быть меньше даты рождения';
					return message;
				}
			}
			, { fields: ['Visa_type']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_VisaType.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Visa_type', 'Visa_series', 'Visa_number', 'Visa_validFrom', 'Visa_validTo', 'Visa_issued', 'Visa_category', 'Visa_multiplicity', 'Visa_visitPurpose', 'Visa_identifier']
				, marks: ['Visa_type']
				, check: function (type, series, number, validFrom, validTo, issued, category, multiplicity, visitPurpose, identifier) {
					if (h_constraints.check_Empty(type) &&
						(h_constraints.check_NotEmpty(series) || h_constraints.check_NotEmpty(number)
						|| h_constraints.check_NotEmpty(validFrom) || h_constraints.check_NotEmpty(validTo) || h_constraints.check_NotEmpty(issued)
						|| h_constraints.check_NotEmpty(category) || h_constraints.check_NotEmpty(multiplicity)
						|| h_constraints.check_NotEmpty(visitPurpose) || h_constraints.check_NotEmpty(identifier))) {
						return 'recommended';
					} else {
						return true;
					}
				}
				, description: function (element, field) { return 'Не указан документ, подтверждающий право пребывания (проживания) в РФ'; }
			}
			, { fields: ['Visa_type', 'Visa_series']
				, marks: ['Visa_series']
				, check: function (type, item) {
					return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.recommend_check_NotEmpty(item) : true;
				}
				, description: function (element, field) { return 'Не указана серия документа, подтверждающего право пребывания (проживания) в РФ'; }
			}
			, { fields: ['Visa_type', 'Visa_series']
				, marks: ['Visa_series']
				, check: function (type, series) {
					if (h_constraints.check_NotEmpty(type) && h_constraints.check_NotEmpty(series) && h_VisaType.idVisaType == type.id) {
						var re = new RegExp(/[^0-9]/);
						return series.length != 2 || re.test(series) ? 'recommended' : true;
					}
					return true;
				}
				, description: function (element, field) { return 'Серия визы должна состоять из двух цифр'; }
			}
			, { fields: ['Visa_type', 'Visa_number']
				, marks: ['Visa_number']
				, check: function (type, item) { return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Номер визы не может быть пустым'; }
			}
			, { fields: ['Visa_type', 'Visa_number']
				, marks: ['Visa_number']
				, check: function (type, number) {
					if (h_constraints.check_NotEmpty(type) && h_constraints.check_NotEmpty(number) && h_VisaType.idVisaType == type.id) {
						var re = new RegExp(/[^0-9]/);
						return number.length != 7 || re.test(number) ? 'recommended' : true;
					}
					return true;
				}
				, description: function (element, field) { return 'Номер визы должен состоять из семи цифр'; }
			}
			, h_constraints.for_Field('Visa_validFrom', h_constraints.Date())
			, h_constraints.for_Field('Visa_validTo', h_constraints.Date())
			, h_constraints.for_Field('Visa_issued', h_constraints.Date())
			, { fields: ['Visa_type', 'Visa_issued']
				, marks: ['Visa_issued']
				, check: function (type, item) {
					return h_constraints.check_NotEmpty(type) && '139373' != type.id && '135717' != type.id ? h_constraints.recommend_check_NotEmpty(item) : true;
				}
				, description: function (element, field) { return 'Не указана дата выдачи документа для РФ'; }
			}
			, { fields: ['Visa_type', 'Visa_validFrom']
				, marks: ['Visa_validFrom']
				, check: function (type, item) { return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.recommend_check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Не указана дата начала действия визы'; }
			}
			, { fields: ['Visa_type', 'Visa_validTo']
				, marks: ['Visa_validTo']
				, check: function (type, item) {
					return h_constraints.check_NotEmpty(type) && '139373' != type.id && '135717' != type.id ? h_constraints.recommend_check_NotEmpty(item) : true;
				}
				, description: function (element, field) { return 'Не указан срок окончиния действия документа для РФ'; }
			}
			, { fields: ['Visa_type', 'Visa_validFrom', 'Visa_validTo']
				, marks: ['Visa_validFrom', 'Visa_validTo']
				, check: function (type, dateFrom, dateTo) {
					return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.check_SrictPeriodDate(dateFrom, dateTo) : true;
				}
				, description: function (element, dateFrom, dateTo) {
					return 'Срок действия визы должен быть больше даты начала ее действия';
				}
			}
			, { fields: ['Visa_type', 'Visa_issued', 'Visa_validFrom']
				, marks: ['Visa_issued', 'Visa_validFrom']
				, check: function (type, dateFrom, dateTo) {
					return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.check_PeriodDate(dateFrom, dateTo) : true;
				}
				, description: function (element, dateFrom, dateTo) {
					return 'Дата начала действия визы не может быть меньше даты выдачи';
				}
			}
			, { fields: ['Visa_type', 'Visa_issued', 'Visa_validTo']
				, marks: ['Visa_issued', 'Visa_validTo']
				, check: function (type, dateFrom, dateTo) {
					return h_constraints.check_NotEmpty(type) && h_VisaType.idTypesWithIssuedDateAndDateTo.indexOf(type.id) > -1
						? h_constraints.check_PeriodDate(dateFrom, dateTo) : true;
				}
				, description: function (element, dateFrom, dateTo) {
					return 'Срок действия документа для РФ должен быть больше даты его выдачи';
				}
			}
			, { fields: ['Visa_type', 'Visa_category']
				, marks: ['Visa_category']
				, check: function (type, item) { return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Категория визы не может быть пустой'; }
			}
			, { fields: ['Visa_category']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_VisaCategory.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Visa_type', 'Visa_multiplicity']
				, marks: ['Visa_multiplicity']
				, check: function (type, item) { return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Кратность визы не может быть пустой'; }
			}
			, { fields: ['Visa_multiplicity']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_VisaMultiplicity.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Visa_type', 'Visa_visitPurpose']
				, marks: ['Visa_visitPurpose']
				, check: function (type, item) { return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Цель поездки не может быть пустой'; }
			}
			, { fields: ['Visa_type', 'Visa_identifier']
				, marks: ['Visa_identifier']
				, check: function (type, item) { return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id ? h_constraints.recommend_check_NotEmpty(item) : true; }
				, description: function (element, field) { return 'Не указан идентификатор визы'; }
			}
			, { fields: ['Visa_type', 'Visa_identifier']
				, marks: ['Visa_identifier']
				, check: function (type, item) {
					return h_constraints.check_NotEmpty(type) && h_VisaType.idVisaType == type.id && h_constraints.check_NotEmpty(item) ?
					h_constraints.recommended_check_Visa_identifier(item) : true;
				}
				, description: function (element, field) { return 'Идентификатор визы должен состоять из трех русских букв, за которыми следуют пять или шесть цифр'; }
			}
			, h_constraints.for_Field('LivingDocDecisionDate', h_constraints.Date())
			, h_constraints.for_Field('RvpVisa_validTo', h_constraints.Date())
			, { fields: ['Visa_type', 'LivingDocDecisionDate']
				, marks: ['LivingDocDecisionDate']
				, check: function (type, item) {
					var documentTypes = '135709, 135710, 135717, 139373, 139387';
					if (h_constraints.check_NotEmpty(type) && documentTypes.indexOf(type.id) > -1) {
						return h_constraints.check_Empty(item) ? 'recommended' : true;
					}
					return true;
				}
				, description: function (element, field) { return 'Не указана дата принятия решения документа для РФ'; }
			}
			, {
				fields: ['Visa_type', 'RvpVisa_validTo']
				, marks: ['RvpVisa_validTo']
				, check: function (type, item) {
					return h_constraints.check_NotEmpty(type) && ('139373' == type.id || '135717' == type.id) ? h_constraints.recommend_check_NotEmpty(item) : true;
				}
				, description: function (element, field) { return 'Не указан срок действия документа для РФ'; }
			}
			, { fields: ['Visa_type', 'LivingDocDecisionDate', 'RvpVisa_validTo']
				, marks: ['LivingDocDecisionDate', 'RvpVisa_validTo']
				, check: function (type, dateFrom, dateTo) {
					return h_constraints.check_NotEmpty(type) && h_VisaType.idTypeWithRvpDateTo.indexOf(type.id) > -1 ? h_constraints.check_SrictPeriodDate(dateFrom, dateTo) : true;
				}
				, description: function (element, dateFrom, dateTo) { return 'Срок действия документа для РФ должен быть больше даты принятия решения'; }
			}
			, { fields: ['Visa_type', 'LivingDocDecisionNumber']
				, marks: ['LivingDocDecisionNumber']
				, check: function (type, number) {
					var documentTypes = '135709, 135710, 135717, 139373, 139387';
					if (h_constraints.check_NotEmpty(type) && documentTypes.indexOf(type.id) > -1) {
						return h_constraints.check_Empty(number) ? 'recommended' : true;
					}
					return true;
				}
				, description: function (element, field) { return 'Не указан номер решения документа для РФ'; }
			}
			, {
				fields: ['Visa_type', 'RvpDocumentGivenBy', 'RvpDocumentGivenBy_text', 'schemaVersion']
				, marks: ['RvpDocumentGivenBy', 'RvpDocumentGivenBy_text']
				, check: function (type, organ, text, schema) {
					var documentTypes = '135709, 135710, 135717, 139373, 139387';
					if (h_constraints.check_NotEmpty(type) && documentTypes.indexOf(type.id) > -1) {
						return '1.102.2' != schema || h_constraints.check_NotEmpty(organ) || h_constraints.check_NotEmpty(text) ? true : 'recommended';
					}
					return true;
				}
				, description: function (element, field) { return 'Не указано кем выдан документ для РФ'; }
			}
			, { fields: ['Visa_type', 'MigrationCard_series', 'MigrationCard_number', 'MigrationCard_dateFrom', 'MigrationCard_dateTo', 'MigrationCard_kpp', 'MigrationCard_visitPurpose']
				, marks: ['MigrationCard_series', 'MigrationCard_number', 'MigrationCard_dateFrom', 'MigrationCard_dateTo', 'MigrationCard_kpp', 'MigrationCard_visitPurpose']
				, check: function (vtype, mseries, mnumber, mdatefrom, mdateto, mkpp, mvisitpurpose) {
					var migrationNotEmpty = '' != mseries || '' != mnumber || '' != mdatefrom || '' != mdateto || '' != mkpp || '' != mvisitpurpose;
					var documentTypes = h_DocumentTypeMig.forbiddenVisaTypeMigrationn;
					return migrationNotEmpty && -1 < documentTypes.indexOf(vtype.id) ? false : true;
				}
				, description: function (element, lvtype, lmseries, lmnumber, lmdatefrom, lmdateto, lmkpp, lmvisitpurpose,
					vtype, mseries, mnumber, mdatefrom, mdateto, mkpp, mvisitpurpose) {
					return 'В случае если документом, разрешающим пребывание (проживание) гражданина на территории РФ, является ' +
						vtype.text + ', миграционная карта не требуется.';
				}
			}
			, { fields: ['Nationality', 'MigrationCard_series', 'MigrationCard_number', 'Visa_type', 'DocumentType']
				, marks: ['MigrationCard_series', 'MigrationCard_number']
				, check: function (nationality, series, number, visa, document) {
					var needCountry = "BLR";
					var countryKAZ = "KAZ";
					var documentKAZ = '103009';
					var documentTypes = h_DocumentTypeMig.forbiddenVisaTypeMigrationn;
					return h_constraints.check_NotEmpty(nationality)
						&& (needCountry.indexOf(nationality.id) == -1 && !(countryKAZ == nationality.id && documentKAZ == document))
						&& (h_constraints.check_Empty(visa) || documentTypes.indexOf(visa.id) == -1)
						&& h_constraints.check_Empty(series) && h_constraints.check_Empty(number) ? 'recommended' : true;
				}
				, description: function(element, field) { return 'Не заполнена миграционная карта'; }
			}
			, h_constraints.for_Fields(['MigrationCard_series'], h_constraints.recommend_SeriesMigration())
			, h_constraints.for_Fields(['MigrationCard_number'], h_constraints.recommend_NumberMigration())
			, h_constraints.for_Field('MigrationCard_dateFrom', h_constraints.Date())
			, h_constraints.for_Field('MigrationCard_dateTo', h_constraints.Date())
			, { fields: ['MigrationCard_series', 'MigrationCard_number', 'MigrationCard_dateFrom', 'MigrationCard_dateTo', 'Visa_type']
				, marks: ['MigrationCard_number']
				, check: function (series, number, dateFrom, dateTo, visa) {
					var documentTypes = h_DocumentTypeMig.forbiddenVisaTypeMigrationn;
					return (h_constraints.check_NotEmpty(series) || h_constraints.check_NotEmpty(dateFrom) || h_constraints.check_NotEmpty(dateTo))
						&& (h_constraints.check_Empty(visa) || documentTypes.indexOf(visa.id) == -1) ?
						h_constraints.check_NotEmpty(number) : true;
				}
				, description: function (element, field) { return 'Номер миграционной карты не может быть пустым'; }
			}
			, { fields: ['MigrationCard_series', 'MigrationCard_number', 'MigrationCard_dateFrom', 'MigrationCard_dateTo', 'Visa_type']
				, marks: ['MigrationCard_dateFrom']
				, check: function (series, number, dateFrom, dateTo, visa) {
					var documentTypes = h_DocumentTypeMig.forbiddenVisaTypeMigrationn;
					return (h_constraints.check_NotEmpty(series) || h_constraints.check_NotEmpty(number) || h_constraints.check_NotEmpty(dateTo))
						&& (h_constraints.check_Empty(visa) || documentTypes.indexOf(visa.id) == -1) ?
						h_constraints.check_NotEmpty(dateFrom) : true;
				}
				, description: function (element, field) { return 'Дата въезда в РФ не может быть пустой'; }
			}
			, { fields: ['MigrationCard_series', 'MigrationCard_number', 'MigrationCard_dateFrom', 'MigrationCard_dateTo', 'Visa_type']
				, marks: ['MigrationCard_dateTo']
				, check: function (series, number, dateFrom, dateTo, visa) {
					var documentTypes = h_DocumentTypeMig.forbiddenVisaTypeMigrationn;
					return (h_constraints.check_NotEmpty(series) || h_constraints.check_NotEmpty(number) || h_constraints.check_NotEmpty(dateFrom))
						&& (h_constraints.check_Empty(visa) || documentTypes.indexOf(visa.id) == -1) ?
						h_constraints.check_NotEmpty(dateTo) : true;
				}
				, description: function (element, field) { return 'Дата выезда из РФ не может быть пустой'; }
			}
			, { fields: ['MigrationCard_series', 'MigrationCard_number', 'MigrationCard_dateFrom', 'MigrationCard_dateTo', 'MigrationCard_kpp', 'Visa_type']
				, marks: ['MigrationCard_kpp']
				, check: function (series, number, dateFrom, dateTo, kpp, visa) {
					var documentTypes = h_DocumentTypeMig.forbiddenVisaTypeMigrationn;
					return (h_constraints.check_NotEmpty(series) || h_constraints.check_NotEmpty(number) || h_constraints.check_NotEmpty(dateFrom) || h_constraints.check_NotEmpty(dateTo))
						&& (h_constraints.check_Empty(visa) || documentTypes.indexOf(visa.id) == -1) ?
						h_constraints.recommend_check_NotEmpty(kpp) : true;
				}
				, description: function (element, field) { return 'Не указано КПП въезда'; }
			}
			, { fields: ['MigrationCard_series', 'MigrationCard_number', 'MigrationCard_dateFrom', 'MigrationCard_dateTo', 'MigrationCard_visitPurpose', 'Visa_type']
				, marks: ['MigrationCard_visitPurpose']
				, check: function (series, number, dateFrom, dateTo, visitPurpose, visa) {
					var documentTypes = h_DocumentTypeMig.forbiddenVisaTypeMigrationn;
					return (h_constraints.check_NotEmpty(series) || h_constraints.check_NotEmpty(number) || h_constraints.check_NotEmpty(dateFrom) || h_constraints.check_NotEmpty(dateTo))
						&& (h_constraints.check_Empty(visa) || documentTypes.indexOf(visa.id) == -1) ?
						h_constraints.check_NotEmpty(visitPurpose) : true;
				}
				, description: function (element, field) { return 'Не заполнена цель въезда по миграционной карте'; }
			}
			, { fields: ['MigrationCard_kpp']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_MigKpp.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['MigrationCard_visitPurpose']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_arrivals.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, h_constraints.for_Field('StateProgram', h_constraints.NotEmpty())
			, { fields: ['StateProgram']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_StateProgram.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Address']
				, check: function (item) { return '' != h_addr.SafePrepareReadableTextWithoutRoom(item, ''); }
				, description: function (element, field) { return 'Поле "Адрес места пребывания" не может быть пустым'; }
			}
			, { fields: ['Address_Company']
				, check: function (item) { return '' != h_addr.SafePrepareReadableTextWithoutRoom(item, ''); }
				, description: function (element, field) { return 'Поле "Фактический адрес" принимающей стороны не может быть пустым'; }
			}
			, { fields: ['Address_CompanyN']
				, check: function (item) { return '' != h_addr.SafePrepareReadableTextWithoutRoom(item, ''); }
				, description: function (element, field) { return 'Поле "Адрес прописки" сотрудника принимающей стороны не может быть пустым'; }
			}
			, h_constraints.for_Field('entrancePurpose', h_constraints.NotEmpty())
			, { fields: ['entrancePurpose']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_arrivals.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, h_constraints.for_Field('stayPeriod_dateFrom', h_constraints.Date())
			, h_constraints.for_Field('stayPeriod_dateTo', h_constraints.Date())
			, { fields: ['stayPeriod_dateFrom']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Дата прибытия гражданина не может быть пустой'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'Birthday']
				, check: function (dateFrom, birthday) { return h_constraints.check_PeriodDate(birthday, dateFrom); }
				, description: function (element, dateFrom, birthday) { return 'Дата прибытия гражданина не может быть меньше даты рождения'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'DocumentGivenDate']
				, check: function (dateFrom, date) { return h_constraints.check_PeriodDate(date, dateFrom); }
				, description: function (element, dateFrom, date) { return 'Дата прибытия гражданина не может быть раньше даты выдачи документа удостоверяющего личность'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'DocumentGivenDate_Relation']
				, check: function (dateFrom, date) { return h_constraints.check_PeriodDate(date, dateFrom); }
				, description: function (element, dateFrom, date) { return 'Дата прибытия гражданина не может быть раньше даты выдачи документа удостоверяющего личность законного представителя'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'Visa_type', 'Visa_validFrom']
				, marks: ['stayPeriod_dateFrom', 'Visa_validFrom']
				, check: function (dateFrom, type, date) { return h_VisaType.idVisaType == type.id ? h_constraints.check_PeriodDate(date, dateFrom) : true; }
				, description: function (element, dateFrom, type, date) { return 'Дата прибытия гражданина не может быть раньше даты начала действия визы'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'Visa_type', 'Visa_issued']
				, marks: ['stayPeriod_dateFrom', 'Visa_issued']
				, check: function (dateFrom, type, date) {
					return h_VisaType.idTypesWithIssuedDateAndDateTo.indexOf(type.id) > -1 ? h_constraints.check_PeriodDate(date, dateFrom) : true;
				}
				, description: function (element, dateFrom, type, date) { return 'Дата прибытия гражданина не может быть раньше даты выдачи документа для РФ'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'Visa_type', 'LivingDocDecisionDate']
				, marks: ['stayPeriod_dateFrom', 'LivingDocDecisionDate']
				, check: function (dateFrom, type, date) { return h_VisaType.idTypeWithRvpDateTo.indexOf(type.id) > -1 ? h_constraints.check_PeriodDate(date, dateFrom) : true; }
				, description: function (element, dateFrom, type, date) { return 'Дата прибытия гражданина не может быть раньше даты принятия решения, указанной в документе для РФ'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'MigrationCard_dateFrom']
				, check: function (dateFrom, date) { return h_constraints.check_PeriodDate(date, dateFrom); }
				, description: function (element, dateFrom, date) { return 'Дата прибытия гражданина не может быть раньше даты въезда в РФ, указанной в миграционной карте'; }
			}
			, { fields: ['stayPeriod_dateFrom']
				, check: function (dateFrom) { return h_constraints.recommend_check_DateNotAfterOneWorkDay(dateFrom); }
				, description: function (element, dateFrom) {
					return 'Уведомление о прибытии иностранного гражданина (лица без гражданства) должно ' +
						'предоставляться администрацией гостиничного учреждения в течение одного рабочего ' +
						'дня, следующего за днем его прибытия в место пребывания, а сегодня ' + h_times.safeDateTime().toLocaleDateString();
				}
			}
			, { fields: ['stayPeriod_dateFrom']
				, check: function (dateFrom) { return h_constraints.check_BeforeDateNow(dateFrom); }
				, description: function (element, dateFrom) { return 'Дата прибытия указана неверно (в будущем, ведь сегодня ' + h_times.safeDateTime().toLocaleDateString() + ')'; }
			}
			, { fields: ['stayPeriod_dateTo']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Дата убытия гражданина не может быть пустой'; }
			}
			, { fields: ['stayPeriod_dateFrom', 'stayPeriod_dateTo']
				, check: function (dateFrom, dateTo) { return h_constraints.check_PeriodDate(dateFrom, dateTo); }
				, description: function (element, dateFrom, dateTo) { return 'Дата убытия гражданина должна быть не меньше даты его прибытия'; }
			}
			, { fields: ['stayPeriod_dateTo', 'DocumentFinishDate']
				, check: function (dateTo, date) { return h_constraints.recommend_check_PeriodDate(dateTo, date); }
				, description: function (element, dateTo, date) { return 'Дата убытия гражданина не может быть позднее даты окончания действия документа удостоверяющего личность'; }
			}
			, { fields: ['stayPeriod_dateTo', 'DocumentFinishDate_Relation']
				, check: function (dateTo, date) { return h_constraints.recommend_check_PeriodDate(dateTo, date); }
				, description: function (element, dateTo, date) { return 'Дата убытия гражданина не может быть позднее даты окончания действия документа удостоверяющего личность законного представителя'; }
			}
			, { fields: ['stayPeriod_dateTo', 'Visa_type', 'Visa_validTo']
				, marks: ['stayPeriod_dateTo', 'Visa_validTo']
				, check: function (dateTo, type, date) {
					return h_VisaType.idTypeWithRvpDateTo.indexOf(type.id) == -1 ? h_constraints.recommend_check_PeriodDate(dateTo, date) : true;
				}
				, description: function (element, dateTo, type, date) { return 'Дата убытия гражданина не может быть позднее даты окончания действия документа для РФ'; }
			}
			, { fields: ['stayPeriod_dateTo', 'Visa_type', 'RvpVisa_validTo']
				, marks: ['stayPeriod_dateTo', 'RvpVisa_validTo']
				, check: function (dateTo, type, date) { return h_VisaType.idTypeWithRvpDateTo.indexOf(type.id) > -1 ? h_constraints.recommend_check_PeriodDate(dateTo, date) : true; }
				, description: function (element, dateto, type, date) { return 'Дата убытия гражданина не может быть позднее срока действия документа для РФ'; }
			}
			, { fields: ['stayPeriod_dateTo', 'MigrationCard_dateTo']
				, check: function (dateTo, date) { return h_constraints.recommend_check_PeriodDate(dateTo, date); }
				, description: function (element, dateTo, date) { return 'Дата убытия гражданина не может быть позднее даты выезда из РФ, указанной в миграционной карте'; }
			}

			, { fields: ['DocumentGivenDate', 'Visa_issued', 'Visa_type', 'Visa_series', 'Visa_number', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentGivenDate', 'Visa_issued']
				, check: function (dateDocumentGiven, date, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					return h_constraints.check_NotEmpty(vtype) && h_VisaType.idTypesWithIssuedDateAndDateTo.indexOf(vtype.id) > -1
						? h_constraints.recommend_check_PeriodDate(dateDocumentGiven, date)
						: true;
				}
				, description: function (element, dateGiven, date, lvtype, lvseries, lvnumber, ldtype, ldseries, ldnumber, 
					vdateGiven, vdate, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					var message = 'Дата выдачи документа ';
					message += vtype && '' != vtype ? '"' + vtype.text + '"' : '';
					message += vseries && '' != vseries ? ' ' + vseries : '';
					message += vnumber && '' != vnumber ? ' ' + vnumber : '';
					message += ' не может быть меньше даты выдачи документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					return message;
				}
			}
			, { fields: ['DocumentGivenDate', 'Visa_validFrom', 'Visa_type', 'Visa_series', 'Visa_number', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentGivenDate', 'Visa_validFrom']
				, check: function (dateDocumentGiven, date, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					return h_constraints.check_NotEmpty(vtype) && h_VisaType.idVisaType == vtype.id
						? h_constraints.recommend_check_PeriodDate(dateDocumentGiven, date)
						: true;
				}
				, description: function (element, dateGiven, date, lvtype, lvseries, lvnumber, ldtype, ldseries, ldnumber, 
					vdateGiven, vdate, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					var message = 'Дата начала действия документа ';
					message += vtype && '' != vtype ? '"' + vtype.text + '"' : '';
					message += vseries && '' != vseries ? ' ' + vseries : '';
					message += vnumber && '' != vnumber ? ' ' + vnumber : '';
					message += ' не может быть меньше даты выдачи документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					return message;
				}
			}
			, { fields: ['DocumentGivenDate', 'LivingDocDecisionDate', 'Visa_type', 'Visa_series', 'Visa_number', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentGivenDate', 'LivingDocDecisionDate']
				, check: function (dateDocumentGiven, date, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					return h_constraints.check_NotEmpty(vtype) && h_VisaType.idTypesWithDecisionDate.indexOf(vtype.id) > -1 ?
					h_constraints.recommend_check_PeriodDate(dateDocumentGiven, date) :
					true;
				}
				, description: function (element, dateGiven, date, lvtype, lvseries, lvnumber, ldtype, ldseries, ldnumber,
					vdateGiven, vdate, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					var message = 'Дата принятия решения документа ';
					message += vtype && '' != vtype ? '"' + vtype.text + '"' : '';
					message += vseries && '' != vseries ? ' ' + vseries : '';
					message += vnumber && '' != vnumber ? ' ' + vnumber : '';
					message += ' не может быть меньше даты выдачи документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					return message;
				}
			}
			, {
				fields: ['DocumentGivenDate', 'MigrationCard_dateFrom', 'MigrationCard_series', 'MigrationCard_number', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentGivenDate', 'MigrationCard_dateFrom']
				, check: function (dateDocumentGiven, date, vseries, vnumber, dtype, dseries, dnumber) {
					return h_constraints.recommend_check_PeriodDate(dateDocumentGiven, date);
				}
				, description: function (element, dateGiven, date, lvseries, lvnumber, ldtype, ldseries, ldnumber,
					vdateGiven, vdate, vseries, vnumber, dtype, dseries, dnumber) {
					var message = 'Дата въезда в РФ, мигр. карта';
					message += vseries && '' != vseries ? ' ' + vseries : '';
					message += vnumber && '' != vnumber ? ' ' + vnumber : '';
					message += ' не может быть меньше даты выдачи документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					return message;
				}
			}
			, { fields: ['DocumentFinishDate', 'Visa_validTo', 'Visa_type', 'Visa_series', 'Visa_number', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentFinishDate', 'Visa_validTo']
				, check: function (dateFinish, date, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					return h_constraints.check_NotEmpty(vtype) && h_VisaType.idTypesWithIssuedDateAndDateTo.indexOf(vtype.id) > -1 ?
					h_constraints.recommend_check_PeriodDate(date, dateFinish) :
					true;
				}
				, description: function (element, dateFinish, date, lvtype, lvseries, lvnumber, ldtype, ldseries, ldnumber,
					vdateFinish, vdate, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					var message = 'Дата окончания действия документа ';
					message += vtype && '' != vtype ? '"' + vtype.text + '"' : '';
					message += vseries && '' != vseries ? ' ' + vseries : '';
					message += vnumber && '' != vnumber ? ' ' + vnumber : '';
					message += ' не может превышать дату окончания действия документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					return message;
				}
			}
			, { fields: ['DocumentFinishDate', 'RvpVisa_validTo', 'Visa_type', 'Visa_series', 'Visa_number', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, marks: ['DocumentFinishDate', 'RvpVisa_validTo']
				, check: function (dateFinish, date, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					return h_constraints.check_NotEmpty(vtype) && h_VisaType.idTypeWithRvpDateTo.indexOf(vtype.id) > -1 ?
					h_constraints.recommend_check_PeriodDate(date, dateFinish) :
					true;
				}
				, description: function (element, dateFinish, date, lvtype, lvseries, lvnumber, ldtype, ldseries, ldnumber,
					vdateFinish, vdate, vtype, vseries, vnumber, dtype, dseries, dnumber) {
					var message = 'Срок действия документа ';
					message += vtype && '' != vtype ? '"' + vtype.text + '"' : '';
					message += vseries && '' != vseries ? ' ' + vseries : '';
					message += vnumber && '' != vnumber ? ' ' + vnumber : '';
					message += ' не может превышать дату окончания действия документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					return message;
				}
			}
			, { fields: ['DocumentFinishDate', 'MigrationCard_dateTo', 'MigrationCard_series', 'MigrationCard_number', 'DocumentType', 'DocumentSeries', 'DocumentNumber']
				, check: function (dateFinish, date, vseries, vnumber, dtype, dseries, dnumber) {
					return h_constraints.recommend_check_PeriodDate(date, dateFinish);
				}
				, description: function (element, dateFinish, date, lvseries, lvnumber, ldtype, ldseries, ldnumber,
					vdateFinish, vdate, vseries, vnumber, dtype, dseries, dnumber) {
					var message = 'Дата выезда из РФ, мигр. карта';
					message += vseries && '' != vseries ? ' ' + vseries : '';
					message += vnumber && '' != vnumber ? ' ' + vnumber : '';
					message += ' не может превышать дату окончания действия документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					return message;
				}
			}

   			, h_constraints.for_Field('Hotel_organization_name', h_constraints.NotEmpty())
			, h_constraints.for_Field('Hotel_organization_inn', h_constraints.Inn())
			, { fields: ['Organization_Person_lastName']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Фамилия ответственного лица не может быть пустой'; }
			}
			, { fields: ['Organization_Person_lastName']
				, check: h_constraints.recommend_check_Ciryllic
				, description: function (element, field) { return 'Фамилия ответственного лица содержит недопустимые символы'; }
			}
			, { fields: ['Organization_Person_firstName']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Имя ответственного лица не может быть пустым'; }
			}
			, { fields: ['Organization_Person_firstName']
				, check: h_constraints.recommend_check_Ciryllic
				, description: function (element, field) { return 'Имя ответственного лица содержит недопустимые символы'; }
			}
			, { fields: ['Organization_Person_middleName']
				, check: h_constraints.recommend_check_Ciryllic
				, description: function (element, field) { return 'Отчество ответственного лица содержит недопустимые символы'; }
			}
			, h_constraints.for_Field('Organization_Person_birthDate', h_constraints.Date())
			, { fields: ['Organization_Person_birthDate']
				, check: h_constraints.recommend_check_BeforeDateNow
				, description: function (element, field) { return 'Недопустимая дата рождения ответственного лица'; }
			}
			, { fields: ['Organization_Person_birthDate']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Дата рождения ответственного лица не может быть пустой'; }
			}
			, { fields: ['Organization_Person_gender']
				, check: function (item) { return '' == item ? 'recommended' : true; }
				, description: function (element, field) { return 'Не указан пол ответственного лица'; }
			}
			, { fields: ['Organization_Person_gender', 'Organization_Person_middleName']
				, check: h_constraints.recommend_check_SexByMiddleName
				, description: h_constraints.description_SexByMiddleName
			}
			, {
				fields: ['Organization_Person_nationality']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_citizenship.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['DocumentType_Company']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Документ, удостоверяющий личность, ответственного лица не может быть пустым'; }
			}
			, { fields: ['DocumentType_Company']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_DocumentType.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Organization_Document_number']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Номер документа ответственного лица не может быть пустым'; }
			}
			, h_constraints.for_Field('Organization_Document_issued', h_constraints.Date())
			, h_constraints.for_Field('Organization_Document_validTo', h_constraints.Date())
			, { fields: ['Organization_Document_issued']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Дата выдачи документа ответственного лица не может быть пустой'; }
			}
			, { fields: ['Organization_Person_birthDate', 'Organization_Document_issued']
				, check: function (birthday, dateDocumentGiven) { return h_constraints.check_PeriodDate(birthday, dateDocumentGiven); }
				, description: function (element, birthday, dateDocumentGiven) { return 'Дата документа, удостоверяющего личность ответственного лица, не может быть меньше даты рождения'; }
			}
			, { fields: ['Organization_Document_issued', 'Organization_Document_validTo']
				, check: h_constraints.check_SrictPeriodDate
				, description: function (element, dateFrom, dateTo) {
					return 'Срок действия документа, удостоверяющего личность ответственного лица, должен быть больше даты его выдачи';
				}
			}
			, {
				fields: ['Organization_Document_authorityOrgan', 'Organization_Document_authorityOrgan_text', 'Organization_Document_authorityOrgan_check', 'DocumentType_Company', 'schemaVersion']
				, check: function (organ, txt, check, type, schema) {
					if ('1.102.2' != schema || (h_constraints.check_Empty(type) && type.id != '103008'))
						return true;
					else
						return !((check && h_constraints.check_Empty(organ)) || ((!check || '' == check) && h_constraints.check_Empty(txt)));
				}
				, description: function (element, field) {
					return 'Не указан орган, выдавший документ, удостоверяющий личность ответственного лица';
				}
			}
		];
		return constraints;
	}
});
