include ..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions
execute_javascript_line "app.profile= { 'Extensions': { 'fms': { 'Abonent': { 'hotel_id': '162534' } } } };"
wait_click_text "Обновить данными из профиля"

execute_javascript_stored_lines add_select2_test_functions
play_stored_lines migrationn_select2_focus_1

wait_click_text "Личные данные"
shot_check_png ..\..\shots\3sav.png
wait_click_text "Данные о пребывании в РФ"
shot_check_png ..\..\shots\19sav_1.png
wait_click_text "Прибытие"
shot_check_png ..\..\shots\3sav_3.png
wait_click_text "Принимающая сторона"
je $('#btn_test_forms_SaveForm').focus();
shot_check_png ..\..\shots\3sav_4.png
wait_click_text "Сохранить содержимое формы"
wait_click_text "Да, сохранить"

dump_js wbt_controller_GetFormContentTextArea ..\..\contents\18sav.result.xml

set_value_id form-content-text-area ""
set_value_id cpw-content-select "fms.migrationn_tempResidencePermit_dict"

wait_click_text "Редактировать"
wait_click_text "Обновить данными из профиля"

play_stored_lines migrationn_select2_focus_1

wait_click_text "Личные данные"
shot_check_png ..\..\shots\3sav.png
wait_click_text "Данные о пребывании в РФ"
shot_check_png ..\..\shots\19sav_1_dict.png
wait_click_text "Прибытие"
shot_check_png ..\..\shots\3sav_3.png
wait_click_text "Принимающая сторона"
je $('#btn_test_forms_SaveForm').focus();
shot_check_png ..\..\shots\3sav_4.png

wait_click_text "Сохранить содержимое формы"
wait_click_text "Да, сохранить"

dump_js wbt_controller_GetFormContentTextArea ..\..\contents\18sav_dict.result.xml

exit