wait_click_text "OK"

include ..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions

execute_javascript_line "app.profile= { 'Extensions': { 'fms': { 'Abonent': { 'hotel_id': '162534', 'employee_ummsId': '1001', 'supplierInfo': '1800000008002001','sono': {'id': '128752'},'subdivision': {'id': '3089'} } } } };"

wait_click_text "Обновить данными из профиля"

execute_javascript_stored_lines add_select2_test_functions

wait_text "Уведомление о прибытии иностранного гражданина"

play_stored_lines migrationn_select2_fields_1
play_stored_lines migrationn_fields_1

  # Данные о пребывании в РФ
    click_text "Данные о пребывании в РФ"
    execute_javascript_line "window.wbt_text_SelectTextInSelect2('cpw_form_Profession','Надуватель жаб');"
    wait_id_text s2id_cpw_form_Profession "Надуватель жаб"

play_stored_lines migrationn_select2_focus_1

wait_click_text "Личные данные"
shot_check_png ..\..\shots\3sav.png
wait_click_text "Данные о пребывании в РФ"
shot_check_png ..\..\shots\12sav_1.png
wait_click_text "Прибытие"
execute_javascript_line "window.wbt_text_SelectTextInSelect2('cpw_form_Organization_Document_authorityOrgan','181-001');"
wait_id_text s2id_cpw_form_Organization_Document_authorityOrgan "181-001"
shot_check_png ..\..\shots\3sav_3.png
wait_click_text "Принимающая сторона"
shot_check_png ..\..\shots\3sav_4.png

wait_click_text "Сохранить содержимое формы"
shot_check_png ..\..\shots\12sav_valid.png

wait_click_text "Да, сохранить"

dump_js wbt_controller_GetFormContentTextArea ..\..\contents\12sav.result.xml

exit