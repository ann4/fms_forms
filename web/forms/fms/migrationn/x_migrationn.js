define([
	  'forms/fms/base/x_fms'
	, 'forms/fms/migrationn/m_migrationn'
	, 'forms/fms/base/town/m_town'
	, 'forms/fms/base/upload/m_upload'
	, 'forms/fms/base/upload/h_upload'
	, 'forms/base/h_times'
	, 'forms/base/log'
	, 'forms/fms/base/h_xsd'
],
function (BaseCodec, model_migration, model_town, model_upload, helper_upload, helper_Times, GetLogger, h_xsd)
{
	var log = GetLogger('x_migration');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.GetRootNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/migration/staying'; };

		res.AddressWithHousingType = true;

		res.Encode = function (m_migrationn)
		{
			log.Debug('Encode() {');
			var xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + this.FormatEOL();
			try
			{
				xml_string += '<ns9:case xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns="http://umms.fms.gov.ru/replication/core" xmlns:ns2="http://umms.fms.gov.ru/replication/core/correction" xmlns:ns3="http://umms.fms.gov.ru/replication/foreign-citizen-core" xmlns:ns4="http://umms.fms.gov.ru/replication/migration" xmlns:ns5="http://umms.fms.gov.ru/replication/hotel" xmlns:ns6="http://umms.fms.gov.ru/replication/migration/staying/case-edit" xmlns:ns7="http://umms.fms.gov.ru/replication/hotel/form5" xmlns:ns8="http://umms.fms.gov.ru/replication/migration/staying/unreg" xmlns:ns9="http://umms.fms.gov.ru/replication/migration/staying" xmlns:ns10="http://umms.fms.gov.ru/replication/invitation-application" xmlns:ns11="http://umms.fms.gov.ru/replication/payment" xmlns:ns12="http://umms.fms.gov.ru/hotel/hotel-response" schemaVersion="1.0">' + this.FormatEOL();

				xml_string = '1.102.2' == this.SchemaVersion ? this.EncodeCoreCaseFieldsFMS(1, m_migrationn, xml_string) : this.EncodeCoreCaseFields(1, m_migrationn, xml_string);

				xml_string += this.StringifyField(1, "ns4:notificationReceived", m_migrationn.notificationReceived, 'Дата приема уведомления');

				xml_string += res.EncodeStayPeriod(1, m_migrationn);

				var personId_tagName = ('1.3.42' == this.SchemaVersion || '1.102.2' == this.SchemaVersion) ? 'personUid' : 'personId';

				log.Debug('out personDataDocument {');
				xml_string += this.FormatTabs(1) + '<ns4:personDataDocument>' + this.FormatEOL();
				xml_string += this.FormatTabs(2) + '<person>' + this.FormatEOL();
				xml_string += this.StringifyField(3, "uid", m_migrationn.person_uid);
				xml_string += this.StringifyField(3, personId_tagName, m_migrationn.person_personId);
				xml_string += this.StringifyField(3, "lastName", m_migrationn.LastName);
				xml_string += this.SafeStringifyField(3, "lastNameLat", m_migrationn.LastName_latin);
				xml_string += this.StringifyField(3, "firstName", m_migrationn.FirstName);
				xml_string += this.SafeStringifyField(3, "firstNameLat", m_migrationn.FirstName_latin);
				if (m_migrationn.MiddleName)
				{
					xml_string += this.StringifyField(3, "middleName", m_migrationn.MiddleName);
					xml_string += this.SafeStringifyField(3, "middleNameLat", m_migrationn.MiddleName_latin);
				}
				xml_string += this.EncodeSex(3, m_migrationn.Sex);
				xml_string += this.SafeStringifyField(3, "birthDate", m_migrationn.Birthday);
				xml_string += this.SafeEncodeNamedDictionaryItem(3, m_migrationn.Nationality, 'citizenship', 'Citizenship');
				xml_string += this.EncodeBirthplace(3, m_migrationn.Birthplace);
				xml_string += this.FormatTabs(2) + '</person>' + this.FormatEOL();

				xml_string += this.FormatTabs(2) + '<document>' + this.FormatEOL();
				xml_string += this.StringifyField(3, "uid", m_migrationn.Document_uid);
				xml_string += this.SafeEncodeNamedDictionaryItem(3, m_migrationn.DocumentType, 'type', '1.102.2' == this.SchemaVersion ? 'mig.identityDocumentType' : 'DocumentType');
				xml_string += this.StringifyField(3, "series", m_migrationn.DocumentSeries);
				xml_string += this.StringifyField(3, "number", m_migrationn.DocumentNumber);
				xml_string += this.StringifyField(3, "authority", m_migrationn.authority);
				xml_string += this.SafeStringifyField(3, "issued", res.DateEncode(m_migrationn.DocumentGivenDate));
				xml_string += this.SafeStringifyField(3, "validFrom", res.DateEncode(m_migrationn.DocumentValidFrom));
				if (m_migrationn.DocumentFinishDate)
					xml_string += this.SafeStringifyField(3, "validTo", res.DateEncode(m_migrationn.DocumentFinishDate));
				xml_string += this.SafeEncodeNamedDictionaryItem(3, m_migrationn.Document_DocumentStatus, 'status', 'DocumentStatus');
				xml_string += this.FormatTabs(2) + '</document>' + this.FormatEOL();

				xml_string += this.StringifyField(2, "entered", m_migrationn.Document_entered, 'Флаг, указывающий на то, вписано ли данное лицо в документ');
				xml_string += this.FormatTabs(1) + '</ns4:personDataDocument>' + this.FormatEOL();
				log.Debug('out personDataDocument }');

				log.Debug('out stayPlace {');
				xml_string += this.OpenTagLine(1, 'ns4:stayPlace');
				xml_string += this.EncodeAddress(2, 'address', m_migrationn.Address);
				xml_string += this.SafeStringifyField(2, "phone", m_migrationn.Phone);
				xml_string += this.CloseTagLine(1, 'ns4:stayPlace');
				log.Debug('out stayPlace }');

				if ('true' == m_migrationn.Document_entered)
				{
					xml_string += this.OpenTagLine(1, 'ns4:representative');
					xml_string += this.OpenTagLine(2, 'ns4:personDataDocument');
					xml_string += this.FormatTabs(3) + '<person>' + this.FormatEOL();
					xml_string += this.StringifyField(4, "uid", m_migrationn.person_uid_Relation);
					xml_string += this.StringifyField(4, personId_tagName, m_migrationn.person_personId_Relation);
					xml_string += this.StringifyField(4, "lastName", m_migrationn.LastName_Relation);
					xml_string += this.SafeStringifyField(4, "lastNameLat", m_migrationn.LastName_Relation_latin);
					xml_string += this.StringifyField(4, "firstName", m_migrationn.FirstName_Relation);
					xml_string += this.SafeStringifyField(4, "firstNameLat", m_migrationn.FirstName_Relation_latin);
					if (m_migrationn.MiddleName_Relation)
					{
						xml_string += this.StringifyField(4, "middleName", m_migrationn.MiddleName_Relation);
						xml_string += this.SafeStringifyField(4, "middleNameLat", m_migrationn.MiddleName_Relation_latin);
					}
					xml_string += this.EncodeSex(4, m_migrationn.Sex_Relation);
					xml_string += this.SafeStringifyField(4, "birthDate", m_migrationn.Birthday_Relation);
					xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn.Nationality_Relation, 'citizenship', 'Citizenship');
					xml_string += this.EncodeBirthplace(4, m_migrationn.Birthplace_Relation);
					xml_string += this.FormatTabs(3) + '</person>' + this.FormatEOL();

					xml_string += this.FormatTabs(3) + '<document>' + this.FormatEOL();
					xml_string += this.StringifyField(4, "uid", m_migrationn.Document_uid_Relation);
					xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn.DocumentType_Relation, 'type', '1.102.2' == this.SchemaVersion ? 'mig.LRDocumentType' : 'DocumentType');
					xml_string += this.StringifyField(4, "series", m_migrationn.DocumentSeries_Relation);
					xml_string += this.StringifyField(4, "number", m_migrationn.DocumentNumber_Relation);
					xml_string += this.SafeStringifyField(4, "authority", m_migrationn.authority_Relation);
					xml_string += this.SafeStringifyField(4, "issued", res.DateEncode(m_migrationn.DocumentGivenDate_Relation));
					xml_string += this.SafeStringifyField(4, "validFrom", res.DateEncode(m_migrationn.DocumentValidFrom_Relation));
					if (m_migrationn.DocumentFinishDate_Relation)
						xml_string += this.SafeStringifyField(4, "validTo", res.DateEncode(m_migrationn.DocumentFinishDate_Relation));
					xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn.DocumentStatus_Relation, 'status', 'DocumentStatus');
					xml_string += this.FormatTabs(3) + '</document>' + this.FormatEOL();

					xml_string += this.StringifyField(3, "entered", '1.102.2' == this.SchemaVersion ? false : true, 'Флаг, указывающий на то, вписано ли данное лицо в документ');
					xml_string += this.CloseTagLine(2, 'ns4:personDataDocument');
					xml_string += this.CloseTagLine(1, 'ns4:representative');
				}

				if (m_migrationn.PreAddress && m_migrationn.PreAddress.russianAddress)
				{
					log.Debug('out arrivalFromPlaceAddress {');
					xml_string += this.EncodeRussianAddress(1, m_migrationn.PreAddress.russianAddress, 'ns4:arrivalFromPlaceAddress');
					log.Debug('out arrivalFromPlaceAddress }');
				}

				xml_string += this.SafeDocResidenceEncode(1, m_migrationn);

				if (m_migrationn.sono)
					xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn.sono, 'ns4:sono', '1.102.2' == this.SchemaVersion ? 'officialOrgan.fns' : 'officialOrgan');

				var ns = 'ns9';
				if ('1.3.40' == this.SchemaVersion || '1.3.42' == this.SchemaVersion || '1.102.2' == this.SchemaVersion)
				{
					xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn.noticeFrom, 'ns4:noticeFrom', 'NoticeFrom');
					xml_string += this.EncodeNamedDictionaryItem(1, { id: '10406', text: 'Нет' }, 'ns4:prolongationReason', 'ProlongationReason');
					ns = 'ns4';
				}
				else if ('1.3.37' == this.SchemaVersion)
				{
					if (m_migrationn.notificationNumber)
						xml_string += this.StringifyField(1, "ns9:notificationNumber", m_migrationn.notificationNumber);
					xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn.noticeFrom, 'ns9:noticeFrom', 'NoticeFrom');
					xml_string += this.EncodeNamedDictionaryItem(1, { id: '10406', text: 'Нет' }, 'ns9:prolongationReason', 'ProlongationReason');
				}
				else // 1.3.36..
				{
					if (m_migrationn.notificationNumber)
						xml_string += this.StringifyField(1, "ns9:notificationNumber", m_migrationn.notificationNumber);
					xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn.noticeFrom, 'ns9:noticeFrom', 'NoticeFrom');
					xml_string += this.StringifyField(1, "ns9:firstArrival", m_migrationn.firstArrival);
				}

				xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn.StateProgram, ns + ':stateProgramMember','mig.specialStatus');

				xml_string += this.OpenTagLine(1,ns + ':host');
				xml_string += this.FormatTabs(2) + '<ns4:organization>' + this.FormatEOL();
				xml_string += this.StringifyField(3, "uid", m_migrationn.Hotel_organization_uid);
				xml_string += this.StringifyField(3, "inn", m_migrationn.Hotel_organization_inn);
				xml_string += this.StringifyField(3, "name", m_migrationn.Hotel_organization_name);
				xml_string += this.FormatTabs(3) + '<address>' + this.FormatEOL();
				xml_string += this.EncodeAddress(4, 'address', m_migrationn.Address_Company);
				xml_string += this.FormatTabs(3) + '</address>' + this.FormatEOL();
				if ('1.102.2' == this.SchemaVersion) {
					xml_string += this.StringifyField(3, "pboul", 10 != m_migrationn.Hotel_organization_inn.length ? 'true' : 'false');
				}
				xml_string += this.SafeStringifyField(3, "headOrganization", m_migrationn.headOrganization);
				xml_string += this.FormatTabs(2) + '</ns4:organization>' + this.FormatEOL();

				xml_string += this.OpenTagLine(2, ns + ':personDataDocument');
				xml_string += this.FormatTabs(3) + '<person>' + this.FormatEOL();
				xml_string += this.StringifyField(4, "uid", m_migrationn.Organization_Person_uid);
				xml_string += this.StringifyField(4, personId_tagName, m_migrationn.Organization_Person_personId);

				var write_Organization_Person_lat= 
					(m_migrationn.Organization_Person_nationality && null != m_migrationn.Organization_Person_nationality && 'RUS' != m_migrationn.Organization_Person_nationality.id);

				xml_string += this.StringifyField(4, "lastName", m_migrationn.Organization_Person_lastName);
				if (write_Organization_Person_lat)
					xml_string += this.SafeStringifyField(4, "lastNameLat", m_migrationn.Organization_Person_lastName_latin);
				xml_string += this.StringifyField(4, "firstName", m_migrationn.Organization_Person_firstName);
				if (write_Organization_Person_lat)
				    xml_string += this.SafeStringifyField(4, "firstNameLat", m_migrationn.Organization_Person_firstName_latin);
				if (m_migrationn.Organization_Person_middleName)
				{
					xml_string += this.StringifyField(4, "middleName", m_migrationn.Organization_Person_middleName);
					if (write_Organization_Person_lat)
    					xml_string += this.SafeStringifyField(4, "middleNameLat", m_migrationn.Organization_Person_middleName_latin);
				}

				xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn.Organization_Person_gender, 'gender', 'Gender');
				xml_string += this.StringifyField(4, "birthDate", m_migrationn.Organization_Person_birthDate);
				xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn.Organization_Person_nationality, 'citizenship', 'Citizenship');
				xml_string += this.EncodeBirthplace(4, m_migrationn.Organization_Person_birthPlace);
				xml_string += this.FormatTabs(3) + '</person>' + this.FormatEOL();

				xml_string += this.FormatTabs(3) + '<document>' + this.FormatEOL();
				xml_string += this.StringifyField(4, "uid", m_migrationn.Organization_Document_uid);
				xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn.DocumentType_Company, 'type', '1.102.2' == this.SchemaVersion ? 'organizationPersonDocumentType' : 'DocumentType');
				xml_string += this.StringifyField(4, "series", m_migrationn.Organization_Document_series);
				xml_string += this.StringifyField(4, "number", m_migrationn.Organization_Document_number);
				xml_string += this.SafeEncodeDocumentGivenByFMS(4, m_migrationn.Organization_Document_authorityOrgan_check, m_migrationn.Organization_Document_authorityOrgan, m_migrationn.Organization_Document_authorityOrgan_text);
				xml_string += this.SafeStringifyField(4, "issued", res.DateEncode(m_migrationn.Organization_Document_issued));
				xml_string += this.SafeStringifyField(4, "validFrom", res.DateEncode(m_migrationn.Organization_Document_validFrom));
				if (m_migrationn.Organization_Document_validTo)
					xml_string += this.SafeStringifyField(4, "validTo", res.DateEncode(m_migrationn.Organization_Document_validTo));
				xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn.Company_DocumentStatus, 'status', 'DocumentStatus');
				xml_string += this.FormatTabs(3) + '</document>' + this.FormatEOL();

				xml_string += this.StringifyField(3, "entered", '1.102.2' == this.SchemaVersion ? false : m_migrationn.Organization_Document_entered, 'Флаг, указывающий на то, вписано ли данное лицо в документ');
				xml_string += this.CloseTagLine(2, ns + ':personDataDocument');

				xml_string += this.OpenTagLine(2, ns + ':contactInfo');
				xml_string += this.EncodeAddress(3, 'address', m_migrationn.Address_CompanyN);
				xml_string += this.SafeStringifyField(3, "phone", m_migrationn.Hotel_organization_phone);
				xml_string += this.CloseTagLine(2, ns + ':contactInfo');

				xml_string += this.CloseTagLine(1, ns + ':host');

				xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn.entrancePurpose, ns + ':entrancePurpose', 'VisitPurpose', 'Цель въезда');

				xml_string += this.SafeMigrationCardEncode(1, m_migrationn, ns);

				if (m_migrationn.Profession && m_migrationn.Profession.id && !m_migrationn.Profession.text)
				{
					xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn.Profession, ns + ':profession', 'Profession');
				}
				else if (m_migrationn.Profession && m_migrationn.Profession.text)
				{
					xml_string += this.SafeStringifyField(1, ns + ':profession', m_migrationn.Profession.text);
				}

				xml_string += '</ns9:case>' + this.FormatEOL();

				xml_string += this.EncodeAdditionalFields(m_migrationn);
			}
			catch (err)
			{
				alert("Ошибка:" + err.description);
				var max_length = 1000;
				for (var i = 0; i < xml_string.length; i += max_length)
				{
					alert(i + '-' + (i + max_length) + ', from ' +
					xml_string.length + ':\r\n' +
					xml_string.substring(i, i + max_length));
				}
			}

			log.Debug('Encode() }');
			return xml_string;
		};

		res.EncodeAdditionalFields = function (m_migrationn)
		{
			var fields = {};
			fields.PersonPhone = m_migrationn && m_migrationn.PersonPhone ? m_migrationn.PersonPhone : '';
			fields.Hotel_Document = m_migrationn && m_migrationn.Hotel_Document ? m_migrationn.Hotel_Document : '';
			return '<!-- ' + JSON.stringify(fields) + ' -->';
		};

		res.DecodeRepresentative = function (node, m_migrationn)
		{
			log.Debug('DecodeRepresentative {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'ns4:personDataDocument': self.DecodeRepresentativePersonDataDocument(child, m_migrationn); break;
				}
			});
			log.Debug('DecodeRepresentative }');
		}

		res.DecodeRepresentativePersonDataDocument = function (node, m_migrationn)
		{
			log.Debug('DecodeRepresentativePersonDataDocument {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'person': self.DecodeRepresentativePerson(child, m_migrationn); break;
					case 'document': self.DecodePersonDataDocument_Document_Relation(child, m_migrationn); break;
				}
			});
			log.Debug('DecodeRepresentativePersonDataDocument }');
		}

		res.DecodeRepresentativePerson = function (node, m_migrationn)
		{
			log.Debug('DecodeRepresentativePerson {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'uid': m_migrationn.person_uid_Relation = child.text; break;
					case 'personId': 
					case 'personUid': 
						m_migrationn.person_personId_Relation = child.text; break;
					case "lastName": m_migrationn.LastName_Relation = child.text; break;
					case "lastNameLat": m_migrationn.LastName_Relation_latin = child.text; break;
					case "firstName": m_migrationn.FirstName_Relation = child.text; break;
					case "firstNameLat": m_migrationn.FirstName_Relation_latin = child.text; break;
					case "middleName": m_migrationn.MiddleName_Relation = child.text; break;
					case "middleNameLat": m_migrationn.MiddleName_Relation_latin = child.text; break;
					case "gender": m_migrationn.Sex_Relation = self.DecodeSex(child); break;
					case "birthDate": m_migrationn.Birthday_Relation = child.text; break;
					case 'citizenship': m_migrationn.Nationality_Relation = self.DecodeDictionaryItem(child, 'Citizenship'); break;
					case 'birthPlace': m_migrationn.Birthplace_Relation = self.DecodeBirthPlace(child); break;
				}
			});
			log.Debug('DecodeRepresentativePerson }');
		}

		res.DecodeStayPlace = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "address":
						m_migrationn.Address = self.DecodeAddressAddress(child);
						break;
					case "phone": m_migrationn.Phone = child.text; break;
				}
			});
		}

		res.DecodePersonDataDocument_Document_Relation = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m_migrationn.DocumentRelation_uid = child.text; break;
					case "type": m_migrationn.DocumentType_Relation = self.DecodeIdentityDocumentType(child); break;
					case "series": m_migrationn.DocumentSeries_Relation = child.text; break;
					case "number": m_migrationn.DocumentNumber_Relation = child.text; break;
					case "authority": m_migrationn.authority_Relation = self.DecodeDictionaryItem(child, 'authorityOrgan'); break;
					case "issued": m_migrationn.DocumentGivenDate_Relation = res.DateDecode(child.text); break;
					case "validTo": m_migrationn.DocumentFinishDate_Relation = res.DateDecode(child.text); break;
					case "status": m_migrationn.DocumentStatus_Relation = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;
				}
			});
		}

		res.DecodeHotelInfoAddress_organization = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "address":
						m_migrationn.Address_Company = self.DecodeAddress(child);
						break;
				}
			});
		};

		res.DecodeHotelInfo_organization = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m_migrationn.Hotel_organization_uid = child.text; break;
					case "phone": m_migrationn.Hotel_organization_phone = child.text; break;
					case "inn": m_migrationn.Hotel_organization_inn = child.text; break;
					case "name": m_migrationn.Hotel_organization_name = child.text; break;
					case "address": m_migrationn.Address_Company = self.DecodeAddress(child); break;
					case "headOrganization": m_migrationn.headOrganization = child.text; break;
				}
			});
		};

		res.DecodeHostDataDocument_Person = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'uid': m_migrationn.Organization_Person_uid = child.text; break;
					case 'personUid': 
					case 'personId': 
						m_migrationn.Organization_Person_personId = child.text; break;

					case "lastName": m_migrationn.Organization_Person_lastName = child.text; break;
					case "firstName": m_migrationn.Organization_Person_firstName = child.text; break;
					case "middleName": m_migrationn.Organization_Person_middleName = child.text; break;

					case "lastNameLat": m_migrationn.Organization_Person_lastName_latin = child.text; break;
					case "firstNameLat": m_migrationn.Organization_Person_firstName_latin = child.text; break;
					case "middleNameLat": m_migrationn.Organization_Person_middleName_latin = child.text; break;

					case "gender": m_migrationn.Organization_Person_gender = self.DecodeSex(child); break;
					case "birthDate": m_migrationn.Organization_Person_birthDate = child.text; break;
					case 'citizenship': m_migrationn.Organization_Person_nationality = self.DecodeDictionaryItem(child, 'citizenship'); break;
					case 'birthPlace': m_migrationn.Organization_Person_birthPlace = self.DecodeBirthPlace(child); break;
				}
			});
		}

		res.DecodeHostDataDocument_Document = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m_migrationn.Organization_Document_uid = child.text; break;
				    case "type": m_migrationn.DocumentType_Company = self.DecodeOrganizationPersonDocumentType(child); break;
					case "series": m_migrationn.Organization_Document_series = child.text; break;
					case "number": m_migrationn.Organization_Document_number = child.text; break;
					case "authorityOrgan":
						m_migrationn.Organization_Document_authorityOrgan = self.DecodeOfficialOrganFMS(child);
						m_migrationn.Organization_Document_authorityOrgan_check = true;
					break;
					case "authority":
						m_migrationn.Organization_Document_authorityOrgan_text = child.text;
						m_migrationn.Organization_Document_authorityOrgan_check = false;
						break;
					case "issued": m_migrationn.Organization_Document_issued = res.DateDecode(child.text); break;
					case "validFrom": m_migrationn.Organization_Document_validFrom = res.DateDecode(child.text); break;
					case "validTo": m_migrationn.Organization_Document_validTo = res.DateDecode(child.text); break;
					case "status": m_migrationn.Company_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;
				}
			});
		}

		res.DecodeHostDataDocument = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'person': self.DecodeHostDataDocument_Person(child, m_migrationn); break;
					case 'document': self.DecodeHostDataDocument_Document(child, m_migrationn); break;
					case 'entered': m_migrationn.Organization_Document_entered = child.text; break;
				}
			});
		}

		res.DecodeContactInfoCompany = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'address': m_migrationn.Address_CompanyN = self.DecodeAddressAddress(child); break;
					case 'phone': m_migrationn.Hotel_organization_phone = child.text; break;
				}
			});
		}

		res.DecodeHotelInfo = function (node, m_migrationn)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "ns4:organization": self.DecodeHotelInfo_organization(child, m_migrationn); break;
					case "ns4:personDataDocument":
					case "ns9:personDataDocument": self.DecodeHostDataDocument(child, m_migrationn); break;
					case "ns4:contactInfo":
					case "ns9:contactInfo": self.DecodeContactInfoCompany(child, m_migrationn); break;
				}
			});
		}

		res.DecodeDocumentPhoto = function (node, m_migrationn, i)
		{
			var self = this;
			var file = model_upload();
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "documentUID": file.uid = child.text; break;
					case "documentPhotoContent": file.content = child.text; break;
					case "documentPhotoFormat": file.format = child.text; break;
				}
			});

			file.src = 'data:' + file.format + ';base64,' + helper_upload.hexToBase64(file.content);
			m_migrationn.DocumentScan.push(file);
		}

		res.DecodeXmlComment = function(node, m_migrationn)
		{
			if (node.text && '' != node.text)
			{
				var dataComment = JSON.parse(node.text);
				m_migrationn.PersonPhone = dataComment && dataComment.PersonPhone ? dataComment.PersonPhone : '';
				m_migrationn.Hotel_Document = dataComment && dataComment.Hotel_Document ? dataComment.Hotel_Document : '';
			}
		}

		res.DecodeXmlDocument = function (doc)
		{
			var m_migrationn = model_migration();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "uid": m_migrationn.uid = child.text; break;
					case "requestId": m_migrationn.requestId = child.text; break;
					case "supplierInfo": m_migrationn.supplierInfo = child.text; break;
				    case "subdivision": m_migrationn.subdivision = this.DecodeOfficialOrganFMS(child); break;
					case "employee": m_migrationn.employee = this.DecodeEmployee(child); break;
					case "date": m_migrationn.date = child.text; break;
					case "number": m_migrationn.number = child.text; break;
					case "documentPhoto": this.DecodeDocumentPhoto(child, m_migrationn); break;

					case "ns4:notificationReceived": m_migrationn.notificationReceived = child.text; break;
					case "ns4:stayPeriod": this.DecodeStayPeriod(child, m_migrationn); break;
					case "ns4:personDataDocument": this.DecodePersonDataDocument(child, m_migrationn); break;
					case "ns4:stayPlace": this.DecodeStayPlace(child, m_migrationn); break;
					case "ns4:representative": this.DecodeRepresentative(child, m_migrationn); break;
					case "ns4:arrivalFromPlaceAddress": m_migrationn.PreAddress =
						{
							Country: { id: 'RUS', text: 'Россия' }
							, russianAddress: this.DecodeAddressAddressRussianAddress(child)
						};
						break;
					case "ns4:docResidence": this.DocResidenceDecode(child, m_migrationn); break;
					case "ns4:sono": m_migrationn.sono = this.DecodeOfficialOrganFNS(child); break;
					case "ns9:notificationNumber":
						{
							m_migrationn.notificationNumber = child.text;
							if (m_migrationn.notificationNumber)
								var parts = m_migrationn.notificationNumber.split('/');
							if (1 < parts.length)
								m_migrationn.hotel_id = parts[1];
						}
						break;
					case "ns4:noticeFrom":
					case "ns9:noticeFrom": m_migrationn.noticeFrom = this.DecodeDictionaryItem(child, 'NoticeFrom'); break;
					case "ns4:firstArrival":
					case "ns9:firstArrival": m_migrationn.firstArrival = child.text; break;
					case "ns4:stateProgramMember":
					case "ns9:stateProgramMember": m_migrationn.StateProgram = this.DecodeDictionaryItem(child, ['mig.programParticipantType', 'mig.specialStatus']); break;
					case "ns4:host":
					case "ns9:host": this.DecodeHotelInfo(child, m_migrationn); break;
					case "ns4:entrancePurpose":
					case "ns9:entrancePurpose": m_migrationn.entrancePurpose = this.DecodeDictionaryItem(child, 'VisitPurpose'); break;
					case "ns4:migrationCard":
					case "ns9:migrationCard": this.DecodeMigrationCardData(child, m_migrationn); break;
					case "ns4:profession":
					case "ns9:profession": m_migrationn.Profession = this.DecodeProfession(child); break;
				}
			}
			var docChilds = doc.childNodes;
			var docChilds_len = docChilds.length;
			for (var i = 0; i < docChilds_len; i++)
			{
				var dChild = docChilds[i];
				switch (dChild.nodeTypeString)
				{
					case "comment": this.DecodeXmlComment(dChild, m_migrationn); break;
				}
			}
			return m_migrationn;
		};

		res.GetSchemaVersionByXml = function (xml_text)
		{
		    var res = '1.3.36';
		    if (-1 != xml_text.indexOf('ns4:residencePermit') || -1 != xml_text.indexOf('ns4:temporaryResidence') || -1 != xml_text.indexOf('ns3:identifier'))
		    {
                 return '1.102.2'
		    }
			else if (-1 != xml_text.indexOf('personUid>'))
			{
				return '1.3.42';
			}
			else if (-1 != xml_text.indexOf('<ns9:prolongationReason>'))
			{
				return '1.3.37';
			}
			else if (-1!=xml_text.indexOf('<ns4:prolongationReason>'))
			{
				return '1.3.40';
			}
			return res;
		}

		var base_Encode = res.Encode;
		res.Encode = function (m_migrationn)
		{
			this.FixScheme('migrationn', this.GetSchemaVersionByModel(m_migrationn));
			log.Debug('schema version ' + this.SchemaVersion);
			this.FixAddressFreeOnlyVersionByModel(m_migrationn);
			return base_Encode.call(this, m_migrationn);
		}

		var base_Decode = res.Decode;
		res.Decode = function (xml_text)
		{
			this.FixScheme('migrationn', this.GetSchemaVersionByXml(xml_text));
			return base_Decode.call(this, xml_text);
		}

		log.Debug('Create }');
		return res;
	}
});
