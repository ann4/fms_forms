define([
	  'forms/base/codec'
	, 'forms/fms/migrationn/x_migrationn'
	, 'forms/fms/migrationn/x_csv_migrationn'
	, 'forms/base/log'
],
function (BaseCodec, x_migrationn, x_csv_migrationn, GetLogger) {
	var log = GetLogger('codec_migrationn');
	return function () {
		var res = BaseCodec();

		res.codec_xml = x_migrationn();
		res.codec_csv = null;

		res.Encode = function (m_migrationn) {
			return this.codec_xml.Encode(m_migrationn);
		}

		res.Decode = function (xml_string) {
			var result = null;
			if (-1 < xml_string.indexOf('<?xml ')) {
				result = this.codec_xml.Decode(xml_string);
			}
			else {
				this.codec_csv = x_csv_migrationn();
				result = this.codec_csv.Decode(xml_string);
			}
			return result;
		};

		return res;
	}
});
