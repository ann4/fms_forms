define([],function ()
{
	return function ()
	{
		var res =
		{
			ReportingSort: null,
			ReportingPeriodFrom: null,
			ReportingPeriodTill: null,
			ShowLatinName: false,
			listData: [],
			CountRussianRegistered: 0,
			CountOtherRegistered: 0,
			CountOtherUnRegistered: 0
		};
		return res;
	};
});