define([],
function (CreateController) {
	var file_format =
	{
		FilePrefix: 'Register'
		, FileExtension: 'xls'
		, Encoding: 65001
		, Description: 'ФМС. Реестр учета гостей.'
	};
	return file_format;
});
