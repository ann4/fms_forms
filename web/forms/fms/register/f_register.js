define(['forms/fms/register/c_register', 'forms/fms/register/ff_register'],
function (CreateController, FileFormat) {
	var form_spec =
	{
		CreateController: CreateController
		, key: 'register'
		, Title: 'Реестр учета гостей'
		, FileFormat: FileFormat
	};
	return form_spec;
});
