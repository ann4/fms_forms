define([
	  'forms/fms/base/c_reporting'
	, 'forms/base/log'
	, 'forms/fms/register/m_register'
	, 'forms/fms/register/x_register'
	, 'forms/fms/register/ff_register'
	, 'tpl!forms/fms/register/e_register.html'
	, 'tpl!forms/fms/register/v_register.xml'
	, 'forms/base/h_times'
],
function (binded_controller, GetLogger, model_report, codec, form_spec, layout_tpl, print_tpl, h_times)
{
	return function ()
	{
		var log = GetLogger('c_register');

		var controller = binded_controller(layout_tpl, form_spec, {
			extension_key: 'register'
		});

		controller.SafeCreateNewContent = function () {
			if (!this.model)
				this.model = model_report();
		};

		var base_Render = controller.Render;
		controller.Render = function (id_form_div) {
			base_Render.call(this, id_form_div);

			$('.d-picker').datepicker({
				dateFormat: "dd.mm.yy",
				closeText: 'Закрыть',
				showButtonPanel: true,
				changeMonth: true,
				changeYear: true
			});
			$('#cpw_form_ReportingPeriodFrom').focus();
		}

		controller.SetFormContent = function (content) {
			log.Debug('SetFormContent {');
			this.SafeCreateNewContent();
			this.model = content;
			if (this.model && this.model.ReportingPeriodFrom) {
				this.model.ReportingPeriodFromDate = controller.parseDate(this.model.ReportingPeriodFrom);
			}
			if (this.model && this.model.ReportingPeriodTill) {
				this.model.ReportingPeriodTillDate = controller.parseDate(this.model.ReportingPeriodTill);
			}
			this.model.CountRussianRegistered = this.model.CountRussianRegistered ? this.model.CountRussianRegistered : 0;
			this.model.CountOtherRegistered = this.model.CountOtherRegistered ? this.model.CountOtherRegistered : 0;
			this.model.CountOtherUnRegistered = this.model.CountOtherUnRegistered ? this.model.CountOtherUnRegistered : 0;
			log.Debug('SetFormContent }');
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			this.model = base_GetFormContent.call(this);
			return this.model;
		};

		var checkByDates = function (data, startDate, finishDate, model) {
			var start = controller.parseDate(data.DateStart);
			if (null == start) {
				controller.writeWarningToLog("  Не указана дата прибытия!");
				return false;
			}
			var finish = null;
			if (data.DateUnreg) {
				finish = controller.parseDate(data.DateUnreg);
			}
			if (null == finish) {
				finish = controller.parseDate(data.DateFinish);
				if (!finish) {
					controller.writeWarningToLog("  Не указана дата убытия! Предполагается, что гражданин выехал через 90 дней.");
					finish = controller.parseDate(data.DateStart);
					finish.setDate(finish.getDate() + 90);
				}
			}
			var isSuit = (start <= startDate && finish > startDate && finish <= finishDate) ||
				(start >= startDate && start <= finishDate && finish >= finishDate) ||
				(start >= startDate && finish <= finishDate) ||
				(start <= startDate && finish >= finishDate);
			if (!isSuit)
				return isSuit;

			if (startDate <= start && start <= finishDate) {
				if (data.Citizenship && 'RUS' == data.Citizenship.id) {
					model.CountRussianRegistered++;
				} else {
					model.CountOtherRegistered++;
				}
			}
			if (startDate <= finish && finish <= finishDate) {
				if ((!data.Citizenship || 'RUS' != data.Citizenship.id) && (data.id_fms_z && '' != data.id_fms_z)) {
					model.CountOtherUnRegistered++;
				}
			}
			return isSuit;
		};

		controller.ProcessingData = function (data) {
			log.Debug('ProcessingData {');
			if (!this.model || !this.model.ReportingPeriodFrom || !this.model.ReportingPeriodTill) {
				controller.writeWarningToLog("  Не заданы параметры проверки!");
				return null;
			}
			this.model.ReportingPeriodFromDate = controller.parseDate(this.model.ReportingPeriodFrom);
			this.model.ReportingPeriodTillDate = controller.parseDate(this.model.ReportingPeriodTill);
			this.model.CountRussianRegistered = this.model.CountRussianRegistered ? this.model.CountRussianRegistered : 0;
			this.model.CountOtherRegistered = this.model.CountOtherRegistered ? this.model.CountOtherRegistered : 0;
			this.model.CountOtherUnRegistered = this.model.CountOtherUnRegistered ? this.model.CountOtherUnRegistered : 0;
			var result = checkByDates(data, this.model.ReportingPeriodFromDate, this.model.ReportingPeriodTillDate, this.model);
			if (!result) {
				controller.writeWarningToLog("  Период пребывания не соответствует указанному!");
				return false;
			} else {
				if (!this.model.listData) {
					this.model.listData = [];
				}
				if (data.id_Message && '' != data.id_Message) {
					data.sidMessage = ('00000000000' + data.id_Message).slice(-11);
					data.sidMessage = "ID" + data.sidMessage.substring(0, 3) + "-" + data.sidMessage.substring(3, 7) + "-" + data.sidMessage.substring(7, 11);
				} else {
					data.sidMessage = '';
				}
				if (data.id_fms_z && '' != data.id_fms_z) {
					data.sidFMSZ = ('00000000000' + data.id_fms_z).slice(-11);
					data.sidFMSZ = "ID" + data.sidFMSZ.substring(0, 3) + "-" + data.sidFMSZ.substring(3, 7) + "-" + data.sidFMSZ.substring(7, 11);
				} else if (data.Citizenship.id == 'RUS') {
					data.sidFMSZ = "не требуется";
				} else {
					data.sidFMSZ = '';
				}
				this.model.listData.push(data);
			}
			log.Debug('ProcessingData }');
			return result;
		};

		var sortElements = function (data1, data2) {
			var val1, val2;
			if (!this.model || !this.model.ReportingSort) {
				val1 = data1.LastName;
				val2 = data2.LastName;
			} else if ('start' == this.model.ReportingSort) {
				val1 = controller.parseDate(data1.DateStart);
				val2 = controller.parseDate(data2.DateStart);
			} else if ('fio' == this.model.ReportingSort) {
				val1 = data1.LastName;
				val2 = data2.LastName;
			}
			if (val1 > val2) { return 1; }
			if (val1 <= val2) { return -1; }
		};

		var base_BuildXamlView = controller.BuildXamlView;
		controller.BuildXamlView = function () {
			var content = base_BuildXamlView.call(this);
			if (this.model && this.model.listData && this.model.listData.length > 0) {
				this.model.listData.sort(sortElements);
			}
			content.Content = print_tpl({
				form: this.model,
				trim: function(value) { return value.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, ''); },
				date: h_times.nowLocalISOStringDateTime()
			});
			return content;
		};

		controller.UseCodec(codec());

		return controller;
	}
});
