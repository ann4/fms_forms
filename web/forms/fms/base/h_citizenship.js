﻿// утф8
define([
	'txt!forms/fms/guides/dict_citizenship.csv',
	'forms/base/h_csv'
]
, function (dict_citizenship, helper_CSV)
{
	var res = {};

	res.GetValues = function ()
	{
		return helper_CSV.CSVToJSON(dict_citizenship, ';');
	}

	return res;
});
