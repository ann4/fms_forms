define([
		'forms/base/c_modal', 'tpl!forms/fms/base/upload/v_upload.html', 'forms/fms/base/upload/h_upload', 'forms/fms/base/upload/m_upload'
	],
	function(controller_base, upload_tpl, helper_upload, model_upload) {
		return function(model, title, OnAfterSave, uid) {
			var controller = controller_base(model);
			controller.width = 750;
			controller.height = 470;
			controller.title = 'Загрузка файлов';

			controller.template = upload_tpl;
			controller.OnAfterSave = OnAfterSave;
			controller.ImageList = '#cpw_form_modal_PreviewImage';
			controller.ImageRemove = '.remove_img';

			controller.InitializeControls = function() {
				controller.InitializeFields();
				controller.InitializeEvents();
			}

			controller.InitializeFields = function() {
				if (controller.model.length > 0) {
					for (var i = 0; i < controller.model.length; i++) {
						if(controller.model[i] && controller.model[i].uid == uid)
							$(controller.ImageList).append('<div class="imageRow" data-id="' + (i + 1) + '"><img width="150" src="' + controller.model[i].src + '"/><span class="remove_img">Удалить</span></div>');
					}
				}
			}

			controller.InitializeEvents = function() {
				var fileInput = $('#cpw_form_modal_Upload');

				fileInput.on({
					change: function() {
						controller.DisplayFiles(this.files);
					}
				});

				$(controller.ImageList).on('click' + controller.ImageRemove, controller.ImageRemove, function() {
					var id = $(this).closest('.imageRow').attr('data-id');
					$(this).closest('.imageRow').remove();
					delete controller.model[id];
				})
			}

			controller.DisplayFiles = function(files) {
				var imgList = $(controller.ImageList);
				if (imgList.find('.imageRow').size() == 0)
					imgList.empty();
				$.each(files, function(i, file) {
					if (!file.type.match(/image.*/)) {
						// Отсеиваем не картинки
						return true;
					}
					var wrap = $('<div class="imageRow" data-id="' + (controller.model.length + 1) + '"/>').appendTo(imgList);
					var img = $('<img/><span class="remove_img">Удалить</span>').appendTo(wrap);
					wrap.get(0).file = file;

					// Создаем объект FileReader и по завершении отображаем миниатюру
					var reader = new FileReader();
					reader.onload = (function(aImg) {
						return function(e) {
							aImg.attr('src', e.target.result);
							aImg.attr('width', 150);
						};
					})(img);
					reader.readAsDataURL(file);
				});
			}

			controller.DestroyControls = function() {

			}

			controller.DataSaveFromControls = function() {
				var imgList = $(controller.ImageList);
				var patternType = /([\w]+)\/([\w]+)/;
				var patternContent = /,([\w\/+=]+)/;
				for (var i = 0; i < controller.model.length; i++) {
					if (controller.model[i] && controller.model[i].uid == uid) {
						delete controller.model[i];
					}
				}
				imgList.find('img').each(function(i) {
					var file = model_upload();
					file.uid = uid;
					file.src = $(this).attr('src');
					file.format = patternType.exec(file.src)[0];
					file.content = helper_upload.base64ToHex(patternContent.exec(file.src)[1]);
					controller.model.push(file);
				})
			}

			controller.DataLoadToControls = function() {

			}

			return controller;
		}
	});