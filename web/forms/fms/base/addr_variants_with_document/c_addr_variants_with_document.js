define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/addr_variants_with_document/e_addr_variants_with_document.html'
	, 'forms/fms/base/russ_addr/c_russ_addr'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/russ_addr_fias/h_fias'
],
function 
(
	  c_binded
	, tpl
	, c_addr
	, helper_Select2
	, h_addr
	, h_fias
)
{
	return function (addr_variants)
	{
		var options =
		{
			field_spec:
			{
				address: { controller: c_addr }
			}
		};

		var controller = c_binded(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			var select2_item = $(sel + ' div.cpw-fms-addr-variants div.select2 div');
			select2_item.select2
			({
			    data: addr_variants
				, placeholder: 'Выберите один из вариантов адреса, указанных в профиле..'
			})
			.on('change', function () { self.OnChangeVariant(); });
			if ('' == $('#document-for-address').val())
			{
				$('#document-for-address').val(this.model.document);
			}
		}

		controller.OnChangeVariant = function ()
		{
			var self = this;
			var sel = this.binding.form_div_selector;
			var select2_item = $(sel + ' div.cpw-fms-addr-variants div.select2 div');
			var selected_option = select2_item.select2('data');
			if (null != selected_option)
			{
				var newAddr = h_addr.ConvertFromOldFormat(selected_option);
				h_fias.ConvertOldFiasToNew(newAddr, $('#fias_version').val(), function (res) {
					var addr_controller = self.binding.fields_controls.address.controller;
					addr_controller.SetFormContent(res.russianAddress);
					var addr_selector = sel + ' div.cpw-fms-addr-variants>div.address';
					$(addr_selector).html('');
					addr_controller.Edit(addr_selector);
					$('#document-for-address').val(!res.document ? '' : res.document);
				});
			}
			select2_item.select2('data', null);
		}

		controller.SafePrepModel = function () {
			if (!this.model)
			this.model = { document: '', russianAddress: {} };
		}

		var base_CreateNew = controller.CreateNew;
		controller.CreateNew = function (sel)
		{
			this.SafePrepModel();
			base_CreateNew.call(this, sel);
		}

		var base_Edit = controller.Edit;
		controller.Edit = function (sel)
		{
			this.SafePrepModel();
			base_Edit.call(this, sel);
		}

	var base_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (content)
		{
			this.model = content;
			content.russianAddress.document = content.document;
			base_SetFormContent.call(this, content.russianAddress);
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			return {
				russianAddress: base_GetFormContent.call(this),
				document: $('#document-for-address').val()
			};
		}

		return controller;
	}
});
