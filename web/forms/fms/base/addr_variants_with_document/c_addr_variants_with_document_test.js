define([
	  'forms/fms/base/addr_variants_with_document/c_addr_variants_with_document'
],
function 
(
	  c_addr_variants
)
{
	return function ()
	{
		var addr_variants =
		[
			{
				id: 0
				, document: 'Договор аренды №12345 от 12.12.2018'
				, text: 'Респ Удмуртская, г Ижевск, ул Удмуртская, д.1, корп.2, кв.3, комн.4'
				, russianAddress: {
					"variant_fias": true,
					"fias": [
						{
							"AOLEVEL": "7",
							"text": "ул Удмуртская",
							"STREETCODE": "0501",
							"IsTerminal": true,
							"AOID": "2dc4370c-562e-4b50-a0d4-809a6057e459"
						},
						{
							"AOLEVEL": "4",
							"text": "г Ижевск",
							"CITYCODE": "001",
							"IsTerminal": false
						},
						{
							"AOLEVEL": "1",
							"text": "Респ Удмуртская",
							"REGIONCODE": "18",
							"IsTerminal": false
						}
					],
					"Дом": "1",
					"Корпус": "2",
					"Квартира": "3",
					"Комната": "4"
				}
			}
			,{
				id: 1
				, text: 'Респ Удмуртская, г Ижевск, ул Бородина, д.21'
				, "russianAddress": {
					"variant_fias": false,
					"free": {
						"Регион": "Удмуртия",
						"Район": "",
						"Город": "Ижевск",
						"НаселенныйПункт": "",
						"ГородскойРайон": "",
						"Улица": "Бородина"
					},
					"Дом": "21"
				}
			}
		];
		return c_addr_variants(addr_variants);
	}
});
