define([
	'forms/base/codec.xsd.xml'
	, 'forms/fms/base/h_xsd'
	, 'forms/base/log'
	, 'forms/fms/base/town/m_town'
	, 'forms/fms/base/town/h_town'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/x_fms_base'
	, 'forms/fms/base/addr/x_addr'
	, 'forms/fms/base/russ_addr/x_russ_addr'
	, 'forms/fms/base/h_country'
	, 'forms/fms/base/addr/h_addr'
],
function (
	  BaseCodec
	, h_xsd
	, GetLogger
	, model_town
	, h_town
	, helper_Select2
	, x_fms_base
	, x_addr
	, x_russ_addr
	, h_country
	, h_addr
	)
{
	var log = GetLogger('x_fms');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.x_fms_base = x_fms_base();
		res.tabs = res.x_fms_base.tabs;

		res.DateEncode = function (txt_dd_mm_yyyy) // 26.04.2014 -> 2014-04-26
		{
			if (!txt_dd_mm_yyyy)
			{
				return '';
			}
			else
			{
				var dd_mm_yyyy = txt_dd_mm_yyyy.split('.');
				return dd_mm_yyyy[2] + '-' + dd_mm_yyyy[1] + '-' + dd_mm_yyyy[0];
			}
		}

		res.DateDecode = function (txt_yyyy_mm_dd) // 2014-04-26 -> 26.04.2014
		{
			if (!txt_yyyy_mm_dd)
			{
				return '';
			}
			else
			{
				var yyyy_mm_dd = txt_yyyy_mm_dd.split('-');
				return yyyy_mm_dd[2] + '.' + yyyy_mm_dd[1] + '.' + yyyy_mm_dd[0];
			}
		}

		res.getObjAttr = function (context, patch)
		{
			var keys = patch.split('.');

			for (var i = 0; i < keys.length; i++)
			{
				try
				{
					var value = context[keys[i]];
				} catch (e)
				{
					return
				}

				if (value !== undefined) context = value;
				else return;
			}

			return context;
		}

		res.SafeText = function (dict_obj)
		{
			return (!dict_obj || !dict_obj.text) ? '' : dict_obj.text;
		}

		res.m_only_free_mode = false;
		res.only_free_mode = function ()
		{
			return this.m_only_free_mode;
		}

		res.FixAddressFreeOnlyVersionByModel = function (m)
		{
			if (m && m.subdivision && m.subdivision.id)
				this.m_only_free_mode = h_addr.only_free_address_by_substitusion_id(m.subdivision.id);
		}

		res.EncodeAddress = function (num_tabs, tag_name, address)
		{
			if (!address || null == address)
			{
				return '';
			}
			else
			{
				log.Debug('EncodeAddress {');
				if (!this.x_addr)
					this.x_addr = x_addr();
				this.x_addr.tabs = this.tabs;
				this.x_addr.m_only_free_mode = this.only_free_mode();
				var xml_string = this.x_addr.EncodeDataItem(num_tabs, address, tag_name);
				log.Debug('EncodeAddress }');
				return xml_string;
			}
		}

		res.EncodeSex = function (num_tabs, sex)
		{
			try
			{
				log.Debug('EncodeSex {');
				return this.SafeEncodeNamedDictionaryItem(num_tabs, sex, 'gender', 'gender');
			}
			finally
			{
				log.Debug('EncodeSex }');
			}
		}

		res.DecodeSex = function (node)
		{
			return this.DecodeDictionaryItem(node, 'Gender');
		}

		res.EncodeDictionaryItem = res.x_fms_base.EncodeDictionaryItem;
		res.EncodeNamedDictionaryItem = res.x_fms_base.EncodeNamedDictionaryItem;
		res.SafeEncodeNamedDictionaryItem = res.x_fms_base.SafeEncodeNamedDictionaryItem;
		res.DecodeDictionaryItem = res.x_fms_base.DecodeDictionaryItem;
		res.CheckDictionaryType = res.x_fms_base.CheckDictionaryType;

		res.DecodeEmployee = function (node)
		{
			var childs = node.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "ummsId": return { ummsId: child.text };
					case "name": return { name: child.text };
				}
			}
			return null;
		}

		res.DecodePersonData = function (node, m_data)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "lastName": m_data.LastName = child.text; break;
					case "firstName": m_data.FirstName = child.text; break;
					case "middleName": m_data.MiddleName = child.text; break;
					case "birthDate": m_data.Birthday = child.text; break;
					case "gender": m_data.Sex = self.DecodeSex(child); break;
					case 'uid': m_data.person_uid = child.text; break;
					case 'personId':
					case 'personUid':
						m_data.person_personId = child.text; break;
					case 'citizenship': m_data.Nationality = self.DecodeDictionaryItem(child, 'citizenship'); break;
					case 'birthPlace': m_data.Birthplace = self.DecodeBirthPlace(child); break;
				}
			});
		}

		res.DecodeBirthPlace = function (node)
		{
			var birth_place = model_town();
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'country': birth_place.Country = self.DecodeDictionaryItem(child, 'Country'); break;
					case 'place': birth_place.Region = child.text; break;
					case 'place2': birth_place.Area = child.text; break;
					case 'place3': birth_place.City = child.text; break;
					case 'place4': birth_place.Town = child.text; break;
				}
			});
			if (birth_place.Country && !birth_place.Country.text && birth_place.Country.id)
			{
				var item = helper_Select2.FindById(h_country.GetValues(), birth_place.Country.id)
				if (item)
					birth_place.Country.text = item.text;
			}
			birth_place.text = h_town.ToReadableStringBirthplace(birth_place, null);
			return birth_place;
		};

		res.DecodePersonDataDocument = function (node, m_migrationn)
		{
			log.Debug('DecodePersonDataDocument {');
			var childs = node.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "person": this.DecodePersonDataDocument_Person(child, m_migrationn); break;
					case "document": this.DecodePersonDataDocument_Document(child, m_migrationn); break;
					case "entered": m_migrationn.Document_entered = child.text; break;
				}
			}
			log.Debug('DecodePersonDataDocument }');
		}

		res.DecodePersonDataDocument_Document = function (node, m_migrationn)
		{
			log.Debug('DecodePersonDataDocument_Document {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m_migrationn.Document_uid = child.text; break;
					case "type": m_migrationn.DocumentType = self.DecodeIdentityDocumentType(child); break;
					case "series": m_migrationn.DocumentSeries = child.text; break;
					case "number": m_migrationn.DocumentNumber = child.text; break;
					case "authority": m_migrationn.authority = child.text; break;
					case "issued": m_migrationn.DocumentGivenDate = res.DateDecode(child.text); break;
					case "validFrom": m_migrationn.DocumentGivenDate = res.DateDecode(child.text); break;
					case "validTo": m_migrationn.DocumentFinishDate = res.DateDecode(child.text); break;
					case "status": m_migrationn.DocumentStatus = m_migrationn.Document_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;
					case "authorityOrgan":
						var ditem = self.DecodeOfficialOrganFMS(child);
						m_migrationn.DocumentGivenBy_code = { id: ditem.id };
						m_migrationn.DocumentGivenBy = { id: ditem.id, text: ditem.text };
						break;
				}
			});
			log.Debug('DecodePersonDataDocument_Document }');
		}

		res.DecodePersonDataDocument_Person = function (node, m_migrationn)
		{
			log.Debug('DecodePersonDataDocument_Person {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'uid': m_migrationn.person_uid = child.text; break;
					case 'personId':
					case 'personUid':
						m_migrationn.person_personId = child.text; break;
					case "lastName": m_migrationn.LastName = child.text; break;
					case "lastNameLat": m_migrationn.LastName_latin = child.text; break;
					case "firstName": m_migrationn.FirstName = child.text; break;
					case "firstNameLat": m_migrationn.FirstName_latin = child.text; break;
					case "middleName": m_migrationn.MiddleName = child.text; break;
					case "middleNameLat": m_migrationn.MiddleName_latin = child.text; break;
					case "gender": m_migrationn.Sex = self.DecodeSex(child); break;
					case "birthDate": m_migrationn.Birthday = child.text; break;
					case 'citizenship': m_migrationn.Nationality = self.DecodeDictionaryItem(child, 'Citizenship'); break;
					case 'birthPlace': m_migrationn.Birthplace = self.DecodeBirthPlace(child); break;
				}
			});
			log.Debug('DecodePersonDataDocument_Person }');
		}

		res.DecodeAddressAddress = function (node)
		{
			log.Debug('DecodeAddressAddress {');
			if (!res.x_addr)
			{
				res.x_addr = x_addr();
				res.x_addr.tabs = res.tabs;
				res.x_addr.AddressWithHousingType = res.AddressWithHousingType;
				res.x_addr.AddressWithHousingTypeWithoutRoom = res.AddressWithHousingTypeWithoutRoom;
			}
			var addr = res.x_addr.DecodeXmlElement(node);
			log.Debug('DecodeAddressAddress }');
			return addr;
		}

		res.DecodeAddress = function (node)
		{
			var self = this;
			var address = null;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "address": address = self.DecodeAddressAddress(child); break;
				}
			});
			return address;
		}

		res.DecodeAddressAddressRussianAddress = function (node)
		{
			log.Debug('DecodeAddressAddressRussianAddress {');
			if (!res.x_russ_addr)
				res.x_russ_addr = x_russ_addr();
			var addr = res.x_russ_addr.DecodeXmlElement(node);
			log.Debug('DecodeAddressAddressRussianAddress }');
			return addr;
		}

		res.EncodeRussianAddress = function (num_tabs, addr, field_name)
		{
			log.Debug('EncodeRussianAddress {');
			if (!res.x_russ_addr)
				res.x_russ_addr = x_russ_addr();
			var xml_string = res.x_russ_addr.EncodeDataItem(num_tabs, addr, field_name);
			log.Debug('EncodeRussianAddress }');
			return xml_string;
		}

		res.DecodeAddressHousing = res.x_fms_base.DecodeAddressHousing;

		res.EncodeBirthplace = function (num_tabs, Birthplace)
		{
			var xml_string = '';
			if (null != Birthplace && null != Birthplace.Country)
			{
				xml_string += this.OpenTagLine(num_tabs, 'birthPlace');
				xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 1, Birthplace.Country, 'country', 'country');
				if (Birthplace.Region)
					xml_string += this.SafeStringifyField(num_tabs + 1, "place", Birthplace.Region);
				if (Birthplace.Area)
					xml_string += this.SafeStringifyField(num_tabs + 1, "place2", Birthplace.Area);
				if (Birthplace.City)
					xml_string += this.SafeStringifyField(num_tabs + 1, "place3", Birthplace.City);
				if (Birthplace.Town)
					xml_string += this.SafeStringifyField(num_tabs + 1, "place4", Birthplace.Town);
				xml_string += this.CloseTagLine(num_tabs, 'birthPlace');
			}
			return xml_string;
		}

		res.GetScheme = function ()
		{
			return h_xsd.GetScheme_migrationn();
		};

		res.EncodeCoreCaseScans = function (num_tabs, m_core_case)
		{
			log.Debug('EncodeCoreCaseScans() {');
			var xml_string = '';
			if (m_core_case.DocumentScan && m_core_case.DocumentScan.length > 0)
			{
				for (var i = 0; i < m_core_case.DocumentScan.length; i++)
				{
					if (m_core_case.DocumentScan[i])
					{
						var scan = m_core_case.DocumentScan[i];
						xml_string += this.FormatTabs(num_tabs) + '<documentPhoto>' + this.FormatEOL();
						xml_string += this.StringifyField(num_tabs + 1, "documentUID", scan.uid);
						if (scan.uid == m_core_case.Document_uid)
							xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 1, m_core_case.DocumentType, 'type', 'DocumentType');
						else if (scan.uid == m_core_case.Visa_uid)
							xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 1, m_core_case.Visa_type, 'type', 'DocumentType');
						xml_string += this.StringifyField(num_tabs + 1, "documentPhotoContent", scan.content);
						xml_string += this.StringifyField(num_tabs + 1, "documentPhotoFormat", scan.format);
						xml_string += this.FormatTabs(num_tabs) + '</documentPhoto>' + this.FormatEOL();
					}
				}
			}
			log.Debug('EncodeCoreCaseScans() }');
			return xml_string;
		}

		res.EncodeCoreCaseFields = function (num_tabs, m_core_case, xml_string)
		{
			log.Debug('EncodeCoreCaseFields() {');
			xml_string += this.StringifyField(num_tabs, "uid", m_core_case.uid);

			if (m_core_case.subdivision && m_core_case.subdivision.id &&
				('140733' == m_core_case.subdivision.id // 260-000 140733 Управление по вопросам миграции ГУ МВД России по Ставропольскому краю
				|| '140765' == m_core_case.subdivision.id // 260-033 140765 Отдел полиции №1 ГУ МВД России по Ставропольскому краю
				|| '140766' == m_core_case.subdivision.id // 260-034 140766 Отдел полиции №2 ГУ МВД России по Ставропольскому краю
				|| '140767' == m_core_case.subdivision.id // 260-035 140765 Отдел полиции №3 ГУ МВД России по Ставропольскому краю
				))
			{
				m_core_case.PositionForSignature = null;
			}
			else
			{
				m_core_case.PositionForSignature = xml_string.length;
			}

			xml_string += this.SafeStringifyField(num_tabs, "requestId", m_core_case.requestId);
			xml_string += this.StringifyField(num_tabs, "supplierInfo", m_core_case.supplierInfo);
			xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs, m_core_case.subdivision, 'subdivision', 'officialOrgan');

			xml_string += this.OpenTagLine(num_tabs, 'employee');
			xml_string += this.StringifyField(num_tabs + 1, "ummsId", m_core_case.employee.ummsId);
			xml_string += this.CloseTagLine(num_tabs, 'employee');

			xml_string += this.StringifyField(num_tabs, "date", m_core_case.date, 'Дата ввода в систему');
			xml_string += this.SafeStringifyField(num_tabs, "number", m_core_case.number);
			xml_string += this.SafeStringifyField(num_tabs, "comments", m_core_case.comments);

			xml_string += this.EncodeCoreCaseScans(num_tabs, m_core_case);
			log.Debug('EncodeCoreCaseFields() }');
			return xml_string;
		}

		res.EncodeCoreCaseFieldsFMS = function (num_tabs, m_core_case, xml_string) {
		    log.Debug('EncodeCoreCaseFields() {');
		    xml_string += this.StringifyField(num_tabs, "uid", m_core_case.uid);

		    if (m_core_case.subdivision && m_core_case.subdivision.id &&
				('140733' == m_core_case.subdivision.id // 260-000 140733 Управление по вопросам миграции ГУ МВД России по Ставропольскому краю
				|| '140765' == m_core_case.subdivision.id // 260-033 140765 Отдел полиции №1 ГУ МВД России по Ставропольскому краю
				|| '140766' == m_core_case.subdivision.id // 260-034 140766 Отдел полиции №2 ГУ МВД России по Ставропольскому краю
				|| '140767' == m_core_case.subdivision.id // 260-035 140765 Отдел полиции №3 ГУ МВД России по Ставропольскому краю
				)) {
		        m_core_case.PositionForSignature = null;
		    }
		    else {
		        m_core_case.PositionForSignature = xml_string.length;
		    }

		    xml_string += this.SafeStringifyField(num_tabs, "requestId", m_core_case.requestId);
		    xml_string += this.StringifyField(num_tabs, "supplierInfo", m_core_case.supplierInfo);
		    xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs, m_core_case.subdivision, 'subdivision', 'officialOrgan.fms');

		    xml_string += this.OpenTagLine(num_tabs, 'employee');
		    xml_string += this.StringifyField(num_tabs + 1, "ummsId", m_core_case.employee.ummsId);
		    xml_string += this.CloseTagLine(num_tabs, 'employee');

		    xml_string += this.StringifyField(num_tabs, "date", m_core_case.date, 'Дата ввода в систему');
		    xml_string += this.SafeStringifyField(num_tabs, "number", m_core_case.number);
		    xml_string += this.SafeStringifyField(num_tabs, "comments", m_core_case.comments);

		    xml_string += this.EncodeCoreCaseScans(num_tabs, m_core_case);
		    log.Debug('EncodeCoreCaseFields() }');
		    return xml_string;
		}

		res.EncodeStayPeriod = function (num_tabs, m, namespace, field_name_dateFrom, field_name_dateTo)
		{
			if (!namespace)
				namespace = 'ns4';
			if (!field_name_dateFrom)
				field_name_dateFrom = 'stayPeriod_dateFrom';
			if (!field_name_dateTo)
				field_name_dateTo = 'stayPeriod_dateTo';
			xml_string = this.FormatTabs(num_tabs) + '<' + namespace + ':stayPeriod>' + this.FormatEOL();
			xml_string += this.StringifyField(num_tabs + 1, "dateFrom", res.DateEncode(m[field_name_dateFrom]));
			if (m[field_name_dateTo] && null != m[field_name_dateTo])
				xml_string += this.StringifyField(num_tabs + 1, "dateTo", res.DateEncode(m[field_name_dateTo]));
			xml_string += this.FormatTabs(num_tabs) + '</' + namespace + ':stayPeriod>' + this.FormatEOL();
			return xml_string;
		}

		res.SafeEncodeDocumentGivenByFMS = function (num_tabs, item_dict, item, item_text) {
			var xml_string = '';
			if ('1.102.2' != this.SchemaVersion) {
				xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs, item, 'authorityOrgan', '1.102.2' == this.SchemaVersion ? 'officialOrgan.fms' : 'officialOrgan');
			} else {
				log.Debug('SafeEncodeDocumentGivenByFMS {');
				if (!item_dict) {
					xml_string += this.StringifyField(num_tabs, "authority", item_text);
				}
				else if (null != item) {
					var ditem = {
						id: item.id
						, text: (item.row && item.row.NAME) ? item.row.NAME : item.text
					};
					xml_string += this.EncodeNamedDictionaryItem(num_tabs, ditem, 'authorityOrgan', '1.102.2' == this.SchemaVersion ? 'officialOrgan.fms' : 'OfficialOrgan');
				}
				log.Debug('SafeEncodeDocumentGivenByFMS }');
			}
			return xml_string;
		}

		res.DecodeOfficialOrganFMS = function (node) {
			try {
				return this.DecodeDictionaryItem(node, 'officialOrgan');
			} catch (ex) {
				return this.DecodeDictionaryItem(node, 'officialOrgan.fms');
			}
		}

		res.DecodeOfficialOrganFNS = function (node) {
			try {
				return this.DecodeDictionaryItem(node, 'officialOrgan');
			} catch (ex) {
				return this.DecodeDictionaryItem(node, 'officialOrgan.fns');
			}
		}

		res.DecodeOfficialOrganKPPPik = function (node) {
			try {
				return this.DecodeDictionaryItem(node, 'officialOrgan');
			} catch (ex) {
				return this.DecodeDictionaryItem(node, 'officialOrgan.kppPik');
			}
		}

		res.DecodeDocResidenceType = function (node) {
			try {
			return this.DecodeDictionaryItem(node, 'DocumentType');
			} catch (ex) {
			return this.DecodeDictionaryItem(node, 'mig.dwellingEvidenceType');
			}
		}

		res.DecodeIdentityDocumentType = function (node) {
			try {
				return this.DecodeDictionaryItem(node, 'DocumentType');
			} catch (ex) {
				try {
					return this.DecodeDictionaryItem(node, 'mig.LRDocumentType');
				} catch (ex) {
					return this.DecodeDictionaryItem(node, 'mig.identityDocumentType');
				}
			}
		}

		res.DecodeOrganizationPersonDocumentType = function (node) {
			try {
				return this.DecodeDictionaryItem(node, 'DocumentType');
			} catch (ex) {
			try {
					return this.DecodeDictionaryItem(node, 'mig.receivingPartyDocType');
				} catch (ex) {
					return this.DecodeDictionaryItem(node, 'organizationPersonDocumentType');
				}
			}
		}

		res.DecodeStayPeriod = function (node, m, field_name_dateFrom, field_name_dateTo)
		{
			if (!field_name_dateFrom)
				field_name_dateFrom = 'stayPeriod_dateFrom';
			if (!field_name_dateTo)
				field_name_dateTo = 'stayPeriod_dateTo';
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "dateFrom":
						m[field_name_dateFrom] = res.DateDecode(child.text);
						break;
					case "dateTo":
						m[field_name_dateTo] = res.DateDecode(child.text);
						break;
				}
			});
		}

		res.DocResidenceVisaDecode = function (node, m)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m.Visa_uid = child.text; break;
					case "type": m.Visa_type = self.DecodeDocResidenceType(child); break;
					case "series": m.Visa_series = child.text; break;
					case "number": m.Visa_number = child.text; break;
					case "issued": m.Visa_issued = res.DateDecode(child.text); break;
					case "validFrom": m.Visa_validFrom = res.DateDecode(child.text); break;
					case "validTo": m.Visa_validTo = res.DateDecode(child.text); break;
					case "status": m.Visa_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;
					case "ns3:category": m.Visa_category = self.DecodeDictionaryItem(child, 'VisaCategory'); break;
					case "ns3:multiplicity": m.Visa_multiplicity = self.DecodeDictionaryItem(child, 'VisaMultiplicity'); break;
					case "ns3:identifier":
					case "ns3:identitifier": m.Visa_identifier = 'null' == child.text ? null : child.text; break;
					case "ns3:visitPurpose": m.Visa_visitPurpose = self.DecodeDictionaryItem(child, 'EntryGoal'); break;
				}
			});
		};

		// Разрешение на временное проживание (tempResidencePermit)
		res.TempResidencePermitDecode = function (node, m)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m.Visa_uid = child.text; break;
					case "type": m.Visa_type = self.DecodeDocResidenceType(child); break;
					case "series": if (m.Visa_type.id == '139372' || m.Visa_type.id == '136366') { m.Visa_series = child.text; } break;
					case "number": if (m.Visa_type.id == '139372' || m.Visa_type.id == '136366') { m.Visa_number = child.text; } break;
					case "issued": if (m.Visa_type.id == '139372' || m.Visa_type.id == '136366') { m.Visa_issued = res.DateDecode(child.text); } break;
					case "validTo":
						if (m.Visa_type.id == '139372' || m.Visa_type.id == '136366') { m.Visa_validTo = res.DateDecode(child.text); }
						else { m.RvpVisa_validTo = res.DateDecode(child.text); }
						break;
					case "status": m.Visa_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;

					case "decisionNumber":
					case "ns4:decisionNumber": m.LivingDocDecisionNumber = child.text; break;
					case "decisionDate":
					case "ns4:decisionDate": m.LivingDocDecisionDate = res.DateDecode(child.text); break;

					case 'authorityOrgan':
						var ditem = self.DecodeOfficialOrganFMS(child);
						m.RvpDocumentGivenBy = { id: ditem.id, text: ditem.text };
						m.RvpDocumentGivenBy_from_dict = true;
						break;

					case 'authority':
						m.RvpDocumentGivenBy_text = child.text;
						m.RvpDocumentGivenBy_from_dict = false;
						break;
				}
			});
		};

		// Вид на жительство (permResidencePermit)
		res.PermResidencePermitDecode = function (node, m)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m.Visa_uid = child.text; break;
					case "type": m.Visa_type = self.DecodeDocResidenceType(child); break;

					case "series": m.Visa_series = child.text; break;
					case "number": m.Visa_number = child.text; break;
					case "issued": m.Visa_issued = res.DateDecode(child.text); break;
					case "validFrom": m.Visa_validFrom = res.DateDecode(child.text); break;

					case "validTo": m.Visa_validTo = res.DateDecode(child.text); break;

					case "status": m.Visa_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;

					case "decisionNumber":
					case "ns4:decisionNumber": m.LivingDocDecisionNumber = child.text; break;
					case "decisionDate":
					case "ns4:decisionDate": m.LivingDocDecisionDate = res.DateDecode(child.text); break;

					case 'authorityOrgan':
						var ditem = self.DecodeOfficialOrganFMS(child);
						m.RvpDocumentGivenBy = { id: ditem.id, text: ditem.text };
						m.RvpDocumentGivenBy_from_dict = true;
						break;

					case 'authority':
						m.RvpDocumentGivenBy_text = child.text;
						m.RvpDocumentGivenBy_from_dict = false;
						break;
				}
			});
		};

		res.DocResidenceDecode = function (node, m)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "ns4:visa": self.DocResidenceVisaDecode(child, m); break;
					case "ns4:residencePermit":
					case "ns4:permResidencePermit": self.PermResidencePermitDecode(child, m); break;
					case "ns4:temporaryResidence":
					case "ns4:tempResidencePermit": self.TempResidencePermitDecode(child, m); break;
					case "ns4:entered": m.Visa_entered = child.text; break;
				}
			});
		}

		res.DocResidenceInternalTagForVisaType = function (Visa_type_id)
		{
			var tag = 'ns4:visa';
			switch (Visa_type_id)
			{
				case '135709': // Вид на жительство ИГ
				case '135710': // Вид на жительство ЛБГ
				case '139387': // Вид на жительство ЛБГ (с биометрией)
					tag = this.SchemaVersion == '1.102.2' ? 'ns4:residencePermit' : 'ns4:permResidencePermit';
					break;
				case '139373': // Разрешение на временное проживание ИГ
				case '135717': // Разрешение на временное проживание ЛБГ
				case '139372': // Патент ИГ (ЛБГ)
				case '136366': // Иной документ, удостоверяющий личность
					tag = this.SchemaVersion == '1.102.2' ? 'ns4:temporaryResidence' : 'ns4:tempResidencePermit';
					break;
			}
			return tag;
		}

		res.SafeDocResidenceVisaFieldsEncode = function (num_tabs, m)
		{
			var xml_string = '';
			xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 2, m.Visa_category, 'ns3:category', 'VisaCategory');
			xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 2, m.Visa_multiplicity, 'ns3:multiplicity', 'VisaMultiplicity');
			xml_string += this.SafeStringifyField(num_tabs + 2, this.SchemaVersion == '1.102.2' ? "ns3:identifier" : "ns3:identitifier", m.Visa_identifier);
			xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 2, m.Visa_visitPurpose, 'ns3:visitPurpose', 'EntryGoal', 'Цель поездки');
			return xml_string;
		}

		res.SafeDocResidenceFieldsEncode = function (num_tabs, m)
		{
			var xml_string = '';
			xml_string += this.StringifyField(num_tabs + 2, "uid", m.Visa_uid);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m.Visa_type, 'type', '1.102.2' == this.SchemaVersion ? 'mig.dwellingEvidenceType' : 'DocumentType');
			xml_string += this.StringifyField(num_tabs + 2, "series", m.Visa_series);
			xml_string += this.StringifyField(num_tabs + 2, "number", m.Visa_number);
			xml_string += this.SafeStringifyField(num_tabs + 2, "issued", res.DateEncode(m.Visa_issued));
			xml_string += this.SafeStringifyField(num_tabs + 2, "validFrom", res.DateEncode(m.Visa_validFrom));
			xml_string += this.SafeStringifyField(num_tabs + 2, "validTo", res.DateEncode(m.Visa_validTo));
			xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 2, m.Visa_DocumentStatus, 'status', 'DocumentStatus');
			return xml_string;
		}

		res.SafeRvpFieldsEncode = function (num_tabs, m, namespace)
		{
			var xml_string = '';
			var ns = '1.102.2' == this.SchemaVersion ? '' : namespace + ':';

			xml_string += this.StringifyField(num_tabs + 2, "uid", m.Visa_uid);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m.Visa_type, 'type', '1.102.2' == this.SchemaVersion ? 'mig.dwellingEvidenceType' : 'DocumentType');
			xml_string += this.StringifyField(num_tabs + 2, "number", m.LivingDocDecisionNumber);

			if (!m.RvpDocumentGivenBy_from_dict)
			{
				xml_string += this.StringifyField(num_tabs + 2, "authority", m.RvpDocumentGivenBy_text);
			}
			else if (null != m.RvpDocumentGivenBy)
			{
				var ditem = {
					id: m.RvpDocumentGivenBy.id
					, text: (m.RvpDocumentGivenBy.row && m.RvpDocumentGivenBy.row.NAME) ? m.RvpDocumentGivenBy.row.NAME : m.RvpDocumentGivenBy.text
				};
				xml_string += this.EncodeNamedDictionaryItem(3, ditem, 'authorityOrgan', '1.102.2' == this.SchemaVersion ? 'officialOrgan.fms' : 'OfficialOrgan');
			}
			xml_string += this.SafeStringifyField(num_tabs + 2, "validTo", res.DateEncode(m.RvpVisa_validTo));

			if ('1.102.2' == this.SchemaVersion) {
				xml_string += this.StringifyField(num_tabs + 2, ns + "decisionNumber", m.LivingDocDecisionNumber);
				xml_string += this.StringifyField(num_tabs + 2, ns + "decisionDate", res.DateEncode(m.LivingDocDecisionDate));
			}

			xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 2, m.Visa_DocumentStatus, 'status', 'DocumentStatus');

			if ('1.102.2' != this.SchemaVersion) {
				xml_string += this.StringifyField(num_tabs + 2, ns + "decisionNumber", m.LivingDocDecisionNumber);
				xml_string += this.StringifyField(num_tabs + 2, ns + "decisionDate", res.DateEncode(m.LivingDocDecisionDate));
			}

			return xml_string;
		}

		// Вид на жительство (tempResidencePermit)
		res.SafeVnzFieldsEncode = function (num_tabs, m, namespace)
		{
			var ns = '1.102.2' == this.SchemaVersion ? '' : namespace + ':';

			var xml_string = '';
			xml_string += this.StringifyField(num_tabs + 2, "uid", m.Visa_uid);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m.Visa_type, 'type', '1.102.2' == this.SchemaVersion ? 'mig.dwellingEvidenceType' : 'DocumentType');
			xml_string += this.SafeStringifyField(num_tabs + 2, "series", m.Visa_series);
			xml_string += this.SafeStringifyField(num_tabs + 2, "number", m.Visa_number);

			if (!m.RvpDocumentGivenBy_from_dict)
			{
				xml_string += this.StringifyField(num_tabs + 2, "authority", m.RvpDocumentGivenBy_text);
			}
			else if (null != m.RvpDocumentGivenBy)
			{
				var ditem = {
					id: m.RvpDocumentGivenBy.id
					, text: (m.RvpDocumentGivenBy.row && m.RvpDocumentGivenBy.row.NAME) ? m.RvpDocumentGivenBy.row.NAME : m.RvpDocumentGivenBy.text
				};
				xml_string += this.EncodeNamedDictionaryItem(3, ditem, 'authorityOrgan', '1.102.2' == this.SchemaVersion ? 'officialOrgan.fms' : 'OfficialOrgan');
			}

			xml_string += this.SafeStringifyField(num_tabs + 2, "issued", res.DateEncode(m.Visa_issued));
			xml_string += this.SafeStringifyField(num_tabs + 2, "validTo", res.DateEncode(m.Visa_validTo));

			if ('1.102.2' == this.SchemaVersion) {
				xml_string += this.SafeStringifyField(num_tabs + 2, ns + "decisionNumber", m.LivingDocDecisionNumber);
				if (m.LivingDocDecisionDate && null != m.LivingDocDecisionDate && '' != m.LivingDocDecisionDate)
					xml_string += this.StringifyField(num_tabs + 2, ns + "decisionDate", res.DateEncode(m.LivingDocDecisionDate));
			}

			xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 2, m.Visa_DocumentStatus, 'status', 'DocumentStatus');

			if ('1.102.2' != this.SchemaVersion) {
				xml_string += this.SafeStringifyField(num_tabs + 2, ns + "decisionNumber", m.LivingDocDecisionNumber);
				if (m.LivingDocDecisionDate && null != m.LivingDocDecisionDate && '' != m.LivingDocDecisionDate)
					xml_string += this.StringifyField(num_tabs + 2, ns + "decisionDate", res.DateEncode(m.LivingDocDecisionDate));
			}

			return xml_string;
		}

		res.SafeDocResidenceEncode = function (num_tabs, m, namespace)
		{
			if ((!m.Visa_number || null == m.Visa_number || '' == m.Visa_number) &&
				(!m.LivingDocDecisionNumber || null == m.LivingDocDecisionNumber || '' == m.LivingDocDecisionNumber))
			{
				return '';
			}
			else
			{
				if (!namespace)
					namespace = 'ns4';

				var Visa_type_id = '' + m.Visa_type.id;
				var tag = this.DocResidenceInternalTagForVisaType(Visa_type_id);

				log.Debug('out docResidence {');
				var xml_string = this.OpenTagLine(num_tabs, namespace + ':docResidence');
				xml_string += this.OpenTagLine(num_tabs + 1, tag);

				if ('139373' == Visa_type_id || // Разрешение на временное проживание ИГ
					'135717' == Visa_type_id) // Разрешение на временное проживание ЛБГ
				{
					xml_string += this.SafeRvpFieldsEncode(num_tabs, m, namespace);
				}
				else if ('135710' == Visa_type_id || // Вид на жительство ИГ
						 '135709' == Visa_type_id || // Вид на жительство ЛБГ
						 '139387' == Visa_type_id)   // Вид на жительство ЛБГ (с биометрией)
				{
					xml_string += this.SafeVnzFieldsEncode(num_tabs, m, namespace);
				}
				else
				{
					xml_string += this.SafeDocResidenceFieldsEncode(num_tabs, m);
				}

				if ('139356' == Visa_type_id ||  // виза
					'136366' == Visa_type_id   ) // Иной документ, удостоверяющий личность
				{
					xml_string += this.SafeDocResidenceVisaFieldsEncode(num_tabs, m);
				}

				xml_string += this.CloseTagLine(num_tabs + 1, tag);

				xml_string += this.StringifyField(num_tabs + 1, "ns4:entered", m.Visa_entered,
								'Флаг, указывающий на то, вписан ли заявитель в документ законного представителя, подтверждающий право на пребывание на территории РФ');
				xml_string += this.CloseTagLine(num_tabs, namespace + ':docResidence');
				log.Debug('out docResidence }');
				return xml_string;
			}
		}

		res.SafeMigrationCardEncode = function (num_tabs, m, namespace)
		{
			if (!m.MigrationCard_number || null == m.MigrationCard_number || '' == m.MigrationCard_number)
			{
				return '';
			}
			else
			{
				if (!namespace)
					namespace = 'ns9';
				var xml_string = this.OpenTagLine(num_tabs, namespace + ':migrationCard');
				xml_string += this.StringifyField(num_tabs + 1, "uid", m.MigrationCard_uid);
				xml_string += this.StringifyField(num_tabs + 1, "ns3:series", m.MigrationCard_series);
				xml_string += this.StringifyField(num_tabs + 1, "ns3:number", m.MigrationCard_number);

				xml_string += res.EncodeStayPeriod(num_tabs + 1, m, 'ns3', 'MigrationCard_dateFrom', 'MigrationCard_dateTo');

				xml_string += this.StringifyField(num_tabs + 1, "ns3:entranceDate", res.DateEncode(m.MigrationCard_dateFrom));
				xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 1, m.MigrationCard_kpp, 'ns3:entranceCheckpoint', '1.102.2' == this.SchemaVersion ? 'officialOrgan.kppPik' : 'officialOrgan');
				xml_string += this.SafeEncodeNamedDictionaryItem(num_tabs + 1, m.MigrationCard_visitPurpose, 'ns3:visitPurpose', 'VisitPurpose', 'Цель въезда.\r\n' +
	'Добавлена для сопоставления цели, указанной в уведомлении, с целью в миграционной карте.\r\n' +
	'Для выявления адм. нарушителей ст. 26 пункт 2 114-ФЗ.');
				xml_string += this.CloseTagLine(num_tabs, namespace + ':migrationCard');
				return xml_string;
			}
		}

		res.DecodeMigrationCardData = function (node, m)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m.MigrationCard_uid = child.text; break;
					case "ns3:series": m.MigrationCard_series = child.text; break;
					case "ns3:number": m.MigrationCard_number = child.text; break;
					case "ns3:stayPeriod": self.DecodeStayPeriod(child, m, 'MigrationCard_dateFrom', 'MigrationCard_dateTo'); break;
					case "ns3:entranceCheckpoint": m.MigrationCard_kpp = self.DecodeOfficialOrganKPPPik(child); break;
					case "ns3:visitPurpose": m.MigrationCard_visitPurpose = self.DecodeDictionaryItem(child, 'VisitPurpose'); break;
				}
			});
		}

		res.DecodeProfession = function (node)
		{
			var childs = node.childNodes;
			var childs_len = childs.length;
			if (childs_len > 1)
			{
				return this.DecodeDictionaryItem(node, 'Profession');
			}
			else
			{
				return { text: node.text };
			}
		}

		res.GetSchemaVersionByModel = function (m)
		{
			var res = '1.3.36';
			if (m.subdivision)
			{
				switch (m.subdivision.id)
				{
					case '3089': /* МИЯКИНСКИЙ РОВД РЕСПУБЛИКИ БАШКОРТОСТАН */
					case '141852': /* 580-101 : Управление по вопросам миграции УМВД России по Пензенской области (обл. Пензенская) */
					case '116162': /* 580-101 : УФМС РОССИИ ПО ПЕНЗЕНСКОЙ ОБЛ. */
					case '7841': /* 581-001 : УВД ПЕНЗЕНСКОЙ ОБЛ. */
					case '10141': /* 491-000 : ОФМС РОССИИ ПО МАГАДАНСКОЙ ОБЛ. */
					case '142750': /* 910-001 : Управление по вопросам миграции МВД по Республике Крым */
					case '142782': /* 920-005 : Управление по вопросам миграции УМВД России по г. Севастополю */
					case '129400': /* 910-001 : Управление ФМС России по Республике Крым и г. Севастополю */
					case '142804': /* 180-000 : Управление по вопросам миграции МВД по Удмуртской Республике (Респ. Удмуртская)*/
					case '118327': /* 180-001 : УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ МИГРАЦИОННОЙ СЛУЖБЫ РОССИИ ПО УДМУРТСКОЙ РЕСПУБЛИКЕ*/
					case '142067': /* 640-000 : Управление по вопросам миграции ГУ МВД России по Саратовской области (обл. Саратовская)*/
					default:
						return '1.102.2';
				}
			}
			return res;
		}

		res.FixScheme = function (form,scheme)
		{
			this.SchemaVersion = scheme;
			res.GetScheme = function () { return h_xsd.GetScheme(form,scheme); };
		}

		log.Debug('Create }');
		return res;
	}
});