define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/russ_addr_free/e_russ_addr_free.html'
],
function 
(
	  c_binded
	, tpl
)
{
	return function ()
	{
		var controller = c_binded(tpl);
		return controller;
	}
});
