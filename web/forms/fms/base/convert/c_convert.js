﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/convert/e_convert.html'
	, 'forms/fms/base/russ_addr_fias/h_fias'
	, 'forms/fms/base/convert/x_convert'
],
function (c_binded, tpl, h_fias, x_convert)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.from-free').click(function () { self.OnFromFree(); });
			$(sel + ' button.from-fias').click(function () { self.OnFromFias(); });
		}

		controller.log= function(data)
		{
			// console.log(data);
		}

		controller.OnFromFias= function()
		{
			$('span.from-fias').text('запустили перестройку из фиас');
			var txt = $('[model_field_name="fias"]').val();
			var addresses = JSON.parse(txt);
			var free_addresses = [];
			for (var i= 0; i<addresses.length; i++)
			{
				var addr_fias = addresses[i];
				var aaddr_fias = [];
				for (var k = 0; k < addr_fias.length; k++)
					aaddr_fias.unshift(addr_fias[k]);
				var addr_free = x_convert.Encode(aaddr_fias);
				free_addresses.push(addr_free);
			}
			$('[model_field_name="free"]').val(JSON.stringify(free_addresses, null, '\t'));
			$('span.from-fias').text('перестройка из фиас выполнена');
		}

		controller.process_from_free_formal_name = function (shortname, formalname, on_process_from_free_formal_name, path)
		{
			var self = this;
			self.log('process_from_free_formal_name { ' + shortname + ' ' + formalname);

			var url = 'http://trust.dev/fias/fias.php?action=SEARCH';

			for (var i = 0; i < path.length; i++)
			{
				var fobj = path[i];
				var aolevel = parseInt(fobj.AOLEVEL);
				var aolevel_spec = h_fias.aolevel_specs_by_aolevel[aolevel];
				url += '&' + aolevel_spec.code_field_name + '=' + fobj[aolevel_spec.code_field_name];
			}

			url += '&term=' + encodeURIComponent(formalname);
			self.log(url);
			$.ajax({
				url: url
				, dataType: "json"
				, cache: false
				, success: function (data, textStatus)
				{
					for (var i = 0; i < data.length; i++)
					{
						var item = data[i];
						if (item && item.length && item.length > 5 && item[5] == formalname && item[4] == shortname)
						{
							self.log('process_from_free_formal_name }');
							on_process_from_free_formal_name(h_fias.DecodeFiasFieldsArr(item));
							return;
						}
					}
					on_process_from_free_formal_name({});
				}
				, error: function (data, textStatus, errorThrown)
				{
					alert('Не удалось получить данные об адресном объекте ФИАС. Запрос "'
						+ url + '" завершён со статусом ' + textStatus);
					alert(errorThrown);
				}
			});
		}

		controller.process_from_free_line = function (line, on_process_line)
		{
			var self = this;
			self.log('process_from_free_line { ' + line);
			var parts = line.split('$');
			var res = [];
			var i = 0;
			var on_process_from_free_formal_name = null;
			on_process_from_free_formal_name= function(res_addr)
			{
				res.push(res_addr);
				i++;
				if (i >= parts.length)
				{
					self.log('process_from_free_line }');
					on_process_line(res);
				}
				else
				{
					part = parts[i];
					var first_space_pos = part.indexOf(' ');
					var shortname = part.substring(0, first_space_pos);
					var formalname = part.substring(first_space_pos + 1, part.length);
					self.process_from_free_formal_name(shortname, formalname, on_process_from_free_formal_name, res);
				}
			}
			var part = parts[i];
			var first_space_pos = part.indexOf(' ');
			var shortname = part.substring(0, first_space_pos);
			var formalname = part.substring(first_space_pos + 1, part.length);
			self.process_from_free_formal_name(shortname, formalname, on_process_from_free_formal_name, res);
		}

		controller.OnFromFree= function()
		{
			var self = this;
			self.log('OnFromFree {');
			$('span.from-free').text('запустили перестройку из исходных');
			var txt = $('[model_field_name="orig"]').val();
			var lines = txt.split('\n');
			var res = [];

			var i = 0;
			var line = lines[i];
			var on_finish = function ()
			{
				self.log('OnFromFree res:');
				self.log(res);
				$('[model_field_name="fias"]').val(JSON.stringify(res, null, '\t'));
				self.log('OnFromFree }');
				$('span.from-free').text('перестройка из исходных выполнена');
			}

			var on_process_line= null;
			on_process_line = function (res_line)
			{
				res.push(res_line);
				i++;
				if (i >= lines.length)
				{
					on_finish();
				}
				else
				{
					line = lines[i];
					if ('' == line)
					{
						on_finish();
					}
					else
					{
						self.process_from_free_line(line, on_process_line);
					}
				}
			}
			self.process_from_free_line(line, on_process_line);
		}

		var codec = {};

		codec.Encode = function (d)
		{
			if (d.orig)
				d.orig = d.orig.split('\n');
			if (d.free)
				d.free = JSON.parse(d.free);
			if (d.fias)
				d.fias = JSON.parse(d.fias);
			return d;
		}

		codec.Decode = function (d)
		{
			if (d.orig)
				d.orig = d.orig.join('\n');
			if (d.free)
				d.free = JSON.stringify(d.free, null, '\t');
			if (d.fias)
				d.fias = JSON.stringify(d.fias, null, '\t');
			return d;
		}

		controller.UseCodec(codec);

		return controller;
	}
});
