﻿define(['forms/fms/base/russ_addr_fias/h_fias'
],
function(h_fias)
{
	var aolevel_specs_by_title = {};
	for (var i = 0; i < h_fias.aolevel_specs.length; i++)
	{
		var aolevel_spec = h_fias.aolevel_specs[i];
		aolevel_specs_by_title[aolevel_spec.title] = aolevel_spec;
	}

	var splitter = ',' + String.fromCharCode(160);

	var GetAddressStreet = function (arr)
	{
		var ipos = null;
		for (var i = arr.length-1; i >= 0; i--)
		{
			if ('Улица' == h_fias.aolevel_specs_by_aolevel[arr[i].AOLEVEL].title)
				ipos = i;
		}
		if (null == ipos)
		{
			return null;
		}
		else
		{
			var res = '';
			while (arr.length!=ipos)
			{
				var fobj = arr.pop();
				if ('' != res)
					res = splitter + res;
				res = fobj.text ? fobj.text : fobj.SHORTNAME + ' ' + fobj.FORMALNAME + res;
			}
			return res;
		}
	}

	var GetAddressPrefix = function (title,arr)
	{
		var aolevel_spec = aolevel_specs_by_title[title];
		var ipos = null;
		for (var i = 0; i < arr.length; i++)
		{
			if (arr[i].AOLEVEL == aolevel_spec.aolevel)
				ipos = i;
		}
		if (null == ipos)
		{
			return null;
		}
		else
		{
			var res = '';
			for (; ipos >= 0; ipos--)
			{
				var fobj = arr.shift();
				if ('' != res)
					res += splitter;
				res += fobj.text ? fobj.text : fobj.SHORTNAME + ' ' + fobj.FORMALNAME;
			}
			return res;
		}
	}

	var SafeSetField= function(obj,name,value)
	{
		return obj[name]= (!value || null==value) ? '' : value;
	}

	var convert_fias_to_free= function(aaddr_fias)
	{
		var addr_fias = [];
		for (var k = 0; k < aaddr_fias.length; k++)
			addr_fias.unshift(aaddr_fias[k]);
		var res = {};

		SafeSetField(res,'Регион',GetAddressPrefix('Регион', addr_fias));
		SafeSetField(res,'Район',GetAddressPrefix('Район', addr_fias));
		SafeSetField(res,'Город', GetAddressPrefix('Город', addr_fias));
		SafeSetField(res, 'НаселенныйПункт', GetAddressPrefix('Нас. пункт', addr_fias));
		var улица= GetAddressStreet(addr_fias);

		if (0 != addr_fias.length)
		{
			var urban = '';
			for (var i = 0; i < addr_fias.length; i++)
			{
				var fobj = addr_fias[i];
				if ('' != urban)
					urban += splitter;
				urban += (fobj.SHORTNAME && fobj.FORMALNAME) ? fobj.SHORTNAME + ' ' + fobj.FORMALNAME : fobj.text;
			}
			if (''!=urban)
			{
				if (res.Город && ''!=res.Город || res.НаселенныйПункт && ''!=res.НаселенныйПункт)
				{
					res.ГородскойРайон = urban;
				}
				else if (!res.НаселенныйПункт || '' == res.НаселенныйПункт)
				{
					res.НаселенныйПункт = urban;
				}
				else
				{
					res.НаселенныйПункт += splitter + urban;
				}
			}
		}
		if (!res.НаселенныйПункт)
			res.НаселенныйПункт = '';
		if (!res.ГородскойРайон)
			res.ГородскойРайон = '';

		SafeSetField(res, 'Улица', улица);

		return res;
	}

	var codec = {};

	codec.Encode= function(addr_fias)
	{
		return convert_fias_to_free(addr_fias);
	}

	return codec;
});
