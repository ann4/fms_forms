define([
	  'forms/base/log'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/h_country'
	, 'forms/base/h_dictionary'
	, 'forms/fms/base/town/h_town'
	, 'forms/fms/base/h_republicSun'
	, 'forms/base/h_names'
	, 'forms/fms/guides/dict_officialorgan_fms.csv'
],
function (
	  GetLogger
	, helper_DocumentType
	, helper_citizenship
	, helper_country
	, h_dictionary
	, h_town
	, h_republicSun
	, helper_Names
	, fms
) {
	var log = GetLogger('x_fms_csv');
	return function () {
		log.Debug('Create {');
		var res = {};

		res.isNullOrEmpty = function (value) {
			return null == value || '' == value;
		};

		res.GetValue = function (data, field) {
			var value = data[field];
			return '' == value ? null : value;
		};

		res.GetDictionaryValue = function (data, field, dictionary) {
			var value = data[field];
			if (!this.isNullOrEmpty(value)) {
				var dictionaryValues = dictionary.GetValues();
				for (var i = 0; i < dictionaryValues.length; i++) {
					if (dictionaryValues[i].id == value)
						return { id: value, text: dictionaryValues[i].text };
				}
			}
			return null;
		};

		res.GetDictionaryValueByText = function (data, field, dictionary) {
			var value = data[field];
			if (!this.isNullOrEmpty(value)) {
				var dictionaryValues = dictionary.GetValues();
				for (var i = 0; i < dictionaryValues.length; i++) {
					if (dictionaryValues[i].text.toLowerCase() == value.toLowerCase())
						return { id: dictionaryValues[i].id, text: dictionaryValues[i].text };
				}
			}
			return null;
		};

		res.GetDictionaryFmsValue = function (data, field) {
			var value = data[field];
			if (!this.isNullOrEmpty(value)) {
				return h_dictionary.FindById(fms, value);
			}
			return null;
		};

		res.DecodeKonturCsvDocument = function (data) {
			var err = { parseException: true, readablemsg: 'Неверный формат' };
			log.Debug('throw parsing error..');
			log.Debug('Неверный формат');
			throw err;
		};
		res.DecodeSferaCsvDocument = function (data) {
			var err = { parseException: true, readablemsg: 'Неверный формат' };
			log.Debug('throw parsing error..');
			log.Debug('Неверный формат');
			throw err;
		};
		res.Decode16CsvDocument = function (data) {
			var err = { parseException: true, readablemsg: 'Неверный формат' };
			log.Debug('throw parsing error..');
			log.Debug('Неверный формат');
			throw err;
		};

		res.Decode = function (data) {
			try {
				log.Debug('DecodeCsvDocument {');
				if (data.length == 95 || data.length == 96) {
					return this.DecodeKonturCsvDocument(data);
				} else if (data.length == 67) {
					return this.Decode16CsvDocument(data);
				} else if (data.length == 29) {
					return this.DecodeSferaCsvDocument(data);
				} else {
					var err = { parseException: true, readablemsg: 'Не соответствует формату документа' };
					log.Debug('throw parsing error..');
					log.Debug('Неизвестный формат');
					throw err;
				}
			} finally {
				log.Debug('DecodeCsvDocument }');
			}
		};

		res.DecodeSex = function (value, middleName) {
			var resultMaleEnds = false;
			var resultFemaleEnds = false;
			if (null != value && '' != value) {
				resultMaleEnds = 'M' == value || 'М' == value;
				resultFemaleEnds = 'F' == value || 'Ж' == value;
			} else {
				var maleEnds = ['вич', 'ич', 'ьич', 'оглы'];
				var femaleEnds = ['вна', 'чна', 'кызы'];
				function checkEnds(ends) {
					for (var i = 0; i < ends.length; i++) {
						if (middleName.match(ends[i] + "$") == ends[i])
							return true;
					}
					return false;
				}
				resultMaleEnds = checkEnds(maleEnds);
				resultFemaleEnds = checkEnds(femaleEnds);
			}
			return resultMaleEnds ? { id: 'M', text: 'Мужской'} :
				(resultFemaleEnds ? { id: 'F', text: 'Женский'} : { id: '', text: '' });
		};

		res.DecodePerson = function (data, model, csvFormat) {
			log.Debug('DecodePerson {');
			model.LastName = helper_Names.NormalizeNameItem(data[csvFormat.LastName]);
			model.FirstName = helper_Names.NormalizeNameItem(data[csvFormat.FirstName]);
			model.MiddleName = helper_Names.NormalizeNameItem(data[csvFormat.MiddleName]);
			model.Sex = this.DecodeSex(data[csvFormat.Sex], model.MiddleName);
			model.Birthday = this.GetValue(data, csvFormat.BirthDay);
			model.Nationality = this.DecodeNationality(data, csvFormat.Citizenship);
			log.Debug('DecodePerson }');
		};

		res.DecodeNationality = function (data, field) {
			var value = data[field];
			if (!this.isNullOrEmpty(value)) {
				value = value == 'РОССИЯ' ? 'RUS' : value;
				var dictionaryValues = helper_citizenship.GetValues();
				for (var i = 0; i < dictionaryValues.length; i++) {
					if (dictionaryValues[i].id == value)
						return { id: value, text: dictionaryValues[i].text };
				}
			}
			return null;
		}

		res.DecodeCountryBirthPlace = function (value, birthday) {
			if (this.isNullOrEmpty(value) || (/^\d+$/.test(value))) {
				value = 'RUS';
			}
			if (value == 'РОССИЯ') {
				value = 'RUS';
			}
			if (h_republicSun.IsSunRepublicByDate(value, birthday)) {
				value = 'SUN';
			}
			var result = null;
			var dictionaryValues = helper_country.GetValues();
			for (var i = 0; i < dictionaryValues.length; i++) {
				if (dictionaryValues[i].id == value) {
					result = { id: value, text: dictionaryValues[i].text };
					break;
				}
			}
			return result;
		};

		res.DecodeBirthPlace = function (data, model, csvFormat) {
			log.Debug('DecodeBirthPlace {');
			if (this.isNullOrEmpty(this.GetValue(data, csvFormat.BirthCountry)) && this.isNullOrEmpty(this.GetValue(data, csvFormat.BirthRegion)) &&
				this.isNullOrEmpty(this.GetValue(data, csvFormat.BirthArea)) && this.isNullOrEmpty(this.GetValue(data, csvFormat.BirthCity)) &&
				this.isNullOrEmpty(this.GetValue(data, csvFormat.BirthPlace))) {
				model.Birthplace = {
				    Country: this.DecodeCountryBirthPlace('RUS', model.Birthday)
				};
			} else {
				model.Birthplace = {
					Country: this.DecodeCountryBirthPlace(data[csvFormat.BirthCountry], model.Birthday),
					Region: this.GetValue(data, csvFormat.BirthRegion),
					Area: this.GetValue(data, csvFormat.BirthArea),
					City: this.GetValue(data, csvFormat.BirthCity),
					Town: this.GetValue(data, csvFormat.BirthPlace)
				};
			}
			model.Birthplace.text = h_town.ToReadableStringBirthplace(model.Birthplace, null);
			log.Debug('DecodeBirthPlace }');
		};

		res.DecodeDocument = function (data, model, csvFormat) {
			log.Debug('DecodeDocument {');
			model.DocumentType = this.GetDictionaryValue(data, csvFormat.Document, helper_DocumentType);
			model.DocumentSeries = this.GetValue(data, csvFormat.DocumentSeries);
			model.DocumentNumber = this.GetValue(data, csvFormat.DocumentNumber);
			model.DocumentGivenDate = this.GetValue(data, csvFormat.DocumentIssued);
			log.Debug('DecodeDocument }');
		};

		res.DecodeDocumentGivenBy = function (data, field) {
			try {
				log.Debug('DecodeDocumentGivenBy {');
				var value = data[field];
				if (!this.isNullOrEmpty(value)) {
					if (/^\d+$/.test(value)) {
						return h_dictionary.FindById(fms, value);
					} else {
						return value;
					}
				}
				return null;
			} finally {
				log.Debug('DecodeDocumentGivenBy }');
			}
		};

		res.DecodeLivingAddress = function (data, model, csvFormat) {
			log.Debug('DecodeLivingAddress {');
			try {
				if (this.isNullOrEmpty(this.GetValue(data, csvFormat.LivingRegion)) && this.isNullOrEmpty(this.GetValue(data, csvFormat.LivingArea)) &&
				this.isNullOrEmpty(this.GetValue(data, csvFormat.LivingPlace)) && this.isNullOrEmpty(this.GetValue(data, csvFormat.LivingCity))) {
					log.Debug('LivingAddress null');
				} else {
					return {
						Country: { id: 'RUS', text: 'Россия' },
						russianAddress: {
							free: {
								Регион: this.GetValue(data, csvFormat.LivingRegion),
								Район: this.GetValue(data, csvFormat.LivingArea),
								Город: this.GetValue(data, csvFormat.LivingCity),
								НаселенныйПункт: this.GetValue(data, csvFormat.LivingPlace),
								ГородскойРайон: "",
								Улица: this.GetValue(data, csvFormat.LivingStreet)
							},
							variant_fias: false,
							Дом: this.GetValue(data, csvFormat.LivingHouse),
							Корпус: this.GetValue(data, csvFormat.LivingHousing),
							Квартира: this.GetValue(data, csvFormat.LivingFlat),
							Комната: ""
						}
					};
				}
			} finally {
				log.Debug('DecodeLivingAddress }');
			}
		};

		res.DecodeLastAddress = function (data, model, csvFormat) {
			log.Debug('DecodeLastAddress {');
			try {
				if (this.isNullOrEmpty(this.GetValue(data, csvFormat.LastRegion)) && this.isNullOrEmpty(this.GetValue(data, csvFormat.LastArea)) &&
				this.isNullOrEmpty(this.GetValue(data, csvFormat.LastPlace)) && this.isNullOrEmpty(this.GetValue(data, csvFormat.LastCity))) {
					log.Debug('LastAddress null');
				} else {
					return {
						Country: { id: 'RUS', text: 'Россия' },
						russianAddress: {
							free: {
								Регион: this.GetValue(data, csvFormat.LastRegion),
								Район: this.GetValue(data, csvFormat.LastArea),
								Город: this.GetValue(data, csvFormat.LastCity),
								НаселенныйПункт: this.GetValue(data, csvFormat.LastPlace),
								ГородскойРайон: "",
								Улица: this.GetValue(data, csvFormat.LastStreet)
							},
							variant_fias: false,
							Дом: this.GetValue(data, csvFormat.LastHouse),
							Корпус: this.GetValue(data, csvFormat.LastHousing),
							Квартира: this.GetValue(data, csvFormat.LastFlat),
							Комната: ""
						}
					};
				}
			} finally {
				log.Debug('DecodeLastAddress }');
			}
		};

		log.Debug('Create }');
		return res;
	}
});