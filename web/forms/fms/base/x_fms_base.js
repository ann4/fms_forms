define([
	  'forms/base/codec.xml'
	, 'forms/base/log'
],
function (
	  BaseCodec
	, GetLogger
	)
{
	var log = GetLogger('x_fms_base');

	return function ()
	{
		//log.Debug('Create {');
		var res = BaseCodec();

		res.tabs = ['', '    ', '        ', '            ', '                ', '                    ', '                        ', '                            ', '                                ', '                                    '];

		res.CheckDictionaryType = function (dictionary_type, child_text, node_tagName)
		{
			if ('string' == typeof dictionary_type)
			{
				if (dictionary_type.toUpperCase() != child_text.toUpperCase())
					throw 'wrong dictionary_type for node "' + node_tagName + '"! expected \"' + dictionary_type + '" but parsed "' + child_text + '"';
			}
			else
			{
				for (var i = 0; i < dictionary_type.length; i++)
				{
					var dt = dictionary_type[i];
					if (dt.toUpperCase() == child_text.toUpperCase())
						return;
				}
				throw 'wrong dictionary_type for node "' + node_tagName + '"! expected \"' + dictionary_type[0] + '" but parsed "' + child_text + '"';
			}
		}

		res.DecodeDictionaryItem = function (node, dictionary_type)
		{
			var dictionary_item = {};
			var childs = node.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "type": this.CheckDictionaryType(dictionary_type, child.text, node.tagName); break;
					case "element": dictionary_item.id = child.text; break;
					case "value": dictionary_item.text = child.text; break;
				}
			}
			return dictionary_item;
		}

		res.DecodeAddressHousing = function (node)
		{
			var res = {}
			var self = this;
			self.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'value': res.text = child.text; break;
					case 'type':
						{
							var ditem = self.DecodeDictionaryItem(child, 'addressObjectType');
							res.id = ditem.id;
						}
						break;
				}
			});
			return res;
		}

		res.EncodeDictionaryItem = function (num_tabs, dictionary_item, dictionary_type)
		{
			log.Debug('EncodeDictionaryItem {');
			var xml_string = '';
			xml_string += this.StringifyField(num_tabs, "type", dictionary_type);
			xml_string += this.StringifyField(num_tabs, "element", dictionary_item.id);
			xml_string += this.SafeStringifyField(num_tabs, "value", dictionary_item.text);
			log.Debug('EncodeDictionaryItem }');
			return xml_string;
		}

		res.EncodeNamedDictionaryItem = function (num_tabs, dictionary_item, name, dictionary_type, comment)
		{
			log.Debug('EncodeNamedDictionaryItem {');
			try
			{
				var xml_string = '';
				xml_string += this.FormatTabs(num_tabs) + '<' + name + '>' + this.FormatEOL();
				if (comment && !this.skipComments)
					xml_string += this.FormatTabs(num_tabs) + '<!--' + comment + '-->' + this.FormatEOL();
				xml_string += (null == dictionary_item) ? '' : this.EncodeDictionaryItem(num_tabs + 1, dictionary_item, dictionary_type);
				xml_string += this.FormatTabs(num_tabs) + '</' + name + '>' + this.FormatEOL();
				return xml_string;
			}
			catch (e)
			{
				log.Error('can not store dictionary item "' + name + '"');
				throw e;
			}
			finally
			{
				log.Debug('EncodeNamedDictionaryItem }');
			}
		}

		res.SafeEncodeNamedDictionaryItem = function (num_tabs, dictionary_item, name, dictionary_type)
		{
			log.Debug('SafeEncodeNamedDictionaryItem {');
			var xml_string = (!dictionary_item || !(dictionary_item.id))
			? '' : res.EncodeNamedDictionaryItem(num_tabs, dictionary_item, name, dictionary_type);
			log.Debug('SafeEncodeNamedDictionaryItem }');
			return xml_string;
		}

		//log.Debug('Create }');
		return res;
	}
});