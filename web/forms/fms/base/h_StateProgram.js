// утф8
define([
	'txt!forms/fms/guides/dict_mig_programparticipanttype.csv',
	'forms/base/h_csv'
]
, function (file_CSV, helper_CSV)
{
	var res = {};
	res.GetValues = function () {
		if(!res.Values)
			res.Values = helper_CSV.CSVToJSON(file_CSV, ';');
		return res.Values;
	}
	return res;
});
