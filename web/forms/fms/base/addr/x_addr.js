define([
	  'forms/fms/base/x_fms_base'
	, 'forms/base/log'
	, 'forms/fms/base/russ_addr/x_russ_addr'
	, 'forms/fms/base/russ_addr_fias/x_fias'
],
function (BaseCodec, GetLogger, x_russ_addr, x_fias)
{
	var log = GetLogger('x_addr');
	return function ()
	{
		log.Debug('Create {');
		var addr_codec = BaseCodec();

		addr_codec.SafePrepRussAddr = function ()
		{
			if (!this.x_russ_addr)
				this.x_russ_addr = x_russ_addr();
			this.x_russ_addr.tabs = this.tabs;
			this.x_russ_addr.m_only_free_mode = this.only_free_mode();
			this.x_russ_addr.AddressWithHousingType = addr_codec.AddressWithHousingType;
			this.x_russ_addr.AddressWithHousingTypeWithoutRoom = addr_codec.AddressWithHousingTypeWithoutRoom;
		}

		addr_codec.m_only_free_mode = false;
		addr_codec.only_free_mode = function ()
		{
			return this.m_only_free_mode;
		}

		addr_codec.EncodeDataItem = function (num_tabs, addr, field_name, schema)
		{
			log.Debug('EncodeDataItem {');
			var xml_string = '';
			xml_string += this.OpenTagLine(num_tabs, field_name);
			var next_num_tabs = num_tabs + 1;

			if (addr.Country && 'RUS' == addr.Country.id)
			{
				addr_codec.SafePrepRussAddr();
				xml_string += this.x_russ_addr.EncodeDataItem(next_num_tabs, addr.russianAddress, 'russianAddress');
			}
			else
			{
				var next_num_tabs2 = next_num_tabs + 1;
				var field_name2 = 'foreignAddress';
				xml_string += this.OpenTagLine(next_num_tabs, field_name2);
				xml_string += this.SafeEncodeNamedDictionaryItem(next_num_tabs2, addr.Country, 'country', 'country');
				xml_string += this.SafeStringifyField(next_num_tabs2, 'unstructured', addr.unstructured);
				xml_string += this.CloseTagLine(next_num_tabs, field_name2);
			}

			xml_string += this.CloseTagLine(num_tabs, field_name);
			log.Debug('EncodeDataItem }');
			return xml_string;
		}

		addr_codec.DecodeXmlElement = function (node, schema)
		{
			log.Debug('DecodeXmlElement {');
			var model = {};
			var self = this;
			self.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'foreignAddress':
						{
							self.DecodeChildNodes(child, function (child2)
							{
								switch (child2.tagName)
								{
									case 'country': model.Country = self.DecodeDictionaryItem(child2, 'country'); break;
									case 'unstructured': model.unstructured = child2.text; break;
								}
							});
							break;
						}
					case 'russianAddress':
						{
							model.Country = { id: 'RUS', text: 'Россия' };
							addr_codec.SafePrepRussAddr();
							model.russianAddress = addr_codec.x_russ_addr.DecodeXmlElement(child);
							break;
						}
				}
			});
			log.Debug('DecodeXmlElement }');
			return model;
		}

		log.Debug('Create }');
		return addr_codec;
	}
});