define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/addr/e_addr.html'
	, 'forms/fms/base/russ_addr/c_russ_addr'
	, 'forms/fms/base/h_country'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/addr/x_addr'
	, 'forms/base/h_names'
],
function 
(
	  c_binded
	, tpl
	, c_russ_addr
	, h_country
	, helper_Select2
	, x_addr
	, helper_Names
)
{
	return function ()
	{
		var Select2Options = function (h_values)
		{
			return function (m, field_name)
			{
				if (m && m[field_name] && !m[field_name].text && m[field_name].id)
				{
					var item = helper_Select2.FindById(h_values.GetValues(), m[field_name].id)
					if (item)
						m[field_name].text = item.text;
				}
				return {
					placeholder: 'Укажите название страны'
					, allowClear: true
					, query: function (q)
					{
						q.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), q.term) });
					}
				};
			}
		}

		var options =
		{
			field_spec:
			{
				russianAddress: { controller: c_russ_addr }
				, Country: Select2Options(h_country)
			}
		};

		var controller = c_binded(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			this.binding.GetControlForField('Country').on('change', function () { self.OnChangeCountry(); })
			$('.encoding_date').change(helper_Names.OnNameChange_NormalizeEncoding);
			self.OnChangeCountry();
		}

		controller.SafePrepModel = function ()
		{
			if (!this.model)
				this.model = { Country: { id: 'RUS', text: "Россия"} };
		}

		var base_CreateNew = controller.CreateNew;
		controller.CreateNew = function (sel)
		{
			this.SafePrepModel();
			base_CreateNew.call(this, sel);
		}

		var base_Edit = controller.Edit;
		controller.Edit = function (sel)
		{
			this.SafePrepModel();
			base_Edit.call(this, sel);
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res = base_GetFormContent.call(this);
			if (res && null != res)
			{
				if (res.Country && 'RUS' == res.Country.id)
				{
					delete res.unstructured;
				}
				else
				{
					delete res.russianAddress;
				}
			}
			return res;
		}

		controller.OnChangeCountry = function ()
		{
			var country = this.binding.GetControlForField('Country').select2('data');
			var sel = this.binding.form_div_selector;
			if (country && 'RUS' == country.id)
			{
				$(sel + ' div.cpw-fms-addr div.russianAddress').show();
				$(sel + ' div.cpw-fms-addr div.foreignAddress').hide();
			}
			else
			{
				$(sel + ' div.cpw-fms-addr div.russianAddress').hide();
				$(sel + ' div.cpw-fms-addr div.foreignAddress').show();
			}
		}

		return controller;
	}
});
