include ..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions
wait_text "Страна:"
shot_check_png ..\..\shots\0new.png
play_stored_lines address_fields_10_select_fias_object
play_stored_lines address_fields_10
shot_check_png ..\..\shots\10sav.png
wait_click_text "Сохранить содержимое формы"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\10sav.json.result.txt
exit