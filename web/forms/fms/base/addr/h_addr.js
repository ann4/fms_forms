define(['forms/fms/base/russ_addr_fias/h_fias', 'forms/base/log', 'forms/fms/base/convert/x_convert'],
function (h_fias, GetLogger, x_convert)
{
	var log = GetLogger('h_addr');
	var h_addr = {};

	h_addr.only_free_address_by_substitusion_id= function(id)
	{
		switch (id)
		{
			case '140733': /* 260-000 140733 Управление по вопросам миграции ГУ МВД России по Ставропольскому краю */
			case '140765': /* 260-033 140765 Отдел полиции №1 ГУ МВД России по Ставропольскому краю */
			case '140766': /* 260-034 140766 Отдел полиции №2 ГУ МВД России по Ставропольскому краю */
			case '140767': /* 260-035 140765 Отдел полиции №3 ГУ МВД России по Ставропольскому краю */
				return true;
			default:
				return false;
		}
	}

	h_addr.PreparePrefixByType = function(data, prefixes, name)
	{
		var type = data[name + '_Type'];
		var ttype = (type && '' != type && null != type) ? type.text : name;
		for(var i = 0; i < prefixes.length; i++)
		{
			var name_prefix = prefixes[i];
			if (name_prefix[1] == ttype)
				return name_prefix[0];
		}
	}

	h_addr.AppendWithPrefix = function (text, prefix, data, name)
	{
		if (data[name])
		{
			var value = data[name];
			if ('' != value && null != value)
			{
				var fixed_text = text;
				if ('' != fixed_text)
					fixed_text += ', ';
				if ('object' == typeof prefix) {
					prefix = this.PreparePrefixByType(data, prefix, name);
				}
				return (0==value.indexOf(prefix)) ? fixed_text + value : fixed_text + prefix + value;
			}
		}
		return text;
	}

	h_addr.Append = function (text, data, names_prefixes)
	{
		for (var i = 0; i < names_prefixes.length; i++)
		{
			var name_prefix = names_prefixes[i];
			if ('string' == typeof name_prefix)
			{
				text = this.AppendWithPrefix(text, '', data, name_prefix);
			}
			else
			{
				text = this.AppendWithPrefix(text, name_prefix[0], data, name_prefix[1]);
			}
		}
		return text;
	}

	h_addr.DecodeOldFiasText = function (parts)
	{
		var fias_text_parts = [];
		for (var i = 0; i < parts.length; i++)
		{
			var part = parts[i];
			if ('' != part)
				fias_text_parts.push(part);
		}
		var readable_text = fias_text_parts.join(', ');
		return readable_text;
	}

	h_addr.PrepareReadableTextFormat = function (data, format)
	{
		var text = '';
		if (data.Country && 'RUS' == data.Country.id)
		{
			var russianAddress = data.russianAddress;
			if (!russianAddress.variant_fias)
			{
				if (russianAddress.free)
				{
					text = this.Append(text, russianAddress.free,
					['Регион', 'Район', ['г ', 'Город'], 'НаселенныйПункт', 'ГородскойРайон', ['ул ','Улица']]);
				}
			}
			else
			{
				if (russianAddress.fias)
				{
					var fias = russianAddress.fias;
					if (fias.old_fias_text)
					{
						text += this.DecodeOldFiasText(fias.old_fias_text.split('$'));
					}
					else if (fias.id)
					{
						text += fias.id;
					}
					else
					{
						for (var i = fias.length - 1; i >= 0; i--)
						{
							if ('' != text)
								text += ', ';
							text += h_fias.PrepareObjectText(fias[i]);
						}
					}
				}
			}
			text = this.Append(text, russianAddress,format);
		}
		else if (data.Country && data.Country.id)
		{
			text += data.Country.text;
			if (data.unstructured && null != data.unstructured && '' != data.unstructured)
				text += ', ' + data.unstructured;
		}
		return text;
	}

	h_addr.PrepareReadableText = function (data)
	{
		return h_addr.PrepareReadableTextFormat(data, [
			[[['д.', 'Дом'], ['влад.', 'Владение'], ['уч-к.', 'Участок'], ['воин.ч..', 'Воинская часть'], ['двлд.', 'Домовладение'], ['соор.', 'Сооружение'], ['бокс', 'Бокс']], 'Дом'],
			['корп.', 'Корпус'],
			[[['кв.', 'Квартира'], ['оф.', 'Офис'], ['комн.', 'Комната'], ['блок кв.', 'Блок квартирный'], ['блок-сек.', 'Блок-секция'], ['помещ.', 'Помещение']], 'Квартира'], 
			['комн.', 'Комната']
		]);
	}

	h_addr.PrepareReadableTextWithoutRoom = function (data)
	{
		return h_addr.PrepareReadableTextFormat(data, [
			[[['д.', 'Дом'], ['влад.', 'Владение'], ['уч-к.', 'Участок'], ['воин.ч..', 'Воинская часть'], ['двлд.', 'Домовладение'], ['соор.', 'Сооружение'], ['бокс', 'Бокс']], 'Дом'],
			['корп.', 'Корпус'],
			[[['кв.', 'Квартира'], ['оф.', 'Офис'], ['комн.', 'Комната'], ['блок кв.', 'Блок квартирный'], ['блок-сек.', 'Блок-секция'], ['помещ.', 'Помещение']], 'Квартира']
		]);
	}

	h_addr.IsEmptyAddress = function (addr)
	{
		if (!addr || !addr.Country || !addr.Country.id)
		{
			return true;
		}
		else
		{
			if ('RUS' == addr.Country.id)
			{
				return !addr.russianAddress || (!addr.russianAddress.Дом && (!addr.russianAddress.fias || 0 == addr.russianAddress.fias.length) && !addr.russianAddress.free);
			}
			else
			{
				return false;
			}
		}
	}

	h_addr.SafePrepareReadableText = function (addr, null_text)
	{
		try
		{
			return !this.IsEmptyAddress(addr)
				? this.PrepareReadableText(addr)
				: null_text ? null_text : 'Кликните, чтобы указать..';
		}
		catch (err)
		{
			log.Error(err);
			return 'Ошибка при формировании строки адреса!';
		}
	}

	h_addr.SafePrepareReadableTextWithoutRoom = function (addr, null_text)
	{
		try
		{
			return this.IsEmptyAddress(addr) ? null_text : this.PrepareReadableTextWithoutRoom(addr);
		}
		catch (err)
		{
			log.Error(err);
			log.Error(err && err.message ? err.message : '');
			return 'Ошибка при формировании строки адреса!';
		}
	}

	h_addr.NewAddressFormat = function (addr)
	{
		return !addr ||
			   !addr.Region
			&& !addr.Area
			&& !addr.City
			&& !addr.Town
			&& !addr.Street
			&& !addr.HouseNumber
			&& !addr.KorpusNumber
			&& !addr.FlatNumber
			;
	}

	h_addr.SafeOldAddrFieldText = function (field)
	{
		return (!field || null == field) ? '' : field.text;
	}

	h_addr.ConvertFromOldFormat = function (old_addr)
	{
		if (this.NewAddressFormat(old_addr))
		{
			return old_addr;
		}
		else
		{
			var addr = { Country: old_addr.Country };
			if ('RUS' != old_addr.Country.id)
			{
				addr.unstructured = 
				[	this.SafeOldAddrFieldText(old_addr.Region)
					, this.SafeOldAddrFieldText(old_addr.Area)
					, this.SafeOldAddrFieldText(old_addr.City)
					, this.SafeOldAddrFieldText(old_addr.Town)
					, this.SafeOldAddrFieldText(old_addr.Street)
					, old_addr.HouseNumber
					, old_addr.KorpusNumber
					, old_addr.FlatNumber
				].join(', ').replace(/(, ,)+/g, ',');
			}
			else
			{
				if (!old_addr.Street)
				{
					return old_addr;
				}
				else
				{
					addr.russianAddress =
					{
						variant_fias: true
						, fias:
						{
							id: old_addr.Street.id
							, old_fias_text:
							[
								this.SafeOldAddrFieldText(old_addr.Region)
								, this.SafeOldAddrFieldText(old_addr.Area)
								, this.SafeOldAddrFieldText(old_addr.City)
								, this.SafeOldAddrFieldText(old_addr.Town)
								, ''
								, this.SafeOldAddrFieldText(old_addr.Street)
							]
							.join('$')
						}
						, Дом: old_addr.HouseNumber
						, Корпус: old_addr.KorpusNumber
						, Квартира: old_addr.FlatNumber
					};
				}
			}
			return addr;
		}
	}

	h_addr.PrepareAddressParts = function (address)
	{
		if (!address || !address.russianAddress)
		{
			return {};
		}
		else
		{
			var russianAddress = address.russianAddress;
			if (!russianAddress.variant_fias)
			{
				return russianAddress.free;
			}
			else
			{
				var fias = russianAddress.fias;
				if (!fias.id)
				{
					return x_convert.Encode(fias);
				}
				else if (fias.old_fias_text)
				{
					return h_fias.DecodeOldFiasAddressToPrintable(fias.old_fias_text);
				}
				else
				{
					var res = {};
					return res;
				}
			}
		}
	}

	return h_addr;
});