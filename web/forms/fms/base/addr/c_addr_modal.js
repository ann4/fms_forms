define([
	  'forms/fms/base/addr/c_addr'
	, 'forms/base/h_msgbox'
	, 'forms/fms/base/russ_addr_fias/h_fias'
],
function
(
	c_addr
	, h_msgbox
	, h_fias
)
{
	return function (model, field_name, title, onlyfias, default_fias, f_getFiasVersion)
	{
		var createController = function (item, onAfterSave)
		{
			var res =
			{
				ShowModal: function (e)
				{
					e.preventDefault();
					var controller = c_addr();
					var field_value = model[field_name];
					if (onlyfias)
					{
						if (!field_value)
							field_value = {};
						if (!field_value.russianAddress)
							field_value.russianAddress = {};
						if (!field_value.russianAddress.onlyfias)
							field_value.russianAddress.onlyfias = true;
						if (!field_value.russianAddress.variant_fias)
							field_value.russianAddress.variant_fias = true;
					}
					if (!default_fias) {
						if (!field_value)
							field_value = {};
						if (!field_value.russianAddress)
							field_value.russianAddress = {};
						if (!field_value.russianAddress.free)
							field_value.russianAddress.variant_fias = true;
						field_value.russianAddress.fias_version = !f_getFiasVersion ? h_fias.GetFiasVersionBySchemaVersion(model) : f_getFiasVersion();
					}
					if (null != field_value)
					{
						if (!field_value.Country || null == field_value.Country)
							field_value.Country = { id: 'RUS', text: "Россия" };
						controller.SetFormContent(field_value);
					}
					var bname_Ok = 'Сохранить';
					h_msgbox.ShowModal({
						controller: controller
						, width: 750
						, height: 400
						, title: !title ? 'Адрес' : title
						, buttons: [bname_Ok, 'Отменить']
						, onclose: function (bname)
						{
							if (bname_Ok == bname)
							{
								model[field_name] = controller.GetFormContent();
								onAfterSave();
							}
							if (model[field_name] && model[field_name].russianAddress) {
								delete model[field_name].russianAddress.onlyfias;
								if (null == model[field_name].russianAddress.fias_version)
									delete model[field_name].russianAddress.fias_version;
							}
						}
					});
				}
			};
			return res;
		};
		return createController;
	}
});
