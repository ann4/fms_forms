﻿define([
	  'forms/base/log'

	, 'txt!forms/fms/schemes/1.3.36/xmldsig-core-schema-without-dtd.xml'

	, 'txt!forms/fms/schemes/1.3.36/core.xsd'
	, 'txt!forms/fms/schemes/1.3.36/core-correction.xsd'
	, 'txt!forms/fms/schemes/1.3.36/foreign-citizen-core.xsd'
	, 'txt!forms/fms/schemes/1.3.36/migration.xsd'
	, 'txt!forms/fms/schemes/1.3.36/migration-by-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.36/migration-by-staying-edit.xsd'
	, 'txt!forms/fms/schemes/1.3.36/application/registration-application.xsd'
	, 'txt!forms/fms/schemes/1.3.36/application/registration-application-by-liv.xsd'
	, 'txt!forms/fms/schemes/1.3.36/application/registration-application-by-stay.xsd'
	, 'txt!forms/fms/schemes/1.3.36/response.xsd'
	, 'txt!forms/fms/schemes/1.3.36/unreg-migration-by-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.36/application/unregistration-application-by-liv.xsd'
	, 'txt!forms/fms/schemes/1.3.36/application/unregistration-application-by-stay.xsd'

	, 'txt!forms/fms/schemes/1.3.36/hotel-form5.xsd'

	, 'txt!forms/fms/schemes/1.3.37/core.xsd'
	, 'txt!forms/fms/schemes/1.3.37/core-correction.xsd'
	, 'txt!forms/fms/schemes/1.3.37/foreign-citizen-core.xsd'
	, 'txt!forms/fms/schemes/1.3.37/migration.xsd'
	, 'txt!forms/fms/schemes/1.3.37/migration-by-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.37/migration-by-staying-edit.xsd'
	, 'txt!forms/fms/schemes/1.3.37/application/registration-application.xsd'
	, 'txt!forms/fms/schemes/1.3.37/application/registration-application-by-liv.xsd'
	, 'txt!forms/fms/schemes/1.3.37/application/registration-application-by-stay.xsd'
	, 'txt!forms/fms/schemes/1.3.37/response.xsd'
	, 'txt!forms/fms/schemes/1.3.37/unreg-migration-by-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.37/application/unregistration-application-by-liv.xsd'
	, 'txt!forms/fms/schemes/1.3.37/application/unregistration-application-by-stay.xsd'

	, 'txt!forms/fms/schemes/1.3.40/core/core.xsd'
	, 'txt!forms/fms/schemes/1.3.40/hotel/core-correction.xsd'
	, 'txt!forms/fms/schemes/1.3.40/core/foreign-citizen-core.xsd'
	, 'txt!forms/fms/schemes/1.3.40/core/migration.xsd'
	, 'txt!forms/fms/schemes/1.3.40/hotel/migration-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.40/hotel/migration-staying-edit.xsd'
	, 'txt!forms/fms/schemes/1.3.40/app/registration.xsd'
	, 'txt!forms/fms/schemes/1.3.40/app/registration-living.xsd'
	, 'txt!forms/fms/schemes/1.3.40/app/registration-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.40/app/response.xsd'
	, 'txt!forms/fms/schemes/1.3.40/hotel/migration-staying-unreg.xsd'
	, 'txt!forms/fms/schemes/1.3.40/app/registration-living-unreg.xsd'
	, 'txt!forms/fms/schemes/1.3.40/app/registration-staying-unreg.xsd'

	, 'txt!forms/fms/schemes/1.3.42/core/core.xsd'
	, 'txt!forms/fms/schemes/1.3.42/hotel/core-correction.xsd'
	, 'txt!forms/fms/schemes/1.3.42/core/foreign-citizen-core.xsd'
	, 'txt!forms/fms/schemes/1.3.42/core/migration.xsd'
	, 'txt!forms/fms/schemes/1.3.42/hotel/migration-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.42/hotel/migration-staying-edit.xsd'
	, 'txt!forms/fms/schemes/1.3.42/app/registration.xsd'
	, 'txt!forms/fms/schemes/1.3.42/app/registration-living.xsd'
	, 'txt!forms/fms/schemes/1.3.42/app/registration-staying.xsd'
	, 'txt!forms/fms/schemes/1.3.42/app/response.xsd'
	, 'txt!forms/fms/schemes/1.3.42/hotel/migration-staying-unreg.xsd'
	, 'txt!forms/fms/schemes/1.3.42/app/registration-living-unreg.xsd'
	, 'txt!forms/fms/schemes/1.3.42/app/registration-staying-unreg.xsd'

	, 'txt!forms/fms/schemes/1.3.42/hotel/hotel-form5.xsd'

	, 'txt!forms/fms/schemes/1.102.2/core.xsd'
	, 'txt!forms/fms/schemes/1.102.2/core-correction.xsd'
	, 'txt!forms/fms/schemes/1.102.2/foreign-citizen-core.xsd'
	, 'txt!forms/fms/schemes/1.102.2/hotel.xsd'
	, 'txt!forms/fms/schemes/1.102.2/migration.xsd'
	, 'txt!forms/fms/schemes/1.102.2/migration-staying-case.xsd'
	, 'txt!forms/fms/schemes/1.102.2/migration-staying-case-edit.xsd'
	, 'txt!forms/fms/schemes/1.102.2/migration-staying-unreg-case.xsd'
	, 'txt!forms/fms/schemes/1.102.2/registration.xsd'
	, 'txt!forms/fms/schemes/1.102.2/registration-staying-case.xsd'
	, 'txt!forms/fms/schemes/1.102.2/registration-staying-case-change.xsd'
	, 'txt!forms/fms/schemes/1.102.2/hotel-response.xsd'
],
function (
	  GetLogger
	, xmldsig

	, core
	, core_correction
	, foreign_citizen_core
	, migration
	, migration_by_staying
	, migration_by_staying_edit
	, registration_application
	, registration_application_by_liv
	, registration_application_by_stay
	, response
	, unreg_migration_by_staying
	, unregistration_application_by_liv
	, unregistration_application_by_stay

	, hotel_form5

	, core_1_3_37
	, core_correction_1_3_37
	, foreign_citizen_core_1_3_37
	, migration_1_3_37
	, migration_by_staying_1_3_37
	, migration_by_staying_edit_1_3_37
	, registration_application_1_3_37
	, registration_application_by_liv_1_3_37
	, registration_application_by_stay_1_3_37
	, response_1_3_37
	, unreg_migration_by_staying_1_3_37
	, unregistration_application_by_liv_1_3_37
	, unregistration_application_by_stay_1_3_37

	, core_1_3_40
	, core_correction_1_3_40
	, foreign_citizen_core_1_3_40
	, migration_1_3_40
	, migration_by_staying_1_3_40
	, migration_by_staying_edit_1_3_40
	, registration_application_1_3_40
	, registration_application_by_liv_1_3_40
	, registration_application_by_stay_1_3_40
	, response_1_3_40
	, unreg_migration_by_staying_1_3_40
	, unregistration_application_by_liv_1_3_40
	, unregistration_application_by_stay_1_3_40

	, core_1_3_42
	, core_correction_1_3_42
	, foreign_citizen_core_1_3_42
	, migration_1_3_42
	, migration_by_staying_1_3_42
	, migration_by_staying_edit_1_3_42
	, registration_application_1_3_42
	, registration_application_by_liv_1_3_42
	, registration_application_by_stay_1_3_42
	, response_1_3_42
	, unreg_migration_by_staying_1_3_42
	, unregistration_application_by_liv_1_3_42
	, unregistration_application_by_stay_1_3_42

	, hotel_form5_1_3_42

	, core_1_102_2
	, core_correction_1_102_2
	, foreign_citizen_core_1_102_2
	, hotel_1_102_2
	, migration_1_102_2
	, migration_staying_case_1_102_2
	, migration_staying_case_edit_1_102_2
	, migration_staying_unreg_case_1_102_2
	, registration_1_102_2
	, registration_staying_case_1_102_2
	, registration_staying_case_change_1_102_2
	, response_1_102_2
	)
{
	var log = GetLogger('h_xsd');
	var helper = {};

	helper.LoadXsd = function (xsd_text)
	{
		var xsd_doc = new ActiveXObject('MSXML2.DOMDocument.6.0');
		var res = xsd_doc.loadXML(xsd_text);
		if (res)
		{
			return xsd_doc;
		}
		else
		{
			log.Error('loading of xsd scheme is failed!');
			return null;
		}
	}

	helper.GetScheme_1_3_36_migrationn = function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying", helper.LoadXsd(migration_by_staying));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_by_staying_edit));
		xs.add("http://umms.fms.gov.ru/application/registration", helper.LoadXsd(registration_application));
		xs.add("http://umms.fms.gov.ru/application/registration/living/reg", helper.LoadXsd(registration_application_by_liv));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/reg", helper.LoadXsd(registration_application_by_stay));
		xs.add("http://umms.fms.gov.ru/replication/response", helper.LoadXsd(response));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(unreg_migration_by_staying));
		xs.add("http://umms.fms.gov.ru/application/registration/living/unreg", helper.LoadXsd(unregistration_application_by_liv));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/unreg", helper.LoadXsd(unregistration_application_by_stay));
		return xs;
	}

	helper.GetScheme_1_3_37_migrationn = function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_3_37));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction_1_3_37));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core_1_3_37));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration_1_3_37));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying", helper.LoadXsd(migration_by_staying_1_3_37));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_by_staying_edit_1_3_37));
		xs.add("http://umms.fms.gov.ru/application/registration", helper.LoadXsd(registration_application_1_3_37));
		xs.add("http://umms.fms.gov.ru/application/registration/living/reg", helper.LoadXsd(registration_application_by_liv_1_3_37));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/reg", helper.LoadXsd(registration_application_by_stay_1_3_37));
		xs.add("http://umms.fms.gov.ru/replication/response", helper.LoadXsd(response_1_3_37));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(unreg_migration_by_staying_1_3_37));
		xs.add("http://umms.fms.gov.ru/application/registration/living/unreg", helper.LoadXsd(unregistration_application_by_liv_1_3_37));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/unreg", helper.LoadXsd(unregistration_application_by_stay_1_3_37));
		return xs;
	}

	helper.GetScheme_1_3_40_migrationn = function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying", helper.LoadXsd(migration_by_staying_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_by_staying_edit_1_3_40));
		xs.add("http://umms.fms.gov.ru/application/registration", helper.LoadXsd(registration_application_1_3_40));
		xs.add("http://umms.fms.gov.ru/application/registration/living/reg", helper.LoadXsd(registration_application_by_liv_1_3_40));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/reg", helper.LoadXsd(registration_application_by_stay_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/response", helper.LoadXsd(response_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(unreg_migration_by_staying_1_3_40));
		xs.add("http://umms.fms.gov.ru/application/registration/living/unreg", helper.LoadXsd(unregistration_application_by_liv_1_3_40));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/unreg", helper.LoadXsd(unregistration_application_by_stay_1_3_40));
		return xs;
	}

	helper.GetScheme_1_3_42_migrationn = function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying", helper.LoadXsd(migration_by_staying_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_by_staying_edit_1_3_42));
		xs.add("http://umms.fms.gov.ru/application/registration", helper.LoadXsd(registration_application_1_3_42));
		xs.add("http://umms.fms.gov.ru/application/registration/living/reg", helper.LoadXsd(registration_application_by_liv_1_3_42));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/reg", helper.LoadXsd(registration_application_by_stay_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/response", helper.LoadXsd(response_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(unreg_migration_by_staying_1_3_42));
		xs.add("http://umms.fms.gov.ru/application/registration/living/unreg", helper.LoadXsd(unregistration_application_by_liv_1_3_42));
		xs.add("http://umms.fms.gov.ru/application/registration/staying/unreg", helper.LoadXsd(unregistration_application_by_stay_1_3_42));
		return xs;
	}

	helper.GetScheme_1_102_2_migrationn = function () {
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying", helper.LoadXsd(migration_staying_case_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_staying_case_edit_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(migration_staying_unreg_case_1_102_2));
		return xs;
	}

	helper.GetScheme_migrationn = function (version)
	{
		switch (version)
		{
			case '1.3.37': return this.GetScheme_1_3_37_migrationn();
			case '1.3.40': return this.GetScheme_1_3_40_migrationn();
			case '1.3.42': return this.GetScheme_1_3_42_migrationn();
			case '1.102.2': return this.GetScheme_1_102_2_migrationn();
			default: return this.GetScheme_1_3_36_migrationn();
		}
	}

	helper.GetScheme_1_3_36_migrationn_z = function () {
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_by_staying_edit));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(unreg_migration_by_staying));
		return xs;
	}

	helper.GetScheme_1_3_40_migrationn_z = function () {
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying", helper.LoadXsd(migration_by_staying_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_by_staying_edit_1_3_40));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(unreg_migration_by_staying_1_3_40));
		return xs;
	}

	helper.GetScheme_1_102_2_migrationn_z = function () {
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/core/correction", helper.LoadXsd(core_correction_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/foreign-citizen-core", helper.LoadXsd(foreign_citizen_core_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration", helper.LoadXsd(migration_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying", helper.LoadXsd(migration_staying_case_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/case-edit", helper.LoadXsd(migration_staying_case_edit_1_102_2));
		xs.add("http://umms.fms.gov.ru/replication/migration/staying/unreg", helper.LoadXsd(migration_staying_unreg_case_1_102_2));
		return xs;
	}

	helper.GetScheme_migrationn_z = function (version) {
		switch (version) {
			case '1.3.40': return this.GetScheme_1_3_40_migrationn_z();
			case '1.102.2': return this.GetScheme_1_102_2_migrationn_z();
			default: return this.GetScheme_1_3_36_migrationn_z();
		}
	}

	helper.GetScheme_1_3_36_questionary= function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core));
		xs.add("http://umms.fms.gov.ru/replication/hotel/form5", helper.LoadXsd(hotel_form5));
		return xs;
	}

	helper.GetScheme_1_3_42_questionary = function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_3_42));
		xs.add("http://umms.fms.gov.ru/replication/hotel/form5", helper.LoadXsd(hotel_form5_1_3_42));
		return xs;
	}

	helper.GetScheme_questionary = function (version)
	{
		switch (version)
		{
			case '1.102.2':
			case '1.3.42': return this.GetScheme_1_3_42_questionary();
			default: return this.GetScheme_1_3_36_questionary();
		}
	}

	helper.GetScheme_old_response = function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core));
		xs.add("http://umms.fms.gov.ru/replication/response", helper.LoadXsd(response));
		return xs;
	}

	helper.GetScheme_1_102_2_response = function ()
	{
		var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
		xs.add("http://www.w3.org/2000/09/xmldsig#", helper.LoadXsd(xmldsig));
		xs.add("http://umms.fms.gov.ru/replication/core", helper.LoadXsd(core_1_102_2));
		xs.add("http://umms.fms.gov.ru/hotel/hotel-response", helper.LoadXsd(response_1_102_2));
		return xs;		
	}

	helper.GetScheme_response = function (version)
	{
		switch (version)
		{
			case '1.102.2': return this.GetScheme_1_102_2_response();
			default: return this.GetScheme_old_response();
		}
	}

	helper.GetScheme = function (form, version)
	{
		switch (form)
		{
			case 'migrationn':  return this.GetScheme_migrationn(version);
			case 'questionary': return this.GetScheme_questionary(version);
			case 'migrationn_z': return this.GetScheme_migrationn_z(version);
			case 'response': return this.GetScheme_response(version);
		}
	}

	return helper;
})