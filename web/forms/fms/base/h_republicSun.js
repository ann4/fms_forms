define(function () {
	var res = {};

	res.GetValues = function () {
		if (!res.Values) {
			res.Values = ['AZE', 'ARM', 'BLR', 'GEO', 'KAZ', 'KGZ', 'LVA', 'LTU', 'MDA', 'RUS', 'TJK', 'TKM', 'UZB', 'UKR', 'EST'];
		}
		return res.Values;
	};

	res.IsSunRepublic = function (value) {
		var republicSun = this.GetValues();
		return (republicSun.toString()).indexOf(value) != -1;
	};

	res.IsSunRepublicByDate = function (country, data) {
		if (null == data) {
			return false;
		} else {
			var republicSun = this.GetValues();
			var dateForSun = new Date(92, 1, 6);
			var dataFormat = new Date(data.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
			return (dataFormat < dateForSun) && (republicSun.toString()).indexOf(country) != -1;
		}
	}

	return res;
});
