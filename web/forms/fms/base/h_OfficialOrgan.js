// утф8
define([
	  'forms/fms/guides/dict_officialorgan_fms.csv'
	, 'forms/base/h_dictionary'
]
, function (fms, h_dictionary)
{
	var helper = {};

	helper.GetAuthorityOrgan = function (type)
	{
		if (!helper.FMS)
		{
			helper.FMS = fms.values;
			helper.AuthorityOrgan_variants = [];
			for (var i = 0; i < helper.FMS.length; i++)
			{
				var row = helper.FMS[i]
				helper.AuthorityOrgan_variants.push({ id: row[0], text: row[1] });
			}

			helper.AuthorityOrgan_code_variants = [];
			for (var i = 0; i < helper.FMS.length; i++)
			{
				var row = helper.FMS[i]
				if (row[2]) helper.AuthorityOrgan_code_variants.push({ id: row[0], text: row[1] });
			}
		}
		if (type == 'v')
			return helper.AuthorityOrgan_variants;
		else if (type == 'c')
			return helper.AuthorityOrgan_code_variants;
		else
			return helper.FMS;
	}


	helper.GetAuthorityOrgan_ByCode = function (code)
	{
		for (var i = 0; i < helper.FMS.length; i++)
		{
			var row = helper.FMS[i];
			if (row[2] == code.text)
				return { id: row[0], text: row[1] };
		}
		return null;
	};

	helper.GetAuthorityOrganCode_ByName = function (name)
	{
		for (var i = 0; i < helper.FMS.length; i++)
		{
			var row = helper.FMS[i];
			if (row[1] == name.text)
				return { id: row[0], text: row[1] };
		}
		return null;
	};

	helper.GetAuthorityOrgan_ById = function (id)
	{
		var fms = helper.GetAuthorityOrgan();
		for (var i = 0; i < fms.length; i++)
		{
			var row = fms[i];
			if (row.id == id.id)
				return { id: row.id, text: row.text, code: row.code };
		}
		return null;
	};

	return helper;
});
