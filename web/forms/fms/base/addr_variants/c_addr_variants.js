define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/addr_variants/e_addr_variants.html'
	, 'forms/fms/base/russ_addr/c_russ_addr'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/russ_addr_fias/h_fias'
],
function 
(
	  c_binded
	, tpl
	, c_addr
	, helper_Select2
	, h_addr
	, h_fias
)
{
	return function (addr_variants)
	{
		var options =
		{
			field_spec:
			{
				address: { controller: c_addr }
			}
		};

		var controller = c_binded(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			var select2_item = $(sel + ' div.cpw-fms-addr-variants div.select2 div');
			select2_item.select2
			({
				data: addr_variants
				, placeholder: 'Выберите один из вариантов адреса, указанных в профиле..'
			})
			.on('change', function () { self.OnChangeVariant(); });
		}

		controller.OnChangeVariant = function ()
		{
			var sel = this.binding.form_div_selector;
			var select2_item = $(sel + ' div.cpw-fms-addr-variants div.select2 div');
			var selected_option = select2_item.select2('data');
			if (null != selected_option)
			{
				var addr_controller = this.binding.fields_controls.address.controller;
				var addr = h_addr.ConvertFromOldFormat(selected_option);
				h_fias.ConvertOldFiasToNew(addr, $('#fias_version').val(), function (res) {
					addr_controller.SetFormContent(res.russianAddress);
					var addr_selector = sel + ' div.cpw-fms-addr-variants>div.address';
					$(addr_selector).html('');
					addr_controller.Edit(addr_selector);
				});
			}
			select2_item.select2('data', null);
		}

		var base_CreateNew = controller.CreateNew;
		controller.CreateNew = function (sel)
		{
			base_CreateNew.call(this, sel);
		}

		var base_Edit = controller.Edit;
		controller.Edit = function (sel)
		{
			base_Edit.call(this, sel);
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res = base_GetFormContent.call(this);
			return res;
		}

		return controller;
	}
});
