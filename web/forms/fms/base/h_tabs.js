// утф8
define(function ()
{
	var helper = {
		bindSwitch: function (div, tag, ctag) {
			var size = div.find(tag).length;
			var index, nextIndex = 0;
			$(window).unbind('keyup.tabsChange').on('keyup.tabsChange', function(event){
				if ($("input:focus").size()) return false;
				index = div.find('.ui-state-active').index();
				var keyCode = (event.keyCode ? event.keyCode : event.which);
				if(keyCode != 37 && keyCode != 39)
					return false;
			  	switch (keyCode) {
			    	case 37:
					    nextIndex = index - 1;
			    		break;
			    	case 39:
						nextIndex = index + 1;
			    		break;
			  	}
			  	if (nextIndex < 0)
			  		nextIndex = size - 1;
			  	else if (nextIndex >= size)
					nextIndex = 0;
				div.find(tag).eq(nextIndex).children('a').click();
				$(ctag + '-' + (nextIndex + 1)).find('input').first().focus();
				$(ctag + '-' + (index + 1)).trigger('tabIndexChanged');
			});
		}, 
		unbindSwitch: function () {
			$(window).unbind('keyup.tabsChange');
		}
	};
	return helper;	
});
