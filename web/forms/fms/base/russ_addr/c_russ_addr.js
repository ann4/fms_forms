define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/russ_addr/e_russ_addr.html'
	, 'forms/fms/base/russ_addr_fias/c_fias'
	, 'forms/fms/base/russ_addr_free/c_russ_addr_free'
	, 'forms/fms/base/russ_addr_fias/h_fias'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/russ_addr/h_houseType'
	, 'forms/fms/base/russ_addr/h_flatType'
	, 'forms/fms/base/convert/x_convert'
],
function 
(
	  c_binded
	, tpl
	, c_russ_addr_fias
	, c_russ_addr_free
	, h_fias
	, helper_Select2
	, h_houseType
	, h_flatType
	, x_convert
)
{
	return function ()
	{
		var Select2Options = function (h_values)
		{
			return function (m, field_name) {
				if (m && m[field_name] && !m[field_name].text && m[field_name].id) {
					var item = helper_Select2.FindById(h_values.GetValues(), m[field_name].id)
					if (item)
						m[field_name].text = item.text;
				}
				return {
					placeholder: 'Укажите значение'
					, allowClear: true
					, query: function (q) {
						q.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), q.term) });
					}
				};
			}
}

		var options =
		{
			field_spec:
			{
				'fias': { controller: c_russ_addr_fias }
				, 'free': { controller: c_russ_addr_free }
				, Дом_Type: Select2Options(h_houseType)
				, Квартира_Type: Select2Options(h_flatType)
			}
		};

		var controller = c_binded(tpl, options);

		var free_field_street_spec = { name: 'Улица' };
		var free_field_place_spec = { name: 'НаселенныйПункт', with_SHORTNAME: true };
		var aolevels_to_free_field_spec =
		{
			'1': { name: 'Регион' }
			, '3': { name: 'Район' }
			, '4': { name: 'Город' }
			, '5': { name: 'ГородскойРайон' }
			, '6': free_field_place_spec
			, '7': free_field_street_spec
			, '90': free_field_place_spec
			, '91': free_field_street_spec
		};

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (this.model && this.model.housing_types && (!this.model.Дом_Type || null == this.model.Дом_Type))
				this.model.Дом_Type = { id: '1202', text: 'Дом' };
			if (this.model && this.model.housing_types && (!this.model.Квартира_Type || null == this.model.Квартира_Type))
			    this.model.Квартира_Type = { id: '1303', text: 'Квартира' };

			base_Render.call(this, sel);

			this.UpdateVariant();

			var fias_checkbox = $(sel + ' .variant_fias input');
			var self = this;
			if (!this.model || (!this.model.variant_fias && false != this.model.variant_fias && null != this.model.variant_fias))
			{
				fias_checkbox.attr('checked', 'checked');
			}
			$('#fias_version').val(!this.model || !this.model.fias_version ? '' : this.model.fias_version);
			console.log('fias_version= ' + $('#fias_version').val());
			fias_checkbox.change(function () { self.OnChangeVariant(); })
			this.UpdateVariant();
		}

		controller.OnChangeVariant = function ()
		{
			var sel = this.binding.form_div_selector;
			var is_variant_fias = 'checked' == $(sel + ' .variant_fias input').attr('checked');

			if (!is_variant_fias)
			{
				var fias_path = this.binding.controls.fias.controller.fias_path;
				if (fias_path && 0 < fias_path.length)
				{
					var free_address = x_convert.Encode(fias_path);
					for (var field_name in free_address)
					{
						var field_value = free_address[field_name];
						$(sel + ' div.cpw-fms-russ-addr>div.free input[model_field_name="' + field_name + '"]').val(field_value);
					}
				}
			}

			this.UpdateVariant();
		}

		controller.UpdateVariant = function ()
		{
			var sel = this.binding.form_div_selector;
			var is_variant_fias = 'checked' == $(sel + ' .variant_fias input').attr('checked');
			if (is_variant_fias)
			{
				$(sel + ' div.cpw-fms-russ-addr .variant_fias span').addClass('checked');
				$(sel + ' div.cpw-fms-russ-addr div.free').hide();
				$(sel + ' div.cpw-fms-russ-addr>div.fias').show();
			}
			else
			{
				$(sel + ' div.cpw-fms-russ-addr .variant_fias span').removeClass('checked');
				$(sel + ' div.cpw-fms-russ-addr div.free').show();
				$(sel + ' div.cpw-fms-russ-addr>div.fias').hide();
			}
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res = base_GetFormContent.call(this);
			if (res && null != res)
			{
				if (true == res.variant_fias)
				{
					delete res.free;
				}
				else
				{
					delete res.fias;
				}
			}
			return res;
		}

		return controller;
	}
});
