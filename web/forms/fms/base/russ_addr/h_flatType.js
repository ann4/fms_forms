﻿// утф8
define([
	'txt!forms/fms/guides/dict_addressobjecttype_flat.csv',
	'forms/base/h_csv'
]
, function (dict_flatType, helper_CSV)
{
	var res = {};

	res.GetValues = function ()
	{
	    return helper_CSV.CSVToJSON(dict_flatType, ';');
	}

	return res;
});
