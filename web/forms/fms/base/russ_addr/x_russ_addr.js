define([
	'forms/fms/base/x_fms_base'
	, 'forms/base/log'
	, 'forms/fms/base/russ_addr_fias/x_fias'
	, 'forms/fms/base/russ_addr/h_houseType'
	, 'forms/fms/base/russ_addr/h_flatType'
	, 'forms/fms/base/convert/x_convert'
],
function (
	BaseCodec
	, GetLogger
	, x_fias
	, h_houseType
	, h_flatType
	, x_convert
	)
{
	var log = GetLogger('x_russ_addr');
	return function ()
	{
		var x_russ_addr_codec = BaseCodec();

		var PrepareHousingVariants = function (h_values)
		{
			var variants = {};
			var types = h_values.GetValues();
			for (var i = 0; i < types.length; i++) {
				if (x_russ_addr_codec.AddressWithHousingTypeWithoutRoom && '1304' == types[i].id)
					continue;
				variants[types[i].id] = types[i].text;
			}
			return variants;
		}

		x_russ_addr_codec.base_housing_variants =
		{
			'1202': 'Дом'
			, '1203': 'Корпус'
			, '1303': 'Квартира'
			, '1304': 'Комната'
		};
		x_russ_addr_codec.h_housing_variants = [];
		x_russ_addr_codec.f_housing_variants = [];

		x_russ_addr_codec.HousingVariantsById = function (id)
		{
			if (x_russ_addr_codec.h_housing_variants[id])
				return x_russ_addr_codec.h_housing_variants[id];
			else if (x_russ_addr_codec.f_housing_variants[id])
				return x_russ_addr_codec.f_housing_variants[id];
			else
				return null;
		}

		x_russ_addr_codec.BaseHousingVariantsById = function (id)
		{
			if (x_russ_addr_codec.AddressWithHousingType && x_russ_addr_codec.h_housing_variants[id])
				return 'Дом';
			else if (x_russ_addr_codec.AddressWithHousingType && x_russ_addr_codec.f_housing_variants[id])
				return 'Квартира';
			else
				return x_russ_addr_codec.base_housing_variants[id];
		}

		x_russ_addr_codec.EncodeHousing = function (num_tabs, id, value, type)
		{
			var xml_string = '';
			xml_string += this.FormatTabs(num_tabs) + '<housing>' + this.FormatEOL();
			var next_num_tabs = num_tabs + 1;
			var next_num_tabs2 = next_num_tabs + 1;
			xml_string += this.FormatTabs(next_num_tabs) + '<type>' + this.FormatEOL();
			xml_string += this.StringifyField(next_num_tabs2, "type", 'addressObjectType');
			if (type && '' != type && null != type) {
				xml_string += this.StringifyField(next_num_tabs2, "element", type.id);
				xml_string += this.StringifyField(next_num_tabs2, "value", type.text);
			} else {
				xml_string += this.StringifyField(next_num_tabs2, "element", id);
				xml_string += this.StringifyField(next_num_tabs2, "value", x_russ_addr_codec.base_housing_variants[id]);
			}
			xml_string += this.FormatTabs(next_num_tabs) + '</type>' + this.FormatEOL();
			xml_string += this.SafeStringifyField(next_num_tabs, "value", value);
			xml_string += this.FormatTabs(num_tabs) + '</housing>' + this.FormatEOL();
			return xml_string;
		}

		x_russ_addr_codec.SafeEncodeHousing = function (num_tabs, field_value, field_name, id, txt)
		{
			return !field_value[field_name]
				? '' : this.EncodeHousing(num_tabs, id, field_value[field_name], field_value[field_name + '_Type'], txt);
		}

		x_russ_addr_codec.SafePrepFiasAddr = function ()
		{
			if (!this.x_fias)
				this.x_fias = x_fias();
			this.x_fias.tabs = this.tabs;
		}

		x_russ_addr_codec.m_only_free_mode = false;
		x_russ_addr_codec.only_free_mode= function()
		{
			return this.m_only_free_mode;
		}

		x_russ_addr_codec.EncodeDataItem = function (num_tabs, addr, field_name)
		{
			log.Debug('EncodeDataItem {');
			var xml_string = '';
			xml_string += this.OpenTagLine(num_tabs, field_name);
			var next_num_tabs = num_tabs + 1;

			if (addr.variant_fias && this.only_free_mode())
			{
				addr.free = x_convert.Encode(addr.fias);
				addr.variant_fias = false;
			}

			if (addr.variant_fias)
			{
				x_russ_addr_codec.SafePrepFiasAddr();
				xml_string += x_russ_addr_codec.x_fias.EncodeDataItem(next_num_tabs, addr.fias, 'addressObject');
			}
			else
			{
				var free = addr.free;
				var astring = (!free.Регион ? '' : free.Регион)
					+ '$' + (!free.Район ? '' : free.Район)
					+ '$' + (!free.Город ? '' : free.Город)
					+ '$' + (!free.НаселенныйПункт ? '' : free.НаселенныйПункт)
					+ '$' + (!free.ГородскойРайон ? '' : free.ГородскойРайон)
					+ '$' + (!free.Улица ? '' : free.Улица)
					;
				xml_string += this.StringifyField(next_num_tabs, "addressObjectString", astring);
			}

			var housing_variants = x_russ_addr_codec.base_housing_variants;
			for (var housing_id in housing_variants)
				xml_string += this.SafeEncodeHousing(next_num_tabs, addr, housing_variants[housing_id], housing_id);

			xml_string += this.CloseTagLine(num_tabs, field_name);
			log.Debug('EncodeDataItem }');
			return xml_string;
		}

		x_russ_addr_codec.DecodeXmlElement = function (node, schema)
		{
			log.Debug('DecodeXmlElement {');
			var russianAddress = {};
			var self = this;
			x_russ_addr_codec.h_housing_variants = PrepareHousingVariants(h_houseType);
			x_russ_addr_codec.f_housing_variants = PrepareHousingVariants(h_flatType);
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case 'addressObject':
						{
							russianAddress.variant_fias = true;
							x_russ_addr_codec.SafePrepFiasAddr();
							russianAddress.fias = x_russ_addr_codec.x_fias.DecodeXmlElement(child);
							break;
						}
					case 'addressObjectString':
						{
							russianAddress.variant_fias = false;
							var parts = child.text.split('$');
							russianAddress.free =
							{
								Регион: parts[0]
								, Район: parts[1]
								, Город: parts[2]
								, НаселенныйПункт: parts[3]
								, ГородскойРайон: parts[4]
								, Улица: parts[5]
							}
							break;
						}
					case 'housing':
						{
							var housing = self.DecodeAddressHousing(child);
							var baseVariant = x_russ_addr_codec.BaseHousingVariantsById(housing.id);
							russianAddress[baseVariant] = housing.text;
							var variant = x_russ_addr_codec.HousingVariantsById(housing.id)
							if (x_russ_addr_codec.AddressWithHousingType && null != variant)
								russianAddress[baseVariant + '_Type'] = { id: housing.id, text: variant };
							break;
						}
				}
			});
			log.Debug('DecodeXmlElement }');
			return russianAddress;
		}

		return x_russ_addr_codec;
	}
});