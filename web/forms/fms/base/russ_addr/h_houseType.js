﻿// утф8
define([
	'txt!forms/fms/guides/dict_addressobjecttype_house.csv',
	'forms/base/h_csv'
]
, function (dict_houseTypes, helper_CSV)
{
	var res = {};

	res.GetValues = function ()
	{
	    return helper_CSV.CSVToJSON(dict_houseTypes, ';');
	}

	return res;
});
