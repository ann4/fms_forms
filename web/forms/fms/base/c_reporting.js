define([
	  'forms/base/c_binded'
	, 'forms/base/log'
],
function (BaseFormController, GetLogger)
{
	return function (view_template, form_spec, options)
	{
		var log = GetLogger('c_reporting');
		log.Debug('Create {');
		var form = form_spec;
		var controller = BaseFormController(view_template, options);

		controller.writeInfoToLog = function (text) {
			log.Debug(text);
		}
		controller.writeWarningToLog = function (text) {
			log.Debug(text);
		}
		controller.writeErrorToLog = function (text, error) {
			log.Debug(text);
		}

		controller.parseDate = function (date) {
			if (!date) {
				return null;
			} else if (date.indexOf('T') != -1) {
				return new Date(date);
			} else if (date.indexOf('.') != -1) {
				var parts = date.split('.');
				var year = parts[2];
				var month = parts[1];
				var day = parts[0];
				return new Date(year, month - 1, day, 0, 0, 0);
			} else if (date.indexOf('-') != -1) {
				var parts = date.split('-');
				var year = parts[0];
				var month = parts[1];
				var day = parts[2];
				return new Date(year, month - 1, day, 0, 0, 0);
			}
			return null;
		}

		controller.ProcessingData = function (data) {
			return false;
		};

		controller.BuildXamlView = function (id_form_div) {
			var sparts = this.model.ReportingPeriodFrom.split('.');
			var fparts = this.model.ReportingPeriodTill.split('.');
			var fileName = form.FilePrefix + '_' + sparts[2] + sparts[1] + sparts[0] + '_' + fparts[2] + fparts[1] + fparts[0];
			return {
				Content: null,
				FileName: fileName,
				FileExtension: form.FileExtension,
				Encoding: form.Encoding,
				Description: form.Description
			};
		};

		log.Debug('Create }');
		return controller;
	}
});
