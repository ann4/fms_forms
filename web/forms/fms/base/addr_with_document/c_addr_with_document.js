define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/addr_with_document/e_addr_with_document.html'
	, 'forms/fms/base/addr/c_addr'
],
function 
(
	  c_binded
	, tpl
	, c_addr
)
{
	return function ()
	{
		var options =
		{
			field_spec:
			{
				address: { controller: c_addr }
			}
		};

		var controller = c_binded(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
		}

		var base_CreateNew = controller.CreateNew;
		controller.CreateNew = function (sel)
		{
			base_CreateNew.call(this, sel);
		}

		var base_Edit = controller.Edit;
		controller.Edit = function (sel)
		{
			base_Edit.call(this, sel);
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res = base_GetFormContent.call(this);
			return res;
		}

		return controller;
	}
});
