﻿// утф8
define([
	'txt!forms/fms/guides/dict_country.csv',
	'forms/base/h_csv'
]
, function (dict_country, helper_CSV)
{
	var res = {};

	res.GetValues = function ()
	{
		return helper_CSV.CSVToJSON(dict_country, ';');
	}

	return res;
});
