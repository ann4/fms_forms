define(['forms/fms/base/x_fms_base', 'forms/fms/base/russ_addr_fias/h_fias', 'forms/base/log'],
function (BaseCodec, h_fias, GetLogger)
{
	var log = GetLogger('x_fias');
	return function ()
	{
		var fias_codec = BaseCodec();

		fias_codec.ClearFiasPathFields = function (fias_path)
		{
			var res = [];
			for (var i = 0; i < fias_path.length; i++)
			{
				var data_item = fias_path[i];
				var res_item = { AOLEVEL: data_item.AOLEVEL };
				res_item.text = h_fias.PrepareObjectText(data_item);
				var aolevel_spec = h_fias.aolevel_specs_by_aolevel[data_item.AOLEVEL];
				res_item[aolevel_spec.code_field_name] = data_item[aolevel_spec.code_field_name];
				res_item.IsTerminal =
				data_item.IsTerminal && (true == data_item.IsTerminal || '1' == data_item.IsTerminal)
				? '1' : '0';
				res.push(res_item);
			}
			return res;
		}

		fias_codec.PrepareTextForFiasPath = function (fias_path)
		{
			var text = '';
			for (var i = 0; i < fias_path.length; i++)
			{
				if (0 != i)
					text += '$';
				var fias_path_item = fias_path[i];
				text += fias_path_item.AOLEVEL + '@';
				text += h_fias.PrepareObjectText(fias_path_item) + '@';
				var aolevel_spec = h_fias.aolevel_specs_by_aolevel[fias_path_item.AOLEVEL];
				text += fias_path_item[aolevel_spec.code_field_name];
				if (fias_path_item.IsTerminal && (1 == fias_path_item.IsTerminal))
					text += '@';
			}
			return text;
		}

		fias_codec.DecodeFiasText = function (text)
		{
			log.Debug('DecodeFiasText {');
			var parts = text.split('$');
			var fias_path = [];
			for (var i = 0; i < parts.length; i++)
			{
				var part = parts[i];
				var part_parts = part.split('@');
				if (4 != part_parts.length && 3 != part_parts.length)
				{
					log.Debug('DecodeFiasText }');
					return text;
				}
				var res_item =
				{
					AOLEVEL: part_parts[0]
					, text: part_parts[1]
				};
				var aolevel_spec = h_fias.aolevel_specs_by_aolevel[res_item.AOLEVEL];
				res_item[aolevel_spec.code_field_name] = part_parts[2];
				res_item.IsTerminal = (4 == part_parts.length);
				fias_path.push(res_item);
			}
			log.Debug('DecodeFiasText }');
			return fias_path;
		}

		fias_codec.Decode = function (data)
		{
			if (!data)
			{
				return null;
			}
			else
			{
				log.Debug('Decode {');
				var res = { fias_id: data.id };
				if (data.text)
				{
					var fias = {}
					this.DecodeFiasText(data.text, fias);
					if (fias && null != fias)
					{
						if (fias.path)
							fias.path[0].AOID = data.id;
						res.fias_path = fias.path;
						res.readable_text = fias.readable_text;
					}
				}
				log.Debug('Decode }');
				return res;
			}
		}

		/*

		<element>c87cd460-3790-4516-ab97-79dea2546ff1</element>
		<value>Респ Удмуртская$$г Ижевск$$$ул Лихвинцева</value>

		-----------

		<element>d59d3758-7afd-4480-a68d-07566a0ed149</element>
		<value>Респ Удмуртская$$г Ижевск$$$ул им Вадима Сивкова</value>

		-----------

		<element>f2822bd4-8d41-4922-93a4-15321288beed</element>
		<value>Респ Удмуртская$$г Воткинск$$$ул Пугачева</value>

		-----------

		<element>d4959c2e-fbe4-42ea-a215-d0e3265cac8b</element>
		<value>Респ Удмуртская$$г Ижевск$$$ул им Наговицына</value>

		-----------

		<element>4391ba0c-7391-450d-a516-9c11f7e98247</element>
		<value>г Москва$$г Москва$$$ул Ильинка</value>

		-----------

		<element>bc9b0325-fb1b-4d2a-a012-3456a09637b8</element>
		<value>Респ Удмуртская$$г Ижевск$$$ул Пушкинская</value>

		-----------

		<element>5a8f02f4-6504-4eb2-9cbb-d1d0d12b98a1</element>
		<value>Респ Удмуртская$$г Воткинск$$$ул Республиканская</value>

		-----------

		<element>bbc0d043-fb4c-4161-ad2d-874cccd69080</element>
		<value>Респ Удмуртская$$г Сарапул$$$ул Дружбы</value>

		<element>0969fb75-4dd7-4c32-876f-ea4fa9ae91c8</element>
		<value>г Ленинград$$г Санкт-Петербург$$$ул Энгельса</value>

		<element>86992cfb-1c71-4153-9b94-05cbf7066dda</element>
		<value>Респ Крым$$г Симферополь$$$ул Будённого</value>

		<element>0c1d1a7c-5dae-4417-a0e0-cca857c3b4c6</element>
		<value>обл Липецкая$р-н Елецкий$$п Соколье$$ул Солнечная</value>

		<element>0</element>
		<value>На деревне у дедушки$$$$$</value>

		<element>5AE96F6B-5D32-4B51-A368-D29EF0CB8AC6</element>

		*/

		fias_codec.EncodeDataItem = function (num_tabs, addr, field_name)
		{
			var xml_string = '';
			if (addr && null!=addr)
			{
				xml_string += this.OpenTagLine(num_tabs, field_name);
				var next_num_tabs = num_tabs + 1;

				xml_string += this.StringifyField(next_num_tabs, "type", 'addressObject');

				var value = null;
				var id = null;
				if (!addr.id)
				{
					id = addr[0].AOID;
					value = this.PrepareTextForFiasPath(addr);
				}
				else
				{
					id = addr.id;
					if (addr.old_fias_text)
						value = addr.old_fias_text;
				}

				xml_string += this.StringifyField(next_num_tabs, "element", id);
				if (null != value)
					xml_string += this.StringifyField(next_num_tabs, "value", value);

				xml_string += this.CloseTagLine(num_tabs, field_name);
			}
			return xml_string;
		}

		fias_codec.DecodeXmlElement = function (node, schema)
		{
			log.Debug('DecodeXmlElement {');
			var id = null;
			var text = null;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "element": id = child.text; break;
					case "value": text = child.text; break;
				}
			});
			var res = null;
			if (null == text)
			{
				res = { id: id };
			}
			else
			{
				res = fias_codec.DecodeFiasText(text);
				if ('string' == typeof res)
				{
					res = { id: id, old_fias_text: res };
				}
				else
				{
					res[0].AOID = id;
				}
			}
			log.Debug('DecodeXmlElement }');
			return res;
		}

		return fias_codec;
	}
});