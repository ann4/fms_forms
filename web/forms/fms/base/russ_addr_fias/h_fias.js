define(function ()
{
	var helper = {}

	helper.GetBaseFiasUrlByVersion = function (version) {
		var BaseFiasUrl = null;
		var BaseFiasUrl2 = null;
		//BaseFiasUrl = 'http://trust.dev/fias/fias.php?';
		//BaseFiasUrl = 'https://probili.ru/fias2/fias.php?';
		//BaseFiasUrl = 'https://probili.ru/fias2/fias2.php?';
		if (window.app && true === window.app.UseDirectFias) {
			BaseFiasUrl = 'http://probili.ru/fast_fias/fias.php?';
			//BaseFiasUrl2 = 'http://192.168.0.244/fias/fias.php?';
			//BaseFiasUrl2 = 'http://192.168.0.193/fias/fias.php?';
			BaseFiasUrl2 = 'http://probili.ru/fast_fias/fias.php?';
		}
		else if (window.app && true === window.app.UseRemoteFias) {
			BaseFiasUrl = window.app.RemoteFiasUrl;
			BaseFiasUrl2 = window.app.RemoteFiasUrl2;
		}
		else {
			BaseFiasUrl = '?r=fms/fias&';
			BaseFiasUrl2 = '?r=fms/fias2&';
		}
		//controller.BaseFiasUrl = 'http://server.russianit.ru:81/fias/fias.php?';

		if (version) {
			switch (version) {
				case '22.05.2020': return BaseFiasUrl2;
				default:
					return BaseFiasUrl;
			}
		}
		return BaseFiasUrl;
	}

	helper.aolevel_specs =
	[
	  { aolevel: '1',  code_field_name: 'REGIONCODE', title: "Регион",     orig_title: 'Регион', other_ending: 'ой', placeholder: 'города, района или населённого пункта' }
	, { aolevel: '3',  code_field_name: 'AREACODE',   title: "Район",      orig_title: 'Район', other_ending: 'ой', placeholder: 'города, населённого пункта или дополнительной территории' }
	, { aolevel: '4',  code_field_name: 'CITYCODE',   title: "Город",      orig_title: 'Город', other_ending: 'ой', placeholder: 'улицы или внутригородской территории' }
	, { aolevel: '5',  code_field_name: 'CTARCODE',   title: "Р-н города", orig_title: 'Внутригородская территория', other_ending: 'ую', placeholder: 'улицы или дополнительной территории' }
	, { aolevel: '6',  code_field_name: 'PLACECODE',  title: "Нас. пункт", orig_title: 'Населенный пункт', other_ending: 'ой', placeholder: 'улицы или дополнительной территории' }
	, { aolevel: '65', code_field_name: 'PLANCODE',   title: "П/структ.", orig_title: 'Элемент планировочной структуры', other_ending: 'ой', placeholder: 'улицы или дополнительной территории' }
	, { aolevel: '7',  code_field_name: 'STREETCODE', title: "Улица",      orig_title: 'Улица', other_ending: 'ую', placeholder: 'дополнительной территории' }
	, { aolevel: '90', code_field_name: 'EXTRCODE',   title: "Территория", orig_title: 'Дополнительная территория', other_ending: 'ую', placeholder: 'улицы' }
	, { aolevel: '91', code_field_name: 'SEXTCODE',   title: "Улица",      orig_title: 'Улица на дополнительной территории', other_ending: 'ую', placeholder: '' }
	];

	helper.aolevel_specs_by_aolevel = {};

	for (var i = 0; i < helper.aolevel_specs.length; i++)
	{
		var aolevel_spec = helper.aolevel_specs[i];
		helper.aolevel_specs_by_aolevel[aolevel_spec.aolevel] = aolevel_spec;
	}

	helper.PrepareObjectText = function (fobj)
	{
		return fobj.text ? fobj.text : (fobj.SHORTNAME + ' ' + fobj.FORMALNAME);
	}

	helper.PrepareFiasSelect2OptionItem = function (fobj)
	{
		var res =
		{
			id: fobj.AOID
			, text: helper.PrepareObjectText(fobj)
			, fias_object: fobj
		};
		return res;
	};

	helper.fias_fields =
	[
		'AOID'
		, 'AOGUID'
		, 'AOLEVEL'
		, 'IsTerminal'
		, 'SHORTNAME'
		, 'FORMALNAME'
		, 'FullPath'
		, 'STARTDATE'
		, 'ENDDATE'
		, 'Variants'
		, 'VariantsInfo'
	];

	helper.fias_fields_code =
	[
		'REGIONCODE'
		, 'AREACODE'
		, 'CITYCODE'
		, 'PLACECODE'
		, 'PLANCODE'
		, 'EXTRCODE'
		, 'STREETCODE'
		, 'SEXTCODE'
	];

	helper.PrepareFiasServiceOptions = function (fias_objects, page)
	{
		var options = [];
		for (var i = 0; i < fias_objects.length; i++)
		{
			var fobj = helper.DecodeFiasFieldsArr(fias_objects[i]);
			options.push(helper.PrepareFiasSelect2OptionItem(fobj));
		}
		return { results: options };
	}

	helper.DecodeFiasFieldsArr = function (farr)
	{
		var fobj = {};
		for (var i = 0, i2 = -helper.fias_fields.length; i < farr.length && i2 < helper.fias_fields_code.length; i++, i2++)
		{
			if (14 == farr.length && 4 == i2) // old service..
				i2++;
			var withoutVariantsInfo = 15 == farr.length && (7 <= i && i <= 10);
			if (withoutVariantsInfo && 7 == i)
			{
				i2 += 4;
			}
			if (i < helper.fias_fields.length && !withoutVariantsInfo)
			{
				fobj[helper.fias_fields[i]] = farr[i];
			}
			else
			{
				fobj[helper.fias_fields_code[i2]] = farr[i];
			}
		}
		this.FixFiasObj(fobj);
		return fobj;
	}

	helper.FixFiasObj= function(fobj)
	{
		/* фикс для Респ Крым, г Симферополь, ул Титова */
		/*if ('2d41d337-8765-4a92-bcef-896de6fe78b2' == fobj.AOGUID && '26531b03-66cb-4030-a981-41103d9b8f07' == fobj.AOID)
			fobj.AOID = '777b8df2-9aee-487c-9622-cbcdc18654f1';*/

		/* фикс для Респ Крым, г Алушта, с Приветное, ул Парниковая */
		if ('172c9b1f-06ec-4bfb-9eac-857344dd5d17' == fobj.AOGUID && 'd4ffb276-5027-4fd4-8a69-72be3e082566' == fobj.AOID)
			fobj.AOID = '64bd100a-d96c-4753-b06e-6236ea0eac9d';

		/* фикс для Респ Крым, пос Аромат, ул Ялтинская */
		if ('f93610df-6734-4cc6-ba50-c776933d215c' == fobj.AOGUID && 'd3b1d5ab-a4d0-4498-97cb-716391675fc6' == fobj.AOID)
			fobj.AOID = 'b57f4e66-8f00-4ff4-8301-d928e4ef3695';
	}

	helper.ReFixFiasAOID = function (aoid)
	{
		switch (aoid)
		{
			/* фикс для Респ Крым, г Симферополь, ул Титова */
			/*case '777b8df2-9aee-487c-9622-cbcdc18654f1': return '26531b03-66cb-4030-a981-41103d9b8f07';*/

			/* фикс для Респ Крым, г Алушта, с Приветное, ул Парниковая */
			case '64bd100a-d96c-4753-b06e-6236ea0eac9d': return 'd4ffb276-5027-4fd4-8a69-72be3e082566';

				/* фикс для Респ Крым, пос Аромат, ул Ялтинская */
			case 'b57f4e66-8f00-4ff4-8301-d928e4ef3695': return 'd3b1d5ab-a4d0-4498-97cb-716391675fc6';

			default: return aoid;
		}
	}

	helper.DecodeOldFiasAddressToPrintable = function (txt)
	{
		var res = null;
		var parts = txt.split('$');
		try
		{
			res =
			{
				Регион: parts[0]
				, Район: parts[1]
				, Город: parts[2]
				, НаселенныйПункт: parts[3]
				, ГородскойРайон: parts[4]
				, Улица: parts[5]
			};
		}
		catch (ex)
		{
			res =
			{
				Регион: txt
				, Район: ''
				, Город: ''
				, НаселенныйПункт: ''
				, ГородскойРайон: ''
				, Улица: ''
			};
		}
		return res;
	}

	helper.BuildPlaceHolder = function (fias_path)
	{
		var placeholder = 'региона, города, района или населённого пункта..';
		if (fias_path)
		{
			var max_aolevel = 0;
			for (var i = 0; i < fias_path.length; i++)
			{
				var fobj = fias_path[i];
				var aolevel = parseInt(fobj.AOLEVEL);
				if (aolevel > max_aolevel)
				{
					max_aolevel = aolevel;
					var aolevel_spec = helper.aolevel_specs_by_aolevel[aolevel];
					placeholder = aolevel_spec.placeholder;
				}
			}
		}
		return 'Укажите здесь название ' + placeholder;
	}

	helper.PrepareAddressParts = function (fias_path)
	{
		var res = {};
		for (var i = 0; i < fias_path.length; i++)
		{
			var path_item = fias_path[i];
			switch (path_item.AOLEVEL)
			{
				case '1': res.Регион = path_item.text; break;
				case '3': res.Район = path_item.text; break;
				case '4': res.Город = path_item.text; break;
				case '90':
				case '6': res.НаселенныйПункт = path_item.text; break;
				case '7': res.Улица = path_item.text; break;
			}
		}
		return res;
	}

	helper.GetFiasVersionBySchemaVersion = function (m)
	{
		var res = null;
		if (m && m.subdivision) {
			switch (m.subdivision.id) {
				case '10141': /* 491-000 : ОФМС РОССИИ ПО МАГАДАНСКОЙ ОБЛ. */
				case '142750': /* 910-001 : Управление по вопросам миграции МВД по Республике Крым */
				case '142782': /* 920-005 : Управление по вопросам миграции УМВД России по г. Севастополю */
				case '129400': /* 910-001 : Управление ФМС России по Республике Крым и г. Севастополю */
				case '142804': /* 180-000 : Управление по вопросам миграции МВД по Удмуртской Республике (Респ. Удмуртская)*/
				case '118327':
				case '142067': /* 640-000 : Управление по вопросам миграции ГУ МВД России по Саратовской области (обл. Саратовская)*/
					return '22.05.2020';
				case '3089': /* МИЯКИНСКИЙ РОВД РЕСПУБЛИКИ БАШКОРТОСТАН */
				case '141852': /* 580-101 : Управление по вопросам миграции УМВД России по Пензенской области (обл. Пензенская) */
				case '116162': /* 580-101 : УФМС РОССИИ ПО ПЕНЗЕНСКОЙ ОБЛ. */
				case '7841':
				default:
					return res;
			}
		}
		return res;
	}

	helper.ConvertOldFiasToNew = function (old_addr, fias_version, callback)
	{
		var fias_version_not_null = fias_version && null != fias_version && '' != fias_version;
		var addr_fias_not_null = old_addr.russianAddress && old_addr.russianAddress.fias;
		if (fias_version_not_null && addr_fias_not_null && (!old_addr.russianAddress.fias_version || fias_version != old_addr.russianAddress.fias_version)) {
			helper.SearchOldFiasToNew(old_addr, fias_version, function (res) {
				old_addr.russianAddress.fias = res;
				old_addr.russianAddress.fias_version = fias_version;
				callback(old_addr);
			});
		} else {
			callback(old_addr);
		}
	}

	helper.SearchOldFiasToNew = function (old_addr, fias_version, callback)
	{
		var item = null;
		for (var i = 0; i < old_addr.russianAddress.fias.length; i++) {
			if (null == item || item.AOLEVEL < old_addr.russianAddress.fias[i].AOLEVEL) {
				item = old_addr.russianAddress.fias[i];
			}
		}
		if (null != item) {
			var url = helper.GetBaseFiasUrlByVersion(fias_version) + 'action=PATHNEW&old_variant=' + item.AOID;
			var ajax_params = {
				url: url
				//, dataType: "jsonp" // TODO: разобраться почему не может распарсить ответ, запрос заканчивается со статусом parseerror, аналогичный запрос в c_fias.js выполняется без ошибок
				, dataType: (!window.app || true !== window.app.UseDirectFias) ? "text" : "jsonp"
				, cache: false
				, error: function (data, textStatus) {
					var txt = 'Не удалось получить данные об адресном объекте ФИАС. Запрос "'
						+ url + '" завершён со статусом ' + textStatus;
					alert(txt);
				}
				, success: function (data, textStatus) {
					var data_object = "string" != typeof data ? data : JSON.parse(data);
					var fias_path = [];
					for (var i = 0; i < data_object.length; i++) {
						fias_path.push(helper.DecodeFiasFieldsArr(data_object[i]));
					}
					callback(fias_path);
				}
			};
			if (!window.app || true !== window.app.UseDirectFias)
				ajax_params.jsonpCallback = 'jsonpCallback';
			$.ajax(ajax_params);
		}
	}

	return helper;
});
