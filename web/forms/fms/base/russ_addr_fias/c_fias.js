define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/base/russ_addr_fias/e_fias.html'
	, 'tpl!forms/fms/base/russ_addr_fias/variants.html'
	, 'forms/fms/base/russ_addr_fias/h_fias'
	, 'forms/fms/base/russ_addr_fias/x_fias'
	, 'forms/base/h_msgbox'
],
function 
(
	  c_binded
	, tpl
	, variants_tpl
	, h_fias
	, x_fias
	, h_msgbox
)
{
	return function ()
	{
		var controller = c_binded(tpl, { foreign_data: { aolevel_specs: h_fias.aolevel_specs } });

		var getBaseFiasUrlByVersion = function () {
			var version = $('#fias_version').val();
			return h_fias.GetBaseFiasUrlByVersion(version);
		}

		var getDesiredValue = function ()
		{
			var desiredValue = $("#cpw-fms-fias-addr-select-select2").data("select2").search.val();
			var parts = desiredValue.split(',');
			var parts1 = [];
			for (var i = 0; i < parts.length; i++)
			{
				Array.prototype.push.apply(parts1, parts[i].split(' '));
			}
			var results = '';
			for (var i = 0; i < parts1.length; i++)
			{
				var part_value = parts1[i].trim();
				if (-1 < results.indexOf(part_value))
				{
					continue;
				}
				if (results != '')
				{
					results += '$';
				}
				results += part_value;
			}
			return results.split('$');
		}

		var prepItemValue = function (text, desiredValues)
		{
			if (!desiredValues || '' == desiredValues)
				return text;
			for (var i = 0; i < desiredValues.length; i++)
			{
				var desiredValue = desiredValues[i];
				var start = 0;
				var end = 0;
				do {
					start = text.toLowerCase().indexOf(desiredValue, start);
					end = start + desiredValue.length;
					if (start !== -1) {
						text = text.substr(0, start) + '<strong>' + text.substr(start, desiredValue.length) + '</strong>' + text.substr(end);
						start = start + desiredValue.length + 17;
					}
				} while (-1 < start);
			}
			return text;
		}

		var formatFiasObjectItemOption = function (item)
		{
			var desiredValue = getDesiredValue();
			var text = item.text;
			if (!item.fias_object) {
				return prepItemValue(text, desiredValue);
			}
			else {
				var fias_object = item.fias_object;
				if (!fias_object.FullPath || '' == fias_object.FullPath || null == fias_object.FullPath) {
					var result = '<div class="cpw-fias-obj-option"><span>' + prepItemValue(text, desiredValue) + '</span>';
				}
				else {
					var path_items = fias_object.FullPath.split('$');
					for (var i = 0; i < path_items.length; i++) {
						path_items[i] = path_items[i].replace(' ', '&nbsp;');
					}
					var addr_txt = path_items.join(', ');
					result = '<div class="cpw-fias-obj-option"><span>' + prepItemValue(text, desiredValue) + ' </span>';
					result += '<div>(';
					result += prepItemValue(addr_txt, desiredValue);
					result += ')</div>';
				}

				var variants_txt = '';
				var re = new RegExp(/[^0-9]/);
				if (null != fias_object.Variants && '' != fias_object.Variants && re.test(fias_object.Variants)) {
					var variants_items = fias_object.Variants.split('&');
					var variants_item_html = [];
					for (var i = 0; i < variants_items.length; i++) {
						var duplicate = variants_item_html.length == 0 ? [] :
							variants_item_html.filter(function (element, index, array) { return element == variants_items[i]; });
						if (text != variants_items[i] && duplicate.length == 0) {
							variants_txt += variants_txt == '' ? '' : ', ';
							variants_txt += '<span>' + variants_items[i] + '</span>';
							variants_item_html.push(variants_items[i]);
						}
					}
				}

				if (null != variants_txt && '' != variants_txt) {
					result += '<br><div class="fias-obj-old-text">др.варианты:&nbsp;';
					result += prepItemValue(variants_txt, desiredValue);
					result += '</div>';
				}
				result += '</div>';

				return result;
			}
		}

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			var ajax_params= {
				url: function () { return self.SearchFiasUrl ? self.SearchFiasUrl : getBaseFiasUrlByVersion(); }
				, type: 'GET'
				, dataType: 'jsonp'
				, quietMillis: 100
				, cache: true
				, data: function (term, page) { return { term: term }; }
				, results: h_fias.PrepareFiasServiceOptions
			};
			if (!window.app || true !== window.app.UseDirectFias)
				ajax_params.jsonpCallback = 'jsonpCallback';
			self.Select2Item().select2
			({
				placeholder: h_fias.BuildPlaceHolder(this.fias_path)
				, minimumInputLength: 0
				, formatResult: formatFiasObjectItemOption
				, ajax: ajax_params
				
			})
			.on('change', function () { self.OnChangeFiasSelect2(); })
			;
			$(sel + ' div.aolevel .reselect a').click(function (e)
			{
				e.preventDefault();
				var aolevel = $(this).attr('aolevel');
				self.OnReselect(aolevel);
			});

			if (this.fias_path)
			{
				this.UpdateByFiasPath()
			}
			else if (this.fias_id)
			{
				$(sel + '>div.cpw-fms-fias-addr-select>div.select').hide();
				$(sel + '>div.cpw-fms-fias-addr-select>div.text-presentation-while-build-fias-path-by-id')
					.text(this.readable_text ? this.readable_text : ('идентифицируемый "' + this.fias_id + '"')).show();
				$(sel + '>div.cpw-fms-fias-addr-select>div.text-veil-while-ajax-request')
					.text('запрашиваем полную справку на объект "' + this.fias_id + '" ..').show();
				this.BuilPathById(this.fias_id);
			}
		}

		controller.OnReselect = function (aolevel)
		{
			var fias_path = this.fias_path;
			var new_fias_path = [];
			for (var i = 0; i < fias_path.length; i++)
			{
				var fobj = fias_path[i];
				if (fobj.AOLEVEL < aolevel)
					new_fias_path.push(fobj);
			}
			this.fias_path = new_fias_path;
			this.UpdateByFiasPath();
		}

		controller.Select2Item = function ()
		{
			return $(this.binding.form_div_selector +
				' div.cpw-fms-fias-addr-select>div.select>div#cpw-fms-fias-addr-select-select2');
		}

		controller.OnChangeFiasSelect2 = function (field_name)
		{
			var selected_option = this.Select2Item().select2('data');
			if (selected_option && selected_option.fias_object) {
				var re = new RegExp(/[^0-9]/);
				if (!selected_option.fias_object.Variants || '' == selected_option.fias_object.Variants || !re.test(selected_option.fias_object.Variants)) {
					this.BuilPathById(selected_option.fias_object.AOID);
				} else {
					this.BuilPathById(selected_option.fias_object.AOID);
					this.SelectVarianNameFiasItem(selected_option.fias_object);
				}
			}
		}

		controller.SelectVarianNameFiasItem = function (fias_object)
		{
			var fias_object_variants = {
				addr_txt: '',
				actual_txt: '',
				variants_items: []
			};
			var path_items = fias_object.FullPath.split('$');
			for (var i = 0; i < path_items.length; i++)
				path_items[i] = path_items[i].replace(' ', '&nbsp;');
			fias_object_variants.addr_txt = path_items.join(', ');
			fias_object_variants.actual_txt = fias_object.SHORTNAME + ' ' + fias_object.FORMALNAME;

			if (null != fias_object.VariantsInfo && '' != fias_object.VariantsInfo) {
				var variants_items = JSON.parse(fias_object.VariantsInfo);
				var isActual = false;
				var actual_item = variants_items.filter(function (element, index, array) { return element.actstatus == 1; });
				if (actual_item.length != 0 && actual_item[0].text != fias_object_variants.actual_txt) {
					actual_item[0].startdate = new Date(actual_item[0].startdate);
					actual_item[0].enddate = new Date(actual_item[0].enddate);
					fias_object_variants.variants_items.push(actual_item[0]);
				}
				fias_object_variants.variants_items.push({
					"id": fias_object.AOID,
					"text": fias_object_variants.actual_txt,
					"startdate": new Date(fias_object.STARTDATE.split(' ')[0]),
					"enddate": new Date(fias_object.ENDDATE.split(' ')[0]),
					"actstatus": actual_item.length == 0
				});

				for (var i = 0; i < variants_items.length; i++) {
					var variants_item = variants_items[i];
					var duplicate = fias_object_variants.variants_items.length == 0 ? [] :
						fias_object_variants.variants_items.filter(function (element, index, array) { return element.text == variants_item.text; });
					if (duplicate.length == 0) {
						variants_item.startdate = new Date(variants_item.startdate);
						variants_item.enddate = new Date(variants_item.enddate);
						fias_object_variants.variants_items.push(variants_item);
					}
				}
			}

			if (0 == fias_object_variants.variants_items.length ||
				(1 == fias_object_variants.variants_items.length && fias_object_variants.actual_txt == fias_object_variants.variants_items[0].text)) {
				return;
			}
			var html_txt = variants_tpl({ content: fias_object_variants });
			h_msgbox.ShowModal({
				boxid: "cpw-fias-msgbox"
				, title: fias_object_variants.addr_txt + ', ' + fias_object_variants.actual_txt
				, html: html_txt
				, width: 800
			});
		}

		controller.BuilPathById = function (aoid)
		{
			aoid = h_fias.ReFixFiasAOID(aoid);
			var self = this;
			var sel = this.binding.form_div_selector;
			var url = getBaseFiasUrlByVersion() + 'action=PATH&AOID=' + aoid;
			$(sel + '>div.cpw-fms-fias-addr-select>div.text-veil-while-ajax-request')
				.text('запрашиваем полную справку на объект "' + aoid + '" ..').show();
			var ajax_params = {
				url: url
				, dataType: "jsonp"
				, cache: false
				, error: function (data, textStatus)
				{
					var txt = 'Не удалось получить данные об адресном объекте ФИАС. Запрос "'
						+ url + '" завершён со статусом ' + textStatus;
					$(sel + '>div.cpw-fms-fias-addr-select>div.text-veil-while-ajax-request').hide();
					$(sel + '>div.cpw-fms-fias-addr-select>div.text-on-error-ajax-request').text(txt).show();
					$(sel + '>div.cpw-fms-fias-addr-select>div.select').show();
				}
				, success: function (data, textStatus)
				{
					var fias_path = [];
					for (var i = 0; i < data.length; i++)
					{
						fias_path.push(h_fias.DecodeFiasFieldsArr(data[i]));
					}
					self.fias_path = fias_path;
					self.UpdateByFiasPath();
				}
			};
			if (!window.app || true !== window.app.UseDirectFias)
				ajax_params.jsonpCallback = 'jsonpCallback';
			$.ajax(ajax_params);
		}

		controller.UpdateByFiasPath = function ()
		{
			var fias_path = this.fias_path;
			var sel = this.binding.form_div_selector;
			var filled_levels = {};
			var new_search_url = getBaseFiasUrlByVersion() + 'action=SEARCH'
			var IsTerminal = false;
			var max_aolevel = 0;
			for (var i = 0; i < fias_path.length; i++)
			{
				var fobj = fias_path[i];
				var aolevel = parseInt(fobj.AOLEVEL);
				filled_levels[aolevel] = true;
				var aolevel_div_sel = sel + ' div.cpw-fms-fias-addr-select div.aolevel_num' + aolevel;
				$(aolevel_div_sel).show();
				//$(aolevel_div_sel + ' .value').text(h_fias.PrepareObjectText(fobj));
				$(aolevel_div_sel + ' .value input').val(h_fias.PrepareObjectText(fobj));
				var aolevel_spec = h_fias.aolevel_specs_by_aolevel[aolevel];
				new_search_url += '&' + aolevel_spec.code_field_name + '=' + fobj[aolevel_spec.code_field_name];
				IsTerminal = IsTerminal || (0 != fobj.IsTerminal);
				if (aolevel > max_aolevel)
					max_aolevel = aolevel;
			}
			this.SearchFiasUrl = new_search_url + '&min_aolevel=' + max_aolevel;
			for (var i = 0; i < h_fias.aolevel_specs.length; i++)
			{
				var aolevel = h_fias.aolevel_specs[i].aolevel;
				if (!filled_levels[aolevel])
				{
					var aolevel_div_sel = sel + ' div.cpw-fms-fias-addr-select div.aolevel_num' + aolevel;
					$(aolevel_div_sel).hide();
				}
			}
			var select2_item = this.Select2Item();
			select2_item.attr("data-placeholder", h_fias.BuildPlaceHolder(fias_path));
			select2_item.select2('data', null);
			var select2_div = $(sel + ' div.cpw-fms-fias-addr-select div.select');
			if (IsTerminal)
			{
				select2_div.hide();
			}
			else
			{
				select2_div.show();
			}
			$(sel + '>div.cpw-fms-fias-addr-select>div.text-on-error-ajax-request').hide();
			$(sel + '>div.cpw-fms-fias-addr-select>div.text-presentation-while-build-fias-path-by-id').hide();
			$(sel + '>div.cpw-fms-fias-addr-select>div.text-veil-while-ajax-request').hide();
		}

		controller.GetFormContent = function ()
		{
			if (this.fias_path && 0 < this.fias_path.length)
			{
				for (var i = 0; i < this.fias_path.length; i++)
				{
					delete this.fias_path[i].Variants;
					delete this.fias_path[i].VariantsInfo;
					delete this.fias_path[i].STARTDATE;
					delete this.fias_path[i].ENDDATE;
				}
			}
			return this.fias_path;
		}

		controller.SetFormContent = function (content)
		{
			var obj = ("string" != typeof content) ? content : JSON.parse(content);
			if (!obj.id)
			{
				this.fias_path = obj;
			}
			else
			{
				this.fias_id = obj.id;
				this.readable_text = obj.text;
			}
		}

		return controller;
	}
});
