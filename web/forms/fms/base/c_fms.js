define([
	  'forms/base/c_binded'
	, 'forms/base/log'
],
function 
(
	  BaseFormController
	, GetLogger
	)
{
	return function (view_template,options)
	{
		var log = GetLogger('c_fms');
		log.Debug('Create {');
		var controller = BaseFormController(view_template,options);

		controller.Sign = function ()
		{
			var content = this.GetFormContent();
			if (null != content)
			{
				var wax = CPW_Singleton();
				if (wax)
				{
					var pos = this.content.PositionForSignature;
					if (null == pos)
					{
						this.SignedContent = content;
					}
					else
					{
						var signature = wax.SignXml(content);
						this.SignedContent = content.substring(0, pos)
							+ signature
							+ content.substring(pos, content.length);
					}
				}
			}
		}

		controller.DataSaveSex = function (html_element_spec_m, html_element_spec_f)
		{
			if ($(html_element_spec_m).is(':checked'))
			{
				return { id: 'M', text: 'Мужской' };
			}
			else if ($(html_element_spec_f).is(':checked'))
			{
				return { id: 'F', text: 'Женский' };
			}
		}

		controller.DataLoadSex = function (html_element_spec_m, html_element_spec_f, sex)
		{
			if (('M' == sex.id || 'М' == sex.id))
			{
				$(html_element_spec_m).attr('checked', 'checked');
			}
			else if ('F' == sex.id || 'Ж' == sex.id)
			{
				$(html_element_spec_f).attr('checked', 'checked');
			}
		}

		controller.SafeFixUids= function(content,txt_creation,field_names)
		{
			for (var i= 0; i<field_names.length; i++)
			{
				var field_name= field_names[i];
				if (!content[field_name] || null == content[field_name])
					content[field_name] = content.supplierInfo + '-' + txt_creation + '-' + i;
			}
		};

		controller.GetDocumentTypeValues = function(withDuplicate) { return []; };

		log.Debug('Create }');
		return controller;
	}
});
