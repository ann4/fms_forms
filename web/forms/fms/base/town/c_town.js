define([
	  'forms/base/c_modal'
	, 'forms/fms/base/h_Select2'
	, 'tpl!forms/fms/base/town/e_town.html'
	, 'forms/fms/base/town/h_town'
	, 'forms/base/h_names'
	, 'forms/base/log'
	, 'forms/fms/base/h_country'
	, 'forms/fms/base/h_republicSun'
],
function (controller_base, helper_Select2, address_town_tpl, helper_town, helper_Names, GetLogger, h_country, h_republicSun)
{
	var log = GetLogger('c_town');
	return function (model, OnAfterSave)
	{
		var controller = controller_base(model);

		controller.width = 750;
		controller.height = 300;
		controller.title = 'Место рождения';

		controller.template = address_town_tpl;
		controller.OnAfterSave = OnAfterSave;

		var checkNull = function (param)
		{
			return param ? param.id : '';
		}

		controller.Select2ClearValues = function (input_id)
		{
			var clear = 0;
			var input_collection = $('.cpw_form_modal_Address').find('input');
			input_collection.each(function () {
				var el = $(this);
				if(el.attr('id') != undefined) {
					if (clear == 1) {
						if (el.attr('tabindex') == -1)
							el.select2('data', '');
						else
							$(this).val('')
					}
					if (el.attr('id') == input_id) clear = 1;
				}
			});
		}

		controller.InitializeControls = function ()
		{
			controller.InitializeFields();
			controller.InitializeEvents();
		}

		controller.InitializeFields = function ()
		{
			// Переделать на код ОКСМ ALPHA3
			$('#cpw_form_modal_Country').select2({
				placeholder: '',
				allowClear: true,
				query: function (query)
				{	// query.term..
					query.callback({ results: helper_Select2.FindForSelect(h_country.GetValues(), query.term) });
				}
			}).on("select2-selecting", function(e) {
				if (!(h_republicSun.IsSunRepublic(e.val) && 'SUN' == $(this).val()) &&
					!(h_republicSun.IsSunRepublic($(this).val()) && 'SUN' == e.val)) {
					controller.Select2ClearValues($(this).attr('id'));
				}
			});
			$('.encoding_date').change(helper_Names.OnNameChange_NormalizeEncoding);
		}

		controller.InitializeEvents = function ()
		{
			$('#cpw_form_modal_Region').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			$('#cpw_form_modal_Area').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			$('#cpw_form_modal_Town').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			$('#cpw_form_modal_City').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			$('#cpw_form_modal_City').focus();
		}

		controller.DestroyControls = function ()
		{
			controller.DestroySelect2('#cpw_form_modal_Country');
			controller.DestroySelect2('#cpw_form_modal_Region');
			controller.DestroySelect2('#cpw_form_modal_Area');
			controller.DestroySelect2('#cpw_form_modal_Town');
			controller.DestroySelect2('#cpw_form_modal_City');
			controller.DestroySelect2('#cpw_form_modal_Street');
			controller.DestroySelect2('#cpw_form_modal_Variants');
		}

		controller.DataSaveFromControls = function ()
		{
			log.Debug('DataSaveFromControls {');
			controller.model.Country = $('#cpw_form_modal_Country').select2('data');
			controller.model.Region = $('#cpw_form_modal_Region').val();
			controller.model.Area = $('#cpw_form_modal_Area').val();
			controller.model.City = $('#cpw_form_modal_City').val();
			controller.model.Town = $('#cpw_form_modal_Town').val();
			controller.model.text = helper_town.ToReadableStringBirthplace(this.model);
			log.Debug('DataSaveFromControls }');
		}

		controller.DataLoadToControls = function ()
		{
			$('#cpw_form_modal_Country').select2('data', (!controller.model || !controller.model.Country) ? {id: 'RUS', text: 'Россия'} : controller.model.Country);
			$('#cpw_form_modal_Region').val((!controller.model || !controller.model.Region) ? null : controller.model.Region);
			$('#cpw_form_modal_Area').val((!controller.model || !controller.model.Area) ? null : controller.model.Area);
			$('#cpw_form_modal_City').val((!controller.model || !controller.model.City) ? null : controller.model.City);
			$('#cpw_form_modal_Town').val((!controller.model || !controller.model.Town) ? null : controller.model.Town);
		}

		return controller;
	}
});
