define([
		'forms/base/c_modal', 'tpl!forms/fms/base/f_upload/v_upload.html', 'forms/fms/base/f_upload/h_upload', 'forms/fms/base/f_upload/m_upload'
	],
	function(controller_base, upload_tpl, helper_upload, model_upload) {
		return function(model, title, OnAfterSave, divContent, id_form_div) {
			var controller = controller_base(model);
			controller.width = 950;
			controller.height = 470;
			controller.title = 'Загрузка формы xml';

			controller.template = upload_tpl;
			controller.OnAfterSave = OnAfterSave;

			controller.InitializeControls = function() {
				controller.InitializeFields();
				controller.InitializeEvents();
			}

			controller.InitializeFields = function() {
				
			}

			controller.InitializeEvents = function() {
				$('#migration-staying-xml').change(function() {
					processFiles(this.files);
				});
			}

			controller.ShowModal = function (e)
			{
				e.preventDefault();
				var dlg_div = controller.dlg_div = $('<div id="cpw-modal-form"></div>')
				dlg_div.appendTo('body');
				dlg_div.html(controller.template("sdlfkj"));
				controller.isOpen = true;
				dlg_div.dialog
				({
					title: controller.title,
					width: controller.width,
					height: controller.height,
					modal: true,
					buttons:
					{
						'Отменить': function ()
						{
							dlg_div.dialog('close');
						},
						'Загрузить данные': function ()
						{
							controller.SaveAndClose();
						}
					},
					close: function ()
					{
						controller.isOpen = false;
						controller.DestroyControls();
						dlg_div.empty();
						e.target.focus();
						dlg_div.dialog('destroy').remove();
					}
				});

				controller.InitializeControls();
				controller.DataLoadToControls();
			}

			var processFiles = function (files) {
			    var file = files[0];
			    var reader = new FileReader();
			    reader.onload = function (e) {
			    	if(divContent)
				    	$(divContent).text(e.target.result);
				};
				reader.readAsText(file);
			}

			controller.DestroyControls = function() {

			}

			controller.DataSaveFromControls = function() {
				controller.model.formContent = $(divContent).text();
			}

			controller.DataLoadToControls = function() {
				$(divContent).text(controller.model.formContent);
			}

			return controller;
		}
	});