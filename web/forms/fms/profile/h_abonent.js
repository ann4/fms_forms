﻿define([
	  'forms/fms/guides/dict_officialorgan_fns.csv'
	, 'forms/fms/guides/dict_officialorgan_fms.csv'
	, 'forms/base/h_dictionary'
	, 'forms/fms/profile/colaborator/h_colaborator'
	, 'forms/fms/base/addr/h_addr'
],
function (fns, fms, h_dictionary, h_colaborator, h_addr)
{
	var res =
	{
		MoveFields: function (from, to, field_names)
		{
			for (var i = 0; i < field_names.length; i++)
			{
				var field_name = field_names[i];
				if (from[field_name])
				{
					to[field_name] = from[field_name];
					delete from[field_name];
				}
			}
		}

		, FixAddressesOfPlacementId: function (res)
		{
			if (res.AddressesOfPlacement)
			{
				for (var i = 0; i < res.AddressesOfPlacement.length; i++)
				{
					var old_addr = res.AddressesOfPlacement[i];
					var addr = h_addr.ConvertFromOldFormat(old_addr);
					if (addr.russianAddress) {
						addr.russianAddress.housing_types = true;
						if (!addr.russianAddress.Дом_Type)
							addr.russianAddress.Дом_Type = { id: '1202', text: 'Дом' };
						if (!addr.russianAddress.Квартира_Type)
							addr.russianAddress.Квартира_Type = { id: '1303', text: 'Квартира' };
					}
					if (!addr.id)
						addr.id = (old_addr.id) ? old_addr.id : '' + i;
					addr.text = h_addr.PrepareReadableText(addr);
					res.AddressesOfPlacement[i] = addr;
				}
				if (!res.CurrentAddressOfPlacement && res.AddressesOfPlacement.length > 0)
				{
					var first_addr = res.AddressesOfPlacement[0];
					res.CurrentAddressOfPlacement = { id: first_addr.id, text: first_addr.text };
				}
			}
		}

		, ResponsibleOfficiersId: function (res)
		{
			if (res.ResponsibleOfficiers)
			{
				for (var i = 0; i < res.ResponsibleOfficiers.length; i++)
				{
					var col = res.ResponsibleOfficiers[i];
					if (!col.id)
						col.id = '' + i;
					if (!col.text)
						h_colaborator.UpdateText(col);
					if (!col.text)
						col.text = col.id;
				}
				if (!res.CurrentColaborator && res.ResponsibleOfficiers.length > 0)
				{
					var first_col = res.ResponsibleOfficiers[0];
					res.CurrentColaborator = { id: first_col.id, text: first_col.text };
				}
			}
		}

		, SafeUpgradeAbonent: function (m_fms_abonent)
		{
			if (!m_fms_abonent.ResponsibleOfficiers)
			{
				m_fms_abonent.ResponsibleOfficiers = [{}];
				var ro = m_fms_abonent.ResponsibleOfficiers[0];
				this.MoveFields(m_fms_abonent, ro, [
					'LastName', 'FirstName', 'MiddleName', 'Birthday', 'Birthplace', 'Phone', 'Nationality', 
					'AddressLegal', 'user_document_serie', 'user_document_number', 'user_document_dateFrom']);
				if ('' === m_fms_abonent.Male) { ro.Sex = 'M'; }
				else if ('' === m_fms_abonent.Female) { ro.Sex = 'F'; }
				delete m_fms_abonent.Male;
				delete m_fms_abonent.Female;
			}
			if (m_fms_abonent.AddressReal)
			{
				m_fms_abonent.AddressesOfPlacement = m_fms_abonent.AddressReal;
				delete m_fms_abonent.AddressReal;
			}
			this.FixAddressesOfPlacementId(m_fms_abonent);
			this.ResponsibleOfficiersId(m_fms_abonent);
			this.SafeFix_subdivision_sono(m_fms_abonent);
			this.SafeFix_ResponsibleOfficiers_AddressLegal(m_fms_abonent);
			m_fms_abonent.AddressRealAbonent = h_addr.ConvertFromOldFormat(m_fms_abonent.AddressRealAbonent);
			return m_fms_abonent;
		}

		, SafeFix_ResponsibleOfficiers_AddressLegal: function (m_fms_abonent)
		{
			if (m_fms_abonent.ResponsibleOfficiers)
			{
				for (var i = 0; i < m_fms_abonent.ResponsibleOfficiers.length; i++)
				{
					var ro = m_fms_abonent.ResponsibleOfficiers[i];
					ro.AddressLegal = h_addr.ConvertFromOldFormat(ro.AddressLegal);
				}
			}
		}

		, SafeFix_subdivision_sono: function (m_fms_abonent)
		{
			if (m_fms_abonent.subdivision)
			{
				var subdivision = m_fms_abonent.subdivision;
				if (subdivision.id && (!subdivision.text || !subdivision.row))
					m_fms_abonent.subdivision = h_dictionary.FindFirstByField(fms, 'ID', subdivision.id);
			}
			if (m_fms_abonent.sono)
			{
				var sono = m_fms_abonent.sono;
				if (sono.id && (!sono.text || !sono.row))
					m_fms_abonent.sono = h_dictionary.FindFirstByField(fns, 'ID', sono.id);
			}
		}

		, SafeGet_dict_field: function (m_fms_abonent, field_name, field_value)
		{
			if (!m_fms_abonent[field_name] || !m_fms_abonent[field_name].row)
			{
				return field_value;
			}
			else
			{
				var row = m_fms_abonent[field_name].row;
				var res = {}
				if (row.ID)
					res.id = row.ID;
				if (row.NAME)
					res.text = row.NAME;
				return res;
			}
		}

		, clone: function (obj)
		{
			if (obj === null || typeof (obj) !== 'object' || 'isActiveClone' in obj)
				return obj;

			var temp = obj.constructor(); // changed

			for (var key in obj)
			{
				if (Object.prototype.hasOwnProperty.call(obj, key))
				{
					obj['isActiveClone'] = null;
					temp[key] = this.clone(obj[key]);
					delete obj['isActiveClone'];
				}
			}
			return temp;
		}

		, SafeGetDefaultAddressesOfPlacement: function (m_fms_abonent)
		{
			if (m_fms_abonent.AddressesOfPlacement && m_fms_abonent.AddressesOfPlacement.length > 0)
			{
				if (m_fms_abonent.CurrentAddressOfPlacement && m_fms_abonent.CurrentAddressOfPlacement.id)
				{
					for (var i = 0; i < m_fms_abonent.AddressesOfPlacement.length; i++)
					{
						var addr = m_fms_abonent.AddressesOfPlacement[i];
						if (m_fms_abonent.CurrentAddressOfPlacement.id == addr.id)
							return this.clone(addr);
					}
				}
				return this.clone(m_fms_abonent.AddressesOfPlacement[0]);
			}
			return null;
		}

		, SafeGetDefaultColaborator: function (m_fms_abonent)
		{
			if (m_fms_abonent.ResponsibleOfficiers && m_fms_abonent.ResponsibleOfficiers.length > 0)
			{
				if (m_fms_abonent.CurrentColaborator && m_fms_abonent.CurrentColaborator.id)
				{
					for (var i = 0; i < m_fms_abonent.ResponsibleOfficiers.length; i++)
					{
						var col = m_fms_abonent.ResponsibleOfficiers[i];
						if (m_fms_abonent.CurrentColaborator.id == col.id)
							return col;
					}
				}
				return m_fms_abonent.ResponsibleOfficiers[0];
			}
			return null;
		}

		,CheckResponsibleOfficiers: function (profile)
		{
			var token = profile && profile.AuthInfo && profile.AuthInfo.token ? profile.AuthInfo.token : null;
			var fmsProfile = profile && profile.Extensions && profile.Extensions.fms ? profile.Extensions.fms : null;
			if (fmsProfile && fmsProfile.Abonent && fmsProfile.Abonent.CheckResponsibleOfficier)
			{
				var needSelectResponsibleOfficier = false;
				var pAbonent = fmsProfile.Abonent;
				if (pAbonent.ResponsibleOfficiers && pAbonent.ResponsibleOfficiers.length > 0)
				{
					var currentResponsibleOfficier = fmsProfile.CurrentResponsibleOfficier;
					if (!currentResponsibleOfficier)
					{
						needSelectResponsibleOfficier = true;
					}
					else if (!pAbonent.CurrentColaborator || !pAbonent.CurrentColaborator.id)
					{
						needSelectResponsibleOfficier = true;
					}
					else
					{
						needSelectResponsibleOfficier = !(token == currentResponsibleOfficier.token &&
							pAbonent.CurrentColaborator.id == currentResponsibleOfficier.user.id &&
							pAbonent.CurrentColaborator.text == currentResponsibleOfficier.user.text);
					}
				}
				if (needSelectResponsibleOfficier)
				{
					var textMessage = '';
					for(var i = 0; i < pAbonent.ResponsibleOfficiers.length; i++)
					{
						var responsibleOfficier = pAbonent.ResponsibleOfficiers[i];
						textMessage += '<div style="margin-top:10px; margin-left:30px;"><label>';
						textMessage += '<input type="radio" name="responsible-officier" value="';
						textMessage += responsibleOfficier.id;
						textMessage += '" /><span>';
						textMessage += responsibleOfficier.text;
						textMessage += '</span>';
						textMessage += '</label></div>';
					}
					throw {parseException:true, type:'currentColaboratorException', readablemsg:'Выберите сотрудника, осуществляющего учет', message:textMessage};
				}
			}
		}
	};
	return res;
});
