define([
	  'forms/base/c_modal'
	, 'tpl!forms/fms/profile/colaborator/e_colaborator.html'
	, 'forms/base/log'
	, 'forms/base/b_object'
	, 'forms/fms/profile/colaborator/h_colaborator'
	, 'forms/fms/base/town/c_town'
	, 'forms/fms/base/h_validate_n'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/town/m_town'
	, 'forms/fms/base/addr/c_addr_modal'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/base/h_names'
	, 'forms/base/h_times'
	, 'forms/base/h_dictionary'
	, 'forms/fms/guides/dict_officialorgan_fms.csv'
	, 'forms/fms/base/russ_addr_fias/h_fias'
],
function (controller_base
	, colaborator_tpl
	, GetLogger
	, CreateBinding
	, h_colaborator
	, controller_Town
	, helper_validate
	, helper_DocumentType
	, h_citizenship
	, helper_Select2
	, TownAddress
	, c_addr_modal
	, h_addr
	, h_names
	, h_times
	, h_dictionary
	, fms
	, h_fias
	)
{
	var log = GetLogger('c_town');
	return function (model, title, OnAfterSave)
	{
		var controller = controller_base(model);

		controller.width = 970;
		controller.height = 620;
		controller.title = title;

		controller.template = colaborator_tpl;
		controller.OnAfterSave = OnAfterSave;

		controller.InitializeControls = function ()
		{
			$('.input-datepicker').datepicker(h_times.DatePickerOptionsPrevious);
		}

		controller.DestroyControls = function () { }

		var AddressTextFunc2 = function (item)
		{
			return h_addr.SafePrepareReadableText(item, 'Кликните, чтобы указать..');
		}

		var formatResult = function (item) {
			return '<div class="cpw-fms-official-organ"><div class="code">код:' +
				item.row.CODE + '</div><div class="name">' +
				item.row.NAME + '</div></div>';
		}

		var formatSelection = function (item) {
			if (!item.row) {
				return item.text;
			}
			else {
				return '<div class="cpw-fms-official-organ" title="' +
					item.row.NAME + '"><div class="name">' +
					item.row.NAME + '</div><div class="code">код:<br/>' +
					item.row.CODE + '</div></div>';
			}
		}

		controller.PrepareHtml = function ()
		{
			this.binding = CreateBinding();
			if (!this.model.Birthplace)
				this.model.Birthplace = TownAddress();
			this.binding.model = this.model;
			this.binding.id_form_div = this.id_div_modal_form;
			this.binding.form_div_selector = '#' + this.id_div_modal_form;
			this.binding.options =
			{
				field_spec:
				{
					AddressLegal: function (model)
					{
						var res = {
							text: AddressTextFunc2,
							controller: c_addr_modal(model, 'AddressLegal', 'Адрес прописки', true, false, function () {
								return h_fias.GetFiasVersionBySchemaVersion({ subdivision: $('#cpw_fms_profile_subdivision').select2('data') })
							})
						};
						return res;
					}
					,Birthplace:
					{
						text: function (item) {
							return !(!item || null == item || !item.text || null == item.text || '' == item.text)
								? item.text
								: 'Кликните, чтобы указать адрес..';
						}
						, controller: function (item, onAfterSave) { return controller_Town(item, onAfterSave); }
					}
				}
			}

			return this.template(this.binding);
		}

		controller.AfterRender = function ()
		{
			this.binding.LoadFromModel_JsWidgets();
			$('.input-datepicker').change(helper_validate.OnConvertToDateFormat);

			this.RenderSelect2($('#cpw_form_DocumentType_Company'), helper_DocumentType);

			if (this.model.user_document_type)
			{
				$('#cpw_form_DocumentType_Company').select2('data', this.model.user_document_type);
			}
			else
			{
				$('#cpw_form_DocumentType_Company').select2('data', { id: 103008, text: "Паспорт гражданина Российской Федерации" });
			}

			this.RenderSelect2($('#cpw_form_Nationality'), h_citizenship);
			if (this.model.Nationality)
			{
				$('#cpw_form_Nationality').select2('data', this.model.Nationality);
			}
			else
			{
				$('#cpw_form_Nationality').select2('data', { id: "RUS", text: "Россия" });
			};
			var _model = this.model;
			$('#cpw_form_Nationality').on('change', function (e)
			{
				if (_model.Birthplace && (!_model.Birthplace.Country || _model.Birthplace.Country == ""))
					_model.Birthplace.Country = $(this).select2('data');
			})

			$('#cpw_form_user_document_givenby').select2({
				placeholder: ''
				, allowClear: true
				, dropdownCssClass: "bigdrop"
				, query: function (q) { q.callback({ results: h_dictionary.FindByNameOrCode(fms, q.term) }); }
				, escapeMarkup: function (m) { return m; }
				, formatResult: formatResult
				, formatSelection: formatSelection
			});
			$('#cpw_form_user_document_givenby_dict').change(function (e) {
				var checked = $(this).attr('checked');
				if ('checked' == checked) {
					$('#s2id_cpw_form_user_document_givenby').show();
					$('#cpw_form_user_document_givenby_text').hide();
				}
				else {
					$('#s2id_cpw_form_user_document_givenby').hide();
					$('#cpw_form_user_document_givenby_text').show();
				}
			});
			var visible = $('#cpw_form_user_document_givenby').is(':visible');
			$('#cpw_form_user_document_givenby').select2('data', this.model.user_document_givenby);
			if (!visible)
				$('#s2id_cpw_form_user_document_givenby').hide();
			$('#cpw_form_user_document_givenby_dict').attr('checked', this.model.user_document_givenby_dict ? 'checked' : null).change();
			$('#cpw_form_user_document_givenby_text').val(this.model.user_document_givenby_text);
			if (!this.model.user_document_givenby_text || $('#cpw_form_user_document_givenby_text').val() == '')
				$('#cpw_form_user_document_givenby_dict').attr('checked', 'checked').change();

			var sel = this.binding.form_div_selector;
			$(sel + ' input[model_field_name=FirstName]').change(h_names.OnNameChange_NormalizeAndLatinize);
			$(sel + ' input[model_field_name=LastName]').change(h_names.OnNameChange_NormalizeAndLatinize);
			$(sel + ' input[model_field_name=MiddleName]').change(h_names.OnNameChange_NormalizeAndLatinize);
		}

		controller.RenderSelect2 = function (select2_element, h_values)
		{
			select2_element.select2({
				placeholder: '',
				allowClear: true,
				query: function (query)
				{
					query.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), query.term) });
				}
			});
		}

		controller.DataSaveFromControls = function ()
		{
			this.binding.SaveFieldsToModel(this.binding.form_div_selector, this.binding.model);
			h_colaborator.UpdateText(this.model);
			this.model.user_document_type = $('#cpw_form_DocumentType_Company').select2('data');
			this.model.Nationality = $('#cpw_form_Nationality').select2('data');
			this.model.user_document_givenby_dict = 'checked' == $('#cpw_form_user_document_givenby_dict').attr('checked');
			this.model.user_document_givenby = $('#cpw_form_user_document_givenby').select2('data');
			this.model.user_document_givenby_text = $('#cpw_form_user_document_givenby_text').val();
			if (!this.model.user_document_givenby_dict) {
			    this.model.user_document_givenby = null;
			} else {
			    this.model.user_document_givenby_text = null;
			}
		}

		controller.DataLoadToControls = function ()
		{
		}

		return controller;
	}
});
