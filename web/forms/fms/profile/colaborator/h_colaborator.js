﻿define(function ()
{
	var res=
	{
		BuildText: function(m_colaborator)
		{
			var text= '';
			if (m_colaborator.LastName)
				text+= m_colaborator.LastName;
			if (m_colaborator.FirstName)
				text+= ' ' + m_colaborator.FirstName;
			if (m_colaborator.MiddleName)
				text+= ' ' + m_colaborator.MiddleName;
			return text;
		}
		,UpdateText: function(m_colaborator)
		{
			m_colaborator.text= this.BuildText(m_colaborator);
		}
	};
	return res;
});
