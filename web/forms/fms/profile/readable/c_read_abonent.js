﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/profile/readable/e_read_abonent.html'
	, 'forms/base/log'
	, 'forms/fms/profile/x_abonent'
	, 'forms/fms/base/addr/h_addr'
],
function (binded_controller, tpl, GetLogger, codec, h_addr) {
	return function () {
		var log = GetLogger('c_read_abonent');

		var controller = binded_controller(tpl, {});

		var base_Render = controller.Render;
		controller.Render = function (id_form_div) {
			if (!this.model)
				this.model = {};
			if (!this.model.AddressRealAbonent)
				this.model.AddressRealAbonent = { Country: { id: 'RUS', text: "Россия"} };
			if (!this.model.AddressesOfPlacement)
				this.model.AddressesOfPlacement = [];
			if (!this.model.ResponsibleOfficiers)
				this.model.ResponsibleOfficiers = [];

			var oas = base_Render.call(this, id_form_div);

			var self = this;
			$('#cpw-fms-print-blue-stamp-on-migrationn').change(function () { self.OnChangeBlueStamp(); })
		}

		controller.OnChangeBlueStamp= function()
		{
			var checked = $('#cpw-fms-print-blue-stamp-on-migrationn').attr('checked');
			$.cookie("print-blue-stamp-on-migrationn", checked ? 'yes' : 'no');
		}

		controller.SetFormContent = function (content) {
			this.model = content;
			if (content.AddressRealAbonent && !content.AddressRealAbonent.text) {
				this.model.AddressRealAbonent.text = h_addr.IsEmptyAddress(this.model.AddressRealAbonent) ? '' :
					h_addr.PrepareReadableText(this.model.AddressRealAbonent);
			}
		}

		controller.UseCodec(codec());

		return controller;
	}
});
