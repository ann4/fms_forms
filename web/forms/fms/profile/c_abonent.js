define([
	  'forms/base/c_binded'
	, 'tpl!forms/fms/profile/e_abonent.html'
	, 'forms/fms/base/town/m_town'
	, 'forms/base/log'
	, 'forms/fms/base/h_validate_n'
	, 'forms/fms/profile/x_abonent'
	, 'forms/fms/guides/dict_officialorgan_fns.csv'
	, 'forms/fms/guides/dict_officialorgan_fms.profile.csv'
	, 'tpl!forms/fms/profile/collection/e_collection.html'
	, 'forms/fms/profile/colaborator/c_colaborator'
	, 'forms/base/h_times'
	, 'forms/base/h_dictionary'
	, 'forms/fms/base//h_Select2'
	, 'forms/fms/base/addr/c_addr_modal'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/base/h_msgbox'
	, 'forms/fms/base/addr_with_document/c_addr_with_document'
	, 'forms/fms/base/h_taxSystem'
	, 'forms/fms/base/h_ksrCategory'
	, 'forms/fms/base/h_ksrTypeOrganization'
	, 'forms/fms/base/h_wellnessProfile'
	, 'forms/fms/base/russ_addr_fias/h_fias'
],
function (binded_controller, tpl, TownAddress, GetLogger, helper_validate, codec, fns, fms, collection_tpl, controller_Colaborator, h_times, h_dictionary, h_select2, c_addr_modal, h_addr, h_msgbox, c_addr,
	h_taxSystem, h_ksrCategory, h_ksrTypeOrganization, h_wellnessProfile, h_fias)
{
	return function ()
	{
		var log = GetLogger('c_abonent');

		var formatResult= function(item)
		{
			return '<div class="cpw-fms-official-organ"><div class="code">код:' + 
				item.row.CODE +'</div><div class="name">' + 
				item.row.NAME + '</div></div>';
		}

		var formatSelection= function(item)
		{
			if (!item.row)
			{
				return item.text;
			}
			else
			{
				return '<div class="cpw-fms-official-organ" title="' + 
					item.row.NAME + '"><div class="name">' + 
					item.row.NAME + '</div><div class="code">код:<br/>' + 
					item.row.CODE + '</div></div>';
			}
		}

		var valuesPeriodOperation = [
			{ id: 'y', text: 'круглый год' },
			{ id: 'p', text: 'сезонный' }
		];

		var fms_fns_select2_options= function(values)
		{
			var res= {
				  placeholder: ''
				, allowClear: true
				, query: function (q) {q.callback({ results: h_dictionary.FindByNameOrCode(values,q.term) });}
				, dropdownCssClass: "bigdrop"
				, formatResult: formatResult
				, formatSelection: formatSelection
				, escapeMarkup: function(m){return m;}
			};
			return res;
		}

		var copy_collection_for_select2= function(values)
		{
			var res= [];
			for (var i= 0; i<values.length; i++)
			{
				var value= values[i];
				res.push({id:value.id,text:value.text});
			}
			return res;
		}

		var collection_select2_options= function(get_collection)
		{
			var create_options= function(m_abonent)
			{
				var res= {
					  placeholder: ''
					, allowClear: true
					, dropdownCssClass: "bigdrop"
					, query: function (q) {q.callback({ results: copy_collection_for_select2(get_collection(m_abonent)) });}
					, escapeMarkup: function(m){return m;}
				};
				return res;
			}
			return create_options;
		}

		var select2Options = function (h_values) {
			return function (m) {
				return {
					placeholder: '',
					allowClear: true,
					query: function (q) {
						q.callback({ results: h_select2.FindForSelect(h_values, q.term) });
					}
				};
			}
		}

		var AddressTextFunc2 = function (item)
		{
			return h_addr.SafePrepareReadableText(item, 'Кликните, чтобы указать..');
		}

		var createAddress = function ()
		{
			return { Country: { id: 'RUS', text: "Россия" }, id: h_times.safeDateTime().getTime() };
		}

		var address_Modal_controller = function (addr,title,after_ok)
		{
			return {
				ShowModal: function (e)
				{
					var controller = c_addr();
					if (!addr)
						addr = {};
					if (!addr.russianAddress)
						addr.russianAddress = {};
					addr.russianAddress.housing_types = true;
					if (!addr.russianAddress.onlyfias)
						addr.russianAddress.onlyfias = true;
					if (!addr.russianAddress.variant_fias)
						addr.russianAddress.variant_fias = true;
					addr.russianAddress.fias_version = h_fias.GetFiasVersionBySchemaVersion({ subdivision: $('#cpw_fms_profile_subdivision').select2('data') });
					controller.SetFormContent(addr);
					var bname_Ok = 'Сохранить';
					h_msgbox.ShowModal({
						controller: controller
						, width: 750
						, height: 474
						, title: title
						, buttons: [bname_Ok, 'Отменить']
						, onclose: function (bname)
						{
							if (bname_Ok == bname)
							{
								addr = controller.GetFormContent();
								addr.text = h_addr.SafePrepareReadableText(addr);
								after_ok();
							}
							if (addr && addr.russianAddress) {
								delete addr.russianAddress.onlyfias;
								if (null == addr.russianAddress.fias_version)
									delete addr.russianAddress.fias_version;
							}
						}
					});
				}
			};
		}

		var controller = binded_controller(tpl,
			{
				extension_key: 'fms_profile'
				, field_spec:
				{
					 sono:                      function(m){return fms_fns_select2_options(fns);}
					,subdivision:               function(m){return fms_fns_select2_options(fms);}
					,CurrentColaborator:        collection_select2_options(function(m){return m.ResponsibleOfficiers;})
					,CurrentAddressOfPlacement: collection_select2_options(function(m){return m.AddressesOfPlacement;})
					, PeriodOperation:          select2Options(valuesPeriodOperation)
					, HotelType:                select2Options(h_ksrTypeOrganization.GetValues())
					, HotelCategory:            select2Options(h_ksrCategory.GetValues())
					, TaxSystem:                select2Options(h_taxSystem.GetValues())
					,AddressesOfPlacement:
					{
						  type: 'collection'
						, tpl: collection_tpl
						, options:
						{
							item_modal_controller: address_Modal_controller
							, title: 'адреса'
							, create_model: createAddress
						}
					}
					,ResponsibleOfficiers:
					{
						  type: 'collection'
						, tpl: collection_tpl
						, options:
						{
							  item_modal_controller : controller_Colaborator
							, title: 'параметров сотрудника'
							, create_model: function () { return { id: h_times.safeDateTime().getTime(), AddressLegal: createAddress(), Birthplace: TownAddress() }; }
						}
					}
					, AddressRealAbonent: function (model)
					{
						var res = {
							text: AddressTextFunc2,
							controller: c_addr_modal(model, 'AddressRealAbonent', 'Фактический адрес офиса', true, false, function () {
								return h_fias.GetFiasVersionBySchemaVersion({ subdivision: $('#cpw_fms_profile_subdivision').select2('data') })
							})
						};
						return res;
					}
				}
			}
		);

		var base_Render = controller.Render;
		controller.Render = function (id_form_div)
		{
			if (!this.model) {
				this.model = {};
				this.model.CheckResponsibleOfficier = false;
			}
			if (!this.model.AddressRealAbonent)
				this.model.AddressRealAbonent = { Country: { id: 'RUS', text: "Россия" } };
			if (!this.model.AddressesOfPlacement)
				this.model.AddressesOfPlacement= [];
			if (!this.model.ResponsibleOfficiers)
				this.model.ResponsibleOfficiers= [];

			var oas = base_Render.call(this, id_form_div);

			$('#cpw_fms_profile_PeriodOperation').on('change', ChangePeriodOperation);
			$('#cpw_fms_profile_HotelType').on('change', ChangeHotelType);
			$('#cpw_fms_profile_StartPeriod').datepicker(h_times.DatePickerOptions);
			$('#cpw_fms_profile_FinishPeriod').datepicker(h_times.DatePickerOptions);
			$('.cpw-forms-fms-profile .datepicker').on("change", function (e) { helper_validate.OnChangeDateInput_FixFormat(e); });
			$('#cpw_fms_profile_PeriodOperation').change();
			$('#cpw_fms_profile_HotelType').change();

			if (this.model.WellnessProfile && !$('.wellness_profile').hasClass('hidden')) {
				for (var i = 0; i < this.model.WellnessProfile.length; i++) {
					$('#cpw_form_WellnessProfile_' + this.model.WellnessProfile[i].id).attr('checked', 'checked')
				}
			}
			$('#cpw_fms_profile_subdivision').on('change', ValidateFiasAddress);
		}

		var UpdateAddressesLinksTextes = function ()
		{
			$('#fms_addresses_legal').text(
				!(this.ResponsibleOfficiers && 
				  this.ResponsibleOfficiers.length>0 && 
				  this.ResponsibleOfficiers[0].AddressLegal && 
				  this.ResponsibleOfficiers[0].AddressLegal.text)
				? 'Добавить адрес'
				: this.AddressLegal.text );
			$('#fms_addresses_legal_abonent').text
				((this.AddressRealAbonent && this.AddressRealAbonent.text) ? this.AddressRealAbonent.text : 'Добавить адрес');
		}

		var ChangePeriodOperation = function()
		{
			var valPeriodOPeration = $('#cpw_fms_profile_PeriodOperation').val();
			if ('y' == valPeriodOPeration || '' == valPeriodOPeration) {
				$('#cpw_fms_profile_StartPeriod').val('');
				$('#cpw_fms_profile_FinishPeriod').val('');
				$('#period_operation').addClass('hidden');
			} else {
				$('#period_operation').removeClass('hidden');
			}
		}

		var ChangeHotelType = function(e)
		{
			var hotelType = $('#cpw_fms_profile_HotelType').val();
			if ("" != hotelType && 105 <= parseInt(hotelType) && parseInt(hotelType) <= 110) {
				$('.wellness_profile').removeClass('hidden');
				var variants = h_wellnessProfile.GetValues();
				for (var i = 0; i < variants.length; i++) {
					var variant = variants[i];
					var idVariant = 'cpw_form_WellnessProfile_' + variant.id;
					var html = '<div>';
					html += '  <input type="checkbox" class="wellness_profile_variant" id="' + idVariant + '" />';
					html += '  <label for="' + idVariant + '">' + variant.text + '</label>';
					html += '</div>';
					$('.wellness_profile .value-blok').append(html);
				}
			} else {
				$('.wellness_profile .value-blok').html('');
				$('.wellness_profile').addClass('hidden');
			}
		}

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			if (this.Validate() != null)
			{
				return null;
			}
			else
			{
				var res = base_GetFormContent.call(this);
				/*if (this.model && this.model.AddressRealAbonent && !this.model.AddressRealAbonent.id)
					this.model.AddressRealAbonent.id = h_times.safeDateTime().getTime();*/
				var _self = this;
				_self.model.WellnessProfile = [];
				$('.wellness_profile_variant').each(function () {
					if ('checked' == $(this).attr('checked')) {
						var id = ($(this).attr('id')).replace('cpw_form_WellnessProfile_', '');
						_self.model.WellnessProfile.push({ id: id, text: $('[for="cpw_form_WellnessProfile_' + id + '"]').text() });
					}
				});
				return res;
			}
		}

		var base_Edit = controller.Edit;
		controller.Edit = function (id_form_div)
		{
			this.AddressLegal = 
				!(controller.ResponsibleOfficiers && 
				  controller.ResponsibleOfficiers.length>0 && 
				  controller.ResponsibleOfficiers[0].AddressLegal)
				? createAddress() : controller.ResponsibleOfficiers[0].AddressLegal;
			
			base_Edit.call(this, id_form_div);
			ValidateFiasAddress();
		}

		var ValidateInit = function (rules)
		{
			var res = 1;
			for (var i = 0; i < rules.length; i++)
			{
				var obj = rules[i];
				for (var j = 0; j < obj.attributes.length; j++)
				{
					res &= obj.handler(obj.attributes[j]);
				};
			};
			tabsStyledErrors(res);
			return res;
		}

		var tabsStyledErrors = function (res)
		{
			$('.ui-tabs-nav a').removeClass('error');
			$(document).find('.error').each(function ()
			{
				var tab = $(this).closest('.ui-tabs-panel');
				var tabId = tab.attr('id');
				$(document).find('a[href="#' + tabId + '"]').addClass('error');
			})
		}

		controller.Validate = function ()
		{
			var rules = [];
			var res_txt = ValidateInit(rules) ? '' : helper_validate.ValidateErrorMsg;
			if ('' != res_txt)
				res_txt += '\r\n\r\n';
			res_txt += ValidateVariantAddress();
			return res_txt == '' ? null : res_txt;
		}

		var ValidateVariantAddress = function()
		{
			var res_txt = '';
			if (!controller.model) return res_txt;
			var currentAddressOfPlacement_item = $('#s2id_CurrentAddressOfPlacement');
			var currentAddressOfPlacement = currentAddressOfPlacement_item.select2('data');
			if (currentAddressOfPlacement && '' != currentAddressOfPlacement.id)
			{
				for (var i = 0; i < controller.model.AddressesOfPlacement.length; i++)
				{
					var addr = controller.model.AddressesOfPlacement[i];
					if (addr.id == currentAddressOfPlacement.id && addr.russianAddress && !addr.russianAddress.variant_fias)
					{
						res_txt += 'Текущий адрес размещения необходимо заполнить по справочнику ФИАС!';
						break;
					}
				}
			}
			var currentCurrentColaborator_item = $('#s2id_CurrentColaborator');
			var currentCurrentColaborator = currentCurrentColaborator_item.select2('data');
			if (currentCurrentColaborator && '' != currentCurrentColaborator.id)
			{
				for (var i = 0; i < controller.model.ResponsibleOfficiers.length; i++)
				{
					var colaborator = controller.model.ResponsibleOfficiers[i];
					if (colaborator.id == currentCurrentColaborator.id && colaborator.AddressLegal && 
						colaborator.AddressLegal.russianAddress && !colaborator.AddressLegal.russianAddress.variant_fias)
					{
						if ('' != res_txt) res_txt += '\r\n';
						res_txt += 'Адрес прописки сотрудника необходимо заполнить по справочнику ФИАС!';
						break;
					}
				}
			}
			if (controller.model.AddressRealAbonent && controller.model.AddressRealAbonent.russianAddress && !controller.model.AddressRealAbonent.russianAddress.variant_fias)
			{
				if ('' != res_txt) res_txt += '\r\n';
				res_txt += 'Фактический адрес необходимо заполнить по справочнику ФИАС!';
			}
			return res_txt;
		}

		var ValidateFiasAddress = function () {
			var res = ValidateVersionFiasAddress();
			if ('' != res) {
				var bname_Ok = 'Обновить адреса';
				h_msgbox.ShowModal({
						title: 'Предупреждение'
						, html:  '<pre>' + res + '</pre>'
						, width: 750
						, buttons: [bname_Ok]
						, onclose: function (bname) {
							if (bname_Ok == bname) {
								ConvertFiasAddressByVersion();
							}
						}
				});
			}
		}

		var ValidateVersionFiasAddress = function () {
			var res_txt = '';
			if (!controller.model) return res_txt;
			var fias_version = h_fias.GetFiasVersionBySchemaVersion({ subdivision: $('#cpw_fms_profile_subdivision').select2('data') });
			var fias_version_not_null = fias_version && null != fias_version && '' != fias_version;
			var currentAddressOfPlacement_item = $('#s2id_CurrentAddressOfPlacement');
			var currentAddressOfPlacement = currentAddressOfPlacement_item.select2('data');
			if (currentAddressOfPlacement && '' != currentAddressOfPlacement.id) {
				var oldFias = false;
				for (var i = 0; i < controller.model.AddressesOfPlacement.length; i++) {
					var addr = controller.model.AddressesOfPlacement[i];
					var addr_fias_not_null = addr.russianAddress && addr.russianAddress.fias;
					oldFias |= addr.id == currentAddressOfPlacement.id && fias_version_not_null && addr_fias_not_null 
						&& (!addr.russianAddress.fias_version || fias_version != addr.russianAddress.fias_version);
				}
				if (oldFias) {
					res_txt += 'Адреса размещения заполнены по старому справочнику ФИАС!';
				}
			}
			var currentCurrentColaborator_item = $('#s2id_CurrentColaborator');
			var currentCurrentColaborator = currentCurrentColaborator_item.select2('data');
			if (currentCurrentColaborator && '' != currentCurrentColaborator.id) {
				var oldFias = false;
				for (var i = 0; i < controller.model.ResponsibleOfficiers.length; i++) {
					var colaborator = controller.model.ResponsibleOfficiers[i];
					var addr_fias_not_null = colaborator.AddressLegal && colaborator.AddressLegal.russianAddress
						&& colaborator.AddressLegal.russianAddress.fias;
					oldFias |= colaborator.id == currentCurrentColaborator.id && fias_version_not_null && addr_fias_not_null
						&& (!addr.russianAddress.fias_version || fias_version != addr.russianAddress.fias_version);
				}
				if (oldFias) {
					if ('' != res_txt) res_txt += '\r\n';
					res_txt += 'Адрес прописки сотрудника заполнен по старому справочнику ФИАС!';
				}
			}
			var addressrealabonent_fias_not_null = controller.model.AddressRealAbonent && controller.model.AddressRealAbonent.russianAddress
				&& controller.model.AddressRealAbonent.russianAddress.fias;
			if (fias_version_not_null && addr_fias_not_null && (!addr.russianAddress.fias_version || fias_version != addr.russianAddress.fias_version))
			{
				if ('' != res_txt) res_txt += '\r\n';
				res_txt += 'Фактический адрес заполнен по старому справочнику ФИАС!';
			}
			return res_txt;
		}

		var ConvertFiasAddressByVersion = function ()
		{
			var fias_version = h_fias.GetFiasVersionBySchemaVersion({ subdivision: $('#cpw_fms_profile_subdivision').select2('data') });

			var currentAddressOfPlacement_item = $('#s2id_CurrentAddressOfPlacement');
			var currentAddressOfPlacement = currentAddressOfPlacement_item.select2('data');
			if (currentAddressOfPlacement && '' != currentAddressOfPlacement.id) {
				for (var i = 0; i < controller.model.AddressesOfPlacement.length; i++) {
					var addr = controller.model.AddressesOfPlacement[i];
					h_fias.ConvertOldFiasToNew(addr, fias_version, function () {
						$('[model_field_name="AddressesOfPlacement"] .collection-item-text-' + (3 + i * 2)).text(h_addr.SafePrepareReadableText(addr));
					});
				}
			}
			if (controller.model.ResponsibleOfficiers && 0 < controller.model.ResponsibleOfficiers.length) {
				for (var i = 0; i < controller.model.ResponsibleOfficiers.length; i++) {
					var colaborator = controller.model.ResponsibleOfficiers[i];
					h_fias.ConvertOldFiasToNew(colaborator.AddressLegal, fias_version, function () { });
				}
			}
			h_fias.ConvertOldFiasToNew(controller.model.AddressRealAbonent, fias_version, function () {
				$('#cpw_fms_profile_AddressRealAbonent').text(h_addr.SafePrepareReadableText(controller.model.AddressRealAbonent));
			});
		}
		controller.UseCodec(codec());

		return controller;
	}
});
