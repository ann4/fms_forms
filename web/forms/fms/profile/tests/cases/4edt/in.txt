include ..\in.lib.txt quiet
execute_javascript_stored_lines add_select2_test_functions

execute_javascript_line "app.cpw_Now= function(){return new Date(2015, 6, 26, 17, 36, 0, 0 ); };"

wait_text "в ППО"

wait_click_text "Текущая смена"
shot_check_png ..\..\shots\ok3addr.png
wait_click_text "Ответственные"
wait_click_text "Сердюков Сердюк Сердюкович"
shot_check_png ..\..\shots\ok3addr_1.png
wait_click_text "Отменить"
wait_click_text "Размещение"
shot_check_png ..\..\shots\ok3addr_2.png
wait_click_text "Абонент"
shot_check_png ..\..\shots\ok3addr_3.png

wait_click_text "Сохранить содержимое формы"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\abonent_ok3addr_edited.json.result.txt


exit