start_store_lines_as add_select2_test_functions

window.wbt_Select2Focus= function(id)
{
  $('#s2id_' + id + ' .select2-focusser').focus();
  $('#s2id_' + id + ' .select2-choice').click().mousedown().mouseup();
  $('#s2id_' + id + ' .select2-focusser').focus();
  $('#' + id).select2('close');
}

window.wbt_async_text_SelectTextInSelect2= function(id,text,count)
{
  var labels= $('.select2-result-label');
  var labels_len= labels.length;
  var labels_text= labels.text().toUpperCase();
  var text_to_check= text.toUpperCase();
  if (labels_len<1 || -1==labels_text.indexOf(text_to_check))
  {
    if (count>0)
    {
      setTimeout(function(){window.wbt_async_text_SelectTextInSelect2(id,text,count-1);},20);
    }
    else
    {
      alert('can not find selected label! labels.length=' + labels_len + ' labels.text()=' + labels_text);
    }
  }
  else
  {
    $('#select2-drop ul li').first().mousedown().mouseup().click();
    $('.select2-input').val('').change().keydown().keyup();
    $('#' + id).select2('close');
  }
}

window.wbt_text_SelectTextInSelect2= function(id,text)
{
  $('#s2id_' + id + ' .select2-focusser').focus();
  $('#s2id_' + id + ' .select2-choice').click().mousedown().mouseup();
  $('#s2id_' + id + ' .select2-focusser').focus();

  for (var i=0; i<text.length; i++)
  {
    $('.select2-input').val(text.substring(0,i)).change().keydown().keyup();
  }
  $('.select2-input').val(text).change().keydown().keyup();

  setTimeout(function(){window.wbt_async_text_SelectTextInSelect2(id,text,100);},20);
}

window.wbt_text_SetValueForModelFieldName= function(name,text)
{
  $('input[model_field_name="' + name + '"]').val(text).change();
}

window.wbt_text_ClickModelFieldName= function(name)
{
  var item= $('*[model_field_name="' + name + '"]');
  item.click();
}
window.wbt_text_ClickModelFieldNameValue= function(name,value)
{
  var item= $('*[model_field_name="' + name + '"][value="' + value + '"]');
  item.click();
}
stop_store_lines

start_store_lines_as fms_profile_fields_1

  # в ППО "Территория"
    click_text "в ППО ""Территория"""
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('employee_ummsId','1001')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('supplierInfo','1800000008002001')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('hotel_id','123456')"

  # Абонент
    click_text "Абонент"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('OrganizationName','ООО Подушка-Одеяло')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('OKPO','1234567890')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('OrganizationPhone','01')"

    execute_javascript_line "window.wbt_text_ClickModelFieldName('AddressRealAbonent')"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Ижевск");
    wait_text "Город:"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Буммашевская");
    wait_text "Улица:"
    js wbt_SetModelFieldValue('Дом',      '11');
    js wbt_SetModelFieldValue('Квартира', '18');
    wait_click_full_text "Сохранить"

    execute_javascript_line "window.wbt_text_SelectTextInSelect2('cpw_fms_profile_subdivision','022-058');"
    wait_id_text s2id_cpw_fms_profile_subdivision "МИЯКИНСКИЙ РОВД РЕСПУБЛИКИ БАШКОРТОСТАН"
    execute_javascript_line "window.wbt_text_SelectTextInSelect2('cpw_fms_profile_sono','Межрайонная инспекция федеральной налоговой службы №6 по Уд');"
    wait_id_text s2id_cpw_fms_profile_sono "1821"

   # Размещение
    click_text "Размещение"
    click_text "Добавить"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Ижевск");
    wait_text "Город:"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Авангардная");
    wait_text "Улица:"
    js wbt_SetModelFieldValue('Дом',      '31');
    js wbt_SetModelFieldValue('Квартира', '18');
    wait_click_full_text "Сохранить"

  # Ответственные
    click_text "Ответственные"
    click_text "Добавить"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('LastName','Хамагай')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('FirstName','Максим')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('MiddleName','Варлампиевич')"

    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('Birthday','5.8.1977.')"
    execute_javascript_line "window.wbt_text_ClickModelFieldNameValue('Sex','M')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('Phone','447276')"

    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('user_document_serie','5187')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('user_document_number','935715')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('user_document_dateFrom','5.8.2010.')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('user_document_dateTo','5.8.2019.')"

    execute_javascript_line "window.wbt_text_SelectTextInSelect2('cpw_form_user_document_givenby','181-001');"
    wait_id_text s2id_cpw_form_user_document_givenby "181-001"

    execute_javascript_line "window.wbt_text_ClickModelFieldName('AddressLegal')"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Ижевск");
    wait_text "Город:"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Лихвинцева");
    wait_text "Улица:"
    js wbt_SetModelFieldValue('Дом',      '7');
    js wbt_SetModelFieldValue('Квартира', '8');
    wait_click_full_text "Сохранить"

    wait_click_full_text "Сохранить"

  # Текущая смена
    click_text "Текущая смена"
    execute_javascript_line "window.wbt_text_SelectTextInSelect2('CurrentColaborator','Хамагай');"
    wait_id_text s2id_CurrentColaborator "Хамагай Максим Варлампиевич"
    execute_javascript_line "window.wbt_text_SelectTextInSelect2('CurrentAddressOfPlacement','Респ Удмуртская, г Ижевск, ул Авангардная, д.31, кв.18');"
    wait_id_text s2id_CurrentAddressOfPlacement "Респ Удмуртская, г Ижевск, ул Авангардная, д.31, кв.18"

stop_store_lines

start_store_lines_as fms_profile_select2_focus_1
  # в ППО "Территория"
    click_text "в ППО ""Территория"""
    execute_javascript_line "window.wbt_Select2Focus('CurrentColaborator')"
    execute_javascript_line "window.wbt_Select2Focus('CurrentAddressOfPlacement')"

  # Абонент
    click_text "Абонент"
    execute_javascript_line "window.wbt_Select2Focus('cpw_fms_profile_subdivision')"
    execute_javascript_line "window.wbt_Select2Focus('cpw_fms_profile_sono')"
stop_store_lines

start_store_lines_as fms_profile_working_1
  # Деятельность
    click_text "Деятельность"
    js wbt_SelectTextInSelect2BySpec("#cpw_fms_profile_PeriodOperation", "сезонный");
    wait_text "сезонный"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('StartPeriod','3.4.2018.')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('FinishPeriod','30.10.2018.')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('RoomAllCount','30')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('RoomHighestCategory','5')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('RoomSpecialCategory','10')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('RoomArea','600')"
    execute_javascript_line "window.wbt_text_SetValueForModelFieldName('CountPlaces','100')"

    js wbt_SelectTextInSelect2BySpec("#cpw_fms_profile_HotelType","санаторий для детей");
    wait_text "санаторий для детей"
    js wbt_SelectTextInSelect2BySpec("#cpw_fms_profile_TaxSystem","упрощ");
    wait_text "упрощенная"
    js wbt_SelectTextInSelect2BySpec("#cpw_fms_profile_HotelCategory","4");
    wait_text "4 звезды"

    execute_javascript_line "$('#cpw_form_WellnessProfile_131').attr('checked', 'checked')"
    execute_javascript_line "$('#cpw_form_WellnessProfile_133').attr('checked', 'checked')"
    execute_javascript_line "$('#cpw_form_WellnessProfile_135').attr('checked', 'checked')"
    execute_javascript_line "$('#cpw_form_WellnessProfile_137').attr('checked', 'checked')"
stop_store_lines

start_store_lines_as fms_profile_address_1
   # Размещение
    click_text "Размещение"
    click_text "Добавить"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Ижевск");
    wait_text "Город:"
    js wbt_SelectTextInSelect2BySpec("#cpw-fms-fias-addr-select-select2", "Первомайская");
    wait_text "Улица:"
    js wbt_SetModelFieldValue('Дом',      '15');
    js wbt_SetModelFieldValue('Квартира', '25');
    js wbt_SetModelFieldValue('document', 'Договор аренды №12345 от 12.12.2012');
    wait_click_full_text "Сохранить"
stop_store_lines
