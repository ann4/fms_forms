{ 
"AuthInfo": { "token": "1234567890" },
"Extensions": { "fms": { "Abonent": {
	"AddressRealAbonent": {
		"Country": { "id": "RUS", "text": "Россия" },
		"russianAddress": { "Дом": "123", "Корпус": "", "Квартира": "23", "Комната": "", "variant_fias": false, "free": { "Регион":"УР", "Район":"", "Город":"Ижевск", "НаселенныйПункт":"", "ГородскойРайон":"", "Улица":"Пушкинская" }}
	},
	"AddressesOfPlacement": [
		{
			"Country": { "id": "RUS", "text": "Россия" },
			"id": 1437917760000,
			"russianAddress": { "Дом": "31", "Корпус": "", "Квартира": "18", "Комната": "", "variant_fias": true,
				"fias": [
					{
						"AOID": "8e228a1a-d7e8-4a5a-ab6e-cf20f5d41f17",
						"AOGUID": "68072c2a-a5d1-4846-a636-f68d36281548",
						"AOLEVEL": "7",
						"IsTerminal": "1",
						"SHORTNAME": "ул",
						"FORMALNAME": "Авангардная",
						"FullPath": "Респ Удмуртская$г Ижевск",
						"REGIONCODE": "18",
						"AREACODE": "000",
						"CITYCODE": "001",
						"PLACECODE": "000",
						"EXTRCODE": "0000",
						"STREETCODE": "0001",
						"SEXTCODE": "000"
					},
					{
						"AOID": "873813f2-bb14-4ca2-b463-637342e7029b",
						"AOGUID": "deb1d05a-71ce-40d1-b726-6ba85d70d58f",
						"AOLEVEL": "4",
						"IsTerminal": "0",
						"SHORTNAME": "г",
						"FORMALNAME": "Ижевск",
						"FullPath": "Респ Удмуртская",
						"REGIONCODE": "18",
						"AREACODE": "000",
						"CITYCODE": "001",
						"PLACECODE": "000",
						"EXTRCODE": "0000",
						"STREETCODE": "0000",
						"SEXTCODE": "000"
					},
					{
						"AOID": "ae58022e-2a56-4766-a331-8d99aca4d8c4",
						"AOGUID": "52618b9c-bcbb-47e7-8957-95c63f0b17cc",
						"AOLEVEL": "1",
						"IsTerminal": "0",
						"SHORTNAME": "Респ",
						"FORMALNAME": "Удмуртская",
						"FullPath": "",
						"REGIONCODE": "18",
						"AREACODE": "000",
						"CITYCODE": "000",
						"PLACECODE": "000",
						"EXTRCODE": "0000",
						"STREETCODE": "0000",
						"SEXTCODE": "000"
					}
				]
			},
			"text": "Респ Удмуртская, г Ижевск, ул Авангардная, д.31, кв.18"
		},
		{
			"Country": { "id": "RUS", "text": "Россия" },
			"id": 1464172879074,
			"text": "УР, г Воткинск, ул Ленина, д.123, кв.12",
			"russianAddress": { "Дом": "123", "Корпус": "", "Квартира": "12", "Комната": "", "variant_fias": false, "free": { "Регион": "УР", "Район": "", "Город": "Воткинск", "НаселенныйПункт": "", "ГородскойРайон": "", "Улица": "Ленина" } }
		}
	],
	"ResponsibleOfficiers": [
		{
			"id": 1437917760000,
			"AddressLegal": {
				"Country": { "id": "RUS", "text": "Россия" },
				"id": 1437917760000,
				"russianAddress": { "Дом": "7", "Корпус": "", "Квартира": "8", "Комната": "", "variant_fias": true,
					"fias": [
						{
							"AOID": "c87cd460-3790-4516-ab97-79dea2546ff1",
							"AOGUID": "5a1f52ae-6930-482d-b861-fa1c943d1b85",
							"AOLEVEL": "7",
							"IsTerminal": "1",
							"SHORTNAME": "ул",
							"FORMALNAME": "Лихвинцева",
							"FullPath": "Респ Удмуртская$г Ижевск",
							"REGIONCODE": "18",
							"AREACODE": "000",
							"CITYCODE": "001",
							"PLACECODE": "000",
							"EXTRCODE": "0000",
							"STREETCODE": "0239",
							"SEXTCODE": "000"
						},
						{
							"AOID": "873813f2-bb14-4ca2-b463-637342e7029b",
							"AOGUID": "deb1d05a-71ce-40d1-b726-6ba85d70d58f",
							"AOLEVEL": "4",
							"IsTerminal": "0",
							"SHORTNAME": "г",
							"FORMALNAME": "Ижевск",
							"FullPath": "Респ Удмуртская",
							"REGIONCODE": "18",
							"AREACODE": "000",
							"CITYCODE": "001",
							"PLACECODE": "000",
							"EXTRCODE": "0000",
							"STREETCODE": "0000",
							"SEXTCODE": "000"
						},
						{
							"AOID": "ae58022e-2a56-4766-a331-8d99aca4d8c4",
							"AOGUID": "52618b9c-bcbb-47e7-8957-95c63f0b17cc",
							"AOLEVEL": "1",
							"IsTerminal": "0",
							"SHORTNAME": "Респ",
							"FORMALNAME": "Удмуртская",
							"FullPath": "",
							"REGIONCODE": "18",
							"AREACODE": "000",
							"CITYCODE": "000",
							"PLACECODE": "000",
							"EXTRCODE": "0000",
							"STREETCODE": "0000",
							"SEXTCODE": "000"
						}
					]
				}
			},
			"Birthplace": { "Country": null, "Region": null, "Area": null, "City": null, "Town": null, "text": "" },
			"LastName": "Хамагай",
			"FirstName": "Максим",
			"MiddleName": "Варлампиевич",
			"LastName_latin": "Khamagai",
			"FirstName_latin": "Maksim",
			"MiddleName_latin": "Varlampievich",
			"Sex": "M",
			"Birthday": "05.08.1977",
			"Phone": "447276",
			"user_document_serie": "5187",
			"user_document_number": "935715",
			"user_document_dateFrom": "05.08.2010",
			"user_document_dateTo": "05.08.2019",
			"text": "Хамагай Максим Варлампиевич",
			"user_document_type": { "id": 103008, "text": "Паспорт гражданина Российской Федерации" },
			"Nationality": { "id": "RUS", "text": "Россия" }
		},
		{
			"id": 1464176793863,
			"AddressLegal": {
				"Country": { "id": "RUS", "text": "Россия" },
				"id": 1464176793863,
				"russianAddress": { "Дом": "12", "Корпус": "", "Квартира": "", "Комната": "", "variant_fias": false, "free": { "Регион": "УР", "Район": "", "Город": "Сарапул", "НаселенныйПункт": "", "ГородскойРайон": "", "Улица": "1Мая" } }
			},
			"Birthplace": { "Country": { "id": "RUS", "text": "Россия" }, "Region": "", "Area": "", "City": "", "Town": "", "text": "Россия" },
			"LastName": "Сидоров",
			"FirstName": "Иван",
			"MiddleName": "Петрович",
			"LastName_latin": "Sidorov",
			"FirstName_latin": "Ivan",
			"MiddleName_latin": "Petrovich",
			"Sex": "M",
			"Birthday": "05.06.1978",
			"Phone": "",
			"user_document_serie": "1011",
			"user_document_number": "1111111",
			"user_document_dateFrom": "04.05.2016",
			"user_document_dateTo": "",
			"text": "Сидоров Иван Петрович",
			"user_document_type": { "id": 103008, "text": "Паспорт гражданина Российской Федерации" },
			"Nationality": { "id": "RUS", "text": "Россия" }
		}
	],
	"OrganizationPhone": "01",
	"supplierInfo": "1800000008002001",
	"employee_ummsId": "1001",
	"hotel_id": "123456",
	"CurrentColaborator": { "id": 1464176793863, "text": "Сидоров Иван Петрович" },
	"CurrentAddressOfPlacement": { "id": 1464172879074, "text": "УР, г Воткинск, ул Ленина, д.123, кв.12" },
	"subdivision": { "id": "3089", "text": "3089", "row": { "ID": "3089", "NAME": "МИЯКИНСКИЙ РОВД РЕСПУБЛИКИ БАШКОРТОСТАН", "CODE": "022-058" } },
	"sono": { "id": "117515", "text": "117515", "row": { "ID": "117515", "NAME": "Межрайонная инспекция Федеральной налоговой службы №6 по Удмуртской Республике", "CODE": "1821" } },
	"CheckResponsibleOfficier": true
} } } 
}