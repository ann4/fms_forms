{
	"CheckResponsibleOfficier": false,
	"AddressRealAbonent": {
		"Country": {
			"id": "RUS",
			"text": "Россия"
		},
		"russianAddress": {
			"variant_fias": true,
			"Дом": "11",
			"Корпус": "",
			"Квартира": "18",
			"Комната": "",
			"fias": [
				{
					"AOID": "082fa888-7a08-4891-a0b2-583eabbc6e0c",
					"AOGUID": "6932c008-d1c0-4d28-b00f-224c86596ae3",
					"AOLEVEL": "7",
					"IsTerminal": "1",
					"SHORTNAME": "ул",
					"FORMALNAME": "Буммашевская",
					"FullPath": "Респ Удмуртская$г Ижевск",
					"REGIONCODE": "18",
					"AREACODE": "000",
					"CITYCODE": "001",
					"PLACECODE": "000",
					"PLANCODE": "0000",
					"EXTRCODE": "0000",
					"STREETCODE": "0046",
					"SEXTCODE": "000"
				},
				{
					"AOID": "75178c92-9cf6-4f89-bd61-d486beb9706d",
					"AOGUID": "deb1d05a-71ce-40d1-b726-6ba85d70d58f",
					"AOLEVEL": "4",
					"IsTerminal": "0",
					"SHORTNAME": "г",
					"FORMALNAME": "Ижевск",
					"FullPath": "Респ Удмуртская",
					"REGIONCODE": "18",
					"AREACODE": "000",
					"CITYCODE": "001",
					"PLACECODE": "000",
					"PLANCODE": "0000",
					"EXTRCODE": "0000",
					"STREETCODE": "0000",
					"SEXTCODE": "000"
				},
				{
					"AOID": "ae58022e-2a56-4766-a331-8d99aca4d8c4",
					"AOGUID": "52618b9c-bcbb-47e7-8957-95c63f0b17cc",
					"AOLEVEL": "1",
					"IsTerminal": "0",
					"SHORTNAME": "Респ",
					"FORMALNAME": "Удмуртская",
					"FullPath": "",
					"REGIONCODE": "18",
					"AREACODE": "000",
					"CITYCODE": "000",
					"PLACECODE": "000",
					"PLANCODE": "0000",
					"EXTRCODE": "0000",
					"STREETCODE": "0000",
					"SEXTCODE": "000"
				}
			]
		}
	},
	"AddressesOfPlacement": [
		{
			"Country": {
				"id": "RUS",
				"text": "Россия"
			},
			"id": 1437917760000,
			"russianAddress": {
				"housing_types": true,
				"variant_fias": true,
				"Дом_Type": {
					"id": "1202",
					"text": "Дом"
				},
				"Квартира_Type": {
					"id": "1303",
					"text": "Квартира"
				},
				"Дом": "31",
				"Корпус": "",
				"Квартира": "18",
				"Комната": "",
				"fias": [
					{
						"AOID": "8e228a1a-d7e8-4a5a-ab6e-cf20f5d41f17",
						"AOGUID": "68072c2a-a5d1-4846-a636-f68d36281548",
						"AOLEVEL": "7",
						"IsTerminal": "1",
						"SHORTNAME": "ул",
						"FORMALNAME": "Авангардная",
						"FullPath": "Респ Удмуртская$г Ижевск",
						"REGIONCODE": "18",
						"AREACODE": "000",
						"CITYCODE": "001",
						"PLACECODE": "000",
						"PLANCODE": "0000",
						"EXTRCODE": "0000",
						"STREETCODE": "0001",
						"SEXTCODE": "000"
					},
					{
						"AOID": "75178c92-9cf6-4f89-bd61-d486beb9706d",
						"AOGUID": "deb1d05a-71ce-40d1-b726-6ba85d70d58f",
						"AOLEVEL": "4",
						"IsTerminal": "0",
						"SHORTNAME": "г",
						"FORMALNAME": "Ижевск",
						"FullPath": "Респ Удмуртская",
						"REGIONCODE": "18",
						"AREACODE": "000",
						"CITYCODE": "001",
						"PLACECODE": "000",
						"PLANCODE": "0000",
						"EXTRCODE": "0000",
						"STREETCODE": "0000",
						"SEXTCODE": "000"
					},
					{
						"AOID": "ae58022e-2a56-4766-a331-8d99aca4d8c4",
						"AOGUID": "52618b9c-bcbb-47e7-8957-95c63f0b17cc",
						"AOLEVEL": "1",
						"IsTerminal": "0",
						"SHORTNAME": "Респ",
						"FORMALNAME": "Удмуртская",
						"FullPath": "",
						"REGIONCODE": "18",
						"AREACODE": "000",
						"CITYCODE": "000",
						"PLACECODE": "000",
						"PLANCODE": "0000",
						"EXTRCODE": "0000",
						"STREETCODE": "0000",
						"SEXTCODE": "000"
					}
				]
			},
			"document": "",
			"text": "Респ Удмуртская, г Ижевск, ул Авангардная, д.31, кв.18"
		}
	],
	"ResponsibleOfficiers": [
		{
			"id": 1437917760000,
			"AddressLegal": {
				"Country": {
					"id": "RUS",
					"text": "Россия"
				},
				"id": 1437917760000,
				"russianAddress": {
					"variant_fias": true,
					"Дом": "7",
					"Корпус": "",
					"Квартира": "8",
					"Комната": "",
					"fias": [
						{
							"AOID": "dd2c6b49-6681-46bd-b51a-c3daa1a7a9fc",
							"AOGUID": "5a1f52ae-6930-482d-b861-fa1c943d1b85",
							"AOLEVEL": "7",
							"IsTerminal": "1",
							"SHORTNAME": "ул",
							"FORMALNAME": "Лихвинцева",
							"FullPath": "Респ Удмуртская$г Ижевск",
							"REGIONCODE": "18",
							"AREACODE": "000",
							"CITYCODE": "001",
							"PLACECODE": "000",
							"PLANCODE": "0000",
							"EXTRCODE": "0000",
							"STREETCODE": "0239",
							"SEXTCODE": "000"
						},
						{
							"AOID": "75178c92-9cf6-4f89-bd61-d486beb9706d",
							"AOGUID": "deb1d05a-71ce-40d1-b726-6ba85d70d58f",
							"AOLEVEL": "4",
							"IsTerminal": "0",
							"SHORTNAME": "г",
							"FORMALNAME": "Ижевск",
							"FullPath": "Респ Удмуртская",
							"REGIONCODE": "18",
							"AREACODE": "000",
							"CITYCODE": "001",
							"PLACECODE": "000",
							"PLANCODE": "0000",
							"EXTRCODE": "0000",
							"STREETCODE": "0000",
							"SEXTCODE": "000"
						},
						{
							"AOID": "ae58022e-2a56-4766-a331-8d99aca4d8c4",
							"AOGUID": "52618b9c-bcbb-47e7-8957-95c63f0b17cc",
							"AOLEVEL": "1",
							"IsTerminal": "0",
							"SHORTNAME": "Респ",
							"FORMALNAME": "Удмуртская",
							"FullPath": "",
							"REGIONCODE": "18",
							"AREACODE": "000",
							"CITYCODE": "000",
							"PLACECODE": "000",
							"PLANCODE": "0000",
							"EXTRCODE": "0000",
							"STREETCODE": "0000",
							"SEXTCODE": "000"
						}
					]
				}
			},
			"Birthplace": {
				"Country": null,
				"Region": null,
				"Area": null,
				"City": null,
				"Town": null,
				"text": ""
			},
			"LastName": "Хамагай",
			"FirstName": "Максим",
			"MiddleName": "Варлампиевич",
			"LastName_latin": "Khamagai",
			"FirstName_latin": "Maksim",
			"MiddleName_latin": "Varlampievich",
			"Sex": "M",
			"Birthday": "05.08.1977",
			"Phone": "447276",
			"user_document_serie": "5187",
			"user_document_number": "935715",
			"user_document_dateFrom": "05.08.2010",
			"user_document_dateTo": "05.08.2019",
			"user_document_givenby_dict": true,
			"user_document_proxy_type_company": "",
			"user_document_proxy_serie": "",
			"user_document_proxy_number": "",
			"user_document_proxy_dateFrom": "",
			"user_document_proxy_dateTo": "",
			"user_document_givenby_text": null,
			"user_document_givenby": {
				"id": "3977",
				"text": "3977",
				"row": {
					"ID": "3977",
					"NAME": "МВД УДМУРТСКОЙ РЕСПУБЛИКИ",
					"CODE": "181-001"
				}
			},
			"text": "Хамагай Максим Варлампиевич",
			"user_document_type": {
				"id": 103008,
				"text": "Паспорт гражданина Российской Федерации"
			},
			"Nationality": {
				"id": "RUS",
				"text": "Россия"
			}
		}
	],
	"OrganizationName": "ООО Подушка-Одеяло",
	"OKPO": "1234567890",
	"OrganizationPhone": "01",
	"supplierInfo": "1800000008002001",
	"employee_ummsId": "1001",
	"hotel_id": "123456",
	"StartPeriod": "",
	"FinishPeriod": "",
	"RoomAllCount": "",
	"RoomHighestCategory": "",
	"RoomSpecialCategory": "",
	"RoomArea": "",
	"CountPlaces": "",
	"CurrentColaborator": {
		"id": 1437917760000,
		"text": "Хамагай Максим Варлампиевич"
	},
	"CurrentAddressOfPlacement": {
		"id": 1437917760000,
		"text": "Респ Удмуртская, г Ижевск, ул Авангардная, д.31, кв.18"
	},
	"subdivision": {
		"id": "3089",
		"text": "3089",
		"row": {
			"ID": "3089",
			"NAME": "МИЯКИНСКИЙ РОВД РЕСПУБЛИКИ БАШКОРТОСТАН",
			"CODE": "022-058"
		}
	},
	"sono": {
		"id": "117515",
		"text": "117515",
		"row": {
			"ID": "117515",
			"NAME": "Межрайонная инспекция Федеральной налоговой службы №6 по Удмуртской Республике",
			"CODE": "1821"
		}
	},
	"PeriodOperation": null,
	"HotelType": null,
	"HotelCategory": null,
	"TaxSystem": null,
	"WellnessProfile": []
}