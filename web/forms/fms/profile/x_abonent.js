﻿define([
	  'forms/base/codec.json'
	, 'forms/base/log'
	, 'forms/fms/profile/h_abonent'
],
function (BaseCodec, GetLogger, h_abonent)
{
	var log = GetLogger('x_fms_profile_abonent');
	return function ()
	{
		log.Debug('Create {');
		var codec = BaseCodec();

		codec.AddressWithHousingType = true;

		codec.Encode = function (data)
		{
			return JSON.stringify(data, null, '\t').replace(/\n/g, '\r\n');
		};

		codec.Decode = function (json_string)
		{
			log.Debug('Decode {');
			var res = json_string;
			if ('string' == typeof json_string)
			{
				try
				{
					res = JSON.parse(json_string);
				}
				catch (ex)
				{
					log.Error('can not parse json :');
					log.Error(json_string);
					throw ex;
				}
			}
			res = h_abonent.SafeUpgradeAbonent(res);
			log.Debug('Decode }');
			return res;
		};

		log.Debug('Create }');
		return codec;
	}
});
