﻿define([
	  'forms/fms/attachments/c_attachments'
	, 'forms/fms/questionary/c_questionary'
	, 'forms/fms/questionary/ff_questionary'
	, 'forms/base/log'
],
function (BaseController, BaseFormController, formSpec, GetLogger)
{
	return function()
	{
		var log = GetLogger('c_a_questionary');

		var controller = BaseController(BaseFormController, formSpec);

		controller.SetStayPeriod = function (period) { }
		controller.GetStayPeriod = function () { return null; }

		controller.GetBody = function () {
			var body = 'Направляем документ "Анкета заселяющегося".';
			if ($('#cpw_form_LastName').val() != '') {
				body += '\r\nПрибывший  гражданин: ' + $('#cpw_form_LastName').val();
				if ($('#cpw_form_FirstName').val() != '')
					body += ' ' + $('#cpw_form_FirstName').val().substr(0, 1) + '.';
				if ($('#cpw_form_MiddleName').val() != '')
					body += '' + $('#cpw_form_MiddleName').val().substr(0, 1) + '.';
				var birthDate = $('#cpw_form_Birthday').val();
				if (birthDate) {
					var birthDateParts = birthDate.split('.');
					if (birthDateParts && birthDateParts.length == 3)
						body += ', ' + birthDateParts[2] + ' г.р.';
				}
			}
			return body;
		}

		controller.GetDefaultDocumentType = function()
		{
			var DocumentType = $('#cpw_form_DocumentType').select2('data');
			return !DocumentType ? { id: 103008, text: "Паспорт гражданина Российской Федерации"} : DocumentType;
		}

		controller.GetDocumentTypeValues = function(withDuplicate)
		{
			types = [];
			var DocumentType = $('#cpw_form_DocumentType').select2('data');
			if (DocumentType)
				types.push({ id: DocumentType.id, text: DocumentType.text, series: $('#cpw_form_DocumentSeries').val(), number: $('#cpw_form_DocumentNumber').val() });
			return types;
		}

		var base_DoValidateAttachments = controller.DoValidateAttachments;
		controller.DoValidateAttachments = function (res_txt) { return res_txt; }

		var ContainsTypeById = function (id, types)
		{
			for (var i = 0; i < types.length; i++)
			{
				if (types[i].id == id)
					return true;
			}
			return false;
		}

		return controller;
	}
});