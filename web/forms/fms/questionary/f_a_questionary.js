define([
	  'forms/fms/questionary/c_a_questionary'
	, 'forms/fms/questionary/ff_questionary'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_questionary'
		, Title: 'Анкета. Форма5'
		, FileFormat: FileFormat
	};
	return form_spec;
});
