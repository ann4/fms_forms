﻿define([
	'forms/base/h_constraints'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_citizenship'
	, 'forms/base/h_times'
],
function (h_constraints, h_addr, h_DocumentType, h_citizenship, h_times)
{
	return function () {
		var constraints =
		[
			  h_constraints.for_Field('LastName', h_constraints.NotEmpty())
			, h_constraints.for_Field('LastName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('FirstName', h_constraints.NotEmpty())
			, h_constraints.for_Field('FirstName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('MiddleName', h_constraints.recommend_NotEmpty('Не указано отчество гражданина'))
			, h_constraints.for_Field('MiddleName', h_constraints.recommend_Ciryllic())
			, h_constraints.for_Field('Birthday', h_constraints.Date())
			, { fields: ['Birthday']
				, check: h_constraints.recommend_check_BeforeDateNow
				, description: function (element, field) { return 'Недопустимая дата рождения'; }
			}
			, h_constraints.for_Field('Birthday', h_constraints.recommend_NotEmpty('Не указана дата рождения'))
			, { fields: ['Nationality']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_citizenship.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['DocumentType']
				, check: function (item) { return h_constraints.check_Select2Value(item, h_DocumentType.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, { fields: ['Birthday', 'DocumentType']
				, check: h_constraints.recommend_check_DocumentTypePassport
				, description: function (element, field) { return 'Паспорт РФ не выдается гражданам, не достигшим 14 лет'; }
			}
			, { fields: ['Birthday', 'DocumentType']
				, check: h_constraints.recommend_check_DocumentTypeNotPassport
				, description: function (element, field) { return 'Документом, удостоверяющим личность гражданина, достигшего 14 лет, является паспорт'; }
			}
			, { fields: ['Sex']
				, check: function (item) { return '' == item ? 'recommended' : true; }
				, description: function (element, field) { return 'Не указан пол гражданина'; }
			}
			, { fields: ['Sex', 'MiddleName']
				, check: h_constraints.recommend_check_SexByMiddleName
				, description: h_constraints.description_SexByMiddleName
			}
			, h_constraints.for_Field('Nationality', h_constraints.NotEmpty())
			, h_constraints.for_Field('Nationality', h_constraints.recommend_NationalityRus())
			, { fields: ['Birthplace']
				, check: function (item) { return !(!item || null == item || !item.Country || null == item.Country || '' == item.Country) ? true : 'recommended'; }
				, description: function (element, field) { return 'Не указано государство рождения'; }
			}
			, { fields: ['Birthday', 'Birthplace']
				, check: h_constraints.recommend_check_CountrySun
				, description: function (element, field) { return 'Государство рождения указано неверно. До 06.02.1992 года государство рождения - СССР'; }
			}
			, h_constraints.for_Field('DocumentType', h_constraints.NotEmpty())
			, { fields: ['DocumentType']
				, check: function (type) {
					return !h_constraints.check_Empty(type) && (h_DocumentType.needValuesQuestionary).indexOf(type.id) == -1 ? 'recommended' : true;
				}
				, description: function (element, field) {
					return 'Согласно российскому законодательству перечень документов, удостоверяющих личность гражданина ' +
					'при заселении в гостиницу, ограничен. Рекомендуется выбрать документ из предложенных в списке';
				}
			}
			, h_constraints.for_Field('DocumentSeries', h_constraints.recommend_NotEmpty('Не указана серия документа, удостоверяющего личность'))
			, h_constraints.for_Fields(['DocumentSeries', 'DocumentType'], h_constraints.recommend_SeriesPassportType())
			, { fields: ['DocumentNumber']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Поле "Номер документа" не может быть пустым'; }
			}
			, h_constraints.for_Fields(['DocumentNumber', 'DocumentType'], h_constraints.recommend_NumberPassportType())
			, { fields: ['DocumentGivenBy', 'DocumentGivenBy_text', 'DocumentGivenBy_check', 'DocumentType']
				, check: function (organ, txt, check, type) {
					if (h_constraints.check_Empty(type) && type.id != '103008')
						return true;
					else
						return (check && h_constraints.check_Empty(organ)) || ((!check || '' == check) && h_constraints.check_Empty(txt)) ? 'recommended' : true;
				}
				, description: function (element, field) {
					return 'Не указан орган, выдавший документ, удостоверяющий личность';
				}
}
			, h_constraints.for_Field('DocumentGivenDate', h_constraints.Date())
			, h_constraints.for_Field('DocumentGivenDate', h_constraints.recommend_NotEmpty('Не указана дата выдачи документа, удостоверяющего личность'))
			, { fields: ['Birthday', 'DocumentGivenDate']
				, check: function (birthday, dateDocumentGiven) { return h_constraints.recommend_check_PeriodDate(birthday, dateDocumentGiven); }
				, description: function (element, birthday, dateDocumentGiven) { return 'Дата документа, удостоверяющего личность, меньше даты рождения'; }
			}
			, { fields: ['RegAddress']
				, check: function (item) {
					return !item || null == item || !item.russianAddress || null == item.russianAddress || item.russianAddress.variant_fias ? true : 'recommended';
				}
				, description: function (element, field) {
					return 'Рекомендуется указывать адрес гражданина в соответствии с ФИАС';
				}
			}
			, { fields: ['RegAddress', 'DocumentType']
				, check: function (item, type) {
					return !h_constraints.check_Empty(type) && type.id != '103007' && '' == h_addr.SafePrepareReadableTextWithoutRoom(item, '') ? 'recommended' : true;
				}
				, description: function (element, field) {
					return 'Не указан адрес места жительства';
				}
			}
			, { fields: ['RegAddress', 'DocumentType']
				, check: function (item, type) {
					if (!h_constraints.check_Empty(type) && type.id == '103007' && '' != h_addr.SafePrepareReadableTextWithoutRoom(item, '')) {
						return item.Country && item.Country.id == 'RUS' ? 'recommended' : true;
					}
					return true;
				}
				, description: function (element, field) {
					return 'Заселение гражданина в гостиницу по загранпаспорту гражданина РФ допускается только в том случае, если он постоянно проживает за пределами Российской Федерации';
				}
			}
			, h_constraints.for_Field('DateFrom', h_constraints.Date())
			, h_constraints.for_Field('DateTill', h_constraints.Date())
			, { fields: ['RegAddress', 'Address', 'DateFrom', 'DateTill']
				, check: function (addrReg, addrStay, dateFrom, dateTill) {
					if ('' == h_addr.SafePrepareReadableTextWithoutRoom(addrReg, '') || '' == h_addr.SafePrepareReadableTextWithoutRoom(addrStay, ''))
						return true;
					if (h_constraints.check_Empty(dateFrom) || h_constraints.check_Empty(dateTill))
					{
						var period = 0;
					}
					else
					{
						var dFrom = new Date(dateFrom.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
						var dTill = new Date(dateTill.replace(/(\d+).(\d+).(\d+)/, '$2/$1/$3'));
						if (dFrom > dTill)
							return true;
						var period = (dTill - dFrom) / 86400000;
					}
					if (!addrReg || !addrReg.Country || addrReg.Country.id != 'RUS')
					    return true;
					if (!addrStay || !addrStay.Country || addrStay.Country.id != 'RUS')
					    return true;
					function getRegioncode(addr)
					{
						for (var i = 0; i < addr.length; i++)
						{
							if (addr[i].REGIONCODE)
								return addr[i].REGIONCODE;
						}
					}
					function getOldRegioncode(addr)
					{
						var parts = addr.russianAddress.fias.old_fias_text.split('$');
						return parts.length > 1 ? parts[1] : null;
					}
					var regionReg = !addrReg || null == addrReg || !addrReg.russianAddress || null == addrReg.russianAddress ? null :
						addrReg.russianAddress.variant_fias && addrReg.russianAddress.fias.length > 0 ? getRegioncode(addrReg.russianAddress.fias) :
						!addrReg.russianAddress.variant_fias && addrReg.russianAddress.free ? addrReg.russianAddress.free.Регион :
						addrReg.russianAddress.variant_fias && addrReg.russianAddress.fias.old_fias_text ? getOldRegioncode(addrReg) : null;
					var regionStay = !addrStay || null == addrStay || !addrStay.russianAddress || null == addrStay.russianAddress ? null :
						addrStay.russianAddress.variant_fias && addrStay.russianAddress.fias.length > 0 ?  getRegioncode(addrStay.russianAddress.fias) :
						!addrStay.russianAddress.variant_fias && addrStay.russianAddress.free ? addrStay.russianAddress.free.Регион :
						addrStay.russianAddress.variant_fias && addrStay.russianAddress.fias.old_fias_text ? getOldRegioncode(addrStay) : null;
					return !(regionReg == regionStay && period <= 90);
				}
				, description: function (element, field) {
					return 'При прибытии в гостиницу гражданина, имеющего постоянную регистрацию (прописку) в том же регионе РФ, ' +
					'в котором находится гостиница, уведомление в ФМС отправлять не нужно в случае, если срок пребывания гражданина не превышает 90 дней';
				}
			}
			, { fields: ['RegAddress']
				, check: function (item) {
					if (h_constraints.check_Empty(item) || !item.russianAddress || null == item.russianAddress ||
						(item.russianAddress.variant_fias && item.russianAddress.fias && item.russianAddress.fias.length == 0))
						return true;
					var housing = item.russianAddress.Дом;
					if (!housing || null == housing || '' == housing)
						return 'recommended';
					else
						return true;
				}
				, description: function (element, field) {
					return 'Не указан номер дома';
				}
			}
			, h_constraints.for_Field('Hotel_organization_name', h_constraints.NotEmpty())
			, h_constraints.for_Field('Hotel_organization_inn', h_constraints.Inn())
			, { fields: ['Address']
				, check: function (item) { return '' != h_addr.SafePrepareReadableTextWithoutRoom(item, ''); }
				, description: function (element, field) { return 'Поле "Адрес места пребывания" не может быть пустым'; }
			}
			, { fields: ['DateFrom']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) { return 'Дата начала регистрации не может быть пустой'; }
			}
			, { fields: ['DateFrom', 'Birthday']
				, check: function (dateFrom, birthday) { return h_constraints.recommend_check_PeriodDate(birthday, dateFrom); }
				, description: function (element, dateFrom, birthday) { return 'Дата заселения гражданина меньше даты рождения'; }
			}
			, { fields: ['DateFrom']
				, check: function (dateFrom) { return h_constraints.check_BeforeDateNow(dateFrom); }
				, description: function (element, dateFrom) { return 'Дата прибытия указана неверно (в будущем, ведь сегодня ' + h_times.safeDateTime().toLocaleDateString() + ')'; }
			}
			, h_constraints.for_Field('DateTill', h_constraints.recommend_NotEmpty('Не указана дата окончания проживания'))
			, { fields: ['DateFrom', 'DateTill']
				, check: function (dateFrom, dateTill) { return h_constraints.recommend_check_PeriodDate(dateFrom, dateTill); }
				, description: function (element, dateFrom, dateTill) { return 'Недопустимая дата окончания проживания'; }
			}
			, h_constraints.for_Field('AppartmentNumber', h_constraints.recommend_NotEmpty('Не указан номер комнаты проживания'))
			, { fields: ['DateFrom']
				, check: function (dateFrom) { return h_constraints.recommend_check_DateNotAfterOneDay(dateFrom); }
				, description: function (element, dateFrom) {
					return 'Анкета заселяющегося по форме №5 должна предоставляться администрацией гостиничного учреждения в течение суток с момента заселения, а сегодня '
						+ h_times.safeDateTime().toLocaleDateString();
				}
			}
		];
		return constraints;
	}
});
