define([
	  'forms/fms/base/x_fms_csv'
	, 'forms/fms/questionary/m_questionary'
	, 'forms/base/log'
	, 'forms/fms/uploading/csv.sample.kontur'
	, 'forms/fms/uploading/csv.sfera'
	, 'forms/fms/uploading/csv.16052018'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/h_country'
	, 'forms/base/h_names'
],
function (
	  BaseCodec
	, model_questionary
	, GetLogger
	, csvFormat
	, csvSfera
	, csv16052018
	, helper_DocumentType
	, helper_citizenship
	, helper_country
	, helper_Names
) {
	var log = GetLogger('x_csv_questionary');
	return function () {
		log.Debug('Create {');
		var res = BaseCodec();

		var base_Decode = res.Decode;
		res.Decode = function (data_string) {
			var data = data_string.split(';');
			if (1 >= data.length) {
				data = data_string.split('","');
				if (1 < data.length) {
					var lastIndex = data.length - 1;
					data[0] = data[0].substring(1, data[0].length);
					data[lastIndex] = data[lastIndex].replace(/\n/g, '').replace(/\"/g, '');
				}
			}
			return base_Decode.call(this, data);
		};

		var base_DecodeKonturCsvDocument = res.DecodeKonturCsvDocument;
		res.DecodeKonturCsvDocument = function (data) {
			log.Debug('DecodeKonturCsvDocument {');
			if (data[0] != '0') {
				var err = { parseException: true, readablemsg: 'Неверный формат' };
				log.Debug('throw parsing error..');
				log.Debug('Неверный формат');
				throw err;
			}
			var m_questionary = model_questionary();
			this.DecodePerson(data, m_questionary, csvFormat);
			this.DecodeBirthPlace(data, m_questionary, csvFormat);
			this.DecodeDocument(data, m_questionary, csvFormat);
			var documentGivenBy = this.DecodeDocumentGivenBy(data, csvFormat.DocumentGivenBy);
			if ('string' == typeof documentGivenBy) {
				m_questionary.DocumentGivenBy_text = documentGivenBy;
				m_questionary.DocumentGivenBy_from_dict = false;
			} else {
				m_questionary.DocumentGivenBy = documentGivenBy;
				m_questionary.DocumentGivenBy_from_dict = true;
			}
			m_questionary.RegAddress = this.DecodeLivingAddress(data, m_questionary, csvFormat);
			m_questionary.AppartmentNumber = this.GetValue(data, csvFormat.AppartmentNumber);
			m_questionary.DateFrom = this.GetValue(data, csvFormat.DateFrom);
			m_questionary.DateTill = this.GetValue(data, csvFormat.DateTo);
			log.Debug('DecodeKonturCsvDocument }');
			return m_questionary;
		};

		var base_DecodeSferaCsvDocument = res.DecodeSferaCsvDocument;
		res.DecodeSferaCsvDocument = function (data) {
			log.Debug('DecodeSferaCsvDocument {');
			if (data[0] != '40') {
				var err = { parseException: true, readablemsg: 'Неверный формат' };
				log.Debug('throw parsing error..');
				log.Debug('Неверный формат');
				throw err;
			}
			var m_questionary = model_questionary();
			m_questionary.LastName = helper_Names.NormalizeNameItem(data[csvSfera.LastName]);
			m_questionary.FirstName = helper_Names.NormalizeNameItem(data[csvSfera.FirstName]);
			m_questionary.MiddleName = helper_Names.NormalizeNameItem(data[csvSfera.MiddleName]);
			m_questionary.Sex = this.DecodeSex(null, m_questionary.MiddleName);
			m_questionary.Birthday = this.GetValue(data, csvSfera.BirthDay);
			m_questionary.Nationality = { id: "RUS", text: "Россия" };
			this.DecodeBirthPlace(data, m_questionary, csvSfera);
			this.DecodeDocument(data, m_questionary, csvSfera);
			m_questionary.DocumentType = this.GetDictionaryValueByText(data, csvSfera.Document, helper_DocumentType);
			var documentGivenBy = this.DecodeDocumentGivenBy(data, csvSfera.DocumentGivenBy);
			if ('string' == typeof documentGivenBy) {
				m_questionary.DocumentGivenBy_text = documentGivenBy;
				m_questionary.DocumentGivenBy_from_dict = false;
			} else {
				m_questionary.DocumentGivenBy = documentGivenBy;
				m_questionary.DocumentGivenBy_from_dict = true;
			}
			m_questionary.RegAddress = this.DecodeLivingAddress(data, m_questionary, csvSfera);
			var appartmentNumber = this.GetValue(data, csvSfera.AppartmentNumber);
			if (null != appartmentNumber) {
				var pos = appartmentNumber.indexOf('_');
				m_questionary.AppartmentNumber = -1 < pos ? appartmentNumber.substring(0, pos) : appartmentNumber;
			}
			m_questionary.DateFrom = this.GetValue(data, csvSfera.DateFrom);
			m_questionary.DateTill = this.GetValue(data, csvSfera.DateTo);
			log.Debug('DecodeSferaCsvDocument }');
			return m_questionary;
		};

		var base_Decode16CsvDocument = res.Decode16CsvDocument;
		res.Decode16CsvDocument = function (data) {
			log.Debug('Decode16CsvDocument {');
			if (data[0] != '0') {
				var err = { parseException: true, readablemsg: 'Неверный формат' };
				log.Debug('throw parsing error..');
				log.Debug('Неверный формат');
				throw err;
			}
			var m_questionary = model_questionary();
			this.DecodePerson(data, m_questionary, csv16052018);
			this.DecodeBirthPlace(data, m_questionary, csv16052018);
			this.DecodeDocument(data, m_questionary, csv16052018);
			var documentGivenBy = this.DecodeDocumentGivenBy(data, csv16052018.DocumentGivenBy);
			if ('string' == typeof documentGivenBy) {
				m_questionary.DocumentGivenBy_text = documentGivenBy;
				m_questionary.DocumentGivenBy_from_dict = false;
			} else {
				m_questionary.DocumentGivenBy = documentGivenBy;
				m_questionary.DocumentGivenBy_from_dict = true;
			}
			m_questionary.RegAddress = this.DecodeLivingAddress(data, m_questionary, csv16052018);
			m_questionary.AppartmentNumber = this.GetValue(data, csv16052018.AppartmentNumber);
			m_questionary.DateFrom = this.GetValue(data, csv16052018.DateFrom);
			m_questionary.DateTill = this.GetValue(data, csv16052018.DateTo);
			log.Debug('Decode16CsvDocument }');
			return m_questionary;
		};

		log.Debug('Create }');
		return res;
	}
});
