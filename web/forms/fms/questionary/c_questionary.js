define([
	  'forms/fms/base/c_fms'
	, 'forms/fms/questionary/codec_questionary'
	, 'forms/fms/base/town/c_town'
	, 'forms/fms/questionary/m_questionary'
	, 'forms/fms/base/town/h_town'
	, 'forms/base/h_names'
	, 'forms/base/h_times'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base//h_Select2'
	, 'forms/fms/base/h_validate_n'
	, 'tpl!forms/fms/questionary/e_questionary.html'
	, 'tpl!forms/fms/questionary/v_questionary.xaml'
	, 'forms/base/log'
	, 'tpl!forms/fms/questionary/p_questionary.html'
	, 'forms/fms/profile/h_abonent'
	, 'forms/fms/base/addr/c_addr'
	, 'forms/base/h_msgbox'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/addr_variants/c_addr_variants'
	, 'forms/fms/base/addr/c_addr_modal'
	, 'forms/fms/questionary/r_questionary'
	, 'forms/base/h_dictionary'
	, 'forms/fms/guides/dict_officialorgan_fns.csv'
	, 'forms/fms/guides/dict_officialorgan_fms.csv'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/russ_addr_fias/h_fias'
],
function 
(
	  BaseFormController
	, codec
	, controller_Town
	, model_Questionary
	, helper_Town
	, helper_Names
	, helper_Times
	, helper_DocumentType
	, helper_Select2
	, helper_validate
	, layout_tpl
	, print_tpl
	, GetLogger
	, attrshtml_tpl
	, h_abonent
	, c_addr
	, h_msgbox
	, h_addr
	, c_addr_variants
	, c_addr_modal
	, r_questionary
	, h_dictionary
	, fns
	, fms
	, h_citizenship
	, h_fias
	)
{
	return function ()
	{
		var log = GetLogger('c_questionary');

		var Select2Options = function (h_values)
		{
			return function (m)
			{
				return {
					placeholder: '',
					allowClear: true,
					query: function (q)
					{
						q.callback({ results: helper_Select2.FindForSelect(h_values(), q.term) });
					}
				};
			}
		}

		var AddressTextFunc = function (item)
		{
			return !(!item || null == item || !item.text || null == item.text || '' == item.text)
					? item.text : 'Кликните, чтобы указать..';
		}

		var AddressTextFunc2 = function (item)
		{
			return h_addr.SafePrepareReadableText(item, 'Кликните, чтобы указать..');
		}

		var AddressTextFunc2WithoutRoom = function (item)
		{
			return h_addr.SafePrepareReadableTextWithoutRoom(item, 'Кликните, чтобы указать..');
		}

		var СreateCreateControllerVariants = function (model, field_name, title, onlyfias, default_fias)
		{
			var createController = function (item, onAfterSave)
			{
				var res =
				{
					ShowModal: function (e)
					{
						e.preventDefault();
						var controller = c_addr_variants(model.Hotel_address_variants);
						var field_value = model[field_name];
						if (onlyfias) {
							if (!field_value)
								field_value = {};
							if (!field_value.russianAddress)
							    field_value.russianAddress = {};
							field_value.russianAddress.housing_types = true;
							if (!field_value.russianAddress.onlyfias)
							{
								field_value.russianAddress.variant_fias = true;
								field_value.russianAddress.onlyfias = true;
							}
						}
						if (!default_fias) {
							if (!field_value)
								field_value = {};
							if (!field_value.russianAddress)
								field_value.russianAddress = {};
							field_value.russianAddress.fias_version = h_fias.GetFiasVersionBySchemaVersion(model);
						}
						if (null != field_value)
							controller.SetFormContent(field_value.russianAddress);
						var bname_Ok = 'Сохранить';
						h_msgbox.ShowModal({
							controller: controller
							, width: 750
							, height: 420
							, title: !title ? 'Адрес' : title
							, buttons: [bname_Ok, 'Отменить']
							, onclose: function (bname)
							{
								if (bname_Ok == bname)
								{
									model[field_name] =
									{
										Country: { id: 'RUS', text: "Россия" }
										,russianAddress: controller.GetFormContent()
									};
									onAfterSave();
								}
							}
						});
					}
				};
				return res;
			};
			return createController;
		}

		var controller = BaseFormController(layout_tpl,
		{
			  constraints: r_questionary()
			, field_spec:
			{
				DocumentType: Select2Options(helper_DocumentType.GetValuesQuestionary)
				, Nationality: Select2Options(h_citizenship.GetValues)
				, Birthplace:
				{
					text: AddressTextFunc
					, controller: function (item, onAfterSave) { return controller_Town(item, onAfterSave); }
				}
				, RegAddress: function (model)
				{
					var res = { text: AddressTextFunc2, controller: c_addr_modal(model, 'RegAddress', 'Место постоянного проживания', false, false) };
					return res;
				}
				, Address: function (model)
				{
					var createController = СreateCreateControllerVariants(model, 'Address', 'Место пребывания', true, false);
					var res = { text: AddressTextFunc2WithoutRoom, controller: createController };
					return res;
				}
			}
		});

		controller.UseProfile = function (profile)
		{
			h_abonent.CheckResponsibleOfficiers(profile);
			this.SafeCreateNewContent();
			if (profile && profile.Abonent)
			{
				if (!this.model.Hotel_organization_name || '' == this.model.Hotel_organization_name)
				{
					if (profile.Abonent.ShortName)
					{
						this.model.Hotel_organization_name = profile.Abonent.ShortName;
					}
					else if (profile.Abonent.Name)
					{
						this.model.Hotel_organization_name = profile.Abonent.Name;
					}
				}
				if (!this.model.Hotel_organization_inn || '' == this.model.Hotel_organization_inn)
				{
					if (profile.Abonent.Inn)
						this.model.Hotel_organization_inn = profile.Abonent.Inn;
				}
			}
			if (profile && profile.Extensions && profile.Extensions.fms)
			{
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent)
				{
					var pAbonent = pfms.Abonent;
					pAbonent = h_abonent.SafeUpgradeAbonent(pAbonent);
					this.model.subdivision = h_abonent.SafeGet_dict_field(pAbonent, 'subdivision', this.model.subdivision);
					if (pAbonent.supplierInfo)
						this.model.supplierInfo = pAbonent.supplierInfo;

					if (pAbonent.AddressesOfPlacement && pAbonent.AddressesOfPlacement.length > 0)
					{
						this.model.Hotel_address_variants = pAbonent.AddressesOfPlacement;
						var defAddressesOfPlacement = h_abonent.SafeGetDefaultAddressesOfPlacement(pAbonent);
						if (defAddressesOfPlacement && null != defAddressesOfPlacement) {
						    this.model.Address = h_addr.ConvertFromOldFormat(defAddressesOfPlacement);
						}
						ConvertAddressWithoutRoom(this.model);
					}

					if (pAbonent.employee_ummsId)
						this.model.employee.ummsId = pAbonent.employee_ummsId;

					controller.ConvertOldFiasAddressesToNew();
				}
				if (pfms.Forma)
				{
					this.ShowAttachmentsPanel = pfms.Forma.ShowAttachmentsPanel;
				}
			}
		}

		var ConvertAddressWithoutRoom = function(model)
		{
			if (model && model.Address && null != model.Address)
			{
				var addressHasRoom = model.Address.russianAddress && null != model.Address.russianAddress && 
				model.Address.russianAddress.Квартира_Type && '1304' == model.Address.russianAddress.Квартира_Type.id;
				if (addressHasRoom)
				{
					model.Комната = model.AppartmentNumber || model.Address.russianAddress.Квартира;
					model.Address.russianAddress.Квартира_Type = null;
					model.Address.russianAddress.Квартира = null;
				}
			}
		}

		var formatResult = function (item)
		{
			return '<div class="cpw-fms-official-organ"><div class="code">код:' +
				item.row.CODE + '</div><div class="name">' +
				item.row.NAME + '</div></div>';
		}

		var formatSelection = function (item)
		{
			if (!item.row)
			{
				return item.text;
			}
			else
			{
				return '<div class="cpw-fms-official-organ" title="' +
					item.row.NAME + '"><div class="name">' +
					item.row.NAME + '</div><div class="code">код:<br/>' +
					item.row.CODE + '</div></div>';
			}
		}

		var DataLoad = function (content)
		{
			controller.DataLoadSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content.Sex)

			var visible = $('#cpw_form_DocumentGivenBy').is(':visible');
			$('#cpw_form_DocumentGivenBy').select2('data', content.DocumentGivenBy);
			if (!visible)
				$('#s2id_cpw_form_DocumentGivenBy').hide();
			$('#cpw_form_DocumentGivenBy_check').attr('checked', content.DocumentGivenBy_from_dict ? 'checked' : null).change();
			$('#cpw_form_DocumentGivenBy_text').val(content.DocumentGivenBy_text);
			controller.ConvertOldFiasAddressesToNew();
		}

		var DataSave = function (content)
		{
			if (content && content.subdivision)
			{
				var itemSubdivision = h_dictionary.FindFirstByField(fms, 'ID', content.subdivision.id)
				if (itemSubdivision && itemSubdivision.row.CODE.substring(0, 2) == "91")
				{
					content.FirstName = content.FirstName.toUpperCase();
					content.LastName = content.LastName.toUpperCase();
					content.MiddleName = content.MiddleName.toUpperCase();
				}
			}

			content.Sex = controller.DataSaveSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content);

			content.DocumentGivenBy = $('#cpw_form_DocumentGivenBy').select2('data');
			content.DocumentGivenBy_from_dict = ('checked' == $('#cpw_form_DocumentGivenBy_check').attr('checked'));
			content.DocumentGivenBy_text = $('#cpw_form_DocumentGivenBy_text').val();

			if (!content.date)
				content.date = helper_Times.nowLocalISOStringDateTime();

			if (!content.DocumentStatus)
				content.DocumentStatus = { id: 102877, text: 'Действительный' };

			if (!content.Document_entered)
				content.Document_entered = true;

			if (!content.headOrganization)
				content.headOrganization = "true";

			var txt_creation = helper_Times.unixDateTimeStamp();

			controller.SafeFixUids(content, txt_creation, ['number', 'uid', 'requestId',
				'Hotel_organization_uid', 'person_uid', 'person_personId', 'Document_uid']);
		}

		controller.ConvertOldFiasAddressesToNew = function () {
			var self = this;
			if (this.model.Address && null != this.model.Address) {
				h_fias.ConvertOldFiasToNew(this.model.Address, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.Address = res;
					self.model.Address.text = h_addr.PrepareReadableText(self.model.Address);
					$('#cpw_form_Address').text(h_addr.SafePrepareReadableText(self.model.Address));
				});
			}
		}

		var base_Render = controller.Render;
		controller.Render = function (id_form_div)
		{
			base_Render.call(this, id_form_div);

			var elLastName = $('#cpw_form_LastName');

			elLastName.change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_FirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_MiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$('#cpw_form_Birthday').datepicker(helper_Times.DatePickerOptionsPrevious);
			$('#cpw_form_DocumentGivenDate').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DateFrom').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DateTill').datepicker(helper_Times.DatePickerOptions);

			$('#cpw_form_DocumentGivenBy').select2({
				placeholder: ''
				, allowClear: true
				, dropdownCssClass: "bigdrop"
				, query: function (q) { q.callback({ results: h_dictionary.FindByNameOrCode(fms, q.term) }); }
				, escapeMarkup: function (m) { return m; }
				, formatResult: formatResult
				, formatSelection: formatSelection
			});

			$('#cpw_form_DocumentGivenBy_check').change(function (e) {
				var checked = $(this).attr('checked');
				if ('checked' == checked) {
					$('#s2id_cpw_form_DocumentGivenBy').show();
					$('#cpw_form_DocumentGivenBy_text').hide();
				}
				else {
					$('#s2id_cpw_form_DocumentGivenBy').hide();
					$('#cpw_form_DocumentGivenBy_text').show();
				}
			});

			var self = this;
			$('.cpw_questionary_form .datepicker').on("change", function (e) { helper_validate.OnChangeDateInput_FixFormat(e); });
			$('#cpw_form_Birthday').on("change", function (e)
			{
				if (!self.model.Birthplace.Country)
				{
					var old_txtdt = $('#cpw_form_Birthday').val();
					var dt = helper_Times.ParseRussianDate(old_txtdt);
					var bound_dt = new Date(1991, 11, 26);
					var country = dt < bound_dt
						? { id: 'SUN', text: 'СССР' }
						: { id: 'RUS', text: 'Россия' };
					self.model.Birthplace.Country = country;
					self.model.Birthplace.text = helper_Town.ToReadableStringBirthplace(self.model.Birthplace);
				}
			});

			$('.encoding_date').change(helper_Names.OnNameChange_NormalizeEncoding);
			DataLoad(this.model);
		}

		controller.SafeCreateNewContent = function ()
		{
			if (!this.model)
				this.model = model_Questionary();
		}

		var RemoveHadFocus = function ()
		{
			$('#cpw_form_Nationality').parent().removeClass('had_focus');
			$('#cpw_form_DocumentType').parent().removeClass('had_focus');
			$('#cpw_form_DocumentGivenBy_check').parent().parent().removeClass('had_focus');
			$('#cpw_form_RegAddress').parent().parent().removeClass('had_focus');
			$('input').parent().parent().removeClass('had_focus');
		}

		controller.CreateNew = function (id_form_div)
		{
			this.SafeCreateNewContent();

			this.Render(id_form_div);
			$('#cpw_form_Nationality').select2('data', { id: 'RUS', text: "Россия" }).change();
			if (!this.model.DocumentType)
				$('#cpw_form_DocumentType').select2('data', { id: 103008, text: "Паспорт гражданина Российской Федерации" }).change();
			if (!this.model.DocumentGivenBy_text || $('#cpw_form_DocumentGivenBy_text').val() == '')
				$('#cpw_form_DocumentGivenBy_check').attr('checked', 'checked').change();
			
			RemoveHadFocus();
			this.BeforeEditValidation();
		};

		controller.Edit = function (form_div_selector)
		{
			this.Render(form_div_selector);
			RemoveHadFocus();
			this.BeforeEditValidation();
		};

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return attrshtml_tpl(this.model);
		};

		controller.PrepareDocumentGivenByLinesToPrint = function ()
		{
			var first_line_len = 30;
			var text = '';
			if (!this.model.DocumentGivenBy_from_dict)
			{
				text = this.model.DocumentGivenBy_text;
			}
			else
			{
				text = !this.model.DocumentGivenBy || !this.model.DocumentGivenBy.text ? '' : this.model.DocumentGivenBy.text;
			}
			if (text.length <= first_line_len)
			{
				return [text, ''];
			}
			else
			{
				return [text.substring(0, first_line_len), text.substring(first_line_len)];
			}
		}

		controller.BuildXamlView = function (id_form_div)
		{
			var RegAddress_parts = (this.model.RegAddress && 'RUS'!=this.model.RegAddress.Country.id)
				? this.model.RegAddress : h_addr.PrepareAddressParts(this.model.RegAddress);
			var content = print_tpl({
				form: this.model
				, helper_Town: helper_Town
				, helper_Times: helper_Times
				, Address: h_addr.SafePrepareReadableTextWithoutRoom(this.model.Address, '')
				, RegAddress: RegAddress_parts
				, DocumentGivenByLines: this.PrepareDocumentGivenByLinesToPrint()
			});
			return content;
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			if (this.SignedContent)
			{
				return this.SignedContent;
			}
			else
			{
				this.model = base_GetFormContent.call(this);
				log.Debug('GetFormContent() {');
				DataSave(this.model);
				if (this.model && this.model.Address && this.model.Address.russianAddress)
					this.model.Address.russianAddress.Комната = this.model.AppartmentNumber;
				log.Debug('GetFormContent() }');
				return this.model;
			}
		};

		controller.GetShortFormContent = function ()
		{
			log.Debug("GetShortFormContent {");
			var region = null;
			if (this.model.RegAddress && this.model.RegAddress.russianAddress) {
				if (!this.model.RegAddress.russianAddress.variant_fias) {
					region = this.model.RegAddress.russianAddress.free.Регион;
				} else {
					var itemFias = null;
					for (var i = 0; i < this.model.RegAddress.russianAddress.fias.length; i++) {
						if (this.model.RegAddress.russianAddress.fias[i].AOLEVEL == 1) {
							itemFias = this.model.RegAddress.russianAddress.fias[i];
							break;
						}
					}
					if (null != itemFias) {
						region = itemFias.REGIONCODE + " " + itemFias.text;
					}
				}
			}
			var shortContent = {
				LastName: this.model.LastName,
				FirstName: this.model.FirstName,
				MiddleName: this.model.MiddleName,
				LastName_latin: !this.model.LastName_latin || '' == this.model.LastName_latin ? helper_Names.PrepareLatinNameItem(this.model.LastName) : '',
				FirstName_latin: !this.model.FirstName_latin || '' == this.model.FirstName_latin ? helper_Names.PrepareLatinNameItem(this.model.FirstName) : '',
				MiddleName_latin: !this.model.MiddleName_latin || '' == this.model.MiddleName_latin ? helper_Names.PrepareLatinNameItem(this.model.MiddleName) : '',
				Gender: this.model.Sex,
				BirthDate: this.model.Birthday,
				Citizenship: this.model.Nationality,
				DocumentPersonalType: this.model.DocumentType,
				DocumentPersonalSerial: this.model.DocumentSeries,
				DocumentPersonalNumber: this.model.DocumentNumber,
				DateStart: this.model.DateFrom,
				DateFinish: this.model.DateTill,
				DocumentResidenceType: null,
				DocumentResidenceSeries: null,
				DocumentResidenceNumber: null,
				VisitPurpose: null,
				RegAddressRegion: null == region || '' == region ? "Без регистрации" : region
			};
			log.Debug("GetShortFormContent }");
			return shortContent;
		}

		controller.Sign = function ()
		{
			var content = this.GetFormContent();
			if (null != content && null != content)
			{
				var wax = CPW_Singleton();
				if (wax)
				{
					var pos = this.model.PositionForSignature;
					if (null == pos)
					{
						this.SignedContent = content;
					}
					else
					{
						var signature = wax.SignXml(content);
						this.SignedContent = content.substring(0, pos)
							+ signature
							+ content.substring(pos, content.length);
					}
				}
			}
		};

		controller.SetFormContent = function (content)
		{
			this.model = content;

			if (this.model && this.model.subdivision)
			{
				var itemSubdivision = h_dictionary.FindFirstByField(fms, 'ID', this.model.subdivision.id)
				if (itemSubdivision && itemSubdivision.row.CODE.substring(0, 2) == "91")
				{
					this.model.FirstName = helper_Names.NormalizeNameItem(this.model.FirstName);
					this.model.LastName = helper_Names.NormalizeNameItem(this.model.LastName);
					this.model.MiddleName = helper_Names.NormalizeNameItem(this.model.MiddleName);
				}
			}
			
			if (content.DocumentGivenBy && !content.DocumentGivenBy.row && content.DocumentGivenBy.id) {
				var item = h_dictionary.FindById(fms, content.DocumentGivenBy.id)
				if (item)
					content.DocumentGivenBy.row = item.row;
			}

			if (this.model && this.model.Address && this.model.Address.russianAddress)
				this.model.AppartmentNumber= this.model.Address.russianAddress.Комната;

			return null;
		};

		controller.CopyFormContent = function (form_content)
		{
			if (form_content) {
				this.model.LastName = form_content.LastName;
				this.model.FirstName = form_content.FirstName;
				this.model.MiddleName = form_content.MiddleName;
				this.model.Sex = form_content.Sex;
				this.model.Birthday = form_content.Birthday;
				this.model.Birthplace = form_content.Birthplace;
				this.model.Nationality = form_content.Nationality;
				this.model.DocumentType = form_content.DocumentType;
				this.model.DocumentSeries = form_content.DocumentSeries;
				this.model.DocumentNumber = form_content.DocumentNumber;
				this.model.DocumentGivenDate = form_content.DocumentGivenDate;
				this.model.DocumentGivenBy = form_content.DocumentGivenBy;
				this.model.DocumentGivenBy_text = form_content.DocumentGivenBy_text;
				this.model.DocumentGivenBy_from_dict = form_content.DocumentGivenBy_from_dict;
				this.model.RegAddress = form_content.RegAddress;
				this.model.AppartmentNumber = form_content.AppartmentNumber;
				this.model.DateFrom = form_content.DateFrom;
				this.model.DateTill = form_content.DateTill;
			}
		}

		controller.PrepareValidationElementText = function (txt, txt_to_add)
		{
			if (null == txt)
			{
				return txt_to_add;
			}
			else
			{
				return txt + ',\r\n     ' + txt_to_add;
			}
		}

		var TryToFindValidationErrors = function (rules)
		{
			if (rules)
			{
				var res_validation_text = null;
				for (var i = 0; i < rules.length; i++)
				{
					var rule = rules[i];
					if (rule && rule.attributes)
					{
						for (var j = 0; j < rule.attributes.length; j++)
						{
							var attribute = rule.attributes[j];
							var element =
								('object' != typeof attribute) ? attribute :
								attribute.selector ? $(attribute.selector) :
								attribute.element;
							var title;
							if ('object' != typeof attribute)
							{
								title = 'непоименованный элемент';
							}
							else
							{
								try
								{
									title = attribute.title ? attribute.title : element.attr('id');
								}
								catch (ex)
								{
									title = 'непоименованный элемент';
								}
							}
							if (!rule.handler || !rule.handler(element))
								res_validation_text = controller.PrepareValidationElementText(res_validation_text, title);
						}
					}
				}
			}
			return res_validation_text;
		}

		controller.PrepareValidationRuleForProfileFields = function ()
		{
			var content = this.model;
			var rule = {
				handler: helper_validate.RequiredValues,
				attributes:
				[
					{ element: content.supplierInfo, title: 'Идентификатор поставщика' },
					{ element: content.employee.ummsId, title: 'Идентификатор пользователя' },
					{ element: content.subdivision, title: 'Территориальный орган / структурное подразделение ФМС' },
				]
			};
			return rule;
		}

		controller.TryToFindValidationErrorsAddresses = function ()
		{
			try
			{
				var res_validation_text = '';
				if (!h_addr.only_free_address_by_substitusion_id(this.model.subdivision.id))
				{
					if (this.model.Address && this.model.Address.russianAddress && !this.model.Address.russianAddress.variant_fias)
					{
						this.model.Address = null;
						$('#cpw_form_Address').text(AddressTextFunc2(this.model.Address));
						res_validation_text += 'Адрес места пребывания необходимо заполнить по справочнику ФИАС!';
					}
				}
				return res_validation_text;
			}
			catch (err)
			{
				log.Error(err);
				return '';
			}
		}

		controller.BeforeEditValidation = function ()
		{
			var rules = [this.PrepareValidationRuleForProfileFields()];
			var res_txt = '';
			var errors = TryToFindValidationErrors(rules);
			if (null != errors)
				res_txt = helper_validate.ProfileErrorText + ':\r\n\r\n     ' + errors;
			errors = this.TryToFindValidationErrorsAddresses();
			if ('' != errors)
				res_txt += '' == res_txt ? errors : '\r\n\r\n' + errors;
			if (null != res_txt && '' != res_txt)
			{
				h_msgbox.ShowModal({
					title: "Недостаточно заполненный профиль для заполнения формы!"
					, html: '<pre>' + res_txt + '</pre>'
					, width: 800
				});
			}
		}

		controller.Validate = function ()
		{
			if (this.binding.CurrentCollectionIndex >= 0)
			{
				return null;
			}
			else
			{
				return this.binding.Validate(this.binding.form_div_selector);
			}
		};

		controller.UseCodec(codec());

		return controller;
	}
});
