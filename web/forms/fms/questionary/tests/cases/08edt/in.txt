include ..\in.lib.txt quiet

execute_javascript_stored_lines add_select2_test_functions

wait_text "Регистрационная анкета проживающего"

execute_javascript_line "window.wbt_Select2Focus('cpw_form_DocumentGivenBy_code');"
js $('#cpw_form_Birthplace').focusout(), 'убираем фокус, чтобы сработала валидация';
focus_id cpw_form_RegAddress
execute_javascript_line "app.cpw_Now= function(){return new Date(2015, 6, 26, 17, 36, 0, 0 ); };"
je $('#cpw_form_DateFrom').focusout();

shot_check_png ..\..\shots\7sav.png

wait_click_text "Сохранить содержимое формы"
wait_click_text "Да, сохранить"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\7sav.result.xml

exit