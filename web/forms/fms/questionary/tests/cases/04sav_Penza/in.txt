wait_click_text "OK"

include ..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet

execute_javascript_line "app.UseConsoleLog= true;"
execute_javascript_line "app.WriteToLog= function(txt){if (!app.LogContentText) {app.LogContentText=txt+'\r\n';} else {app.LogContentText+= txt+'\r\n';}};"
execute_javascript_line "app.profile= { 'Extensions': { 'fms': { 'Abonent': { 'employee_ummsId': '1001', 'supplierInfo': '1800000008002001','sono': {'id': '128752'},'subdivision': {'id': '116162'} } } } };"
execute_javascript_line "app.cpw_Now= function(){return new Date(2015, 6, 26, 17, 36, 0, 0 ); };"

wait_click_text "Обновить данными из профиля"

execute_javascript_stored_lines add_wbt_std_functions
execute_javascript_stored_lines add_select2_test_functions

wait_text "Регистрационная анкета проживающего"

play_stored_lines questionary_fields_1
play_stored_lines questionary_select2_fields_1

focus_id cpw_form_RegAddress

shot_check_png ..\..\shots\4sav_Penza.png

wait_click_text "Сохранить содержимое формы"
wait_click_text "Да, сохранить"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\4sav_Penza.result.xml

wait_click_text "Редактировать"

click_id_value cpw_form_DocumentGivenBy_check on

set_value_id cpw_form_DocumentGivenBy_text "Местный РОВД"
js $('#cpw_form_DocumentGivenBy_text').focusout(), 'убираем фокус, чтобы сработала валидация';

shot_check_png ..\..\shots\4sav_1_Penza.png

wait_click_text "Сохранить содержимое формы"
wait_click_text "Да, сохранить"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\4sav_1_Penza.result.xml

execute_javascript_line "wbt_controller_GetLogContentText= function(){return !app.LogContentText ? '' : app.LogContentText;}"
dump_js wbt_controller_GetLogContentText log.result.txt

exit