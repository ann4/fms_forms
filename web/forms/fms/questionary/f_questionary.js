define([
	  'forms/fms/questionary/c_questionary'
	, 'forms/fms/questionary/ff_questionary'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'questionary'
		, Title: 'Анкета. Форма5'
		, FileFormat: FileFormat
	};
	return form_spec;
});
