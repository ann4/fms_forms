define([
	  'forms/fms/base/town/m_town'
],function (TownAddress)
{
	return function ()
	{
		var res =
		{
			// нередактируемые атрибуты {
			uid: null,
			requestId: null,

			supplierInfo: null,

			subdivision: null, // {id:'', text:''}
			employee: { ummsId: null },

			date: null, // 'ДД.ММ.ГГГГ'

			Hotel_organization_uid: null,
			// нередактируемые атрибуты }

			Hotel_organization_inn: '',
			Hotel_organization_name: '',
			Hotel_address_variants: [],
			Hotel_organization_pboul: false, // Предприниматель без образования юр. лица

			FirstName : '',
			LastName : '',
			MiddleName : '',

			FirstName_latin : '',
			LastName_latin : '',
			MiddleName_latin: '',

			person_uid: null, // Уникальный идентификатор
			person_personId: null, // Идентификатор физического лица, связанного с персональными данными

			Sex: { id: '', text: '' }, // М|Ж
			Birthday: null, // 'ДД.ММ.ГГГГ'
			Nationality: null, // {id:'',text:''}
			Birthplace: TownAddress(),

			DocumentType: null, // {id:'',text:''}
			DocumentSeries: '',
			DocumentNumber: '',
			DocumentGivenDate: null, // 'ДД.ММ.ГГГГ'
			DocumentGivenBy_from_dict: true,
			DocumentGivenBy: null, // {id:'',text:''}

			Document_entered: null, // true|false

			DocumentStatus: {id: 102877, text: 'Действительный'}, // {id:'',text:''}
			Document_uid: null,

			AppartmentNumber: '',
			DateFrom: null, // 'ДД.ММ.ГГГГ'
			DateTill: null   // 'ДД.ММ.ГГГГ'
		};
		return res;
	};
});