define([
	  'forms/fms/base/x_fms'
	, 'forms/fms/questionary/m_questionary'
	, 'forms/fms/base/town/m_town'
	, 'forms/base/log'
	, 'forms/fms/base/h_xsd'
],
function (BaseCodec, model_questionary, model_town, GetLogger, h_xsd)
{
	var log = GetLogger('x_questionary');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();
		res.tabs = ['', '  ', '    ', '      ', '        ', '          ', '            ', '              ', '                ', '                  '];
		res.x_fms_base.tabs = res.tabs;
		res.AddressWithHousingType = true;
		res.AddressWithHousingTypeWithoutRoom = true;
		res.GetRootNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/hotel/form5'; };
		res.Encode = function (m_questionary)
		{
			log.Debug('Encode() {');

			this.FixAddressFreeOnlyVersionByModel(m_questionary);

			var xml_string = '<?xml version="1.0" encoding="utf-8"?>' + this.FormatEOL();
			xml_string += '<ns6:form5 xmlns:ds="http://www.w3.org/2000/09/xmldsig#" schemaVersion="1.0" xmlns="http://umms.fms.gov.ru/replication/core" xmlns:ns2="http://umms.fms.gov.ru/replication/migration" xmlns:ns3="http://umms.fms.gov.ru/replication/foreign-citizen-core" xmlns:ns4="http://umms.fms.gov.ru/replication/migration/staying" xmlns:ns5="http://umms.fms.gov.ru/replication/migration/staying/unreg" xmlns:ns6="http://umms.fms.gov.ru/replication/hotel/form5" xmlns:ns7="http://umms.fms.gov.ru/replication/migration/staying/case-edit" xmlns:ns8="http://umms.fms.gov.ru/replication/core/correction">' + this.FormatEOL();

			xml_string = '1.102.2' == this.SchemaVersion ? this.EncodeCoreCaseFieldsFMS(1, m_questionary, xml_string) : this.EncodeCoreCaseFields(1, m_questionary, xml_string);

			var personId_tagName = ('1.3.42' == this.SchemaVersion || '1.102.2' == this.SchemaVersion) ? 'personUid' : 'personId';

			xml_string += this.FormatTabs(1) + '<ns6:personDataDocument>' + this.FormatEOL();
			xml_string += this.FormatTabs(2) + '<person>' + this.FormatEOL();
			xml_string += this.StringifyField(3, "uid", m_questionary.person_uid);
			xml_string += this.StringifyField(3, personId_tagName, m_questionary.person_personId);
			xml_string += this.StringifyField(3, "lastName", m_questionary.LastName);
			xml_string += this.StringifyField(3, "firstName", m_questionary.FirstName);
			xml_string += this.SafeStringifyField(3, "middleName", m_questionary.MiddleName);
			xml_string += this.EncodeSex(3, m_questionary.Sex);
			xml_string += this.SafeStringifyField(3, "birthDate", m_questionary.Birthday);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_questionary.Nationality, 'citizenship', 'citizenship');
			xml_string += this.EncodeBirthplace(3, m_questionary.Birthplace);
			xml_string += this.FormatTabs(2) + '</person>' + this.FormatEOL();

			xml_string += this.FormatTabs(2) + '<document>' + this.FormatEOL();
			xml_string += this.StringifyField(3, "uid", m_questionary.Document_uid);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_questionary.DocumentType, 'type', 'DocumentType');
			xml_string += this.SafeStringifyField(3, "series", m_questionary.DocumentSeries);
			xml_string += this.StringifyField(3, "number", m_questionary.DocumentNumber);
			if (!m_questionary.DocumentGivenBy_from_dict) {
				xml_string += this.StringifyField(3, "authority", m_questionary.DocumentGivenBy_text);
			}
			else if (null != m_questionary.DocumentGivenBy) {
				var ditem = {
					id: m_questionary.DocumentGivenBy.id
					, text: (m_questionary.DocumentGivenBy.row && m_questionary.DocumentGivenBy.row.NAME) ? m_questionary.DocumentGivenBy.row.NAME : m_questionary.DocumentGivenBy.text
				};
				xml_string += this.EncodeNamedDictionaryItem(3, ditem, 'authorityOrgan', 'OfficialOrgan');
			}
			xml_string += this.SafeStringifyField(3, "issued", res.DateEncode(m_questionary.DocumentGivenDate));
			xml_string += this.SafeStringifyField(3, "validFrom", res.DateEncode(m_questionary.DocumentGivenDate));
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_questionary.DocumentStatus, 'status', 'DocumentStatus');
			xml_string += this.FormatTabs(2) + '</document>' + this.FormatEOL();

			if (null != m_questionary.Document_entered)
				xml_string += this.StringifyField(2, "entered", '1.102.2' == this.SchemaVersion ? false : m_questionary.Document_entered, 'Флаг, указывающий на то, вписано ли данное лицо в документ');
			xml_string += this.FormatTabs(1) + '</ns6:personDataDocument>' + this.FormatEOL();

			if (m_questionary.RegAddress && null!=m_questionary.RegAddress)
				xml_string += this.EncodeAddress(1, 'ns6:livingAddress', m_questionary.RegAddress);

			xml_string += this.FormatTabs(1) + '<ns6:stayPeriod>' + this.FormatEOL();
			xml_string += this.StringifyField(2, "dateFrom", res.DateEncode(m_questionary.DateFrom));
			if (null != m_questionary.DateTill)
				xml_string += this.SafeStringifyField(2, "dateTo", res.DateEncode(m_questionary.DateTill));
			xml_string += this.FormatTabs(1) + '</ns6:stayPeriod>' + this.FormatEOL();

			xml_string += this.StringifyField(1, "ns6:registrationDate", res.DateEncode(m_questionary.DateFrom));

			log.Debug('hotelInfo {');
			xml_string += this.FormatTabs(1) + '<ns6:hotelInfo>' + this.FormatEOL();
			xml_string += this.FormatTabs(2) + '<ns6:organization>' + this.FormatEOL();
			xml_string += this.StringifyField(3, "uid", m_questionary.Hotel_organization_uid);
			xml_string += this.StringifyField(3, "inn", m_questionary.Hotel_organization_inn);
			xml_string += this.StringifyField(3, "name", m_questionary.Hotel_organization_name);

			xml_string += this.FormatTabs(3) + '<address>' + this.FormatEOL();
			xml_string += this.EncodeAddress(4, 'address', m_questionary.Address);
			xml_string += this.FormatTabs(3) + '</address>' + this.FormatEOL();

			xml_string += this.StringifyField(3, "shortName", m_questionary.Hotel_organization_name);
			if (null != m_questionary.Hotel_organization_pboul)
				xml_string += this.StringifyField(3, "pboul", m_questionary.Hotel_organization_pboul);
			xml_string += this.SafeStringifyField(3, "headOrganization", m_questionary.headOrganization);
			xml_string += this.FormatTabs(2) + '</ns6:organization>' + this.FormatEOL();
			xml_string += this.FormatTabs(1) + '</ns6:hotelInfo>' + this.FormatEOL();
			log.Debug('hotelInfo }');

			xml_string += '</ns6:form5>';
			log.Debug('Encode() }');
			return xml_string;
		};

		res.DecodeStayPeriod = function (node, m_questionary)
		{
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "dateFrom": m_questionary.DateFrom = res.DateDecode(child.text); break;
					case "dateTo": m_questionary.DateTill = res.DateDecode(child.text); break;
				}
			});
		}

		res.DecodePersonDataDocument = function (node, m_questionary)
		{
			var childs = node.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "person": this.DecodePersonDataDocument_Person(child, m_questionary); break;
					case "document": this.DecodePersonDataDocument_Document(child, m_questionary); break;
					case "entered": m_questionary.Document_entered = ('true' == child.text ? true : false); break;
				}
			}
		}

		res.DecodePersonDataDocument_Document = function (node, m_questionary)
		{
			log.Debug('DecodePersonDataDocument_Document {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m_questionary.Document_uid = child.text; break;
					case "type": m_questionary.DocumentType = self.DecodeDictionaryItem(child, 'DocumentType'); break;
					case "series": m_questionary.DocumentSeries = child.text; break;
					case "number": m_questionary.DocumentNumber = child.text; break;
					case "issued": m_questionary.DocumentGivenDate = res.DateDecode(child.text); break;
					case "validFrom": m_questionary.DocumentGivenDate = res.DateDecode(child.text); break;
					case "validTo": m_questionary.DocumentFinishDate = res.DateDecode(child.text); break;
					case "status": m_questionary.DocumentStatus = m_questionary.Document_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;
					case "authorityOrgan":
						var ditem = self.DecodeDictionaryItem(child, 'officialOrgan');
						m_questionary.DocumentGivenBy_code = { id: ditem.id };
						m_questionary.DocumentGivenBy = { id: ditem.id, text: ditem.text };
						m_questionary.DocumentGivenBy_from_dict = true;
						break;
					case 'authority':
						m_questionary.DocumentGivenBy_text = child.text;
						m_questionary.DocumentGivenBy_from_dict = false;
						break;
				}
			});
			log.Debug('DecodePersonDataDocument_Document }');
		}

		res.DecodeHotelInfo_organization = function (node, m_questionary)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "uid": m_questionary.Hotel_organization_uid = child.text; break;
					case "inn": m_questionary.Hotel_organization_inn = child.text; break;
					case "name": m_questionary.Hotel_organization_name = child.text; break;
					case "address": m_questionary.Address = self.DecodeAddress(child); break;
					case "pboul": m_questionary.Hotel_organization_pboul = ('true' == child.text ? true : false);; break;
					case "headOrganization": m_questionary.headOrganization = child.text; break;
				}
			});
		};

		res.DecodeHotelInfo = function (node, m_questionary)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (self.GetTagNameWithoutNamespace(child))
				{
					case "organization": self.DecodeHotelInfo_organization(child, m_questionary); break;
				}
			});
		}

		res.DecodeXmlDocument = function (doc)
		{
			var m_questionary = model_questionary();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (this.GetTagNameWithoutNamespace(child))
				{
					case "uid": m_questionary.uid = child.text; break;
					case "requestId": m_questionary.requestId = child.text; break;
					case "supplierInfo": m_questionary.supplierInfo = child.text; break;
				    case "subdivision": m_questionary.subdivision = this.DecodeOfficialOrganFMS(child); break;
					case "date": m_questionary.date = child.text; break;
					case "number": m_questionary.number = child.text; break;
					case "employee": m_questionary.employee = this.DecodeEmployee(child); break;
					case "personDataDocument": log.Debug('personDataDocument'); this.DecodePersonDataDocument(child, m_questionary); break;
					case "livingAddress":
						log.Debug('livingAddress {');
						m_questionary.RegAddress = this.DecodeAddressAddress(child);
						log.Debug('livingAddress }');
						break;
					case "stayPeriod": this.DecodeStayPeriod(child, m_questionary); break;
					case "registrationDate": m_questionary.DateFrom = res.DateDecode(child.text); break;
					case "hotelInfo": log.Debug('hotelInfo'); this.DecodeHotelInfo(child, m_questionary); break;
				}
			}
			return m_questionary;
		};

		var base_Encode = res.Encode;
		res.Encode = function (m_questionary)
		{
			this.FixScheme('questionary', this.GetSchemaVersionByModel(m_questionary));
			log.Debug('schema version ' + this.SchemaVersion);
			return base_Encode.call(this, m_questionary);
		}

		res.GetSchemaVersionByXml = function (xml_text)
		{
			var res = '1.3.36';
			if (-1 != xml_text.indexOf('personUid>'))
			{
				return '1.3.42';
			}
			return res;
		}

		var base_Decode = res.Decode;
		res.Decode = function (xml_text)
		{
			this.FixScheme('questionary', this.GetSchemaVersionByXml(xml_text));
			return base_Decode.call(this, xml_text);
		}

		log.Debug('Create }');
		return res;
	}
});
