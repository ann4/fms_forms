﻿define([],
function () {
	var file_format =
	{
		  FilePrefix: 'Form5'
		, FileExtension: 'xml'
		, Description: 'ФМС. Анкета. Форма5'
	};
	return file_format;
});
