define([
	  'forms/base/codec'
	, 'forms/fms/questionary/x_questionary'
	, 'forms/fms/questionary/x_csv_questionary'
	, 'forms/base/log'
],
function (BaseCodec, x_questionary, x_csv_questionary, GetLogger) {
	var log = GetLogger('codec_questionary');
	return function () {
		var res = BaseCodec();

		res.codec_xml = x_questionary();
		res.codec_csv = null;

		res.Encode = function (m_questionary) {
			return this.codec_xml.Encode(m_questionary);
		}

		res.Decode = function (xml_string) {
			var result = null;
			if (-1 < xml_string.indexOf('<?xml ')) {
				result = this.codec_xml.Decode(xml_string);
			}
			else {
				this.codec_csv = x_csv_questionary();
				result = this.codec_csv.Decode(xml_string);
			}
			return result;
		};

		return res;
	}
});
