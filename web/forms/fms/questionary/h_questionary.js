// утф8
define([
	  'forms/fms/guides/dict_officialorgan_fms.csv'
	, 'forms/base/h_dictionary'
]
, function (fms, h_dictionary)
{
	var helper = {};

	helper.GetValues = function (type)
	{
		if (!helper.dict_officialorgan_fms)
		{
			helper.dict_officialorgan_fms = fms.rows;
			helper.DocumentGivenBy_variants = [];
			for (var i = 0; i < helper.dict_officialorgan_fms.length; i++)
			{
				var row = helper.dict_officialorgan_fms[i];
				helper.DocumentGivenBy_variants.push({ id: row[0], text: row[1] });
			}

			helper.DocumentGivenBy_code_variants = [];
			for (var i = 0; i < helper.dict_officialorgan_fms.length; i++)
			{
				var row = helper.dict_officialorgan_fms[i];
				if (row[2])
					helper.DocumentGivenBy_code_variants.push({ id: row[0], text: row[2] });
			}
		}
		if (type == 'v')
			return helper.DocumentGivenBy_variants;
		else if (type = 'c')
			return helper.DocumentGivenBy_code_variants;
	}

	helper.GetDocumentGivenBy_ByCode = function (code)
	{
		for (var i = 0; i < helper.dict_officialorgan_fms.length; i++)
		{
			var row = helper.dict_officialorgan_fms[i];
			if (row[0] == code.id)
				return { id: row[0], text: row[1] };
		}
		return null;
	};

	return helper;
});
