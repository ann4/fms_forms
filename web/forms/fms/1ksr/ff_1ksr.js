define([],
function (CreateController) {
	var file_format =
	{
		FilePrefix: '1КСР'
		, FileExtension: 'xls'
		, Encoding: 65001
		, Description: 'ФМС. Сведения о деятельности КСР.'
	};
	return file_format;
});
