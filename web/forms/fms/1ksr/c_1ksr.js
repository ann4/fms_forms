define([
	  'forms/fms/base/c_reporting'
	, 'forms/base/log'
	, 'forms/fms/1ksr/m_1ksr'
	, 'forms/fms/1ksr/x_1ksr'
	, 'forms/fms/1ksr/ff_1ksr'
	, 'tpl!forms/fms/1ksr/e_1ksr.html'
	, 'tpl!forms/fms/1ksr/v_1ksr.xml'
	, 'forms/fms/base/h_Select2'
	, 'forms/base/h_times'
],
function (binded_controller, GetLogger, model_1ksr, codec, form_spec, layout_tpl, print_tpl, h_select2, h_times)
{
	return function ()
	{
		var log = GetLogger('c_1ksr');

		var range = function (start, end) {
			var res = [];
			for (var i = start; i <= end; i++) {
				res.push({ id: i.toString(), text: i.toString() });
			}
			return res;
		};

		var select2Options = function (h_values) {
			return function (m) {
				return {
					placeholder: '',
					allowClear: true,
					query: function (q) {
						q.callback({ results: h_select2.FindForSelect(h_values, q.term) });
					}
				};
			}
		};

		var stringAddress = function (address) {
			var txt= '';
			txt += !address.Country || ''==address.Country ? 'Россия' : address.Country;
			if (address.Province && '' != address.Province)
				txt += ', ' + address.Province;
			if (address.City && '' != address.City)
				txt += ', г.' + address.City;
			if (address.Area && '' != address.Area)
				txt += ', район ' + address.Area;
			if (address.Locality && '' != address.Locality)
				txt += ', ' + address.Locality;
			if (address.Street && '' != address.Street)
				txt += ', ул.' + address.Street;
			if (address.HouseNumber && '' != address.HouseNumber)
				txt += ', д.' + address.HouseNumber;
			if (address.HousingNumber && '' != address.HousingNumber)
				txt += ', корпус ' + address.HousingNumber;
			if (address.FlatNumber && '' != address.FlatNumber)
				txt += ', кв.' + address.FlatNumber;
			if (address.ZipIndex && '' != address.ZipIndex)
				txt += ', индекс ' + address.ZipIndex;
			return txt;
		}

		var controller = binded_controller(layout_tpl, form_spec, {
			extension_key: '1ksr'
			, field_spec:
			{
				ReportingPeriod: select2Options(range((new Date).getFullYear() - 5, (new Date).getFullYear()))
				, ReportingPeriodQuarter: select2Options([
					{ id: 1, text: 'первый' },
					{ id: 2, text: 'второй' },
					{ id: 3, text: 'третий' },
					{ id: 4, text: 'четвертый' }
				])
			}
		});

		controller.UseProfile = function (profile) {
			//h_abonent.CheckResponsibleOfficiers(profile); // выбор ответственного лица, осуществляющего учет
			log.Debug('UseProfile {');
			this.SafeCreateNewContent();
			if (profile && profile.Abonent) {
				if (!this.model.OrganizationName || '' == this.model.OrganizationName) {
					if (profile.Abonent.ShortName) {
						this.model.OrganizationName = profile.Abonent.ShortName;
					}
					else if (profile.Abonent.Name) {
						this.model.OrganizationName = profile.Abonent.Name;
					}
				}
				if (!this.model.OrganizationAddress || '' == this.model.OrganizationAddress) {
					if (profile.Abonent.Address) {
						this.model.OrganizationAddress = profile.Abonent.Address;
						this.model.OrganizationAddress.text = '' == profile.Abonent.Address.AddressText ?
							stringAddress(profile.Abonent.Address) : profile.Abonent.Address.AddressText;
					}
				}
				if (!this.model.OrganizationRepresentative || '' == this.model.OrganizationRepresentative) {
					if (profile.User && profile.User.LastName && profile.User.FirstName && profile.User.MiddleName) {
						this.model.OrganizationRepresentative = profile.User.LastName;
						this.model.OrganizationRepresentative += '' != this.model.OrganizationRepresentative && '' != profile.User.FirstName ? ' ' : '';
						this.model.OrganizationRepresentative += profile.User.FirstName;
						this.model.OrganizationRepresentative += '' != this.model.OrganizationRepresentative && '' != profile.User.MiddleName ? ' ' : '';
						this.model.OrganizationRepresentative += profile.User.MiddleName;
					}
				}
				if (!this.model.OrganizationRepresentativeJobTitle || '' == this.model.OrganizationRepresentativeJobTitle) {
					if (profile.User && profile.User.jobTitle) {
						this.model.OrganizationRepresentativeJobTitle = profile.User.jobTitle;
					}
				}
				if (!this.model.OrganizationPhone || '' == this.model.OrganizationPhone) {
					if (profile.Abonent && profile.Abonent.phone) {
						this.model.OrganizationPhone = profile.Abonent.phone;
					}
				}
				if (!this.model.OrganizationEmail || '' == this.model.OrganizationEmail) {
					if (profile.Abonent && profile.Abonent.email) {
						this.model.OrganizationEmail = profile.Abonent.email;
					}
				}
			}
			if (profile && profile.Extensions && profile.Extensions.fms) {
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent) {
					var pAbonent = pfms.Abonent;
					this.model.CountRoom.all = pAbonent.RoomAllCount;
					this.model.CountRoom.highestCategory = pAbonent.RoomHighestCategory;
					this.model.CountRoom.specialCategory = pAbonent.RoomSpecialCategory;
					this.model.RoomArea = pAbonent.RoomArea;
					this.model.CountPlaces = pAbonent.CountPlaces;
					this.model.PeriodOperation = pAbonent.PeriodOperation;
					this.model.ReportingPeriodFrom = pAbonent.StartPeriod;
					this.model.ReportingPeriodTill = pAbonent.FinishPeriod;
					this.model.HotelName = pAbonent.OrganizationName;
					this.model.OKPO = pAbonent.OKPO;
					this.model.HotelAddress = pAbonent.CurrentAddressOfPlacement;
					this.model.HotelType = pAbonent.HotelType;
					this.model.HotelCategory = pAbonent.HotelCategory;
					this.model.TaxSystem = pAbonent.TaxSystem;
					this.model.WellnessProfile = pAbonent.WellnessProfile;
				}
			}
			log.Debug('UseProfile }');
		};

		controller.SafeCreateNewContent = function () {
			if (!this.model)
				this.model = model_1ksr();
		};

		var base_Render = controller.Render;
		controller.Render = function (id_form_div) {
			base_Render.call(this, id_form_div);
			var _self = this;
			var changedReportingPerion = function (e) {
				var quarter = $('#cpw_1ksr_ReportingPeriodQuarter').val();
				var year = $('#cpw_1ksr_ReportingPeriod').val();
				if ("" == quarter || "" == year) {
					$('#cpw_1ksr_ReportingPeriodFrom').val("");
					$('#cpw_1ksr_ReportingPeriodTill').val("");
					$('#cpw_1ksr_ReportingPeriodFrom').attr('readonly', 'readonly');
					$('#cpw_1ksr_ReportingPeriodTill').attr('readonly', 'readonly');
					$('#cpw_1ksr_note').text('(Неверно указан период отчетности!)');
				} else {
					var monthStart = quarter == 1 ? '01' :
						(quarter == 2 ? '04' :
						(quarter == 3 ? '07' : '10'));
					var monthFinish = quarter == 1 ? '03' :
						(quarter == 2 ? '06' :
						(quarter == 3 ? '09' : '12'));
					var dayFinish = quarter == 1 || quarter == 4 ? 31 : 30;
					$('#cpw_1ksr_ReportingPeriodFrom').val('01.' + monthStart + '.' + year);
					$('#cpw_1ksr_ReportingPeriodTill').val(dayFinish + '.' + monthFinish + '.' + year);
					$('#cpw_1ksr_ReportingPeriodFrom').attr('readonly', 'readonly');
					$('#cpw_1ksr_ReportingPeriodTill').attr('readonly', 'readonly');
					$('#cpw_1ksr_note').text('(Даты проставляются в соответствии с указанным периодом отчетности.)');
				}
			};
			$('#cpw_1ksr_ReportingPeriodQuarter').change(changedReportingPerion);
			$('#cpw_1ksr_ReportingPeriod').change(changedReportingPerion);
			/*$('#cpw_1ksr_ReportingPeriod').change(function (e) {
				if (_self.model && _self.model.PeriodOperation && 'y' == _self.model.PeriodOperation.id) {
					var startPeriod = moment($('#cpw_1ksr_ReportingPeriod').val(), 'YYYY');
					if (startPeriod != null) {
						var finishPeriod = moment(startPeriod).add('y', 1).subtract('d', 1);
						$('#cpw_1ksr_ReportingPeriodFrom').val(startPeriod.format('DD.MM.YYYY'));
						$('#cpw_1ksr_ReportingPeriodTill').val(finishPeriod.format('DD.MM.YYYY'));
						$('#cpw_1ksr_ReportingPeriodFrom').attr('readonly', 'readonly');
						$('#cpw_1ksr_ReportingPeriodTill').attr('readonly', 'readonly');
						$('#cpw_1ksr_note').text('(Даты проставляются в соответствии с периодом. Период "круглый год" не позволяет редактировать даты.)');
					}
				} else if (_self.model && _self.model.PeriodOperation && 'p' == _self.model.PeriodOperation.id) {
					$('.d-picker').datepicker({
						dateFormat: "dd.mm.yy",
						closeText: 'Закрыть',
						showButtonPanel: true,
						changeMonth: true,
						changeYear: true
					});
					var currentYear = $('#cpw_1ksr_ReportingPeriod').val();
					if (currentYear != null) {
						var startPeriod = $('#cpw_1ksr_ReportingPeriodFrom').val();
						var finishPeriod = $('#cpw_1ksr_ReportingPeriodTill').val();
						if ('' != startPeriod) {
							var partsStartPeriod = startPeriod.split('.');
							$('#cpw_1ksr_ReportingPeriodFrom').val(partsStartPeriod[0] + '.' + partsStartPeriod[1] + '.' + currentYear);
						}
						if ('' != finishPeriod) {
							var partsFinishPeriod = finishPeriod.split('.');
							$('#cpw_1ksr_ReportingPeriodTill').val(partsFinishPeriod[0] + '.' + partsFinishPeriod[1] + '.' + currentYear);
						}
						$('#cpw_1ksr_ReportingPeriodFrom').attr('readonly', null);
						$('#cpw_1ksr_ReportingPeriodTill').attr('readonly', null);
						$('#cpw_1ksr_note').text('(Даты проставляются в соответствии с периодом. Период "сезонный" позволяет редактировать даты.)');
					}
				} else if (!_self.model || !_self.model.PeriodOperation) {
					$('.row-period-operation').addClass('hidden');
					$('.apply').attr('disabled', 'disabled');
					$('#cpw_1ksr_note').removeClass('note-default');
					$('#cpw_1ksr_note').addClass('note-error');
					$('#cpw_1ksr_note').text('Необходимо указать период функционирования в профиле!');
				}
			});*/
			$('.d-picker').change(function (e) {
				var startPeriod = $('#cpw_1ksr_ReportingPeriodFrom').val();
				var finishPeriod = $('#cpw_1ksr_ReportingPeriodTill').val();
				$('.apply').attr('disabled', '' == startPeriod || '' == finishPeriod ? 'disabled' : null);
			});
			$('#cpw_1ksr_ReportingPeriod').change();
		};

		var setCountNights = function (content, data, isBefore18Year, isAfter55Year) {
			var fullDay = 0;
			if (data.DateStart) {
				var dateStart = controller.parseDate(data.DateStart);
				var dateUnreg = !data.DateUnreg ? null : controller.parseDate(data.DateUnreg);
				var dateFinish = !data.DateFinish ? null : controller.parseDate(data.DateFinish);
				if (!dateFinish) {
					dateFinish = new Date(dateStart);
					dateFinish.setDate(dateFinish.getDate() + 90);
				}
				if (!!dateUnreg && dateUnreg < dateFinish) {
					dateFinish = dateUnreg;
				}
				var now = new Date();
				var dateNow = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0)
				var periodReportStart = controller.parseDate(content.ReportingPeriodFrom);
				var periodReportFinish = controller.parseDate(content.ReportingPeriodTill);
				periodReportFinish = periodReportFinish > dateNow ? dateNow : periodReportFinish;
				var start = dateStart < periodReportStart ? periodReportStart : dateStart;
				var finish = dateFinish > periodReportFinish ? periodReportFinish : dateFinish;
				fullDay = (finish - start) / 86400000;
				content.CountNights.all += fullDay;
			}
			if (isBefore18Year) {
				content.CountNights.junior += fullDay;
			} else if (isAfter55Year) {
				content.CountNights.oldster += fullDay;
			}
			//setByDurationStay(content, fullDay, (data.Citizenship && 'RUS' == data.Citizenship.id));
			if ('RUS' != data.Citizenship.id) {
				setByDurationStayAndVisitPurposeAndCitizenship(content, fullDay, data.Citizenship, data.VisitPurpose);
			}
		};

		var setCountPerson = function (content, data, isBefore18Year, isAfter55Year) {
			if (data.Citizenship && 'RUS' == data.Citizenship.id) {
				content.CountGuestRussian.all++;
				if (isBefore18Year) {
					content.CountGuestRussian.junior++;
				} else if (isAfter55Year) {
					content.CountGuestRussian.oldster++;
				}
			} else {
				content.CountGuestForeign.all++;
				if (isBefore18Year) {
					content.CountGuestForeign.junior++;
				} else if (isAfter55Year) {
					content.CountGuestForeign.oldster++;
				}
			}
		};

		/*var setByCitizenship = function (content, data) {
			if (data.Citizenship && 'RUS' != data.Citizenship.id) {
				if (null == content.CountGuestByCountryCitizenship) {
					content.CountGuestByCountryCitizenship = {};
				}
				var existingCountry = content.CountGuestByCountryCitizenship[data.Citizenship.id];
				if (!existingCountry) {
					content.CountGuestByCountryCitizenship[data.Citizenship.id] = { country: data.Citizenship.text, count: 1 };
				} else {
					content.CountGuestByCountryCitizenship[data.Citizenship.id] = { country: existingCountry.country, count: existingCountry.count + 1 };
				}
			}
		};*/

		var visitPurpose_Vocation = '139345,103131,103132,103093,103094,103092,103096'; //отпуск, досуг и отдых
		var visitPurpose_Education = '139347,135533,135534,135535,135536,135540,136354'; //образование и профессиональ-ная подготовка
		var visitPurpose_Therapy = '103087'; //лечебные и оздоровительные процедуры
		var visitPurpose_Religion = '103110,103111'; //религиозные/паломнические
		var visitPurpose_Other = '139349,139350,139352,139353,136355,103117,103119,103128,135543,136326,103076,103077,103078,103079,103080,136356'; //посещение магазинов и прочие 
		var visitPurpose_Professional = '139344,139346,139348,139351,139354,103120,103057,103088,103105,103126,103127,103129,103130,103086,103134,' +
			'103046,135539,135541,135542,103044,103045,103047,103048,103049,103050,103051,103052,103053,103104,103116,136330,136331,136332,136333,' +
			'136334,103054,103055,103056,103058,103059,103060,103061,103062,103063,103064,103065,103066,103067,103068,103069,103070,103071,103072,' +
			'103073,103074,103075,103076,103077,103078,103079,103080,103082,103083,103084,103085,103089,103090,103091,103101,103102,103081,103106,' +
			'103107,103108,103109,103112,103113,103114'; //Деловые и профессиональные

		/*var setByVisitPurpose = function (content, data) {
			if (data.VisitPurpose) {
				var isRUS = data.Citizenship && 'RUS' == data.Citizenship.id;
				if (-1 < visitPurpose_Vocation.indexOf(data.VisitPurpose.id)) {
					(isRUS ? content.CountGuestRussianByVisitPurpose : content.CountGuestForeignByVisitPurpose).vocation++; //отпуск, досуг и отдых
				} else if (-1 < visitPurpose_Education.indexOf(data.VisitPurpose.id)) {
					(isRUS ? content.CountGuestRussianByVisitPurpose : content.CountGuestForeignByVisitPurpose).education++; //образование и профессиональ-ная подготовка
				} else if (-1 < visitPurpose_Therapy.indexOf(data.VisitPurpose.id)) {
					(isRUS ? content.CountGuestRussianByVisitPurpose : content.CountGuestForeignByVisitPurpose).therapy++; //лечебные и оздоровительные процедуры
				} else if (-1 < visitPurpose_Religion.indexOf(data.VisitPurpose.id)) {
					(isRUS ? content.CountGuestRussianByVisitPurpose : content.CountGuestForeignByVisitPurpose).religion++; //религиозные/паломнические
				} else if (-1 < visitPurpose_Other.indexOf(data.VisitPurpose.id)) {
					(isRUS ? content.CountGuestRussianByVisitPurpose : content.CountGuestForeignByVisitPurpose).other++; //посещение магазинов и прочие 
				} else if (-1 < visitPurpose_Professional.indexOf(data.VisitPurpose.id)) {
					(isRUS ? content.CountGuestRussianByVisitPurpose : content.CountGuestForeignByVisitPurpose).professional++; //Деловые и профессиональные
				}
			}
		};

		var setByDurationStay = function (content, fullDay, isRUS) {
			if (fullDay <= 0) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).night0++;
			} else if (1 <= fullDay && fullDay <= 3) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).night3++;
			} else if (4 <= fullDay && fullDay <= 7) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).night7++;
			} else if (8 <= fullDay && fullDay <= 14) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).night14++;
			} else if (15 <= fullDay && fullDay <= 28) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).night28++;
			} else if (29 <= fullDay && fullDay <= 91) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).night91++;
			} else if (92 <= fullDay && fullDay <= 182) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).night182++;
			} else if (183 <= fullDay) {
				(isRUS ? content.CountGuestRussianByDurationStay : content.CountGuestForeignByDurationStay).more183++;
			}
		};
		*/
		var setByCitizenshipCountry = function (content, citizenship) {
			if (!content['allCount']) {
				content['allCount'] = { count: 1 };
			} else {
				content['allCount'] = { count: (content['allCount']).count + 1 };
			}
			if (citizenship) {
				var existingCountry = content[citizenship.id];
				if (!existingCountry) {
					content[citizenship.id] = { country: citizenship.text, count: 1 };
				} else {
					content[citizenship.id] = { country: existingCountry.country, count: existingCountry.count + 1 };
				}
			}
		};

		var setByVisitPurposeAndCitizenship = function (content, citizenship, visitPurpose) {
			if (visitPurpose) {
				if (-1 < visitPurpose_Vocation.indexOf(visitPurpose.id)) {
					setByCitizenshipCountry(content.vocation, citizenship);
				} else if (-1 < visitPurpose_Education.indexOf(visitPurpose.id)) {
					setByCitizenshipCountry(content.education, citizenship);
				} else if (-1 < visitPurpose_Therapy.indexOf(visitPurpose.id)) {
					setByCitizenshipCountry(content.therapy, citizenship);
				} else if (-1 < visitPurpose_Religion.indexOf(visitPurpose.id)) {
					setByCitizenshipCountry(content.religion, citizenship);
				} else if (-1 < visitPurpose_Other.indexOf(visitPurpose.id)) {
					setByCitizenshipCountry(content.other, citizenship);
				} else if (-1 < visitPurpose_Professional.indexOf(visitPurpose.id)) {
					setByCitizenshipCountry(content.professional, citizenship);
				}
			}
		};

		var setByDurationStayAndVisitPurposeAndCitizenship = function (content, fullDay, citizenship, visitPurpose) {
			if (null == content.AllCitizenships) {
				content.AllCitizenships = {};
			}
			if (citizenship) {
				var existingCountry = content.AllCitizenships[citizenship.id];
				if (!existingCountry) {
					content.AllCitizenships[citizenship.id] = citizenship.text;
				}
			}
			if (fullDay <= 0) {
				setByVisitPurposeAndCitizenship(content.CountGuestBy.night0, citizenship, visitPurpose);
			} else if (1 <= fullDay && fullDay <= 4) {
				setByVisitPurposeAndCitizenship(content.CountGuestBy.night4, citizenship, visitPurpose);
			} else if (5 <= fullDay && fullDay <= 7) {
				setByVisitPurposeAndCitizenship(content.CountGuestBy.night7, citizenship, visitPurpose);
			} else if (8 <= fullDay && fullDay <= 14) {
				setByVisitPurposeAndCitizenship(content.CountGuestBy.night14, citizenship, visitPurpose);
			} else if (15 <= fullDay && fullDay <= 28) {
				setByVisitPurposeAndCitizenship(content.CountGuestBy.night28, citizenship, visitPurpose);
			} else if (29 <= fullDay && fullDay <= 90) {
				setByVisitPurposeAndCitizenship(content.CountGuestBy.night90, citizenship, visitPurpose);
			} else if (91 <= fullDay) {
				setByVisitPurposeAndCitizenship(content.CountGuestBy.more90, citizenship, visitPurpose);
			}
		};

		var checkBirthDate = function (birthDate, currentDate, periodYears) {
			var dateCurrentDate = controller.parseDate(currentDate);
			if (null == dateCurrentDate) {
				throw { parseException: true, type:'processingDateException', message: "Не указана дата въезда!" };
			}
			var dateCurrentDateYear = null == dateCurrentDate ? 0 : dateCurrentDate.getFullYear();
			var dateBirthDate = controller.parseDate(birthDate);
			if (null == dateCurrentDate) {
				controller.writeWarningToLog("Не указана дата рождения! Гражданин будет считаться как взрослый.");
			}
			var dateBirthDateYear = null == dateBirthDate ? 0 : dateBirthDate.getFullYear();
			var dateDiff = dateCurrentDateYear - dateBirthDateYear;
			if (0 < dateDiff && dateDiff < periodYears) {
				return true;
			}
			else if (dateDiff > periodYears || dateDiff < 0) {
				return false;
			}
			else {
				dateBirthDate.setYear(dateBirthDateYear + periodYears);
				return dateBirthDate < dateCurrentDate;
			}
		};

		var checkByDates = function (data, reportingPeriodFrom, reportingPeriodTill) {
			var start = controller.parseDate(data.DateStart);
			if (null == start) {
				controller.writeWarningToLog("  Не указана дата прибытия!");
				return false;
			}
			var finish = null;
			if (data.DateUnreg) {
				finish = controller.parseDate(data.DateUnreg);
			}
			if (null == finish) {
				finish = controller.parseDate(data.DateFinish);
				if (!finish) {
					controller.writeWarningToLog("  Не указана дата убытия! Предполагается, что гражданин выехал через 90 дней.");
					finish = controller.parseDate(data.DateStart);
					finish.setDate(finish.getDate() + 90);
				}
			}
			var startDate = controller.parseDate(reportingPeriodFrom);
			var finishDate = controller.parseDate(reportingPeriodTill);
			return (start <= startDate && finish > startDate && finish <= finishDate) ||
				(start >= startDate && start <= finishDate && finish >= finishDate) ||
				(start >= startDate && finish <= finishDate) ||
				(start <= startDate && finish >= finishDate);
		};

		controller.SetFormContent = function (content) {
			log.Debug('SetFormContent {');
			this.SafeCreateNewContent();
			if (content) {
				this.model.ReportingPeriod = content.ReportingPeriod;
				this.model.ReportingPeriodFrom = content.ReportingPeriodFrom;
				this.model.ReportingPeriodTill = content.ReportingPeriodTill;
				this.model.PeriodOperation = content.PeriodOperation;
			}
			log.Debug('SetFormContent }');
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			this.model = base_GetFormContent.call(this);
			return this.model;
		};

		controller.ProcessingData = function (data) {
			log.Debug('ProcessingData {');
			if (!this.model || !this.model.ReportingPeriodFrom || !this.model.ReportingPeriodTill) {
				controller.writeWarningToLog("  Не заданы параметры проверки!");
				return false;
			}
			try {
				if (!checkByDates(data, this.model.ReportingPeriodFrom, this.model.ReportingPeriodTill)) {
					controller.writeWarningToLog("  Период пребывания не соответствует указанному!");
					return false;
				}
				var isBefore18Year = false;
				var isAfter55Year = true;
				if (null != data.BirthDate && null != data.DateStart) {
					isBefore18Year = checkBirthDate(data.BirthDate, data.DateStart, 18);
					isAfter55Year = !checkBirthDate(data.BirthDate, data.DateStart, 55);
				}
				setCountNights(this.model, data, isBefore18Year, isAfter55Year);
				setCountPerson(this.model, data, isBefore18Year, isAfter55Year);
				//setByCitizenship(this.model, data);
				//setByVisitPurpose(this.model, data);
			} catch (e) {
				controller.writeErrorToLog(e.message, e);
				return false;
			}
			log.Debug('ProcessingData }');
			return true;
		};

		var countAdditionalRows = function (data) {
			var countAdditionalRows = 0;
			var countNight0Rows = 0;
			var countNight4Rows = 0;
			var countNight7Rows = 0;
			var countNight14Rows = 0;
			var countNight28Rows = 0;
			var countNight90Rows = 0;
			var countMore90Rows = 0;
			if (null != data.AllCitizenships) {
				for (var country_key in data.AllCitizenships) {
					if (data.CountGuestBy.night0.vocation[country_key] || data.CountGuestBy.night0.education[country_key] || data.CountGuestBy.night0.therapy[country_key]
						|| data.CountGuestBy.night0.religion[country_key] || data.CountGuestBy.night0.other[country_key] || data.CountGuestBy.night0.professional[country_key]) {
						countNight0Rows++;
					}
					if (data.CountGuestBy.night4.vocation[country_key] || data.CountGuestBy.night4.education[country_key] || data.CountGuestBy.night4.therapy[country_key]
						|| data.CountGuestBy.night4.religion[country_key] || data.CountGuestBy.night4.other[country_key] || data.CountGuestBy.night4.professional[country_key]) {
						countNight4Rows++;
					}
					if (data.CountGuestBy.night7.vocation[country_key] || data.CountGuestBy.night7.education[country_key] || data.CountGuestBy.night7.therapy[country_key]
						|| data.CountGuestBy.night7.religion[country_key] || data.CountGuestBy.night7.other[country_key] || data.CountGuestBy.night7.professional[country_key]) {
						countNight7Rows++;
					}
					if (data.CountGuestBy.night14.vocation[country_key] || data.CountGuestBy.night14.education[country_key] || data.CountGuestBy.night14.therapy[country_key]
						|| data.CountGuestBy.night14.religion[country_key] || data.CountGuestBy.night14.other[country_key] || data.CountGuestBy.night14.professional[country_key]) {
						countNight14Rows++;
					}
					if (data.CountGuestBy.night28.vocation[country_key] || data.CountGuestBy.night28.education[country_key] || data.CountGuestBy.night28.therapy[country_key]
						|| data.CountGuestBy.night28.religion[country_key] || data.CountGuestBy.night28.other[country_key] || data.CountGuestBy.night28.professional[country_key]) {
						countNight28Rows++;
					}
					if (data.CountGuestBy.night90.vocation[country_key] || data.CountGuestBy.night90.education[country_key] || data.CountGuestBy.night90.therapy[country_key]
						|| data.CountGuestBy.night90.religion[country_key] || data.CountGuestBy.night90.other[country_key] || data.CountGuestBy.night90.professional[country_key]) {
						countNight90Rows++;
					}
					if (data.CountGuestBy.more90.vocation[country_key] || data.CountGuestBy.more90.education[country_key] || data.CountGuestBy.more90.therapy[country_key]
						|| data.CountGuestBy.more90.religion[country_key] || data.CountGuestBy.more90.other[country_key] || data.CountGuestBy.more90.professional[country_key]) {
						countMore90Rows++;
					}
				}
			}
			return (countNight0Rows > 4 ? (countNight0Rows - 4) : 0) +
				(countNight4Rows > 4 ? (countNight4Rows - 4) : 0) +
				(countNight7Rows > 4 ? (countNight7Rows - 4) : 0) +
				(countNight14Rows > 4 ? (countNight14Rows - 4) : 0) +
				(countNight28Rows > 4 ? (countNight28Rows - 4) : 0) +
				(countNight90Rows > 4 ? (countNight90Rows - 4) : 0) +
				(countMore90Rows > 4 ? (countMore90Rows - 4) : 0);
		};

		var base_BuildXamlView = controller.BuildXamlView;
		controller.BuildXamlView = function (id_form_div) {
			var content = base_BuildXamlView.call(this);
			var wellnessProfileContainsVariant = function (model, idVariants){
				if (model.WellnessProfile && 0 < model.WellnessProfile.length) {
					for (var i = 0; i < model.WellnessProfile.length; i++) {
						if (idVariants == model.WellnessProfile[i].id)
							return true;
					}
				}
				return false;
			}
			content.Content = print_tpl({
				form: this.model,
				date: h_times.nowLocalISOStringDateTime(),
				wellnessProfileContains: wellnessProfileContainsVariant,
				countAdditionalRows: countAdditionalRows(this.model)
			});
			return content;
		};

		controller.UseCodec(codec());

		return controller;
	}
});
