define([],function ()
{
	return function ()
	{
		var res =
		{
			ReportingPeriod: { id: (new Date).getFullYear(), text: (new Date).getFullYear() },
			ReportingPeriodQuarter: { id: 1, text: 'первый' },
			ReportingPeriodFrom: null,
			ReportingPeriodTill: null,
			PeriodOperation: null,
			HotelName: '',
			HotelAddress: '',
			OrganizationName: '',
			OrganizationAddress: '',
			OrganizationRepresentative: '',
			OrganizationRepresentativeJobTitle: '',
			OrganizationPhone: '',
			OrganizationEmail: '',
			OKPO: '',
			HotelType: null,
			HotelCategory: null,
			TaxSystem: null,
			WellnessProfile: null,
			CountRoom: {
				all: 0,
				highestCategory: 0,
				specialCategory: 0
			},
			RoomArea: 0,
			CountPlaces: 0,
			CountNights: {
				all: 0,
				junior: 0,
				oldster: 0
			},
			CountGuestRussian: {
				all: 0,
				junior: 0,
				oldster: 0
			},
			CountGuestForeign: {
				all: 0,
				junior: 0,
				oldster: 0
			},
			CountGuestByTravelVoucherRussian: {
				all: 0,
				junior: 0,
				oldster: 0
			},
			CountGuestByTravelVoucherForeign: {
				all: 0,
				junior: 0,
				oldster: 0
			},
			CountGuestByTreatment: {
				all: 0,
				junior: 0,
				oldster: 0
			},
			/*CountGuestByCountryCitizenship: null,
			CountGuestRussianByVisitPurpose: {
				vocation: 0,
				education: 0,
				therapy: 0,
				religion: 0,
				other: 0,
				professional: 0
			},
			CountGuestForeignByVisitPurpose: {
				vocation: 0,
				education: 0,
				therapy: 0,
				religion: 0,
				other: 0,
				professional: 0
			},
			CountGuestRussianByDurationStay: {
				night0: 0,
				night3: 0,
				night7: 0,
				night14: 0,
				night28: 0,
				night91: 0,
				night182: 0,
				more183: 0
			},
			CountGuestForeignByDurationStay: {
				night0: 0,
				night3: 0,
				night7: 0,
				night14: 0,
				night28: 0,
				night91: 0,
				night182: 0,
				more183: 0
			},*/
			AllCitizenships: null,
			CountGuestBy: {
				night0: {
					vocation: {},
					education: {},
					therapy: {},
					religion: {},
					other: {},
					professional: {}
				},
				night4: {
					vocation: {},
					education: {},
					therapy: {},
					religion: {},
					other: {},
					professional: {}
				},
				night7: {
					vocation: {},
					education: {},
					therapy: {},
					religion: {},
					other: {},
					professional: {}
				},
				night14: {
					vocation: {},
					education: {},
					therapy: {},
					religion: {},
					other: {},
					professional: {}
				},
				night28: {
					vocation: {},
					education: {},
					therapy: {},
					religion: {},
					other: {},
					professional: {}
				},
				night90: {
					vocation: {},
					education: {},
					therapy: {},
					religion: {},
					other: {},
					professional: {}
				},
				more90: {
					vocation: {},
					education: {},
					therapy: {},
					religion: {},
					other: {},
					professional: {}
				}
			}
		};
		return res;
	};
});