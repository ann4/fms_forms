define(['forms/fms/1ksr/c_1ksr', 'forms/fms/1ksr/ff_1ksr'],
function (CreateController, FileFormat) {
	var form_spec =
	{
		CreateController: CreateController
		, key: '1ksr'
		, Title: 'Сведения о деятельности КСР'
		, FileFormat: FileFormat
	};
	return form_spec;
});
