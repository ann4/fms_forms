﻿define([
	  'forms/fms/base/c_fms'
	, 'forms/fms/arrival/x_arrival'
	, 'forms/fms/base/town/c_town'
	, 'forms/fms/arrival/m_arrival'
	, 'forms/fms/base/town/h_town'
	, 'forms/base/h_names'
	, 'forms/fms/base/h_OfficialOrgan'
	, 'forms/base/h_times'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base//h_Select2'
	, 'forms/fms/base/h_tabs'
	, 'forms/fms/base/h_validate_n'
	, 'tpl!forms/fms/arrival/e_arrival.html'
	, 'tpl!forms/fms/arrival/v_arrival.xaml'
	, 'tpl!forms/fms/arrival/p_arrival.html'
	, 'forms/base/log'
	, 'forms/fms/questionary/h_questionary'
	, 'forms/fms/profile/h_abonent'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/base/addr/c_addr'
	, 'forms/base/h_msgbox'
	, 'forms/fms/base/h_citizenship'
],
function 
(
	  BaseFormController
	, codec
	, controller_Town
	, model_Arrival
	, helper_Town
	, helper_Names
	, helper_OfficialOrgan
	, helper_Times
	, helper_DocumentType
	, helper_Select2
	, helper_tabs
	, helper_validate
	, layout_tpl
	, print_tpl
	, attrshtml_tpl
	, GetLogger
	, helper_Questionary
	, h_abonent
	, h_addr
	, c_addr
	, h_msgbox
	, h_citizenship
	)
{
	return function ()
	{
		var log = GetLogger('c_arrival');

		var Select2Options = function (h_values)
		{
			return function (m)
			{
				return {
					placeholder: '',
					allowClear: true,
					query: function (q)
					{
						q.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), q.term) });
					}
				};
			}
		}

		var controller = BaseFormController(layout_tpl,
		{
			field_spec:
			{
				  DocumentGivenBy:      Select2Options({ GetValues: function () { return helper_Questionary.GetValues('v'); } })
				, DocumentGivenBy_code: Select2Options({ GetValues: function () { return helper_Questionary.GetValues('c'); } })
			}
		});

		controller.Sign = function () { }

		controller.UseProfile = function (profile)
		{
			if (!this.model)
				this.model = model_Arrival();
			if (profile && profile.Extensions && profile.Extensions.fms)
			{
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent)
				{
					var pAbonent = pfms.Abonent;
					pAbonent = h_abonent.SafeUpgradeAbonent(pAbonent);

					this.model.subdivision = h_abonent.SafeGet_dict_field(pAbonent, 'subdivision', this.model.subdivision);

					if (pAbonent.supplierInfo)
						this.model.supplierInfo = pAbonent.supplierInfo;
					if (pAbonent.AddressReal)
						this.model.RegAddress_variants = pAbonent.AddressReal;
					if (pAbonent.AddressReal && pAbonent.AddressReal.length == 1)
						this.model.RegAddress = pAbonent.AddressReal[0];

					var col = h_abonent.SafeGetDefaultColaborator(pAbonent);
					if (null != col)
					{
						if (col.LastName)
							this.model.fioComposing = col.LastName;

						if (col.FirstName)
							this.model.fioComposing = !this.model.fioComposing ? col.FirstName : this.model.fioComposing + ' ' + col.FirstName;
						
						if (col.MiddleName)
							this.model.fioComposing = !this.model.fioComposing ? col.MiddleName : this.model.fioComposing + ' ' + col.MiddleName;
					}
				}
				if (pfms.Forma)
				{
					this.ShowAttachmentsPanel = pfms.Forma.ShowAttachmentsPanel;
				}
			}
		}

		var UpdateAddressesLinksTextes = function ()
		{
			var null_text = 'Кликните, чтобы указать..';

			$('#cpw_form_Birthplace').text(!controller.model.Birthplace.text ? null_text : controller.model.Birthplace.text);
			$('#cpw_form_RegAddress').text(h_addr.SafePrepareReadableText(controller.model.RegAddress));
			$('#cpw_form_OldAddress').text(h_addr.SafePrepareReadableText(controller.model.OldAddress));

			$('#cpw_form_RegAddress').trigger('changeAddressText');
		}

		var DataLoad = function (content)
		{
			$('#cpw_form_LastName').val(content.LastName);
			$('#cpw_form_FirstName').val(content.FirstName);
			$('#cpw_form_MiddleName').val(content.MiddleName);

			$('#cpw_form_OldLastName').val(content.OldLastName);
			$('#cpw_form_OldFirstName').val(content.OldFirstName);
			$('#cpw_form_OldMiddleName').val(content.OldMiddleName);

			$('#cpw_form_Birthday').val(!content.Birthday ? '' : content.Birthday);
			$('#cpw_form_OldBirthday').val(!content.OldBirthday ? '' : content.OldBirthday);
			$('#cpw_form_DocumentGivenDate').val(!content.DocumentGivenDate ? '' : content.DocumentGivenDate);
			$('#cpw_form_DateFrom').val(!content.DateFrom ? '' : content.DateFrom);
			$('#cpw_form_DateTill').val(!content.DateTill ? '' : content.DateTill);
			$('#cpw_form_OtherReason').val(!content.OtherReason ? '' : content.OtherReason);

			content.dateComposing = content.dateComposing ? content.dateComposing : helper_Times.nowDateUTC('r');
			content.OldAddressSameCityAddress ? $('#cpw_form_OldAddressSameCityAddress').attr('checked', 'checked') : '';

			UpdateAddressesLinksTextes(content);

			$('#cpw_form_DocumentSeries').val(content.DocumentSeries);
			$('#cpw_form_DocumentNumber').val(content.DocumentNumber);

			if (!content.DocumentGivenBy)
				$('#cpw_form_DocumentGivenBy').select2('data', { id: null, text: '' });

			controller.DataLoadSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content.Sex)
			controller.DataLoadSex('#cpw_form_OldSex_male', '#cpw_form_OldSex_female', content.OldSex)

			$('#cpw_form_Nationality').select2('data', content.Nationality);
			$('#cpw_form_DocumentType').select2('data', content.DocumentType);
		}

		var DataSave = function (content)
		{
			content.LastName = $.trim($('#cpw_form_LastName').val());
			content.FirstName = $.trim($('#cpw_form_FirstName').val());
			content.MiddleName = $.trim($('#cpw_form_MiddleName').val());
			content.OldLastName = $.trim($('#cpw_form_OldLastName').val());
			content.OldFirstName = $.trim($('#cpw_form_OldFirstName').val());
			content.OldMiddleName = $.trim($('#cpw_form_OldMiddleName').val());
			content.Birthday = $('#cpw_form_Birthday').val();
			content.OldBirthday = $('#cpw_form_OldBirthday').val();
			content.DocumentGivenDate = $('#cpw_form_DocumentGivenDate').val();
			content.DocumentSeries = $('#cpw_form_DocumentSeries').val().replace(/\s+/g, '');
			content.DocumentNumber = $('#cpw_form_DocumentNumber').val().replace(/\s+/g, '');
			content.DateFrom = $.trim($('#cpw_form_DateFrom').val());
			content.DateTill = $.trim($('#cpw_form_DateTill').val());
			content.OtherReason = $.trim($('#cpw_form_OtherReason').val());

			content.Sex = controller.DataSaveSex('#cpw_form_Sex_male', '#cpw_form_Sex_female');
			content.OldSex = controller.DataSaveSex('#cpw_form_OldSex_male', '#cpw_form_OldSex_female');

			content.Nationality = $('#cpw_form_Nationality').select2('data');
			content.DocumentType = $('#cpw_form_DocumentType').select2('data');

			content.OldAddressSameCityAddress = $('#cpw_form_OldAddressSameCityAddress').is(':checked');

			var txt_creation = helper_Times.unixDateTimeStamp();

			controller.SafeFixUids(content, txt_creation, ['number', 'uid', 'requestId',
				'person_uid', 'person_personId', 'Document_uid']);
		}

		var base_Render = controller.Render;
		controller.Render = function (form_div_selector)
		{
			base_Render.call(this, form_div_selector);

			var elLastName = $('#cpw_form_LastName');

			elLastName.change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_FirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_MiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$('#cpw_form_OldLastName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_OldFirstName').change(helper_Names.OnNameChange_NormalizeAndLatinize);
			$('#cpw_form_OldMiddleName').change(helper_Names.OnNameChange_NormalizeAndLatinize);

			$('#cpw_form_Birthday').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_OldBirthday').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DocumentGivenDate').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DateFrom').datepicker(helper_Times.DatePickerOptions);
			$('#cpw_form_DateTill').datepicker(helper_Times.DatePickerOptions);

			this.RenderSelect2($('#cpw_form_Nationality'), h_citizenship);
			this.RenderSelect2($('#cpw_form_DocumentType'), helper_DocumentType);

			$('#cpw_form_DocumentGivenBy_code').on("select2-selecting", function (e)
			{
				var DocumentGivenBy = helper_Questionary.GetDocumentGivenBy_ByCode(e.object);
				$('#cpw_form_DocumentGivenBy').select2('data', DocumentGivenBy);
			});

			var content = this.model;
			var onUpdateAddressesLinksTextes = function () { UpdateAddressesLinksTextes(content); };
			var controller_Birthplace = controller_Town(this.model.Birthplace, onUpdateAddressesLinksTextes);

			$('#cpw_form_Birthplace').click(function (e) { controller_Birthplace.ShowModal(e); });

			$('#cpw_form_RegAddress').click(this.CreateOnClickAddressHandler(this.model, 'RegAddress', 'Место пребывания'));
			$('#cpw_form_OldAddress').click(this.OldAddress_Click);

			DataLoad(this.model);
			$('#form-tabs').tabs();
			helper_tabs.bindSwitch($('#form-tabs'), 'li', '#form-tab');

			$('.encoding_date').change(helper_Names.OnNameChange_NormalizeEncoding);

			elLastName.focus();
			elLastName.select();
		}

		controller.OnClickAddress = function (content, field_name, title)
		{
			var controller = c_addr();
			var field_value = content[field_name];
			if (null != field_value && (!field_value.Country || null == field_value.Country))
				field_value.Country = { id: 'RUS', text: "Россия" };
			controller.SetFormContent(field_value);
			var bname_Ok = 'Сохранить';
			h_msgbox.ShowModal({
				controller: controller
				, width: 750
				, height: 400
				, title: !title ? 'Адрес' : title
				, buttons: [bname_Ok, 'Отменить']
				, onclose: function (bname)
				{
					if (bname_Ok == bname)
					{
						content[field_name] = controller.GetFormContent();
						UpdateAddressesLinksTextes(content);
					}
				}
			});
		}

		controller.CreateOnClickAddressHandler = function (content, field_name, title)
		{
			return function (e)
			{
				e.preventDefault();
				controller.OnClickAddress(content, field_name, title);
			}
		}

		controller.RenderSelect2 = function (select2_element, h_values)
		{
			select2_element.select2({
				placeholder: '',
				allowClear: true,
				query: function (query)
				{
					query.callback({ results: helper_Select2.FindForSelect(h_values.GetValues(), query.term) });
				}
			});
		}

		controller.OldAddress_Click = function (e)
		{
			controller.model.OldAddressSameCityAddress = $('#cpw_form_OldAddressSameCityAddress').is(':checked');
			if (controller.model.OldAddressSameCityAddress)
				helper_Town.CopyFromTo(controller.model.RegAddress, controller.model.OldAddress);
			controller.OnClickAddress(controller.model, 'OldAddress', 'Прежнее место жительства');
		}

		controller.CreateNew = function (form_div_selector)
		{
			if (!this.model)
				this.model = model_Arrival();
			this.Render(form_div_selector);

			$('#cpw_form_Nationality').select2('data', { id: 'RUS', text: "Россия" });
			$('#cpw_form_DocumentType').select2('data', { id: 103008, text: "Паспорт гражданина Российской Федерации" });

			this.BeforeEditValidation();
		};

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return attrshtml_tpl(this.model);
		};

		controller.BuildXamlView = function (form_div_selector)
		{
			var RegAddress_parts = h_addr.PrepareAddressParts(this.model.RegAddress);
			var OldAddress_parts = h_addr.PrepareAddressParts(this.model.OldAddress);
			var content = print_tpl({
				form: this.model
				, helper_Town: helper_Town
				, helper_Times: helper_Times
				, helper_OfficialOrgan: helper_OfficialOrgan
				, RegAddress: RegAddress_parts
				, OldAddress: OldAddress_parts
			});
			return content;
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			this.model = base_GetFormContent.call(this);
			log.Debug('GetFormContent() {');
			DataSave(this.model);
			log.Debug('GetFormContent() }');
			return this.model;
		};

		controller.SetFormContent = function (content)
		{
			this.model = content;

			if (content.DocumentGivenBy_code && !content.DocumentGivenBy_code.text && content.DocumentGivenBy_code.id)
			{
				var item = helper_Select2.FindById(helper_Questionary.GetValues('c'), content.DocumentGivenBy_code.id)
				if (item)
					content.DocumentGivenBy_code.text = item.text;
			}

			return null;
		};

		controller.BeforeEditValidation = function ()
		{
			var rules = [
				{
					handler: helper_validate.RequiredValues,
					attributes: [
						[
							this.model.subdivision,
							this.model.supplierInfo
						]
					]
				},
			];
			var res_txt = ValidateInit(rules);
			if (null != res_txt)
				alert(helper_validate.ProfileErrorText);
		}

		controller.PrepareValidationElementText = function (txt, txt_to_add)
		{
			if (null == txt)
			{
				return txt_to_add;
			}
			else
			{
				return txt + ', ' + txt_to_add;
			}
		}

		var tabsStyledErrors = function (res)
		{
			$('.ui-tabs-nav a').removeClass('error');
			$(document).find('.error').each(function ()
			{
				var tab = $(this).closest('.ui-tabs-panel');
				var tabId = tab.attr('id');
				$(document).find('a[href="#' + tabId + '"]').addClass('error');
			})
		}

		var ValidateInit = function (rules)
		{
			var res = null;
			for (var i = 0; i < rules.length; i++)
			{
				var obj = rules[i];
				for (var j = 0; j < obj.attributes.length; j++)
				{
					var attribute = obj.attributes[j];
					if (!attribute.html_element)
					{
						var html_element = attribute;
						if (0 == html_element.length)
						{
							res = controller.PrepareValidationElementText(res, 'отсутствующий элемент "' + html_element.selector + '"');
						}
						else if (!obj.handler || !obj.handler(html_element))
						{
							try
							{
								res = controller.PrepareValidationElementText(res, html_element.attr('id'));
							}
							catch (ex)
							{
								res = controller.PrepareValidationElementText(res, 'элемент без id "' + html_element.selector + '"');
							}
						}
					}
					else
					{
					}
				}
			}
			tabsStyledErrors();
			return res;
		}

		controller.Validate = function ()
		{
			var rules = [
				{
					handler: helper_validate.RequiredValues,
					attributes: [
						[
							controller.model.subdivision,
							controller.model.supplierInfo
						]
					]
				},
				{
					handler: helper_validate.ValidateIsNotEmpty,
					attributes: [
						$('#cpw_form_FirstName'),
						$('#cpw_form_LastName'),
						$('#cpw_form_MiddleName'),
						/*$('#cpw_form_OldLastName'),
						$('#cpw_form_OldFirstName'),
						$('#cpw_form_OldMiddleName'),*/
						$('#cpw_form_DocumentNumber'),
						$('#cpw_form_DateFrom'),
						$('#cpw_form_DateTill')
					]
				},
			// Нужно переделать регулярку
			// {
			// 	handler: helper_validate.IsValidName,
			// 	attributes: [
			// 		$('#cpw_form_FirstName'),
			// 		$('#cpw_form_LastName'),
			// 		$('#cpw_form_MiddleName'),
			// 		$('#cpw_form_OldLastName'),
			// 		$('#cpw_form_OldFirstName'),
			// 		$('#cpw_form_OldMiddleName')
			// 	]
			// },
				{
				handler: helper_validate.IsValidDate,
				attributes: [
						$('#cpw_form_Birthday'),
						$('#cpw_form_DocumentGivenDate'),
						$('#cpw_form_DateFrom'),
						$('#cpw_form_DateTill')
					]
			},
				{
					handler: helper_validate.IsValidDateBeforeNow,
					attributes: [
						$('#cpw_form_Birthday'),
						$('#cpw_form_DocumentGivenDate'),
						$('#cpw_form_DateFrom')
					]
				},
				{
					handler: helper_validate.IsValidPeriodDateParams,
					attributes: [
						[
							$('#cpw_form_Birthday'),
							$('#cpw_form_DocumentGivenDate'),
							helper_validate.PeriodBirtdayErrorMsg
						],
						[
							$('#cpw_form_DateFrom'),
							$('#cpw_form_DateTill'),
							helper_validate.DocPeriodRegistrationErrorMsg
						]
					]
				},
				/*{
					handler: helper_validate.OnValidateAddressIsNotEmpty,
					attributes: [
						[
							this.model.RegAddress,
							'cpw_form_RegAddress'
						]
					]
				},*/
				{
					handler: helper_validate.ValidateSelect2IsNotEmpty,
					attributes: [
						$('#cpw_form_Nationality'),
						$('#cpw_form_DocumentType')
					]
				}
			];
			var res_txt = ValidateInit(rules);
			return null == res_txt ? null : helper_validate.ValidateErrorMsgConstruct + ' (' + res_txt + ')';
		};

		controller.UseCodec(codec());

		return controller;
	}
});