﻿define([
	  'forms/fms/base/town/m_town'
],function (TownAddress)
{
	return function ()
	{
		var res =
		{
			// нередактируемые атрибуты {
			subdivision: { id: -1, text: 'НЕ указано в профиле!' }, // {id:'', text:''}
			dateComposing: null, // 'ДД.ММ.ГГГГ'
			fioComposing: null,
			// нередактируемые атрибуты }

			FirstName : '',
			LastName : '',
			MiddleName : '',

			OldFirstName : '',
			OldLastName : '',
			OldMiddleName : '',

			Sex: { id: '', text: '' }, // М|Ж
			OldSex: { id: '', text: '' }, // М|Ж
			Birthday: null, // 'ДД.ММ.ГГГГ'
			OldBirthday: null, // 'ДД.ММ.ГГГГ'
			Nationality: null, // {id:'',text:''}
			Birthplace: TownAddress(),

			DocumentType: null, // {id:'',text:''}
			DocumentSeries: '',
			DocumentNumber: '',
			DocumentGivenDate: null, // 'ГГГГ-ММ-ДД'
			DocumentGivenBy_code: null, // {id:'',text:''}
			DocumentGivenBy: null, // {id:'',text:''}

			Document_status: {id: 102877, text: 'Действительный'},
			Document_uid: null, // {id:'',text:''}

			OldAddressSameCityAddress: false,

			RegAddress_variants: [],

			DateFrom: null, // 'ДД.ММ.ГГГГ'
			DateTill: null,  // 'ДД.ММ.ГГГГ'

			OtherReason: ''

		};
		return res;
	};
});