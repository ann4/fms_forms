define([
	  'forms/fms/arrival/c_a_arrival'
	, 'forms/fms/arrival/ff_arrival'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_arrival'
		, Title: 'Листок прибытия. Форма2'
		, FileFormat: FileFormat
	};
	return form_spec;
});
