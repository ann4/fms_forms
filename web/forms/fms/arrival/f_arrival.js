define([
	  'forms/fms/arrival/c_arrival'
	, 'forms/fms/arrival/ff_arrival'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'arrival'
		, Title: 'Листок прибытия. Форма2'
		, FileFormat: FileFormat
	};
	return form_spec;
});
