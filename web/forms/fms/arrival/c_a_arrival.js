﻿define([
	  'forms/fms/attachments/c_attachments'
	, 'forms/fms/arrival/c_arrival'
	, 'forms/fms/arrival/ff_arrival'
	, 'forms/base/log'
],
function (BaseController, BaseFormController, formSpec, GetLogger)
{
	return function()
	{
		var log = GetLogger('c_arrival');

		var controller = BaseController(BaseFormController, formSpec);

		controller.Sign = function () { };

		controller.SetStayPeriod = function (period) { }
		controller.GetStayPeriod = function () { return null; }

		controller.GetBody = function () {
			var body = 'Направляем документ "Адресный листок прибытия".';
			if ($('#cpw_form_LastName').val() != '')
			{
				body += '\r\nПрибывший  гражданин: ' + $('#cpw_form_LastName').val();
				if ($('#cpw_form_FirstName').val() != '')
					body += ' ' + $('#cpw_form_FirstName').val().substr(0, 1) + '.';
				if ($('#cpw_form_MiddleName').val() != '')
					body += '' + $('#cpw_form_MiddleName').val().substr(0, 1) + '.';
				var birthDate = $('#cpw_form_Birthday').val();
				if (birthDate) {
					var birthDateParts = birthDate.split('.');
					if (birthDateParts && birthDateParts.length == 3)
						body += ', ' + birthDateParts[2] + ' г.р.';
				}
			}
			return body;
		}

		return controller;
	}
});