﻿define([
	  'forms/fms/base/x_fms'
	, 'forms/fms/arrival/m_arrival'
	, 'forms/fms/base/town/m_town'
	, 'forms/fms/base/town/h_town'
	, 'txt!forms/fms/schemes/1.3.36/addrSheet.xsd'
	, 'txt!forms/fms/schemes/1.3.36/core.xsd'
	, 'txt!forms/fms/schemes/1.3.36/xmldsig-core-schema-without-dtd.xml'
	, 'forms/base/log'
	, 'forms/fms/base/addr/h_addr'
],
function (BaseCodec, model_arrival, model_town, helper_town, addrSheet, core, xmldsig, GetLogger, h_addr)
{
	var log = GetLogger('x_arrival');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.tabs = ['', '  ', '    ', '      ', '        ', '          ', '            ', '              ', '                ', '                  '];
		res.x_fms_base.tabs = res.tabs;

		res.GetRootNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/addrSheet'; };

		res.Encode = function (m_arrival)
		{
			var xml_string = '<?xml version="1.0" encoding="utf-8"?>' + this.FormatEOL();
			xml_string += '<addrSheet:addressSheetRoot' + this.FormatEOL();
			xml_string += ' xmlns="http://umms.fms.gov.ru/replication/core"' + this.FormatEOL();
			xml_string += ' xmlns:addrSheet="http://umms.fms.gov.ru/replication/addrSheet">' + this.FormatEOL();
			xml_string += this.FormatEOL();

			xml_string += this.OpenTagLine(1, 'addrSheet:arrivalAddressSheet');

			xml_string += this.OpenTagLine(2, 'addrSheet:personData');
			xml_string += this.StringifyField(3, "uid", m_arrival.person_uid);
			xml_string += this.StringifyField(3, "personId", m_arrival.person_personId);
			xml_string += this.StringifyField(3, "lastName", m_arrival.LastName);
			xml_string += this.StringifyField(3, "firstName", m_arrival.FirstName);
			xml_string += this.SafeStringifyField(3, "middleName", m_arrival.MiddleName);
			xml_string += this.EncodeSex(3, m_arrival.Sex);
			xml_string += this.SafeStringifyField(3, "birthDate", m_arrival.Birthday);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_arrival.Nationality, 'citizenship', 'citizenship');
			xml_string += this.EncodeBirthplace(3, m_arrival.Birthplace);
			xml_string += this.CloseTagLine(2, 'addrSheet:personData');

			xml_string += this.OpenTagLine(2, 'addrSheet:document');
			xml_string += this.StringifyField(3, "uid", m_arrival.Document_uid);
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_arrival.DocumentType, 'type', 'DocumentType');
			xml_string += this.SafeStringifyField(3, "series", m_arrival.DocumentSeries);
			xml_string += this.StringifyField(3, "number", m_arrival.DocumentNumber);
			if (null != m_arrival.DocumentGivenBy && null != m_arrival.DocumentGivenBy_code)
				xml_string += this.EncodeNamedDictionaryItem(3, { id: m_arrival.DocumentGivenBy_code.id, text: m_arrival.DocumentGivenBy.text }, 'authorityOrgan', 'OfficialOrgan');
			xml_string += this.SafeStringifyField(3, "issued", res.DateEncode(m_arrival.DocumentGivenDate));
			xml_string += this.SafeEncodeNamedDictionaryItem(3, m_arrival.Document_status, 'status', 'DocumentStatus');
			xml_string += this.CloseTagLine(2, 'addrSheet:document');

			if ((m_arrival.OldLastName && m_arrival.OldLastName != '') && (m_arrival.OldFirstName && m_arrival.OldFirstName != ''))
			{
				xml_string += this.OpenTagLine(2, 'addrSheet:changePersonData');
				xml_string += this.StringifyField(3, "uid", m_arrival.person_uid);
				xml_string += this.StringifyField(3, "personId", m_arrival.person_personId);
				xml_string += this.StringifyField(3, "lastName", m_arrival.OldLastName);
				xml_string += this.StringifyField(3, "firstName", m_arrival.OldFirstName);
				xml_string += this.SafeStringifyField(3, "middleName", m_arrival.OldMiddleName);
				xml_string += this.EncodeSex(3, m_arrival.OldSex);
				xml_string += this.SafeStringifyField(3, "birthDate", m_arrival.OldBirthday);
				xml_string += this.CloseTagLine(2, 'addrSheet:changePersonData');
			}

			xml_string += this.SafeStringifyField(2, "addrSheet:otherRegistrationReason", m_arrival.OtherReason);

			if (m_arrival.dateComposing != null || m_arrival.fioComposing != null)
			{
				xml_string += this.OpenTagLine(2, 'addrSheet:composingResponsibleData');
				xml_string += this.StringifyField(3, "addrSheet:date", res.DateEncode(m_arrival.dateComposing));
				xml_string += this.StringifyField(3, "addrSheet:fio", m_arrival.fioComposing);
				xml_string += this.CloseTagLine(2, 'addrSheet:composingResponsibleData');
			}

			xml_string += this.OpenTagLine(2, 'addrSheet:stayingRegistrationData');
			xml_string += this.StringifyField(3, "dateFrom", res.DateEncode(m_arrival.DateFrom));
			xml_string += this.StringifyField(3, "dateTo", res.DateEncode(m_arrival.DateTill));
			if (m_arrival.RegAddress && null != m_arrival.RegAddress)
				xml_string += this.EncodeAddress(3, 'address', m_arrival.RegAddress);
			xml_string += this.CloseTagLine(2, 'addrSheet:stayingRegistrationData');

			xml_string += this.SafeEncodeNamedDictionaryItem(2, m_arrival.subdivision, 'addrSheet:territorialRegOrgan', 'officialOrgan.fms');

			if (!h_addr.IsEmptyAddress(m_arrival.OldAddress))
			{
				if (m_arrival.OldAddressSameCityAddress)
				{
					xml_string += this.EncodeAddress(2, 'addrSheet:previousAddressSameCityAddress', m_arrival.OldAddress);
				}
				else
				{
					xml_string += this.EncodeAddress(2, 'addrSheet:previousAddress', m_arrival.OldAddress);
				}
			}

			xml_string += this.CloseTagLine(1, 'addrSheet:arrivalAddressSheet');

			xml_string += this.FormatEOL();
			xml_string += '</addrSheet:addressSheetRoot>';
			return xml_string;
		};

		res.DecodeOldPersonData = function (node, m_arrival)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "lastName": m_arrival.OldLastName = child.text; break;
					case "firstName": m_arrival.OldFirstName = child.text; break;
					case "middleName": m_arrival.OldMiddleName = child.text; break;
					case "birthDate": m_arrival.OldBirthday = child.text; break;
					case "gender": m_arrival.OldSex = self.DecodeSex(child); break;
					case 'citizenship': m_arrival.OldNationality = self.DecodeDictionaryItem(child, 'citizenship'); break;
					case 'birthPlace': m_arrival.OldBirthplace = self.DecodeBirthPlace(child); break;
				}
			});
		}

		res.DecodeStayingRegistration = function (node, m_arrival)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "dateFrom": m_arrival.DateFrom = res.DateDecode(child.text); break;
					case "dateTo": m_arrival.DateTill = res.DateDecode(child.text); break;
					case "address": m_arrival.RegAddress = self.DecodeAddressAddress(child); break;
				}
			});
		}

		res.DecodeResponsibleData = function (node, m_arrival)
		{
			log.Debug('DecodeResponsibleData {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "addrSheet:date": m_arrival.dateComposing = res.DateDecode(child.text); break;
					case "addrSheet:fio": m_arrival.fioComposing = child.text; break;
				}
			});
			log.Debug('DecodeResponsibleData }');
		}

		res.DecodeArrivalAddressSheet = function (node, m_arrival)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (child.tagName)
				{
					case "addrSheet:territorialRegOrgan": m_arrival.subdivision = self.DecodeDictionaryItem(child, 'officialOrgan.fms'); break;
					case "addrSheet:personData": self.DecodePersonData(child, m_arrival); break;
					case "addrSheet:document": self.DecodePersonDataDocument_Document(child, m_arrival); break;
					case "addrSheet:changePersonData": self.DecodeOldPersonData(child, m_arrival); break;
					case "addrSheet:otherRegistrationReason": m_arrival.OtherReason = child.text; break;
					case "addrSheet:composingResponsibleData": self.DecodeResponsibleData(child, m_arrival); break;
					case "addrSheet:stayingRegistrationData": self.DecodeStayingRegistration(child, m_arrival); break;
					case "addrSheet:previousAddress": self.DecodeAddressAddress(child, m_arrival.OldAddress); break;
					case "addrSheet:previousAddressSameCityAddress":
						m_arrival.OldAddressSameCityAddress = true;
						m_arrival.OldAddress= self.DecodeAddressAddress(child); break;
				}
			});
		};

		res.DecodeXmlDocument = function (doc)
		{
			var m_arrival = model_arrival();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "addrSheet:arrivalAddressSheet": this.DecodeArrivalAddressSheet(child, m_arrival); break;
				}
			}
			return m_arrival;
		};

		res.GetScheme = function ()
		{
			var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");

			xs.add("http://www.w3.org/2000/09/xmldsig#", res.LoadXsd(xmldsig));
			xs.add("http://umms.fms.gov.ru/replication/core", res.LoadXsd(core));
			xs.add("http://umms.fms.gov.ru/replication/addrSheet", res.LoadXsd(addrSheet));

			return xs;
		};

		log.Debug('Create }');
		return res;
	}
});