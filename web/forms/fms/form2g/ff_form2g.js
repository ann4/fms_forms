define([],
function (CreateController) {
	var file_format =
	{
		FilePrefix: 'Form2g'
		, FileExtension: 'xls'
		, Encoding: 65001
		, Description: 'Журнал регистрации иностранных граждан в гостинице (Форма 2-Г).'
	};
	return file_format;
});
