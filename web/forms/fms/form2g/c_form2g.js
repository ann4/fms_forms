define([
	  'forms/fms/base/c_reporting'
	, 'forms/base/log'
	, 'forms/fms/form2g/m_form2g'
	, 'forms/fms/form2g/x_form2g'
	, 'forms/fms/form2g/ff_form2g'
	, 'tpl!forms/fms/form2g/e_form2g.html'
	, 'tpl!forms/fms/form2g/v_form2g.xml'
	, 'forms/base/h_times'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/h_DocumentType'
],
function (binded_controller, GetLogger, model_report, codec, form_spec, layout_tpl, print_tpl, h_times, h_select2, h_DocumentType)
{
	return function ()
	{
		var log = GetLogger('c_form2g');

		var range = function (start, end) {
			var res = [];
			for (var i = start; i <= end; i++) {
				res.push({ id: i.toString(), text: i.toString() });
			}
			return res;
		};

		var select2Options = function (h_values) {
			return function (m) {
				return {
					placeholder: '',
					allowClear: true,
					query: function (q) {
						q.callback({ results: h_select2.FindForSelect(h_values, q.term) });
					}
				};
			}
		};

		var lastDayOfMonth = function (y, m) {
			return new Date(y, m, 0).getDate();
		};

		var controller = binded_controller(layout_tpl, form_spec, {
			extension_key: 'form2g'
			, field_spec:
			{
				ReportingPeriodYear: select2Options(range((new Date).getFullYear() - 5, (new Date).getFullYear()))
				, ReportingPeriodMonthFrom: select2Options([
					{ id: 1, text: 'январь' },
					{ id: 2, text: 'февраль' },
					{ id: 3, text: 'март' },
					{ id: 4, text: 'апрель' },
					{ id: 5, text: 'май' },
					{ id: 6, text: 'июнь' },
					{ id: 7, text: 'июль' },
					{ id: 8, text: 'август' },
					{ id: 9, text: 'сентябрь' },
					{ id: 10, text: 'октябрь' },
					{ id: 11, text: 'ноябрь' },
					{ id: 12, text: 'декабрь' }
				])
				, ReportingPeriodMonthTill: select2Options([
					{ id: 1, text: 'январь' },
					{ id: 2, text: 'февраль' },
					{ id: 3, text: 'март' },
					{ id: 4, text: 'апрель' },
					{ id: 5, text: 'май' },
					{ id: 6, text: 'июнь' },
					{ id: 7, text: 'июль' },
					{ id: 8, text: 'август' },
					{ id: 9, text: 'сентябрь' },
					{ id: 10, text: 'октябрь' },
					{ id: 11, text: 'ноябрь' },
					{ id: 12, text: 'декабрь' }
				])
			}
		});

		controller.SafeCreateNewContent = function () {
			if (!this.model)
				this.model = model_report();
		};

		controller.UseProfile = function (profile) {
			log.Debug('UseProfile {');
			this.SafeCreateNewContent();
			if (profile && profile.Extensions && profile.Extensions.fms) {
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent) {
					var pAbonent = pfms.Abonent;
					this.model.HotelName = pAbonent.OrganizationName;
				}
			}
			log.Debug('UseProfile }');
		};

		var base_Render = controller.Render;
		controller.Render = function (id_form_div) {
			base_Render.call(this, id_form_div);
			$('#cpw_form_ReportingPeriodMonthFrom').focus();
		};

		controller.SetFormContent = function (content) {
			log.Debug('SetFormContent {');
			this.SafeCreateNewContent();
			if (content) {
				this.model.ReportingPeriodMonthFrom = content.ReportingPeriodMonthFrom;
				this.model.ReportingPeriodMonthTill = content.ReportingPeriodMonthTill;
				this.model.ReportingPeriodYear = content.ReportingPeriodYear;
				this.model.Form = content.Form;
			}
			preparePeriodDate(this.model);
			if (this.model && this.model.ReportingPeriodFrom) {
				this.model.ReportingPeriodFromDate = controller.parseDate(this.model.ReportingPeriodFrom);
			}
			if (this.model && this.model.ReportingPeriodTill) {
				this.model.ReportingPeriodTillDate = controller.parseDate(this.model.ReportingPeriodTill);
			}
			log.Debug('SetFormContent }');
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			this.model = base_GetFormContent.call(this);
			preparePeriodDate(this.model);
			this.model.Form = 'FMS-M';
			this.model.listData = [];
			return this.model;
		};

		var preparePeriodDate = function (model) {
			if (model && model.ReportingPeriodMonthFrom && model.ReportingPeriodMonthTill && model.ReportingPeriodYear) {
				model.ReportingPeriodFrom = "01." + (model.ReportingPeriodMonthFrom.id > 9 ? '' : '0') + model.ReportingPeriodMonthFrom.id + "." + model.ReportingPeriodYear.id;
				var dayTill = lastDayOfMonth(model.ReportingPeriodYear.id, model.ReportingPeriodMonthTill.id);
				model.ReportingPeriodTill = dayTill + "." + (model.ReportingPeriodMonthTill.id > 9 ? '' : '0') + model.ReportingPeriodMonthTill.id + "." + model.ReportingPeriodYear.id;
			}
		};

		var checkByDates = function (data, startDate, finishDate, model) {
			var start = controller.parseDate(data.stayPeriod_dateFrom);
			if (null == start) {
				controller.writeWarningToLog("  Не указана дата прибытия!");
				return false;
			}
			var finish = null;
			if (data.DateUnreg) {
				finish = controller.parseDate(data.DateUnreg);
			}
			if (null == finish) {
				finish = controller.parseDate(data.stayPeriod_dateTo);
				if (!finish) {
					controller.writeWarningToLog("  Не указана дата убытия! Предполагается, что гражданин выехал через 90 дней.");
					finish = controller.parseDate(data.stayPeriod_dateFrom);
					finish.setDate(finish.getDate() + 90);
				}
			}
			var isSuit = (start <= startDate && finish > startDate && finish <= finishDate) ||
				(start >= startDate && start <= finishDate && finish >= finishDate) ||
				(start >= startDate && finish <= finishDate) ||
				(start <= startDate && finish >= finishDate);
			return isSuit;
		};

		var safeAddDataToList = function(data, model) {
			var _data = {};
			_data.HotelName = data.Hotel_organization_name;
			_data.DateSend = '';
			if (null != data.DateSend) {
				var dateSend = controller.parseDate(data.DateSend);
				_data.DateSend = dateSend.toLocaleString();
				var lastIndex = _data.DateSend.lastIndexOf(':');
				if (lastIndex != -1) {
					_data.DateSend = _data.DateSend.substring(0, lastIndex);
				}
			}
			_data.Name = data.LastName + ' ' + data.FirstName + ' ' + data.MiddleName + ' / ' + 
				data.LastName_latin + ' ' + data.FirstName_latin + ' ' + data.MiddleName_latin + ' / ' +
				(null == data.Sex ? '' : ('Ж' == data.Sex.id || 'F' == data.Sex.id) ? 'Ж' : 'М');
			var dateBirthDate = controller.parseDate(data.Birthday);
			_data.BirthYear = null == dateBirthDate ? 0 : dateBirthDate.getFullYear();
			_data.DocumentPersonal = (null == data.Nationality ? '' : data.Nationality.text + ', ') +
				(null == data.DocumentType ? '' : h_DocumentType.GetShortName(data.DocumentType) + ' ') +
				(null == data.DocumentSeries || '' == data.DocumentSeries ? '' : data.DocumentSeries + ' ') +
				(null == data.DocumentNumber || '' == data.DocumentNumber ? '' : '№' + data.DocumentNumber);
			_data.Migration = (null == data.MigrationCard_kpp ? 'КПП не указан, ' : data.MigrationCard_kpp.text + ', ') +
				((null != data.MigrationCard_series && '' != data.MigrationCard_series) || (null != data.MigrationCard_number && '' != data.MigrationCard_number) ? 'МК ' : '') +
				(null == data.MigrationCard_series || '' == data.MigrationCard_series ? '' : data.MigrationCard_series + ' ') +
				(null == data.MigrationCard_number || '' == data.MigrationCard_number ? '' : '№' + data.MigrationCard_number);
			_data.Visa = '';
			if (null != data.Visa_type) {
				_data.Visa = h_DocumentType.GetShortName(data.Visa_type) + ' ' +
				(null == data.Visa_series || '' == data.Visa_series ? '' : data.Visa_series + ' ') +
				(null == data.Visa_number || '' == data.Visa_number ? '' : '№' + data.Visa_number + ' ') +
				'до ' + data.Visa_validTo + ', ';
			}
			_data.Visa = _data.Visa + 'прибыл до ' + data.stayPeriod_dateTo;
			_data.DateUnreg = null == data.DateUnreg ? '' : data.DateUnreg;
			_data.OtherInfo = '';
			if (null != data.LastName_Relation && null != data.FirstName_Relation) {
				_data.OtherInfo = 'з/представ. ' + data.LastName_Relation + ' ' + data.FirstName_Relation + ' ' + data.MiddleName_Relation + ' / ' + 
					data.LastName_Relation_latin + ' ' + data.FirstName_Relation_latin + ' ' + data.MiddleName_Relation_latin + ' / ' + 
					(null == data.Sex_Relation ? '' : ('Ж' == data.Sex_Relation.id || 'F' == data.Sex_Relation.id) ? 'Ж, ' : 'М, ') +
					(null == data.Nationality_Relation ? '' : data.Nationality_Relation.text + ', ') +
					(null == data.DocumentType_Relation ? '' : h_DocumentType.GetShortName(data.DocumentType_Relation) + ' ') +
					(null == data.DocumentSeries_Relation ? '' : data.DocumentSeries_Relation + ' ') +
					(null == data.DocumentNumber_Relation ? '' : '№' + data.DocumentNumber_Relation);
			}
			if (null != data.StateProgram && 'Нет' != data.StateProgram.text) {
				_data.OtherInfo += ('' == _data.OtherInfo ? '' : ', ') + data.StateProgram.text;
			}
			model.listData.push(_data);
		};


		controller.ProcessingData = function (data) {
			log.Debug('ProcessingData {');
			if (!this.model || !this.model.ReportingPeriodFrom || !this.model.ReportingPeriodTill) {
				controller.writeWarningToLog("  Не заданы параметры проверки!");
				return null;
			}
			this.model.ReportingPeriodFromDate = controller.parseDate(this.model.ReportingPeriodFrom);
			this.model.ReportingPeriodTillDate = controller.parseDate(this.model.ReportingPeriodTill);
			var result = checkByDates(data, this.model.ReportingPeriodFromDate, this.model.ReportingPeriodTillDate, this.model);
			if (!result) {
				controller.writeWarningToLog("  Период пребывания не соответствует указанному!");
				return false;
			} else {
				if (!this.model.listData) {
					this.model.listData = [];
				}
				safeAddDataToList(data, this.model);
			}
			log.Debug('ProcessingData }');
			return result;
		};

		var base_BuildXamlView = controller.BuildXamlView;
		controller.BuildXamlView = function () {
			var content = base_BuildXamlView.call(this);
			content.Content = print_tpl({
				form: this.model,
				trim: function(value) { return value.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, ''); },
				date: h_times.nowLocalISOStringDateTime()
			});
			return content;
		};

		controller.UseCodec(codec());

		return controller;
	}
});
