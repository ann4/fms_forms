define([],function ()
{
	return function ()
	{
		var res =
		{
			ReportingPeriodMonthFrom: null,
			ReportingPeriodMonthTill: null,
			ReportingPeriodYear: null,
			listData: [],
			Form: 'FMS-M'
		};
		return res;
	};
});