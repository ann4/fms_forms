{
	"uid": "1800000008002001-1437917760-1",
	"requestId": "1800000008002001-1437917760-2",
	"supplierInfo": "1800000008002001",
	"subdivision": {
		"id": "3089",
		"text": "МИЯКИНСКИЙ РОВД РЕСПУБЛИКИ БАШКОРТОСТАН"
	},
	"sono": {
		"id": "128752",
		"text": "Межрайонная инспекция Федеральной налоговой службы № 11 по Краснодарскому краю"
	},
	"authority": "",
	"employee": {
		"ummsId": "1001"
	},
	"date": "2015-07-26T17:36:00+04:00",
	"notificationReceived": "2015-05-17",
	"RvpDocumentGivenBy_from_dict": false,
	"FirstName": "Иван",
	"LastName": "Иванов",
	"MiddleName": "Иванович",
	"FirstName_latin": "Ivan",
	"LastName_latin": "Ivanov",
	"MiddleName_latin": "Ivanovich",
	"person_uid": "1800000008002001-1437917760-3",
	"person_personId": "1800000008002001-1437917760-4",
	"number": "1800000008002001-1437917760-0",
	"DocumentRelation_uid": null,
	"RelationType": "",
	"DocumentType_Relation": null,
	"DocumentStatus_Relation": {
		"id": 102877,
		"text": "Действительный"
	},
	"DocumentSeries_Relation": "",
	"DocumentNumber_Relation": "",
	"DocumentGivenDate_Relation": "",
	"DocumentFinishDate_Relation": "",
	"Sex_Relation": {
		"id": "",
		"text": ""
	},
	"Birthplace_Relation": {
		"Country": null,
		"Region": null,
		"Area": null,
		"City": null,
		"Town": null,
		"text": ""
	},
	"Nationality_Relation": null,
	"Sex": {
		"id": "M",
		"text": "Мужской"
	},
	"Birthday": "03.02.1976",
	"Nationality": {
		"id": "PYF",
		"text": "Французская Полинезия"
	},
	"Birthplace": {
		"Country": {
			"id": "KHM",
			"text": "Камбоджа"
		},
		"Region": null,
		"Area": null,
		"City": "Пномпень",
		"Town": null,
		"text": "Камбоджа, Пномпень"
	},
	"PersonPhone": "",
	"Phone": "322223",
	"DocumentType": {
		"id": "103009",
		"text": "Национальный заграничный паспорт"
	},
	"DocumentSeries": "1234",
	"DocumentNumber": "567896",
	"Document_issued": null,
	"DocumentGivenDate": "03.02.2002",
	"DocumentValidFrom": null,
	"DocumentFinishDate": "04.05.2022",
	"DocumentGivenBy_code": null,
	"DocumentGivenBy": null,
	"Document_DocumentStatus": {
		"id": "102877",
		"text": "Действительный"
	},
	"Document_uid": "1800000008002001-1437917760-6",
	"Document_entered": "false",
	"Visa_uid": "1800000008002001-1437917760-7",
	"Visa_type": {
		"id": "135709",
		"text": "Вид на жительство ИГ"
	},
	"Visa_series": "82",
	"Visa_number": "0907871",
	"Visa_issued": "17.05.2015",
	"Visa_validFrom": null,
	"Visa_validTo": "17.06.2020",
	"Visa_category": null,
	"Visa_multiplicity": null,
	"Visa_identifier": null,
	"Visa_visitPurpose": null,
	"Visa_DocumentStatus": {
		"id": "102877",
		"text": "Действительный"
	},
	"Visa_entered": "false",
	"MigrationCard_uid": null,
	"MigrationCard_series": "",
	"MigrationCard_number": "",
	"MigrationCard_dateFrom": null,
	"MigrationCard_dateTo": null,
	"MigrationCard_entranceDate": null,
	"MigrationCard_EntranceCheckpoint": null,
	"MigrationCard_visitPurpose": null,
	"MigrationCard_kpp": null,
	"profession": null,
	"noticeFrom": {
		"id": "02"
	},
	"hotel_id": "780-100",
	"notificationNumber": null,
	"firstArrival": false,
	"stateProgramMember": null,
	"entrancePurpose": {
		"id": "139354",
		"text": "Коммерческая"
	},
	"stayPlace": null,
	"AppartmentNumber": "",
	"AppartmentNumber_Company": "",
	"AppartmentNumber_CompanyN": "",
	"stayPeriod_dateFrom": "17.05.2015",
	"stayPeriod_dateTo": "11.06.2015",
	"local_stayPeriod_dateFrom": null,
	"Hotel_organization_uid": "1800000008002001-1437917760-9",
	"Hotel_organization_inn": "500100732259",
	"Hotel_organization_phone": "02",
	"Hotel_organization_name": "Гостиница гемологического профиля От заката до рассвета",
	"Hotel_address_variants": [],
	"Hotel_Document": "",
	"Organization_Person_uid": "1800000008002001-1437917760-10",
	"Organization_Person_personId": "1800000008002001-1437917760-12",
	"Organization_Person_lastName": "Федюлькин",
	"Organization_Person_firstName": "Фидель",
	"Organization_Person_middleName": "Аристархович",
	"Organization_Person_gender": {
		"id": "M",
		"text": "Мужской"
	},
	"Organization_Person_nationality": null,
	"Organization_Person_birthDate": "07.02.1965",
	"Organization_Person_birthPlace": {
		"Country": null,
		"Region": null,
		"Area": null,
		"City": null,
		"Town": null,
		"text": ""
	},
	"Organization_Document_uid": "1800000008002001-1437917760-11",
	"Organization_Document_series": "0192",
	"Organization_Document_number": "384756",
	"Organization_Document_authorityOrgan": {
		"id": "3977",
		"text": "МВД УДМУРТСКОЙ РЕСПУБЛИКИ"
	},
	"Organization_Document_authorityOrgan_text": null,
	"Organization_Document_authorityOrgan_check": true,
	"Organization_Document_issued": "08.03.2000",
	"Organization_Document_validFrom": null,
	"Organization_Document_validTo": null,
	"DocumentType_Company": {
		"id": "103008",
		"text": "Паспорт гражданина Российской Федерации"
	},
	"Organization_Document_DocumentStatus": {
		"id": 102877,
		"text": "Действительный"
	},
	"Organization_Document_entered": "false",
	"DocumentScan": [],
	"DocumentScanDoc": [],
	"DocumentScanVisa": [],
	"DocumentStatus": {
		"id": "102877",
		"text": "Действительный"
	},
	"Address": {
		"Country": {
			"id": "RUS",
			"text": "Россия"
		},
		"russianAddress": {
			"variant_fias": true,
			"fias": [
				{
					"AOLEVEL": "7",
					"text": "ул Лихвинцева",
					"STREETCODE": "0239",
					"IsTerminal": true,
					"AOID": "dd2c6b49-6681-46bd-b51a-c3daa1a7a9fc"
				},
				{
					"AOLEVEL": "4",
					"text": "г Ижевск",
					"CITYCODE": "001",
					"IsTerminal": false
				},
				{
					"AOLEVEL": "1",
					"text": "Респ Удмуртская",
					"REGIONCODE": "18",
					"IsTerminal": false
				}
			],
			"Дом": "56",
			"Дом_Type": {
				"id": "1202",
				"text": "Дом"
			},
			"Квартира": "67",
			"Квартира_Type": {
				"id": "1303",
				"text": "Квартира"
			}
		}
	},
	"RvpDocumentGivenBy_text": "УФМС России по Воронежской области",
	"LivingDocDecisionNumber": "1452/14",
	"LivingDocDecisionDate": "17.06.2015",
	"StateProgram": {
		"id": "1022",
		"text": "ВКС"
	},
	"Address_Company": {
		"Country": {
			"id": "RUS",
			"text": "Россия"
		},
		"russianAddress": {
			"variant_fias": true,
			"fias": [
				{
					"AOLEVEL": "7",
					"text": "ул Ильинка",
					"STREETCODE": "7027",
					"IsTerminal": true,
					"AOID": "6179b355-d907-4ac1-9d53-f4d854a195f2"
				},
				{
					"AOLEVEL": "1",
					"text": "г Москва",
					"REGIONCODE": "77",
					"IsTerminal": false
				}
			],
			"Дом": "1",
			"Дом_Type": {
				"id": "1202",
				"text": "Дом"
			},
			"Квартира": "2",
			"Квартира_Type": {
				"id": "1303",
				"text": "Квартира"
			}
		}
	},
	"headOrganization": "true",
	"Company_DocumentStatus": {
		"id": "102877",
		"text": "Действительный"
	},
	"Address_CompanyN": {
		"Country": {
			"id": "RUS",
			"text": "Россия"
		},
		"russianAddress": {
			"variant_fias": true,
			"fias": [
				{
					"AOLEVEL": "7",
					"text": "ул Ворошилова",
					"STREETCODE": "0063",
					"IsTerminal": true,
					"AOID": "1c9b2794-ed4a-4c0d-9b0e-3c29a951e209"
				},
				{
					"AOLEVEL": "4",
					"text": "г Ижевск",
					"CITYCODE": "001",
					"IsTerminal": false
				},
				{
					"AOLEVEL": "1",
					"text": "Респ Удмуртская",
					"REGIONCODE": "18",
					"IsTerminal": false
				}
			],
			"Дом": "222",
			"Дом_Type": {
				"id": "1202",
				"text": "Дом"
			},
			"Корпус": "1",
			"Квартира": "67",
			"Квартира_Type": {
				"id": "1303",
				"text": "Квартира"
			}
		}
	},
	"Profession": {
		"text": "Следователь"
	}
}