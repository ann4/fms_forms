define(['forms/fms/form2g/c_form2g', 'forms/fms/form2g/ff_form2g'],
function (CreateController, FileFormat) {
	var form_spec =
	{
		CreateController: CreateController
		, key: 'form2g'
		, Title: 'Форма 2-Г'
		, FileFormat: FileFormat
	};
	return form_spec;
});
