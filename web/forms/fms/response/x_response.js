﻿define([
      'forms/fms/base/x_fms'
    , 'forms/fms/response/m_response'
    , 'forms/base/log'
],
function (BaseCodec, model_response, GetLogger)
{
    var log = GetLogger('x_response');

    return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.GetRootNamespaceURI = function () { return 'http://umms.fms.gov.ru/hotel/hotel-response'; };

		res.FixXmlField = function (txt)
		{
			if (0 == txt.indexOf('<'))
			{
				return txt;
			}
			else
			{
				strpart = function (str, n)
				{
					if (n > 0)
					{
						return str.match(new RegExp(".{1," + n + "}", "g"));
					} else if (n < 0)
					{
						var an = Math.abs(n), t1 = (Math.ceil(str.length / an) * an) - str.length, t2 = "";
						for (var i = 0; i < t1; i++)
						{
							t2 += "_";
						}
						t2 += str;
						t2 = t2.match(new RegExp(".{1," + an + "}", "g"));
						t2[0] = t2[0].substr(t1, an - t1);
						return t2;
					}
				}
				function hex2a(hex)
				{
					var s = strpart(hex, 2), output = "";
					for (var i = 0; i < s.length; i++)
					{
						output += String.fromCharCode(parseInt(s[i], 16));
					}
					var fixedstring = decodeURIComponent(escape(output));
					return fixedstring;
				}
				return hex2a(txt);
			}
		}

		res.DecodeXmlFile = function(node, m_response)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (self.GetTagNameWithoutNamespace(child))
				{
					case "fileName": m_response.xmlFileName = child.text; break;
					case "fileData": m_response.xmlFileData = self.FixXmlField(child.text); break;
				}
			});
		}

		res.DecodeSuccessResponse = function(node, m_response)
		{
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (self.GetTagNameWithoutNamespace(child))
				{
					case "externalSystemId": m_response.externalSystemId = child.text; break;
					case "externalCaseId": m_response.externalCaseId = child.text; break;
					case "ummsId": m_response.ummsId = child.text; break;
				}
			});
		}

		res.DecodeErrorResponse = function (node, m_response)
		{
			log.Debug('DecodeErrorResponse {');
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (self.GetTagNameWithoutNamespace(child))
				{
					case "externalSystemId": m_response.externalSystemId = child.text; break;
					case "externalCaseId": m_response.externalCaseId = child.text; break;
					case "xml": res.DecodeXmlFile(child, m_response); break;
					case "errorMsg": m_response.errorMsg = child.text; break;
				}
			});
			log.Debug('DecodeErrorResponse }');
		}

		res.DecodeConflictResponse= function (node,m_response)
		{
			log.Debug('DecodeConflictResponse {');
			m_response.isConflict= true;
			var self = this;
			this.DecodeChildNodes(node, function (child)
			{
				switch (self.GetTagNameWithoutNamespace(child))
				{
					case "externalSystemId": m_response.externalSystemId = child.text; break;
					case "externalCaseId": m_response.externalCaseId = child.text; break;
					case "xml": res.DecodeXmlFile(child, m_response); break;
					case "errorMsg": m_response.errorMsg = child.text; break;
				}
			});
			log.Debug('DecodeConflictResponse }');
		}

		res.DecodeXmlDocument = function (doc)
		{
			log.Debug('DecodeXmlDocument {');
			var self = this;
			var m_response = model_response();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (self.GetTagNameWithoutNamespace(child))
				{
					case "requestId": m_response.requestId = child.text; break;
					case "success": res.DecodeSuccessResponse(child, m_response); break;
					case "error": res.DecodeErrorResponse(child, m_response); break;
					case "conflict": res.DecodeConflictResponse(child, m_response); break;
				}
			}
			log.Debug('DecodeXmlDocument }');
			return m_response;
		};

		res.GetSchemaVersionByXml = function (xml_text)
		{
			var res = '1.3.36';
			if (-1 != xml_text.indexOf('ummsVersion>'))
			{
				return '1.102.2';
			}
			return res;
		};

		var base_Decode = res.Decode;
		res.Decode = function (xml_text)
		{
			this.FixScheme('response', this.GetSchemaVersionByXml(xml_text));
			return base_Decode.call(this, xml_text);
		}

		var base_Encode = res.Encode;
		res.Encode = function (response)
		{
			this.FixScheme('response', this.GetSchemaVersionByModel(response));
			log.Debug('schema version ' + this.SchemaVersion);
			return base_Encode.call(this, response);
		}

		log.Debug('Create }');
		return res;
    }
});