﻿define([
	  'forms/base/controller'
	, 'tpl!forms/fms/response/v_response.xaml'
    , 'forms/fms/response/x_response'
    , 'forms/fms/response/m_response'
    , 'forms/base/log'
],
function (base_controller, tpl, codec, model, GetLogger)
{
    return function()
    {
        var log = GetLogger('c_response');
        var controller = base_controller();

        controller.GetFormContent = function ()
        {
            if (!this.content)
                this.content = model();
            return this.content;
        }

        controller.SetFormContent = function (form_content)
        {
            this.content = form_content;
            return null;
        };

        var InitializeHtmlForm = function (id_form_div)
        {
            if (!this.content)
                this.content = model();
            var form_div = $('#' + id_form_div);
            form_div.html('<div>Форма имеет только печатное представление</div>');
        }

        controller.CreateNew = function (id_form_div)
        {
            InitializeHtmlForm(id_form_div);
        }

        controller.Edit = function (id_form_div)
        {
            InitializeHtmlForm(id_form_div);
        }

        controller.BuildXamlView = function ()
        {
            var content = tpl({ form: this.content });
            return content;
        }

        controller.UseCodec(codec());

        return controller;
    }
});