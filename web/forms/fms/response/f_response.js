define(['forms/fms/response/c_response'],
function (CreateController)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'response'
		, Title: 'Ответ от ППО "Территория"'
		, FileFormat:
			{
				FileExtension: 'xml'
				, Description: 'ФМС. Ответ от ППО "Территория'
			}
	};
	return form_spec;
});
