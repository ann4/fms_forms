﻿define([
	  'forms/fms/base/town/m_town'
],function (TownAddress)
{
	return function ()
	{
		var res =
		{
            requestId: null,
            externalSystemId: null,
            externalCaseId: null,
            ummsId: null,
            errorMsg: null,
            xmlFileName: null,
            xmlFileData: null
        };
        return res;
    }
});