﻿define([
	  'forms/fms/attachments/c_attachments'
	, 'forms/fms/migrationn_z/c_p_mig_unreg'
	, 'forms/fms/migrationn_z/ff_mig_unreg'
	, 'forms/base/h_file'
	, 'forms/base/h_times'
	, 'forms/base/log'
],
function (BaseController, BaseFormController, formSpec, helper_File, helper_times, GetLogger)
{
	return function()
	{
		var log = GetLogger('c_a_mig_unreg');

		var controller = BaseController(BaseFormController, formSpec);

		controller.Sign = function () { };

		controller.SetStayPeriod = function (period) { }
		controller.GetStayPeriod = function () { return null; }

		controller.GetBody = function () {
			var body = 'Направляем документ "Уведомление об убытии ИГ или ЛБГ".';
			if ($('#unreg-text-regCaseId').val() != '')
				body += '\r\nНомер дела о постановке на миграционный учет по месту пребывания - ' + $('#unreg-text-regCaseId').val() + '.';
			return body;
		}
		controller.GetDocumentTypeValues = function (withDuplicate) { return []; };

		var baseform_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function () {
			var attachments = [];
			var content = baseform_GetFormContent();
			content.attachments.push(controller.GetFormContentPrinted());
			return content;
		};

		var baseform_SetFormContent = controller.SetFormContent;
		controller.SetFormContent = function (form_content) {
			var res = baseform_SetFormContent(form_content);
			res = controller.SetFormContentPrinted(form_content);
			return res;
		};

		return controller;
	}
});