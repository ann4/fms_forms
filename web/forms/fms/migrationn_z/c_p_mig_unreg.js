﻿define([
	  'forms/fms/migrationn_z/c_mig_unreg'
	, 'forms/fms/migrationn_z/x_p_mig_unreg'
	, 'forms/fms/migrationn_z/ff_p_mig_unreg'
	, 'forms/base/h_file'
	, 'forms/base/h_times'
	, 'forms/base/log'
],
function (BaseController, codec, formSpec, helper_File, helper_times, GetLogger)
{
	return function()
	{
		var log = GetLogger('c_p_mig_unreg');

		var controller = BaseController();
		var p_codec = codec();

		controller.GetFormContentPrinted = function () {
			log.Debug('GetFormContentPrinted {');
			var name = helper_File.FMSPrepareFileName(formSpec.FilePrefix, helper_times.safeDateTime());
			var content = p_codec.EncodePrinted(controller.model);
			log.Debug('GetFormContentPrinted }');
			return {
				Name: name,
				FileName: name + '.' + formSpec.FileExtension,
				FilePath: name + '.' + formSpec.FileExtension,
				FileSize: helper_File.GetSizeOfUtf8(content),
				Description: '',
				Format: '',
				Content: content,
				Note: formSpec.Description,
				AttachmentType: 'p'
			};
		};

		controller.SetFormContentPrinted = function (form_content) {
			log.Debug('SetFormContentPrinted {');
			var content = 'object' == (typeof form_content) ? form_content : JSON.parse(form_content);
			var _content = '';
			if (content.attachments && content.attachments.length >= 1) {
				for (var i = 0; i < content.attachments.length; i++) {
					if ('p' == content.attachments[i].AttachmentType) {
						_content = content.attachments[i].Content;
						controller.model = p_codec.DecodePrinted(_content, controller.model);
						break;
					}
				}
			}
			log.Debug('SetFormContentPrinted }');
		};

		return controller;
	}
});