define([
	  'forms/fms/migrationn_z/c_a_mig_unreg'
	, 'forms/fms/migrationn_z/ff_mig_unreg'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		  CreateController: CreateController
		, key: 'a_z_migrationn'
		, Title: 'Убытие ИГ или ЛБГ'
		, FileFormat: FileFormat
	};
	return form_spec;
});
