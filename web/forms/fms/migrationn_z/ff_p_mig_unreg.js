﻿define([],
function () {
	var file_format =
	{
		  FilePrefix: 'PrUnregCs'
		, FileExtension: 'xml'
		, Description: 'Печатная форма. ФМС. Убытие ИГ или ЛБГ.'
	};
	return file_format;
});