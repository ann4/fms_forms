include ..\in.lib.txt quiet
execute_javascript_stored_lines add_select2_test_functions
execute_javascript_line "app.cpw_Now= function(){return new Date(1976, 1, 3, 17, 36, 0, 0 ); };"

wait_text "Уведомление о снятии с миграционного учета по месту пребывания"

play_stored_lines z_migrationn_close_datepicker

check_stored_lines z_migrationn_fields_1

play_stored_lines z_migrationn_select2_focus_1

shot_check_png ..\..\shots\2sav.png

wait_click_text "Сохранить содержимое формы"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\2sav.result.xml

exit