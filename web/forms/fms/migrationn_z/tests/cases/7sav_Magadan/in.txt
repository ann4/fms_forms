wait_click_text "OK"

include ..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

include ..\in.lib.txt quiet
execute_javascript_stored_lines add_select2_test_functions

wait_text "Уведомление о снятии с миграционного учета по месту пребывания"
play_stored_lines z_migrationn_close_datepicker

shot_check_png ..\..\shots\7sav_Magadan_0.png

execute_javascript_line "app.profile= { 'Extensions': { 'fms': { 'Abonent': { 'hotel_id': '162534', 'employee_ummsId': '1001', 'supplierInfo': '1800000008002001','sono': {'id': '128752'},'subdivision': {'id': '10141'} } } } };"
execute_javascript_line "app.cpw_Now= function(){return new Date(1976, 1, 3, 17, 36, 0, 0 ); };"

wait_click_text "Вернуться без сохранения"
wait_click_text "Создать"
wait_text "Уведомление о снятии с миграционного учета по месту пребывания"
wait_click_text "OK"

play_stored_lines z_migrationn_close_datepicker
play_stored_lines z_migrationn_fields_2
play_stored_lines z_migrationn_select2_fields_2
play_stored_lines z_migrationn_select2_focus_1

wait_click_text "Данные о снятии с учета"
shot_check_png ..\..\shots\7sav_Magadan.png

wait_click_text "Сохранить содержимое формы"
execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\7sav_Magadan_short.result.xml

wait_click_text "Редактировать"
wait_text "Уведомление о снятии с миграционного учета по месту пребывания"

play_stored_lines z_migrationn_fields_3
play_stored_lines z_migrationn_select2_fields_3
play_stored_lines z_migrationn_select2_focus_1

wait_click_text "Данные о снятии с учета"
shot_check_png ..\..\shots\7sav_Magadan_1.png
wait_click_text "Данные из уведомления о прибытии"
shot_check_png ..\..\shots\7sav_Magadan_2.png
wait_click_text "Принимающая сторона"
shot_check_png ..\..\shots\7sav_Magadan_3.png

wait_click_text "Сохранить содержимое формы"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\7sav_Magadan.result.xml

exit
