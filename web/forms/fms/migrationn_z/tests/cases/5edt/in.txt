include ..\in.lib.txt quiet
execute_javascript_stored_lines add_select2_test_functions
execute_javascript_line "app.cpw_Now= function(){return new Date(1976, 1, 3, 17, 36, 0, 0 ); };"

wait_text "Уведомление о снятии с миграционного учета по месту пребывания"

play_stored_lines z_migrationn_close_datepicker

set_value_id cpw_form_stayPeriod_dateFrom "20.01.1978"
set_value_id cpw_form_stayPeriod_dateTo "25.01.1978"

check_stored_lines z_migrationn_fields_1

play_stored_lines z_migrationn_select2_focus_1

wait_click_text "Сохранить содержимое формы"
shot_check_png ..\..\shots\5edt.png
wait_click_text "OK"

set_value_id cpw_form_Date "28.01.1978"

wait_click_text "Сохранить содержимое формы"
shot_check_png ..\..\shots\5edt1.png
wait_click_text "OK"

set_value_id cpw_form_stayPeriod_dateFrom "10.01.1978"
set_value_id cpw_form_stayPeriod_dateTo "18.01.1978"

set_value_id cpw_form_Date "18.01.1978"

execute_javascript_line "wbt_controller_GetFormContentTextArea= function(){return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');}"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\2sav.result.xml

exit