﻿define([
	'forms/base/h_constraints'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_docTypeMigrationn'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/addr/h_addr'
],
function (h_constraints
	, h_DocumentType
	, h_DocumentTypeMig
	, h_citizenship
	, h_addr) {
	return function () {
		var constraints =
		[
			h_constraints.for_Field('unregDate', h_constraints.NotEmpty())
			, { fields: ['unregDate', 'stayPeriod_dateFrom', 'stayPeriod_dateTo']
				, check: h_constraints.check_DateNotAfterPeriod
				, description: function (element, ldate, ldateFrom, ldateTo, date, dateFrom, dateTo) {
				    return 'Данный гражданин поставлен на миграционный учет с ' +
                            dateFrom +
                            ' по ' + dateTo +
                            '. Уведомить территориальный орган об убытии датой ' + date + ' нельзя.' +
                            ' Вам необходимо уведомить территориальный орган об убытии датой ' +
                            dateTo +
                            ', после этого повторно поставить на миграционный учет данного гражданина с ' +
                            dateTo +
                            ' по ' + date +
                            '. Только после положительного приема второго уведомления можно будет уведомить территориальный орган об убытии гражданина датой ' +
                            date + '.';
				}
			}
			, { fields: ['unregDate', 'stayPeriod_dateFrom', 'stayPeriod_dateTo']
				, check: h_constraints.check_DateNotBeforePeriod
				, description: function (element, ldate, ldateFrom, ldateTo, date, dateFrom, dateTo) {
				    return 'Данный гражданин поставлен на миграционный учет с ' +
                            dateFrom +
                            ' по ' + dateTo +
                            '. Уведомить территориальный орган об убытии датой ' + date + ' нельзя.';
				}
			}
			, { fields: ['comments']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) {
				    return 'Поле "Комментарий" должно быть заполнено. Укажите в данном поле фамилию и инициалы гражданина, который будет снят с миграционного учета.';
				}
			}
			, {
				fields: ['unreg-text-uid']
				, check: h_constraints.check_NotEmpty
				, description: function (element, field) {
					return 'Уникальный идентификатор запроса в системе поставщика данных не может быть пустым';
				}
			}
			, {
				fields: ['unreg-text-regCaseId', 'schemaVersion']
				, marks: ['unreg-text-regCaseId']
				, check: function (regCaseId, schema) {
					return '1.102.2' == schema || h_constraints.check_NotEmpty(regCaseId);
				}
				, description: function (element, lregId, lschema, regId, schema) { 
					return 'Идентификатор запроса о постановке на учет не может быть пустым';
				}
			}
			, {
				fields: ['unreg-text-regCaseId', 'LastName', 'schemaVersion']
				, marks: ['unreg-text-regCaseId']
				, check: function (regCaseId, name, schema) {
					return '1.102.2' != schema || (h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(name));
				}
				, description: function (element, lregId, lname, lschema, regId, name, schema) {
					return 'Необходимо указать либо идентификатор запроса о постановке на учет, либо данные из уведомления о прибытии';
				}
			}
			, {
				fields: ['LastName', 'unreg-text-regCaseId']
				, marks: ['LastName']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: h_constraints.description_NotEmpty
			}
			, {
				fields: ['LastName', 'unreg-text-regCaseId']
				, marks: ['LastName']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Ciryllic(item); }
				, description: h_constraints.description_Ciryllic
			}
			, {
				fields: ['FirstName', 'unreg-text-regCaseId']
				, marks: ['FirstName']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: h_constraints.description_NotEmpty
			}
			, {
				fields: ['FirstName', 'unreg-text-regCaseId']
				, marks: ['FirstName']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Ciryllic(item); }
				, description: h_constraints.description_Ciryllic
			}
			, {
				fields: ['MiddleName', 'unreg-text-regCaseId']
				, marks: ['MiddleName']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Ciryllic(item); }
				, description: h_constraints.description_Ciryllic
			}
			, {
				fields: ['LastName_latin', 'unreg-text-regCaseId']
				, marks: ['LastName_latin']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_NotEmpty(item); }
				, description: function (element, label) { return 'Не указана фамилия гражданина (латиницей)'; }
			}
			, {
				fields: ['LastName_latin', 'unreg-text-regCaseId']
				, marks: ['LastName_latin']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Latin(item); }
				, description: h_constraints.description_Latin
			}
			, {
				fields: ['FirstName_latin', 'unreg-text-regCaseId']
				, marks: ['FirstName_latin']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_NotEmpty(item); }
				, description: function (element, label) { return 'Не указано имя гражданина (латиницей)'; }
			}
			, {
				fields: ['FirstName_latin', 'unreg-text-regCaseId']
				, marks: ['FirstName_latin']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Latin(item); }
				, description: h_constraints.description_Latin
			}
			, {
				fields: ['MiddleName_latin', 'unreg-text-regCaseId']
				, marks: ['MiddleName_latin']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Latin(item); }
				, description: h_constraints.description_Latin
			}
			, {
				fields: ['MiddleName', 'MiddleName_latin', 'unreg-text-regCaseId']
				, marks: ['MiddleName_latin']
				, check: function (name, item, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) || (h_constraints.check_NotEmpty(name) ? h_constraints.recommend_check_NotEmpty(item) : true);
				}
				, description: function (element, field) {
					return 'Не указано отчество гражданина (латиницей)';
				}
			}
			, {
				fields: ['Birthday', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					var _checkDate = h_constraints.check_Date();
					return h_constraints.check_NotEmpty(regCaseId) || _checkDate(item);
				}
				, description: h_constraints.description_Date
			}
			, {
				fields: ['Birthday', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_BeforeDateNow(item);
				}
				, description: function (element, field) { return 'Недопустимая дата рождения'; }
			}
			, {
				fields: ['Birthday', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_NotEmpty(item);
				}
				, description: function (element, field) { return 'Не указана дата рождения'; }
			}
			, {
				fields: ['Birthday', 'DocumentType', 'unreg-text-regCaseId']
				, check: function (date, type, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_DocumentTypePassport(date, type);
				}
				, description: function (element, field) { return 'Паспорт РФ не выдается гражданам, не достигшим 14 лет'; }
			}
			, {
				fields: ['Birthday', 'DocumentType', 'unreg-text-regCaseId']
				, check: function (date, type, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_DocumentTypeNotPassport(date, type);
				}
				, description: function (element, field) { return 'Документом, удостоверяющим личность гражданина, достигшего 14 лет, является паспорт'; }
			}
			, {
				fields: ['Sex', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_Empty(regCaseId) && '' == item ? 'recommended' : true; }
				, description: function (element, field) { return 'Не указан пол гражданина'; }
			}
			, {
				fields: ['Sex', 'MiddleName', 'unreg-text-regCaseId']
				, check: function (sex, name, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_SexByMiddleName(sex, name); }
				, description: h_constraints.description_SexByMiddleName
			}
			, {
				fields: ['Nationality', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: h_constraints.description_NotEmpty
			}
			, {
				fields: ['Nationality', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || !(h_constraints.check_NotEmpty(item) && item.id == 'RUS'); }
				, description: function (element, field) { return 'Недопустимое гражданство при прибытии иностранного гражданина'; }
			}
			, {
				fields: ['Nationality', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_Select2Value(item, h_citizenship.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, {
				fields: ['Birthplace', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || !(!item || null == item || !item.text || null == item.text || '' == item.text) ? true : 'recommended'; }
				, description: function (element, field) { return 'Не указано государство рождения'; }
			}
			, {
				fields: ['Birthday', 'Birthplace', 'unreg-text-regCaseId']
				, check: function (item1, item2, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_CountrySun(item1, item2); }
				, description: function (element, field) { return 'Государство рождения указано неверно. До 06.02.1992 года государство рождения - СССР'; }
			}
			, {
				fields: ['DocumentType', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: h_constraints.description_NotEmpty
			}
			, {
				fields: ['DocumentType', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_Select2Value(item, h_DocumentTypeMig.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, {
				fields: ['DocumentType', 'unreg-text-regCaseId']
				, check: function (type, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) || !(h_constraints.check_NotEmpty(type) && (h_DocumentTypeMig.russianValuesMigrationn).indexOf(type.id) > -1);
				}
				, description: function (element, field) {
					return 'Иностранный гражданин не может иметь документ, удостоверяющий личность гражданина РФ';
				}
			}
			, {
				fields: ['DocumentType', 'DocumentSeries', 'DocumentNumber', 'unreg-text-regCaseId']
				, marks: ['DocumentType']
				, check: function (type, docSeries, docNumber, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) || !(h_constraints.check_NotEmpty(type) &&
						((h_DocumentTypeMig.forbiddenValuesMigrationn).indexOf(type.id) > -1 || (h_DocumentTypeMig.notViewValuesMigration).indexOf(type.id) > -1));
				}
				, description: function (element, ltype, ldocSeries, ldocNumber, type, docSeries, docNumber) {
					return 'Выбранный тип документа (' + type.text +
						('' == docSeries ? '' : (' серия ' + docSeries)) +
						' №' + docNumber + ') не является документом, удостоверяющим личность.';
				}
			}
			, {
				fields: ['DocumentType', 'DocumentSeries', 'DocumentNumber', 'unreg-text-regCaseId']
				, marks: ['DocumentType']
				, check: function (type, docSeries, docNumber, regCaseId) {
					return h_constraints.check_Empty(regCaseId) && h_constraints.check_NotEmpty(type) && (h_DocumentTypeMig.limitedValuesMigrationn).indexOf(type.id) > -1 ? 'recommended' : true;
				}
				, description: function (element, ltype, ldocSeries, ldocNumber, type, docSeries, docNumber) {
					return 'Выбранный тип документа (' + type.text +
						('' == docSeries ? '' : (' серия ' + docSeries)) +
						' №' + docNumber + ') , как правило, не является документом, удостоверяющим личность. ' +
						'Сверьте указанный тип документа с предъявленным гражданином при заселении.';
				}
			}
			, {
				fields: ['DocumentType', 'Nationality', 'unreg-text-regCaseId']
				, check: function (type, nationality, regCaseId) {
					var needCountry = "BLR, UKR, KAZ, ARM, KGZ, ABH, OST";
					return h_constraints.check_Empty(regCaseId) && h_constraints.check_NotEmpty(type) && h_constraints.check_NotEmpty(nationality)
						&& type.id == '103012' && needCountry.indexOf(nationality.id) == -1 ? 'recommended' : true;
				}
				, description: function (element, field) {
					return 'Документ "Иностранный паспорт" в Российской Федерации может удостоверять личность граждан только следующих государств: ' +
						'Беларусь, Украина, Казахстан, Армения, Киргизия, Абхазия, Южная Осетия';
				}
			}
			, {
				fields: ['DocumentNumber', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(item) || h_constraints.check_NotEmpty(regCaseId); }
				, description: function (element, field) { return 'Поле "Номер документа" не может быть пустым'; }
			}
			, {
				fields: ['DocumentGivenDate', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					var _checkDate = h_constraints.check_Date();
					return h_constraints.check_NotEmpty(regCaseId) || _checkDate(item);
				}
				, description: h_constraints.description_Date
			}
			, {
				fields: ['DocumentFinishDate', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					var _checkDate = h_constraints.check_Date();
					return h_constraints.check_NotEmpty(regCaseId) || _checkDate(item);
				}
				, description: h_constraints.description_Date
			}
			, {
				fields: ['DocumentGivenDate', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: function (element, field) { return 'Дата выдачи документа, удостоверяющего личность не может быть пустой'; }
			}
			, {
				fields: ['Birthday', 'DocumentGivenDate', 'DocumentType', 'DocumentSeries', 'DocumentNumber', 'unreg-text-regCaseId']
				, marks: ['Birthday', 'DocumentGivenDate']
				, check: function (birthday, dateDocumentGiven, docType, dateDocumentSeries, dateDocumentNumber, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_PeriodDate(birthday, dateDocumentGiven);
				}
				, description: function (element, lbirthday, ldateDocumentGiven, ldtype, ldseries, ldnumber, birthday, dateDocumentGiven, dtype, dseries, dnumber) {
					var message = 'Дата выдачи документа ';
					message += dtype && '' != dtype ? '"' + dtype.text + '"' : '';
					message += dseries && '' != dseries ? ' ' + dseries : '';
					message += dnumber && '' != dnumber ? ' ' + dnumber : '';
					message += ' не может быть меньше даты рождения';
					return message;
				}
			}
			, {
				fields: ['DocumentType', 'DocumentFinishDate', 'unreg-text-regCaseId']
				, marks: ['DocumentFinishDate']
				, check: function (type, date, regCaseId) {
					return h_constraints.check_NotEmpty(regCaseId) || (h_constraints.check_NotEmpty(type) && '103012' == type.id ? true : h_constraints.recommend_check_NotEmpty(date));
				}
				, description: function (element, type, date) { return 'Не указан срок действия документа, удостоверяющего личность'; }
			}
			, {
				fields: ['StayPlace', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || '' != h_addr.SafePrepareReadableTextWithoutRoom(item, ''); }
				, description: function (element, field) { return 'Поле "Адрес места пребывания" не может быть пустым'; }
			}
			, {
				fields: ['Hotel_address', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || '' != h_addr.SafePrepareReadableTextWithoutRoom(item, ''); }
				, description: function (element, field) { return 'Поле "Фактический адрес" принимающей стороны не может быть пустым'; }
			}
			, {
				fields: ['Organization_Person_Address', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || '' != h_addr.SafePrepareReadableTextWithoutRoom(item, ''); }
				, description: function (element, field) { return 'Поле "Адрес прописки" сотрудника принимающей стороны не может быть пустым'; }
			}
			, {
				fields: ['Hotel_organization_name', 'unreg-text-regCaseId']
				, marks: ['Hotel_organization_name']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: h_constraints.description_NotEmpty
			}
			, {
				fields: ['Hotel_organization_inn', 'unreg-text-regCaseId']
				, marks: ['Hotel_organization_inn']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_Inn(item); }
				, description: h_constraints.description_Inn
			}
			, {
				fields: ['Organization_Person_lastName', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: function (element, field) { return 'Фамилия ответственного лица не может быть пустой'; }
			}
			, {
				fields: ['Organization_Person_lastName', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Ciryllic(item); }
				, description: function (element, field) { return 'Фамилия ответственного лица содержит недопустимые символы'; }
			}
			, {
				fields: ['Organization_Person_firstName', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: function (element, field) { return 'Имя ответственного лица не может быть пустым'; }
			}
			, {
				fields: ['Organization_Person_firstName', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Ciryllic(item); }
				, description: function (element, field) { return 'Имя ответственного лица содержит недопустимые символы'; }
			}
			, {
				fields: ['Organization_Person_middleName', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.recommend_check_Ciryllic(item); }
				, description: function (element, field) { return 'Отчество ответственного лица содержит недопустимые символы'; }
			}
			, {
				fields: ['Organization_Person_birthDate', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					var _checkDate = h_constraints.check_Date();
					return h_constraints.check_NotEmpty(regCaseId) || _checkDate(item);
				}
				, description: h_constraints.description_Date
			}
			, {
				fields: ['Organization_Person_birthDate', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_BeforeDateNow(item); }
				, description: function (element, field) { return 'Недопустимая дата рождения ответственного лица'; }
			}
			, {
				fields: ['Organization_Person_birthDate', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: function (element, field) { return 'Дата рождения ответственного лица не может быть пустой'; }
			}
			, {
				fields: ['Organization_Person_gender', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_Empty(regCaseId) && '' == item ? 'recommended' : true; }
				, description: function (element, field) { return 'Не указан пол ответственного лица'; }
			}
			, {
				fields: ['Organization_Person_gender', 'Organization_Person_middleName', 'unreg-text-regCaseId']
				, check: function (sex, name, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) ? true : h_constraints.recommend_check_SexByMiddleName(sex, name); }
				, description: h_constraints.description_SexByMiddleName
			}
			, {
				fields: ['Organization_Person_nationality', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_Select2Value(item, h_citizenship.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, {
				fields: ['Organization_Person_DocumentType', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: function (element, field) { return 'Документ, удостоверяющий личность, ответственного лица не может быть пустым'; }
			}
			, {
				fields: ['Organization_Person_DocumentType', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_Select2Value(item, h_DocumentType.GetValues); }
				, description: h_constraints.description_Select2Value
			}
			, {
				fields: ['Organization_Person_Document_number', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: function (element, field) { return 'Номер документа ответственного лица не может быть пустым'; }
			}
			, {
				fields: ['Organization_Person_Document_issued', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					var _checkDate = h_constraints.check_Date();
					return h_constraints.check_NotEmpty(regCaseId) || _checkDate(item);
				}
				, description: h_constraints.description_Date
			}
			, {
				fields: ['Organization_Person_Document_validTo', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) {
					var _checkDate = h_constraints.check_Date();
					return h_constraints.check_NotEmpty(regCaseId) || _checkDate(item);
				}
				, description: h_constraints.description_Date
			}
			, {
				fields: ['Organization_Person_Document_issued', 'unreg-text-regCaseId']
				, check: function (item, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_NotEmpty(item); }
				, description: function (element, field) { return 'Дата выдачи документа ответственного лица не может быть пустой'; }
			}
			, {
				fields: ['Organization_Person_birthDate', 'Organization_Person_Document_issued', 'unreg-text-regCaseId']
				, check: function (birthday, dateDocumentGiven, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_PeriodDate(birthday, dateDocumentGiven); }
				, description: function (element, birthday, dateDocumentGiven) { return 'Дата документа, удостоверяющего личность ответственного лица, не может быть меньше даты рождения'; }
			}
			, {
				fields: ['Organization_Person_Document_issued', 'Organization_Person_Document_validTo', 'unreg-text-regCaseId']
				, check: function (birthday, dateDocumentGiven, regCaseId) { return h_constraints.check_NotEmpty(regCaseId) || h_constraints.check_SrictPeriodDate(birthday, dateDocumentGiven); }
				, description: function (element, dateFrom, dateTo) {
					return 'Срок действия документа, удостоверяющего личность ответственного лица, должен быть больше даты его выдачи';
				}
			}
		];
		return constraints;
	}
});
