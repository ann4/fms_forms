﻿define([
	'forms/fms/migrationn_z/x_mig_unreg'
	, 'forms/fms/migrationn_z/m_mig_unreg'
	, 'forms/base/log'
],
function (BaseCodec, model_migration, GetLogger)
{
	var log = GetLogger('x_p_mig_unreg');
	return function()
	{
		var res = BaseCodec();

		res. Encode = function (content)
		{
			log.Debug('Encode()');
			return content;
		};

		res.Decode = function (content)
		{
			log.Debug('Decode()');
			return content;
		};

		res.EncodePrinted = function (m_migrationn_z) {
			log.Debug('EncodePrinted() {');
			if ('string' == typeof m_migrationn_z) {
				log.Debug('EncodePrinted() } signed');
				return m_migrationn_z;
			}
			else {
				var xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + this.FormatEOL();
				xml_string += '<ns8:case xmlns:ns8="http://umms.fms.gov.ru/replication/migration/staying/unreg" xmlns:ns4="http://umms.fms.gov.ru/replication/migration">' + this.FormatEOL();;

				xml_string = this.EncodeCoreCaseFields(1, m_migrationn_z, xml_string);
				var personId_tagName = 'personUid';

				xml_string += this.StringifyField(1, "ns4:regCaseId", m_migrationn_z.regCaseId, 'Идентификатор дела о постановке, для которого проводится снятие с учета');
				xml_string += this.StringifyField(1, "ns4:unregDate", m_migrationn_z.unregDate);

				xml_string += this.FormatTabs(1) + '<person>' + this.FormatEOL();
				xml_string += this.StringifyField(2, "uid", m_migrationn_z.person_uid);
				xml_string += this.StringifyField(2, personId_tagName, m_migrationn_z.person_personId);
				xml_string += this.StringifyField(2, "lastName", m_migrationn_z.LastName);
				xml_string += this.StringifyField(2, "firstName", m_migrationn_z.FirstName);
				if (m_migrationn_z.MiddleName) {
					xml_string += this.StringifyField(2, "middleName", m_migrationn_z.MiddleName);
				}
				xml_string += this.SafeStringifyField(2, "birthDate", m_migrationn_z.Birthday);
				xml_string += this.FormatTabs(1) + '</person>' + this.FormatEOL();

				xml_string += this.OpenTagLine(1, 'stayPlace');
				xml_string += this.EncodeAddress(2, 'address', m_migrationn_z.StayPlace);
				xml_string += this.CloseTagLine(1, 'stayPlace');

				xml_string += this.OpenTagLine(1, 'host');
				xml_string += this.FormatTabs(2) + '<organization>' + this.FormatEOL();
				xml_string += this.StringifyField(3, "uid", m_migrationn_z.Hotel_organization_uid);
				xml_string += this.StringifyField(3, "inn", m_migrationn_z.Hotel_organization_inn);
				xml_string += this.StringifyField(3, "name", m_migrationn_z.Hotel_organization_name);
				xml_string += this.FormatTabs(3) + '<address>' + this.FormatEOL();
				xml_string += this.EncodeAddress(4, 'address', m_migrationn_z.Hotel_address);
				xml_string += this.FormatTabs(3) + '</address>' + this.FormatEOL();
				xml_string += this.FormatTabs(2) + '</organization>' + this.FormatEOL();

				xml_string += this.OpenTagLine(2, 'personDataDocument');
				xml_string += this.FormatTabs(3) + '<person>' + this.FormatEOL();
				xml_string += this.StringifyField(4, "uid", m_migrationn_z.Organization_Person_uid);
				xml_string += this.StringifyField(4, personId_tagName, m_migrationn_z.Organization_Person_personId);
				xml_string += this.StringifyField(4, "lastName", m_migrationn_z.Organization_Person_lastName);
				xml_string += this.StringifyField(4, "firstName", m_migrationn_z.Organization_Person_firstName);
				if (m_migrationn_z.Organization_Person_middleName) {
					xml_string += this.StringifyField(4, "middleName", m_migrationn_z.Organization_Person_middleName);
				}
				xml_string += this.StringifyField(4, "phone", m_migrationn_z.Organization_Person_phone);
				xml_string += this.FormatTabs(3) + '</person>' + this.FormatEOL();

				xml_string += this.FormatTabs(3) + '<document>' + this.FormatEOL();
				xml_string += this.StringifyField(4, "uid", m_migrationn_z.Organization_Person_Document_uid);
				xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn_z.Organization_Person_DocumentType, 'type', 'DocumentType');
				xml_string += this.StringifyField(4, "series", m_migrationn_z.Organization_Person_Document_series);
				xml_string += this.StringifyField(4, "number", m_migrationn_z.Organization_Person_Document_number);
				xml_string += this.StringifyField(4, "issued", res.DateEncode(m_migrationn_z.Organization_Person_Document_issued));
				if (m_migrationn_z.Organization_Person_Document_validTo)
					xml_string += this.StringifyField(4, "validTo", res.DateEncode(m_migrationn_z.Organization_Person_Document_validTo));
				xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn_z.Organization_Person_DocumentStatus, 'status', 'DocumentStatus');
				xml_string += this.FormatTabs(3) + '</document>' + this.FormatEOL();

				xml_string += this.CloseTagLine(2, 'personDataDocument');

				xml_string += this.FormatTabs(2) + '<documentAuthority>' + this.FormatEOL();
				xml_string += this.StringifyField(3, "uid", m_migrationn_z.Organization_Document_uid);
				xml_string += this.StringifyField(3, "type", m_migrationn_z.Organization_DocumentType);
				xml_string += this.StringifyField(3, "series", m_migrationn_z.Organization_Document_series);
				xml_string += this.StringifyField(3, "number", m_migrationn_z.Organization_Document_number);
				xml_string += this.StringifyField(3, "issued", res.DateEncode(m_migrationn_z.Organization_Document_issued));
				if (m_migrationn_z.Organization_Document_validTo)
				    xml_string += this.StringifyField(3, "validTo", res.DateEncode(m_migrationn_z.Organization_Document_validTo));
				xml_string += this.FormatTabs(2) + '</documentAuthority>' + this.FormatEOL();
				xml_string += this.CloseTagLine(1, 'host');

				xml_string += '</ns8:case>' + this.FormatEOL();
				log.Debug('EncodePrinted() }');
				return xml_string;
			}
		};

		res.DecodePerson = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case 'uid': m_migrationn_z.person_uid = child.text; break;
					case 'personUid': m_migrationn_z.person_personId = child.text; break;
					case "lastName": m_migrationn_z.LastName = child.text; break;
					case "firstName": m_migrationn_z.FirstName = child.text; break;
					case "middleName": m_migrationn_z.MiddleName = child.text; break;
					case "birthDate": m_migrationn_z.Birthday = child.text; break;
				}
			});
		};

		res.DecodeStayPlace = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "address":
						m_migrationn_z.StayPlace = self.DecodeAddressAddress(child);
						break;
				}
			});
		};

		res.DecodeHotelInfo_organization = function (node, m_migrationn_z) {
			log.Debug("DecodeHotelInfo_organization {");
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "uid": m_migrationn_z.Hotel_organization_uid = child.text; break;
					case "inn": m_migrationn_z.Hotel_organization_inn = child.text; break;
					case "name": m_migrationn_z.Hotel_organization_name = child.text; break;
					case "address": m_migrationn_z.Hotel_address = self.DecodeAddress(child); break;
				}
			});
			log.Debug("DecodeHotelInfo_organization }");
		};

		res.DecodeHostDataDocument_Person = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case 'uid': m_migrationn_z.Organization_Person_uid = child.text; break;
					case 'personUid': m_migrationn_z.Organization_Person_personId = child.text; break;
					case "lastName": m_migrationn_z.Organization_Person_lastName = child.text; break;
					case "firstName": m_migrationn_z.Organization_Person_firstName = child.text; break;
					case "middleName": m_migrationn_z.Organization_Person_middleName = child.text; break;
					case "phone": m_migrationn_z.Organization_Person_phone = child.text; break;
				}
			});
		};

		res.DecodeHostDataDocument_Document = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "uid": m_migrationn_z.Organization_Person_Document_uid = child.text; break;
					case "type": m_migrationn_z.Organization_Person_DocumentType = self.DecodeDictionaryItem(child, 'DocumentType'); break;
					case "series": m_migrationn_z.Organization_Person_Document_series = child.text; break;
					case "number": m_migrationn_z.Organization_Person_Document_number = child.text; break;
					case "issued": m_migrationn_z.Organization_Person_Document_issued = res.DateDecode(child.text); break;
					case "validTo": m_migrationn_z.Organization_Person_Document_validTo = res.DateDecode(child.text); break;
					case "status": m_migrationn_z.Company_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;
				}
			});
		};

		res.DecodeHostDataDocument = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case 'person': self.DecodeHostDataDocument_Person(child, m_migrationn_z); break;
					case 'document': self.DecodeHostDataDocument_Document(child, m_migrationn_z); break;
				}
			});
		};

		res.DecodeHostDataDocument_DocumentAuthority = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "uid": m_migrationn_z.Organization_Document_uid = child.text; break;
					case "type": m_migrationn_z.Organization_DocumentType = child.text; break;
					case "series": m_migrationn_z.Organization_Document_series = child.text; break;
					case "number": m_migrationn_z.Organization_Document_number = child.text; break;
					case "issued": m_migrationn_z.Organization_Document_issued = res.DateDecode(child.text); break;
					case "validTo": m_migrationn_z.Organization_Document_validTo = res.DateDecode(child.text); break;
				}
			});
		};

		res.DecodeHotelInfo = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "organization": self.DecodeHotelInfo_organization(child, m_migrationn_z); break;
					case "personDataDocument": self.DecodeHostDataDocument(child, m_migrationn_z); break;
					case "documentAuthority": self.DecodeHostDataDocument_DocumentAuthority(child, m_migrationn_z); break;
				}
			});
		};

		res.DecodeShortXmlDocument = function (doc, m_migrationn_z) {
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++) {
				var child = childs[i];
				switch (child.tagName) {
					case "person": this.DecodePerson(child, m_migrationn_z); break;
					case "stayPlace": this.DecodeStayPlace(child, m_migrationn_z); break;
					case "host": this.DecodeHotelInfo(child, m_migrationn_z); break;
				}
			}
			return m_migrationn_z;
		}

		res.GetScheme = function () {
			log.Debug('GetScheme() {');
			log.Debug('unimplemented GetScheme');
			log.Debug('GetScheme() }');
		};

		res.DecodePrinted = function (xml_string, m_migrationn_z) {
			log.Debug('DecodePrinted() {');
			var doc = this.LoadXmlDocument(xml_string);
			if (this.CheckLoadedXmlDocument(doc)) {
				var empty = !m_migrationn_z;
				var root = doc.documentElement;
				var childs = root.childNodes;
				var childs_len = childs.length;
				if (empty) {
					m_migrationn_z = this.DecodeXmlDocument(doc);
				}
				this.DecodeShortXmlDocument(doc, m_migrationn_z);
				log.Debug('DecodePrinted() }');
			}
			return m_migrationn_z;
		};

		return res;
	}
});