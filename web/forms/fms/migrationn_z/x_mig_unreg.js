define([
	  'forms/fms/base/x_fms'
	, 'forms/fms/migrationn_z/m_mig_unreg'
	, 'forms/fms/base/town/m_town'
	, 'forms/fms/base/upload/m_upload'
	, 'forms/fms/base/upload/h_upload'
	, 'forms/base/h_times'
	, 'forms/base/log'
],
function (BaseCodec, model_migration, model_town, model_upload, helper_upload, helper_Times, GetLogger)
{
	var log = GetLogger('x_mig_unreg');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		res.GetRootNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/migration/staying'; };
		res.GetEditNamespaceURI = function () { return 'http://umms.fms.gov.ru/replication/migration/staying/unreg'; };

		res.tabs = ['', '    ', '        ', '            ', '                ', '                    ', '                        ', '                            ', '                                ', '                                    '];

		res.Encode = function (m_migrationn_z)
		{
			log.Debug('Encode() {');
			this.FixScheme('migrationn_z', this.GetSchemaVersionByModel(m_migrationn_z));
			log.Debug('schema version ' + this.SchemaVersion);
			if ('string' == typeof m_migrationn_z)
			{
				log.Debug('Encode() } signed');
				return m_migrationn_z;
			}
			else
			{
				var xml_string = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + this.FormatEOL();
				xml_string += '<ns8:case xmlns="http://umms.fms.gov.ru/replication/core" xmlns:ns2="http://umms.fms.gov.ru/replication/core/correction" xmlns:ns3="http://umms.fms.gov.ru/replication/foreign-citizen-core" xmlns:ns4="http://umms.fms.gov.ru/replication/migration" xmlns:ns5="http://umms.fms.gov.ru/replication/hotel" xmlns:ns6="http://umms.fms.gov.ru/replication/migration/staying/case-edit" xmlns:ns7="http://umms.fms.gov.ru/replication/hotel/form5" xmlns:ns8="http://umms.fms.gov.ru/replication/migration/staying/unreg" xmlns:ns9="http://umms.fms.gov.ru/replication/migration/staying" xmlns:ns10="http://umms.fms.gov.ru/replication/invitation-application" xmlns:ns11="http://umms.fms.gov.ru/replication/payment" xmlns:ns12="http://umms.fms.gov.ru/hotel/hotel-response" schemaVersion="1.0">' + this.FormatEOL();;

				xml_string = '1.102.2' == this.SchemaVersion ? this.EncodeCoreCaseFieldsFMS(1, m_migrationn_z, xml_string) : this.EncodeCoreCaseFields(1, m_migrationn_z, xml_string);
				var ns = ('1.3.40' == this.SchemaVersion || '1.3.42' == this.SchemaVersion || '1.102.2' == this.SchemaVersion) ? 'ns4' : 'ns8';

				if ('1.102.2' == this.SchemaVersion && (null == m_migrationn_z.regCaseId || '' == m_migrationn_z.regCaseId)) {
					xml_string += this.FormatTabs(1) + '<ns4:personDataDocument>' + this.FormatEOL();
					xml_string += this.FormatTabs(2) + '<person>' + this.FormatEOL();
					xml_string += this.StringifyField(3, "uid", m_migrationn_z.person_uid);
					xml_string += this.StringifyField(3, "personUid", m_migrationn_z.person_personId);
					xml_string += this.StringifyField(3, "lastName", m_migrationn_z.LastName);
					xml_string += this.SafeStringifyField(3, "lastNameLat", m_migrationn_z.LastName_latin);
					xml_string += this.StringifyField(3, "firstName", m_migrationn_z.FirstName);
					xml_string += this.SafeStringifyField(3, "firstNameLat", m_migrationn_z.FirstName_latin);
					if (m_migrationn_z.MiddleName) {
						xml_string += this.StringifyField(3, "middleName", m_migrationn_z.MiddleName);
						xml_string += this.SafeStringifyField(3, "middleNameLat", m_migrationn_z.MiddleName_latin);
					}
					xml_string += this.EncodeSex(3, m_migrationn_z.Sex);
					xml_string += this.SafeStringifyField(3, "birthDate", m_migrationn_z.Birthday);
					xml_string += this.SafeEncodeNamedDictionaryItem(3, m_migrationn_z.Nationality, 'citizenship', 'Citizenship');
					xml_string += this.EncodeBirthplace(3, m_migrationn_z.Birthplace);
					xml_string += this.FormatTabs(2) + '</person>' + this.FormatEOL();

					xml_string += this.FormatTabs(2) + '<document>' + this.FormatEOL();
					xml_string += this.StringifyField(3, "uid", m_migrationn_z.Document_uid);
					xml_string += this.SafeEncodeNamedDictionaryItem(3, m_migrationn_z.DocumentType, 'type', '1.102.2' == this.SchemaVersion ? 'mig.identityDocumentType' : 'DocumentType');
					xml_string += this.StringifyField(3, "series", m_migrationn_z.DocumentSeries);
					xml_string += this.StringifyField(3, "number", m_migrationn_z.DocumentNumber);
					xml_string += this.SafeStringifyField(3, "issued", res.DateEncode(m_migrationn_z.DocumentGivenDate));
					if (m_migrationn_z.DocumentFinishDate)
						xml_string += this.SafeStringifyField(3, "validTo", res.DateEncode(m_migrationn_z.DocumentFinishDate));
					xml_string += this.SafeEncodeNamedDictionaryItem(3, m_migrationn_z.Document_DocumentStatus, 'status', 'DocumentStatus');
					xml_string += this.FormatTabs(2) + '</document>' + this.FormatEOL();

					xml_string += this.StringifyField(2, "entered", false, 'Флаг, указывающий на то, вписано ли данное лицо в документ');
					xml_string += this.FormatTabs(1) + '</ns4:personDataDocument>' + this.FormatEOL();

					if (m_migrationn_z.StayPlace && m_migrationn_z.StayPlace.russianAddress) {
						xml_string += this.EncodeRussianAddress(1, m_migrationn_z.StayPlace.russianAddress, 'ns4:stayAddress');
					}

					xml_string += this.OpenTagLine(1, 'ns4:host');
					xml_string += this.FormatTabs(2) + '<ns4:organization>' + this.FormatEOL();
					xml_string += this.StringifyField(3, "uid", m_migrationn_z.Hotel_organization_uid);
					xml_string += this.StringifyField(3, "inn", m_migrationn_z.Hotel_organization_inn);
					xml_string += this.StringifyField(3, "name", m_migrationn_z.Hotel_organization_name);
					xml_string += this.FormatTabs(3) + '<address>' + this.FormatEOL();
					xml_string += this.EncodeAddress(4, 'address', m_migrationn_z.Hotel_address);
					xml_string += this.FormatTabs(3) + '</address>' + this.FormatEOL();
					if ('1.102.2' == this.SchemaVersion) {
						xml_string += this.StringifyField(3, "pboul", 10 != m_migrationn_z.Hotel_organization_inn.length ? 'true' : 'false');
					}
					xml_string += this.SafeStringifyField(3, "headOrganization", m_migrationn_z.headOrganization);
					xml_string += this.FormatTabs(2) + '</ns4:organization>' + this.FormatEOL();

					xml_string += this.OpenTagLine(2, 'ns4:personDataDocument');

					xml_string += this.FormatTabs(3) + '<person>' + this.FormatEOL();
					xml_string += this.StringifyField(4, "uid", m_migrationn_z.Organization_Person_uid);
					xml_string += this.StringifyField(4, "personUid", m_migrationn_z.Organization_Person_personId);

					var write_Organization_Person_lat =
						(m_migrationn_z.Organization_Person_nationality && null != m_migrationn_z.Organization_Person_nationality && 'RUS' != m_migrationn_z.Organization_Person_nationality.id);

					xml_string += this.StringifyField(4, "lastName", m_migrationn_z.Organization_Person_lastName);
					if (write_Organization_Person_lat)
						xml_string += this.SafeStringifyField(4, "lastNameLat", m_migrationn_z.Organization_Person_lastName_latin);
					xml_string += this.StringifyField(4, "firstName", m_migrationn_z.Organization_Person_firstName);
					if (write_Organization_Person_lat)
						xml_string += this.SafeStringifyField(4, "firstNameLat", m_migrationn_z.Organization_Person_firstName_latin);
					if (m_migrationn_z.Organization_Person_middleName) {
						xml_string += this.StringifyField(4, "middleName", m_migrationn_z.Organization_Person_middleName);
						if (write_Organization_Person_lat)
							xml_string += this.SafeStringifyField(4, "middleNameLat", m_migrationn_z.Organization_Person_middleName_latin);
					}

					xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn_z.Organization_Person_gender, 'gender', 'Gender');
					xml_string += this.StringifyField(4, "birthDate", m_migrationn_z.Organization_Person_birthDate);
					xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn_z.Organization_Person_nationality, 'citizenship', 'Citizenship');
					xml_string += this.EncodeBirthplace(4, m_migrationn_z.Organization_Person_birthPlace);
					xml_string += this.FormatTabs(3) + '</person>' + this.FormatEOL();

					xml_string += this.FormatTabs(3) + '<document>' + this.FormatEOL();
					xml_string += this.StringifyField(4, "uid", m_migrationn_z.Organization_Person_Document_uid);
					xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn_z.Organization_Person_DocumentType, 'type', '1.102.2' == this.SchemaVersion ? 'organizationPersonDocumentType' : 'DocumentType');
					xml_string += this.StringifyField(4, "series", m_migrationn_z.Organization_Person_Document_series);
					xml_string += this.StringifyField(4, "number", m_migrationn_z.Organization_Person_Document_number);
					xml_string += this.StringifyField(4, "issued", res.DateEncode(m_migrationn_z.Organization_Person_Document_issued));
					if (m_migrationn_z.Organization_Person_Document_validTo)
						xml_string += this.StringifyField(4, "validTo", res.DateEncode(m_migrationn_z.Organization_Person_Document_validTo));
					xml_string += this.SafeEncodeNamedDictionaryItem(4, m_migrationn_z.Organization_Person_DocumentStatus, 'status', 'DocumentStatus');
					xml_string += this.FormatTabs(3) + '</document>' + this.FormatEOL();

					xml_string += this.StringifyField(3, "entered", false, 'Флаг, указывающий на то, вписано ли данное лицо в документ');
					xml_string += this.CloseTagLine(2, 'ns4:personDataDocument');

					xml_string += this.OpenTagLine(2, ns + ':contactInfo');
					xml_string += this.EncodeAddress(3, 'address', m_migrationn_z.Organization_Person_Address);
					xml_string += this.SafeStringifyField(3, "phone", m_migrationn_z.Organization_Person_phone);
					xml_string += this.CloseTagLine(2, ns + ':contactInfo');

					xml_string += this.CloseTagLine(1, 'ns4:host');
				} else {
					xml_string += this.StringifyField(1, ns + ":regCaseId", m_migrationn_z.regCaseId, 'Идентификатор дела о постановке, для которого проводится снятие с учета');
				}

				xml_string += this.StringifyField(1, ns + ":unregDate", m_migrationn_z.unregDate);
				xml_string += this.SafeEncodeNamedDictionaryItem(1, m_migrationn_z.reason_Unreg, ns + ':reason', 'mig.StayingRemovalReason');

				xml_string += '</ns8:case>' + this.FormatEOL();
				log.Debug('Encode() }');
				return xml_string;
			}
		};

		res.DecodeHotelInfo_organization = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "uid": m_migrationn_z.Hotel_organization_uid = child.text; break;
					case "inn": m_migrationn_z.Hotel_organization_inn = child.text; break;
					case "name": m_migrationn_z.Hotel_organization_name = child.text; break;
					case "address": m_migrationn_z.Hotel_address = self.DecodeAddress(child); break;
					case "headOrganization": m_migrationn_z.headOrganization = child.text; break;
				}
			});
		};

		res.DecodeHostDataDocument_Person = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case 'uid': m_migrationn_z.Organization_Person_uid = child.text; break;
					case 'personUid':
						m_migrationn_z.Organization_Person_personId = child.text; break;

					case "lastName": m_migrationn_z.Organization_Person_lastName = child.text; break;
					case "firstName": m_migrationn_z.Organization_Person_firstName = child.text; break;
					case "middleName": m_migrationn_z.Organization_Person_middleName = child.text; break;

					case "lastNameLat": m_migrationn_z.Organization_Person_lastName_latin = child.text; break;
					case "firstNameLat": m_migrationn_z.Organization_Person_firstName_latin = child.text; break;
					case "middleNameLat": m_migrationn_z.Organization_Person_middleName_latin = child.text; break;

					case "gender": m_migrationn_z.Organization_Person_gender = self.DecodeSex(child); break;
					case "birthDate": m_migrationn_z.Organization_Person_birthDate = child.text; break;
					case 'citizenship': m_migrationn_z.Organization_Person_nationality = self.DecodeDictionaryItem(child, 'citizenship'); break;
					case 'birthPlace': m_migrationn_z.Organization_Person_birthPlace = self.DecodeBirthPlace(child); break;
				}
			});
		}

		res.DecodeHostDataDocument_Document = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "uid": m_migrationn_z.Organization_Person_Document_uid = child.text; break;
				    case "type": m_migrationn_z.Organization_Person_DocumentType = self.DecodeOrganizationPersonDocumentType(child); break;
					case "series": m_migrationn_z.Organization_Person_Document_series = child.text; break;
					case "number": m_migrationn_z.Organization_Person_Document_number = child.text; break;
					case "issued": m_migrationn_z.Organization_Person_Document_issued = res.DateDecode(child.text); break;
					case "validFrom": m_migrationn_z.Organization_Person_Document_validFrom = res.DateDecode(child.text); break;
					case "validTo": m_migrationn_z.Organization_Person_Document_validTo = res.DateDecode(child.text); break;
					case "status": m_migrationn_z.Organization_Person_DocumentStatus = self.DecodeDictionaryItem(child, 'DocumentStatus'); break;
				}
			});
		}

		res.DecodeHostDataDocument = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case 'person': self.DecodeHostDataDocument_Person(child, m_migrationn_z); break;
					case 'document': self.DecodeHostDataDocument_Document(child, m_migrationn_z); break;
					case 'entered': m_migrationn_z.Organization_Person_Document_entered = child.text; break;
				}
			});
		}

		res.DecodeContactInfoCompany = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case 'address': m_migrationn_z.Organization_Person_Address = self.DecodeAddressAddress(child); break;
					case 'phone': m_migrationn_z.Organization_Person_phone = child.text; break;
				}
			});
		}

		res.DecodeHotelInfo = function (node, m_migrationn_z) {
			var self = this;
			this.DecodeChildNodes(node, function (child) {
				switch (child.tagName) {
					case "ns4:organization": self.DecodeHotelInfo_organization(child, m_migrationn_z); break;
					case "ns4:personDataDocument": self.DecodeHostDataDocument(child, m_migrationn_z); break;
					case "ns4:contactInfo": self.DecodeContactInfoCompany(child, m_migrationn_z); break;
				}
			});
		}

		res.DecodeXmlDocument = function (doc)
		{
			var m_migrationn_z = model_migration();
			var root = doc.documentElement;
			var childs = root.childNodes;
			var childs_len = childs.length;
			for (var i = 0; i < childs_len; i++)
			{
				var child = childs[i];
				switch (child.tagName)
				{
					case "uid": m_migrationn_z.uid = child.text; break;
					case "requestId": m_migrationn_z.requestId = child.text; break;
					case "supplierInfo": m_migrationn_z.supplierInfo = child.text; break;
				    case "subdivision": m_migrationn_z.subdivision = this.DecodeOfficialOrganFMS(child); break;
					case "employee": m_migrationn_z.employee = this.DecodeEmployee(child); break;
					case "date": m_migrationn_z.date = child.text; break;
					case "number": m_migrationn_z.number = child.text; break;
					case "comments": m_migrationn_z.comments = child.text; break;
					case "ns4:regCaseId":
					case "ns8:regCaseId": m_migrationn_z.regCaseId = child.text; break;
					case "ns4:unregDate":
					case "ns8:unregDate": m_migrationn_z.unregDate = child.text; break;
					case "ns4:reason":
					case "ns8:reason": m_migrationn_z.reason_Unreg = this.DecodeDictionaryItem(child, 'mig.StayingRemovalReason'); break;
					case "ns4:personDataDocument": this.DecodePersonDataDocument(child, m_migrationn_z); break;
					case "ns4:stayAddress": m_migrationn_z.StayPlace = {
						Country: { id: 'RUS', text: 'Россия' }
						, russianAddress: this.DecodeAddressAddressRussianAddress(child)
					};
						break; //this.DecodeStayPlace(child, m_migrationn_z); break;
					case "ns4:host": this.DecodeHotelInfo(child, m_migrationn_z); break;
				}
			}
			return m_migrationn_z;
		}

		res.GetSchemaVersionByXml = function (xml_text)
		{
			var res = '1.3.36';
			if (-1 != xml_text.indexOf('<ns4:personDataDocument>'))
			{
				return '1.102.2';
			}
			else if (-1 != xml_text.indexOf('<ns4:regCaseId>'))
			{
				return '1.3.40';
			}
			return res;
		}

		var base_Decode = res.Decode;
		res.Decode = function (xml_text)
		{
			this.FixScheme('migrationn_z', this.GetSchemaVersionByXml(xml_text));
			return base_Decode.call(this, xml_text);
		}

		log.Debug('Create }');
		return res;
	}
});
