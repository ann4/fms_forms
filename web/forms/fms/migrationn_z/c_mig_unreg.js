define([
	  'forms/fms/base/c_fms'
	, 'forms/fms/migrationn_z/x_mig_unreg'
	, 'forms/fms/migrationn_z/m_mig_unreg'
	, 'forms/fms/base/f_upload/c_upload'
	, 'forms/fms/base/f_upload/h_upload'
	, 'forms/base/h_times'
	, 'tpl!forms/fms/migrationn_z/v_mig_unreg.xaml'
	, 'tpl!forms/fms/migrationn_z/e_replyto.html'
	, 'tpl!forms/fms/migrationn_z/e_mig_unreg.html'
	, 'forms/fms/base/h_dict_mig_stayingremovalreason'
	, 'forms/fms/base/h_Select2'
	, 'forms/fms/base/h_validate_n'
	, 'forms/base/log'
	, 'tpl!forms/fms/migrationn_z/p_mig_unreg.html'
	, 'forms/fms/profile/h_abonent'
	, 'forms/fms/base/addr/h_addr'
	, 'forms/fms/migrationn_z/r_mig_unreg'
	, 'forms/fms/base/h_DocumentType'
	, 'forms/fms/base/h_docTypeMigrationn'
	, 'forms/fms/base/h_citizenship'
	, 'forms/fms/base/town/c_town'
	, 'forms/fms/base/russ_addr/c_russ_addr'
	, 'forms/fms/base/addr/c_addr'
	, 'forms/fms/base/addr/c_addr_modal'
	, 'forms/fms/base/addr_variants/c_addr_variants'
	, 'forms/base/h_msgbox'
	, 'forms/base/h_names'
	, 'forms/fms/base/h_tabs'
	, 'forms/fms/base/town/h_town'
	, 'forms/base/codec.tpl.xaml'
	, 'forms/fms/base/russ_addr_fias/h_fias'
	, 'forms/base/h_dictionary'
	, 'forms/fms/guides/dict_officialorgan_fms.csv'
],
function 
(
	  BaseFormController
	, codec
	, model_MigrationNotify
	, controller_UploadFile
	, helper_Upload
	, helper_Times
	, print_tpl
	, layout_tpl
	, layout_tpl_unreg
	, h_dict_mig_stayingremovalreason
	, helper_Select2
	, helper_validate
	, GetLogger
	, attrshtml_tpl
	, h_abonent
	, h_addr
	, r_mig_unreg
	, helper_DocumentType
	, helper_DocumentTypeMig
	, helper_citizenship
	, c_Town
	, c_russ_addr
	, c_addr
	, c_addr_modal
	, c_addr_variants
	, h_msgbox
	, h_names
	, h_tabs
	, h_town
	, codec_tpl_xaml
	, h_fias
	, h_dictionary
	, fms
	)
{
	return function ()
	{
		var log = GetLogger('c_mig_unreg');
		log.Debug('Create {');

		var Select2Options = function (h_values) {
			return function (m) {
				return {
					placeholder: '',
					allowClear: true,
					query: function (query) {
						query.callback({ results: helper_Select2.FindForSelect(h_values(), query.term) });
					}
				};
			}
		}

		var AddressTextFunc = function (item) {
			return !(!item || null == item || !item.text || null == item.text || '' == item.text)
					? item.text : 'Кликните, чтобы указать..';
		}

		var AddressTextFunc2 = function (item) {
			return h_addr.SafePrepareReadableText(item, 'Кликните, чтобы указать..');
		}

		var СreateCreateControllerVariants = function (model, field_name, title, onlyfias, default_fias) {
			var createController = function (item, onAfterSave) {
				var res =
				{
					ShowModal: function (e) {
						e.preventDefault();
						var controller = c_addr_variants(model.Hotel_address_variants);
						var field_value = model[field_name];
						if (onlyfias) {
							if (!field_value)
								field_value = {};
							if (!field_value.russianAddress)
								field_value.russianAddress = {};
							field_value.russianAddress.housing_types = true;
							if (!field_value.russianAddress.onlyfias) {
								field_value.russianAddress.variant_fias = true;
								field_value.russianAddress.onlyfias = true;
							}
						}
						if (!default_fias) {
							if (!field_value)
								field_value = {};
							if (!field_value.russianAddress)
								field_value.russianAddress = {};
							field_value.russianAddress.fias_version = h_fias.GetFiasVersionBySchemaVersion(model);
						}
						if (null != field_value)
							controller.SetFormContent(field_value.russianAddress);
						var bname_Ok = 'Сохранить';
						h_msgbox.ShowModal({
							controller: controller
							, width: 750
							, height: 420
							, title: !title ? 'Адрес' : title
							, buttons: [bname_Ok, 'Отменить']
							, onclose: function (bname) {
								if (bname_Ok == bname) {
									var content = controller.GetFormContent();
									model[field_name] =
									{
										Country: { id: 'RUS', text: "Россия" }
										, russianAddress: content
									};
									onAfterSave();
								}
							}
						});
					}
				};
				return res;
			};
			return createController;
		}

		var controller = BaseFormController(layout_tpl_unreg,
		{
			constraints: r_mig_unreg()
			, field_spec:
			{
				DocumentType: Select2Options(helper_DocumentTypeMig.GetValues)
				, Organization_Person_DocumentType: Select2Options(helper_DocumentType.GetValues)
				, Nationality: Select2Options(helper_citizenship.GetValues)
				, Organization_Person_nationality: Select2Options(helper_citizenship.GetValues)
				, Birthplace: {
					text: AddressTextFunc
					, controller: function (item, onAfterSave) { return c_Town(item, onAfterSave); }
				}
				, Organization_Person_birthPlace: {
					text: AddressTextFunc
					, controller: function (item, onAfterSave) { return c_Town(item, onAfterSave); }
				}
				, StayPlace: function (model) {
					var createController = СreateCreateControllerVariants(model, 'StayPlace', 'Место пребывания', true, false);
					var res = { text: AddressTextFunc2, controller: createController };
					return res;
				}
				, Hotel_address: function (model) {
					var res = { text: AddressTextFunc2, controller: c_addr_modal(model, 'Hotel_address', 'Фактический адрес', true, false) };
					return res;
				}
				, Organization_Person_Address: function (model) {
					var res = { text: AddressTextFunc2, controller: c_addr_modal(model, 'Organization_Person_Address', 'Адрес прописки', true, false) };
					return res;
				}
			}
		});

		controller.FileExtension = "xml";
		controller.Description = "ФМС. Убытие ИГ из места проживания.";

		controller.UseProfile = function (profile)
		{
			h_abonent.CheckResponsibleOfficiers(profile);
			if (!this.model)
				this.model = model_MigrationNotify();

			if (profile && profile.Abonent) {
				if (!this.model.Hotel_organization_name || '' == this.model.Hotel_organization_name) {
				if (profile.Abonent.ShortName) {
						this.model.Hotel_organization_name = profile.Abonent.ShortName;
					}
					else if (profile.Abonent.Name) {
						this.model.Hotel_organization_name = profile.Abonent.Name;
					}
				}
				if (!this.model.Hotel_organization_inn || '' == this.model.Hotel_organization_inn) {
					if (profile.Abonent.Inn)
						this.model.Hotel_organization_inn = profile.Abonent.Inn;
				}
			}
			if (profile && profile.Extensions && profile.Extensions.fms)
			{
				var pfms = profile.Extensions.fms;
				if (pfms.Abonent)
				{
					var pAbonent = pfms.Abonent;
					pAbonent= h_abonent.SafeUpgradeAbonent(pAbonent);
					this.model.subdivision= h_abonent.SafeGet_dict_field(pAbonent,'subdivision',this.model.subdivision);
					if (pAbonent.supplierInfo)
						this.model.supplierInfo = pAbonent.supplierInfo;
					if (pAbonent.employee_ummsId)
						this.model.employee.ummsId = pAbonent.employee_ummsId;
					if (pAbonent.AddressRealAbonent) {
						this.model.Hotel_address = h_addr.ConvertFromOldFormat(pAbonent.AddressRealAbonent);
					}
					if (pAbonent.AddressesOfPlacement && pAbonent.AddressesOfPlacement.length > 0) {
						this.model.Hotel_address_variants = pAbonent.AddressesOfPlacement;
						var defAddressesOfPlacement = h_abonent.SafeGetDefaultAddressesOfPlacement(pAbonent);
						if (defAddressesOfPlacement && null != defAddressesOfPlacement) {
							this.model.StayPlace = h_addr.ConvertFromOldFormat(defAddressesOfPlacement);
						}
					}
					var col = h_abonent.SafeGetDefaultColaborator(pAbonent);
					if (null != col) {
						if (col.FirstName)
							this.model.Organization_Person_firstName = col.FirstName;
						if (col.LastName)
							this.model.Organization_Person_lastName = col.LastName;
						if (col.MiddleName)
							this.model.Organization_Person_middleName = col.MiddleName;
						if (col.FirstName_latin)
							this.model.Organization_Person_firstName_latin = col.FirstName_latin;
						if (col.LastName_latin)
							this.model.Organization_Person_lastName_latin = col.LastName_latin;
						if (col.MiddleName_latin)
							this.model.Organization_Person_middleName_latin = col.MiddleName_latin;
						if (col.Birthday)
							this.model.Organization_Person_birthDate = col.Birthday;
						if (col.Birthplace) {
							this.model.Organization_Person_birthPlace = col.Birthplace;
						}
						if (col.Nationality)
							this.model.Organization_Person_nationality = col.Nationality;
						if ('M' == col.Sex)
							this.model.Organization_Person_gender = { id: 'M', text: 'Мужской' };
						else if ('F' == col.Sex)
							this.model.Organization_Person_gender = { id: 'F', text: 'Женский' };

						if (col.Phone)
							this.model.Organization_Person_phone = col.Phone;
						if (col.user_document_serie)
							this.model.Organization_Person_Document_series = col.user_document_serie;
						if (col.user_document_number)
							this.model.Organization_Person_Document_number = col.user_document_number;
						if (col.user_document_dateFrom)
							this.model.Organization_Person_Document_issued = col.user_document_dateFrom;
						if (col.user_document_dateTo)
							this.model.Organization_Person_Document_validTo = col.user_document_dateTo;
						if (col.user_document_type)
							this.model.Organization_Person_DocumentType = col.user_document_type;

						if (col.AddressLegal && null != col.AddressLegal) {
							this.model.Organization_Person_Address = h_addr.ConvertFromOldFormat(col.AddressLegal);
						}

						if (col.user_document_proxy_serie)
							this.model.Organization_Document_series = col.user_document_proxy_serie;
						if (col.user_document_proxy_number)
							this.model.Organization_Document_number = col.user_document_proxy_number;
						if (col.user_document_proxy_dateFrom)
							this.model.Organization_Document_issued = col.user_document_proxy_dateFrom;
						if (col.user_document_proxy_dateTo)
							this.model.Organization_Document_validTo = col.user_document_proxy_dateTo;
						if (col.user_document_proxy_type_company)
							this.model.Organization_DocumentType = col.user_document_proxy_type_company;

						if (col.user_document_givenby) {
							this.model.Organization_Document_authorityOrgan = h_abonent.SafeGet_dict_field(col, 'user_document_givenby', col.user_document_givenby);
							var item = h_dictionary.FindById(fms, this.model.Organization_Document_authorityOrgan.id)
							if (item) {
								this.model.Organization_Document_authorityOrgan.row = item.row;
							}
							this.model.Organization_Document_authorityOrgan_check = true;
						}
						if (col.user_document_givenby_text) {
							this.model.Organization_Document_authorityOrgan_text = col.user_document_givenby_text;
							this.model.Organization_Document_authorityOrgan_check = false;
						}

					}
					controller.ConvertOldFiasAddressesToNew();
				}
				if (pfms.Forma)
				{
					this.ShowAttachmentsPanel = pfms.Forma.ShowAttachmentsPanel;
				}
			}
			this.Render('#report-form');
			DataLoad(this.model, this.UsedCodecInfo);
		}

		controller.Sign = function () { };

		controller.Edit = function (form_div_selector)
		{
			log.Debug('Edit() {');
			this.Render(form_div_selector);
			//DataLoad(this.model);
			log.Debug('Edit() }');
		};

		var DataLoad = function (content, codecInfo)
		{
			function dateFromString(s)
			{
				var bits = s.split(/[-T:+]/g);
				var d = new Date(bits[0], bits[1]-1, bits[2]);

				var bits5= bits[5];
				var pos= bits5.indexOf('Z');
				var len= bits5.length;
				if ((len-1)==pos)
				{
					bits5= bits5.substring(0,len-2);
				}

				d.setHours(bits[3], bits[4], bits5);

				var offsetMinutes = 0;
				// Get supplied time zone offset in minutes
				try
				{
					var offsetMinutes = bits[6] * 60 + Number(bits[7]);
					var sign = /\d\d-\d\d:\d\d$/.test(s)? '-' : '+';
					// Apply the sign
					offsetMinutes = 0 + (sign == '-'? -1 * offsetMinutes : offsetMinutes);
				}
				catch (ex)
				{
					offsetMinutes = 0;
				}
				if (isNaN(offsetMinutes))
					offsetMinutes= 0;

				var ut= d.getTime()/1000;
				var minutes= ut/60;
				var fixedminutes= minutes - offsetMinutes - d.getTimezoneOffset();

				d= new Date(fixedminutes * 60 * 1000);

				// d is now a local time equivalent to the supplied time
				return d;
			}
			var dt= (!content.unregDate) ? null : dateFromString(content.unregDate);

			function pad2(number)
			{
				var r = String(number);
				return (1!==r.length) ? r : '0' + r;
			}

			if (dt != null)
			{
				$('#cpw_form_Date').val(pad2(dt.getDate()) + '.' + pad2(dt.getMonth()+1) + '.' + dt.getFullYear());
				$('#cpw_form_Time').val(pad2(dt.getHours()) + ':' + pad2(dt.getMinutes()));
			}

			$('#unreg-text-uid').val(content.uid);
			$('#unreg-text-regCaseId').val(content.regCaseId);
			$('#cpw_form_Comments').val(content.comments);
			$('#cpw_form_stayPeriod_dateFrom').val(content.stayPeriod_dateFrom);
			$('#cpw_form_stayPeriod_dateTo').val(content.stayPeriod_dateTo);

			if (content.reason_Unreg && !content.reason_Unreg.text && content.reason_Unreg.id)
			{
				var item = helper_Select2.FindById(h_dict_mig_stayingremovalreason.GetValues(), content.reason_Unreg.id)
				if (item)
					content.reason_Unreg.text = item.text;
			}
			$('#unreg-text-reason').select2('data', content.reason_Unreg);

			var methodGetSchemaVersion = codecInfo.Codec.GetSchemaVersionByModel ? codecInfo.Codec.GetSchemaVersionByModel : codecInfo.previous.CodecInfo.Codec.GetSchemaVersionByModel;
			if ('1.102.2' == methodGetSchemaVersion(content)) {
				controller.DataLoadSex('#cpw_form_Sex_male', '#cpw_form_Sex_female', content.Sex);
				controller.DataLoadSex('#cpw_form_Organization_Person_gender_male', '#cpw_form_Organization_Person_gender_female', content.Organization_Person_gender);

				$('#cpw_form_Nationality').select2('data', content.Nationality);
				$('#cpw_form_Organization_Person_nationality').select2('data', content.Organization_Person_nationality);
				$('#cpw_form_DocumentType').select2('data', content.DocumentType);
				$('#cpw_form_Organization_Person_DocumentType').select2('data', content.Organization_Person_DocumentType);

				if (null != $('#cpw_form_LastName').val() && '' == $('#cpw_form_LastName_latin').val()) {
					$('#cpw_form_LastName').change();
				}
				if (null != $('#cpw_form_FirstName').val() && '' == $('#cpw_form_FirstName_latin').val()) {
					$('#cpw_form_FirstName').change();
				}
				if (null != $('#cpw_form_MiddleName').val() && '' == $('#cpw_form_MiddleName_latin').val()) {
					$('#cpw_form_MiddleName').change();
				}
				if (null == $('#cpw_form_Organization_Person_DocumentType').select2('data'))
					$('#cpw_form_Organization_Person_DocumentType').select2('data', { id: 103008, text: "Паспорт гражданина Российской Федерации" });
				if (null == $('#cpw_form_DocumentType').select2('data'))
					$('#cpw_form_DocumentType').select2('data', { id: 103009, text: "Национальный заграничный паспорт" });
			}
			controller.ConvertOldFiasAddressesToNew();
		}

		var DataSave = function (content, codecInfo)
		{
			content.uid= $.trim($('#unreg-text-uid').val());
			content.regCaseId= $.trim($('#unreg-text-regCaseId').val());

			content.reason_Unreg = $('#unreg-text-reason').select2('data');
			content.comments = $('#cpw_form_Comments').val();

			var txt_dt= $('#cpw_form_Date').val();
			var txt_tm= $('#cpw_form_Time').val();

			var txt_dt_parts= txt_dt.split('.');
			var txt_tm_parts= txt_tm.split(':');

			var unregDate= new Date(txt_dt_parts[2],(txt_dt_parts[1]-1),txt_dt_parts[0],txt_tm_parts[0],txt_tm_parts[1]);

			if (!Date.prototype.toISOString) {
			  (function() {

				function pad(number) {
				  if (number < 10) {
					return '0' + number;
				  }
				  return number;
				}

				Date.prototype.toISOString = function() {
				  return this.getUTCFullYear() +
					'-' + pad(this.getUTCMonth() + 1) +
					'-' + pad(this.getUTCDate()) +
					'T' + pad(this.getUTCHours()) +
					':' + pad(this.getUTCMinutes()) +
					':' + pad(this.getUTCSeconds()) +
					'.' + (this.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) +
					'Z';
				};

			  }());
			}
			content.unregDate= unregDate.toISOString();

			var getDateTime = function (date) {
				if (!date) return '';
				var res = '';
				var dobj = new Date();
				var timezone = dobj.getTimezoneOffset();
				var z = timezone >= 0 ? '-' : '+';
				function pad(number) {
					var r = String(number);
					if (r.length === 1) {
						r = '0' + r;
					}
					return r;
				}
				date = date.split('.');
				res += date[2] + '-';
				res += date[1] + '-';
				res += date[0] + 'T';
				res += '00:00:00' + z + pad(Math.abs(timezone) / 60) + ':' + pad(Math.abs(timezone) % 60);
				return res;
			}
			if (!content.date) {
				content.date = helper_Times.nowLocalISOStringDateTime();
			} else if (content.date && -1 == content.date.indexOf('T')) {
				content.date = getDateTime(content.date);
			}

			var methodGetSchemaVersion = codecInfo.Codec.GetSchemaVersionByModel ? codecInfo.Codec.GetSchemaVersionByModel : codecInfo.previous.CodecInfo.Codec.GetSchemaVersionByModel;
			if ('1.102.2' == methodGetSchemaVersion(content)) {
				if (!content.Document_entered)
					content.Document_entered = true;

				if (!content.Organization_Person_Document_entered)
					content.Organization_Person_Document_entered = true;

				if (!content.headOrganization)
					content.headOrganization = "true";

				content.Sex = controller.DataSaveSex('#cpw_form_Sex_male', '#cpw_form_Sex_female');
				content.Organization_Person_gender = controller.DataSaveSex('#cpw_form_Organization_Person_gender_male', '#cpw_form_Organization_Person_gender_female');
			}

			var SafeFixUids = function (content, txt_creation, field_names)
			{
				for (var i = 0; i < field_names.length; i++)
				{
					var field_name = field_names[i];
					if (!content[field_name] || null == content[field_name])
						content[field_name] = content.supplierInfo + '-' + txt_creation + '-' + i;
				}
			};
			var txt_creation= helper_Times.unixDateTimeStamp();
			SafeFixUids(content, txt_creation, ['requestId']);
		}

		controller.ConvertOldFiasAddressesToNew = function () {
			var self = this;
			if (this.model.Hotel_address && null != this.model.Hotel_address) {
				h_fias.ConvertOldFiasToNew(this.model.Hotel_address, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.Hotel_address = res;
					self.model.Hotel_address.text = h_addr.PrepareReadableText(self.model.Hotel_address);
					$('#cpw_form_Hotel_address').text(h_addr.SafePrepareReadableText(self.model.Hotel_address));
				});
			}
			if (this.model.StayPlace && null != this.model.StayPlace) {
				h_fias.ConvertOldFiasToNew(this.model.StayPlace, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.StayPlace = res;
					self.model.StayPlace.text = h_addr.PrepareReadableText(self.model.StayPlace);
					$('#cpw_form_StayPlace').text(h_addr.SafePrepareReadableText(self.model.StayPlace));
				});
			}
			if (this.model.Organization_Person_Address && null != this.model.Organization_Person_Address) {
				h_fias.ConvertOldFiasToNew(this.model.Organization_Person_Address, h_fias.GetFiasVersionBySchemaVersion(this.model), function (res) {
					self.model.Organization_Person_Address = res;
					self.model.Organization_Person_Address.text = h_addr.PrepareReadableText(self.model.Organization_Person_Address);
					$('#cpw_form_Organization_Person_Address').text(h_addr.SafePrepareReadableText(self.model.Organization_Person_Address));
				});
			}
		}

		controller.DataGenerate = function ()
		{
			var zc = codec();
			var fc = controller.model.formContent;
			var res = zc.Decode(fc);
			res.formContent = fc;
			if (res)
				DataLoad(res, this.UsedCodecInfo, controller.form_div_selector);
			else
				return res;
		}

		controller.Validate = function ()
		{
			if (this.binding.CurrentCollectionIndex >= 0)
			{
				return null;
			}
			else
			{
				return this.binding.Validate(this.binding.form_div_selector);
			}
		};

		var SaveFormId = function (form_div_selector)
		{
			controller.form_div_selector = form_div_selector;
		}

		controller.OnUpdate_Organization_Person_nationality = function (nationality) {
			if (!nationality || !nationality.id || 'RUS' == nationality.id) {
				$('.foreign-organization-person').addClass('invisible');
			}
			else {
				$('.foreign-organization-person').removeClass('invisible');
				$('#cpw_form_Organization_Person_lastName').change();
				$('#cpw_form_Organization_Person_firstName').change();
				$('#cpw_form_Organization_Person_middleName').change();
			}
		};

		var old_USSR_countries =
			{
				"RUS": "Россия"
				, "UKR": "Украина"
				, "BLR": "Белоруссия"

				, "LVA": "Латвия"
				, "LTU": "Литва"
				, "EST": "Эстония"

				, "GEO": "Грузия"
				, "ARM": "Армения"
				, "AZE": "Республика Азербайджан"

				, "KGZ": "Кыргызстан"
				, "KAZ": "Республика Казахстан"
				, "TKM": "Туркменистан"

				, "TJK": "Республика Таджикистан"
				, "UZB": "Республика Узбекистан"

				, "MDA": "Республика Молдова"
			};

		var base_Render = controller.Render;
		controller.Render = function(form_div_selector){
			base_Render.call(this, form_div_selector);
			log.Debug('Render() {');
			var content = this.model;

			$('#unreg-text-reason').select2({
				placeholder: '',
				allowClear: true,
				query: function (query) {
					query.callback({ results: helper_Select2.FindForSelect(h_dict_mig_stayingremovalreason.GetValues(), query.term) });
				}
			});
			controller_Upload = controller_UploadFile(this.model, 'Загрузка формы xml', controller.DataGenerate, '#migration-staying-text', form_div_selector);
			$('#open-window-content-load').click(controller_Upload.ShowModal);

			$('.input-datepicker').datepicker(helper_Times.DatePickerOptions);
			$('.input-datepicker').change(helper_validate.OnChangeDateInput_FixFormat);

			$('#cpw_form_Date').datepicker();
			$('#cpw_form_Date').on("change", function (e) { helper_validate.OnChangeDateInput_FixFormat(e); });

			var methodGetSchemaVersion = this.UsedCodecInfo.Codec.GetSchemaVersionByModel ? this.UsedCodecInfo.Codec.GetSchemaVersionByModel : this.UsedCodecInfo.previous.CodecInfo.Codec.GetSchemaVersionByModel;
			var schemaVersion = methodGetSchemaVersion(this.model);
			if ('1.102.2' == schemaVersion) {
				$('#cpw_form_schemaVersion').val(schemaVersion);

				$('#unreg-text-regCaseId').change(function (e) {
					e.preventDefault();
					var checked = '' == $(this).val();
					$('#form-tabs').tabs(checked ? 'enable' : 'disable', '#form-tab-2');
					$('#form-tabs').tabs(checked ? 'enable' : 'disable', '#form-tab-3');
				});
				$('#unreg-text-regCaseId').change();

				$('#cpw_form_LastName').change(h_names.OnNameChange_NormalizeAndLatinize);
				$('#cpw_form_FirstName').change(h_names.OnNameChange_NormalizeAndLatinize);
				$('#cpw_form_MiddleName').change(h_names.OnNameChange_NormalizeAndLatinize);
				$('#cpw_form_Organization_Person_lastName').change(h_names.OnNameChange_NormalizeAndLatinize);
				$('#cpw_form_Organization_Person_firstName').change(h_names.OnNameChange_NormalizeAndLatinize);
				$('#cpw_form_Organization_Person_middleName').change(h_names.OnNameChange_NormalizeAndLatinize);

				$("#cpw_form_Hotel_organization_inn").keydown(helper_validate.ValidateSymbolsINN);

				$('.input-datepicker').datepicker(helper_Times.DatePickerOptions);
				$('.input-datepicker-birthday').datepicker(helper_Times.DatePickerOptionsPrevious);
				$('.input-datepicker').change(helper_validate.OnChangeDateInput_FixFormat);
				$('.input-datepicker-birthday').change(helper_validate.OnChangeDateInput_FixFormat);

				var set_birthplace_country_by_citizenship = function (birthplace, country, birthday_sel) {
					if (birthplace && (!birthplace.Country || birthplace.Country == "") &&
						country && country.id && "L-B-G" != country.id) // Лицо без гражданства..
					{
						if ("N-G-L" == country.id) // Негражданин Латвии
							country = { id: "LVA", text: "Латвия" };
						if ("N-G-E" == country.id) // Негражданин Эстонии
							country = { id: "EST", text: "Эстония" };
						if (old_USSR_countries[country.id]) {
							var old_txtdt = $(birthday_sel).val();
							if ('' != old_txtdt) {
								var dt = helper_Times.ParseRussianDate(old_txtdt);
								var bound_dt = new Date(1991, 11, 26);
								if (dt < bound_dt) {
									country = { id: 'SUN', text: 'СССР' };
								}
							}
						}
						birthplace.Country = country;
					}
				};
				$('#cpw_form_Nationality').on('change', function (e) {
					set_birthplace_country_by_citizenship(content.Birthplace, $(this).select2('data'), '#cpw_form_Birthday');
				});

				var self = this;
				$('#cpw_form_Organization_Person_nationality').on('change', function (e) {
					var nationality = $(this).select2('data');
					if (content.Organization_Person_birthPlace && (!content.Organization_Person_birthPlace.Country || content.Organization_Person_birthPlace.Country == "")) {
						content.Organization_Person_birthPlace.Country = nationality;
						content.Organization_Person_birthPlace.text = h_town.ToReadableString(content.Organization_Person_birthPlace);
						$('#cpw_form_Organization_Person_birthPlace').text(!content.Organization_Person_birthPlace.text ? null_text : content.Organization_Person_birthPlace.text);
					}
					self.OnUpdate_Organization_Person_nationality(nationality);
				});
				self.OnUpdate_Organization_Person_nationality(content.Organization_Person_nationality);

				$('#form-tabs').tabs();
				h_tabs.bindSwitch($('#form-tabs'), 'li', '#form-tab');
			}
			DataLoad(this.model, this.UsedCodecInfo);
			log.Debug('Render() }');
		};

		controller.PrepareValidationElementText = function (txt, txt_to_add) {
			if (null == txt) {
				return txt_to_add;
			}
			else {
				return txt + ',\r\n	 ' + txt_to_add;
			}
		};

		var TryToFindValidationErrors = function (rules) {
			if (rules) {
				var res_validation_text = null;
				for (var i = 0; i < rules.length; i++) {
					var rule = rules[i];
					if (rule && rule.attributes) {
						for (var j = 0; j < rule.attributes.length; j++) {
							var attribute = rule.attributes[j];
							var element =
								('object' != typeof attribute) ? attribute :
								attribute.selector ? $(attribute.selector) :
								attribute.element;
							var title;
							if ('object' != typeof attribute) {
								title = 'непоименованный элемент';
							}
							else {
								try {
									title = attribute.title ? attribute.title : element.attr('id');
								}
								catch (ex) {
									title = 'непоименованный элемент';
								}
							}
							if (!rule.handler || !rule.handler(element))
								res_validation_text = controller.PrepareValidationElementText(res_validation_text, title);
						}
					}
				}
			}
			return res_validation_text;
		};

		controller.TryToFindValidationErrorsAddresses = function () {
			var res_validation_text = '';
			if (!this.model.StayPlace || !this.model.StayPlace.Country) {
				res_validation_text += 'Адрес места пребывания не указан в профиле!';
			}
			if (!this.model.Hotel_address || !this.model.Hotel_address.Country) {
				res_validation_text += '' == res_validation_text ? '' : '\r\n'
				res_validation_text += 'Фактический адрес не указан в профиле!';
			}
			if (!this.model.Organization_Person_Address || !this.model.Organization_Person_Address.Country) {
				res_validation_text += '' == res_validation_text ? '' : '\r\n'
				res_validation_text += 'Адрес прописки сотрудника не указан в профиле!';
			}
			if (!this.model.subdivision || !h_addr.only_free_address_by_substitusion_id(this.model.subdivision.id)) {
				if (this.model.StayPlace && this.model.StayPlace.russianAddress && !this.model.StayPlace.russianAddress.variant_fias) {
					this.model.StayPlace = null;
					res_validation_text += '' == res_validation_text ? '' : '\r\n'
					res_validation_text += 'Адрес места пребывания необходимо заполнить по справочнику ФИАС!';
				}
				if (this.model.Hotel_address && this.model.Hotel_address.russianAddress && !this.model.Hotel_address.russianAddress.variant_fias) {
					this.model.Hotel_address = null;
					res_validation_text += '' == res_validation_text ? '' : '\r\n'
					res_validation_text += 'Фактический адрес необходимо заполнить по справочнику ФИАС!';
				}
				if (this.model.Organization_Person_Address && this.model.Organization_Person_Address.russianAddress && !this.model.Organization_Person_Address.russianAddress.variant_fias) {
					this.model.Organization_Person_Address = null;
					res_validation_text += '' == res_validation_text ? '' : '\r\n'
					res_validation_text += 'Адрес прописки сотрудника необходимо заполнить по справочнику ФИАС!';
				}
			}
			UpdateAddressesLinksTextes(this.model);
			return res_validation_text;
		};

		var UpdateAddressesLinksTextes = function (content) {
			var null_text = 'Кликните, чтобы указать..';
			try {
				$('#cpw_form_Birthplace').text(!content.Birthplace.text ? null_text : content.Birthplace.text);
				$('#cpw_form_Organization_Person_birthPlace').text(!content.Organization_Person_birthPlace.text ? null_text : content.Organization_Person_birthPlace.text);

				$('#cpw_form_StayPlace').text(h_addr.SafePrepareReadableText(content.StayPlace, null_text));
				$('#cpw_form_Hotel_address').text(h_addr.SafePrepareReadableText(content.Hotel_address));
				$('#cpw_form_Organization_Person_Address').text(h_addr.SafePrepareReadableText(content.Organization_Person_Address));

				$('#cpw_form_StayPlaces').trigger('changeAddressText');
				$('#cpw_form_Hotel_address').trigger('changeAddressText');
				$('#cpw_form_Organization_Person_Address').trigger('changeAddressText');
			}
			catch (err) {
				log.Error(err);
			}
		};

		controller.BeforeEditValidation = function () {
			var rules = [
				{
					handler: helper_validate.RequiredValues,
					attributes: [
						{ element: this.model.supplierInfo, title: 'Идентификатор поставщика' },
						{ element: this.model.employee.ummsId, title: 'Идентификатор пользователя' },
						{ element: this.model.subdivision, title: 'Территориальный орган / структурное подразделение ФМС' },
					]
				},
			];
			var res_txt = '';
			var errors = TryToFindValidationErrors(rules);
			if (null != errors)
				res_txt = helper_validate.ProfileErrorText + ':\r\n\r\n	 ' + errors;
			var methodGetSchemaVersion = this.UsedCodecInfo.Codec.GetSchemaVersionByModel ? this.UsedCodecInfo.Codec.GetSchemaVersionByModel : this.UsedCodecInfo.previous.CodecInfo.Codec.GetSchemaVersionByModel;
			if ('1.102.2' == methodGetSchemaVersion(this.model)) {
				var errors = this.TryToFindValidationErrorsAddresses();
				if ('' != errors)
					res_txt += '' == res_txt ? errors : '\r\n\r\n' + errors;
			}
			if (null != res_txt && '' != res_txt) {
				h_msgbox.ShowModal({
					title: "Недостаточно заполненный профиль для заполнения формы!"
					, html: '<pre>' + res_txt + '</pre>'
					, width: 800
				});
			}
		};

		controller.CreateNew = function (form_div_selector)
		{
			if (!this.model)
				this.model = model_MigrationNotify();
			this.Render(form_div_selector);
			$('#cpw_form_Date').focus();
			$('#cpw_form_Time').val('12:00');
			var methodGetSchemaVersion = this.UsedCodecInfo.Codec.GetSchemaVersionByModel ? this.UsedCodecInfo.Codec.GetSchemaVersionByModel : this.UsedCodecInfo.previous.CodecInfo.Codec.GetSchemaVersionByModel;
			if ('1.102.2' == methodGetSchemaVersion(this.model)) {
				if (!this.model.Organization_Person_DocumentType)
					$('#cpw_form_Organization_Person_DocumentType').select2('data', { id: 103008, text: "Паспорт гражданина Российской Федерации" });
				if (!this.model.DocumentType)
					$('#cpw_form_DocumentType').select2('data', { id: 103009, text: "Национальный заграничный паспорт" });
			}
			//DataLoad(this.model);
			this.BeforeEditValidation();
		};

		var base_GetFormContent = controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			this.model = base_GetFormContent.call(this);
			log.Debug('GetFormContent() {');
			DataSave(this.model, this.UsedCodecInfo);
			log.Debug('GetFormContent() }');
			return this.model;
		};

		controller.SetFormContent = function (form_content)
		{
			this.model = form_content;
			return null;
		};

		controller.BuildHtmlViewForProfiledData = function ()
		{
			return attrshtml_tpl(this.model);
		};

		controller.PrepareHotelNameLinesToPrint = function () {
			log.Debug("PrepareHotelNameLinesToPrint {");
			if (!this.model.Hotel_organization_name) {
				log.Debug("PrepareHotelNameLinesToPrint } empty");
				return ['', ''];
			}
			else {
				var fixed_text = this.model.Hotel_organization_name;
				if (fixed_text.length <= 33) {
					log.Debug("PrepareHotelNameLinesToPrint } 1");
					return [fixed_text, ''];
				}
				else if (fixed_text.length <= 71) {
					var line1 = fixed_text.substring(0, 33);
					var line2 = fixed_text.substring(33, 71);
					log.Debug("PrepareHotelNameLinesToPrint } 2");
					return [line1, line2];
				}
				else {
					var line1 = fixed_text.substring(0, 33);
					var line2 = fixed_text.substring(33, 71);
					var line3 = fixed_text.substring(71, 92);
					log.Debug("PrepareHotelNameLinesToPrint } 3");
					return [line1, line2, line3];
				}
			}
		}

		controller.PrepareAddresssLinesToPrint = function (address) {
			log.Debug("PrepareHotelAddresssLinesToPrint {");
			if (!address) {
				log.Debug("PrepareHotelAddresssLinesToPrint } empty");
				return ['', ''];
			}
			else {
				var fixed_text = h_addr.PrepareReadableText(address);
				fixed_text = fixed_text.replace('Россия, ', '');
				fixed_text = fixed_text.replace('д. ', 'д.');
				fixed_text = fixed_text.replace('кв. ', 'кв.');
				fixed_text = fixed_text.replace('корп. ', 'корп.');
				fixed_text = fixed_text.replace('г Москва, г Москва, ', 'г Москва, ');
				if (fixed_text.length <= 34) {
					log.Debug("PrepareHotelAddresssLinesToPrint } 1");
					return [fixed_text, ''];
				}
				else {
					var line1 = fixed_text.substring(0, 34);
					var line2 = fixed_text.substring(34, fixed_text.length);
					log.Debug("PrepareHotelAddresssLinesToPrint } 2");
					return [line1, line2];
				}
			}
		}

		controller.PrepareAddresssToPrint = function (address) {
			log.Debug("PrepareAddresssToPrint");
			return h_addr.PrepareAddressParts(address);
		}

		controller.PrepareFMSSignerTitleLinesToPrint = function () {
			log.Debug("PrepareSignerTitleLinesToPrint {");
			if (!this.model.FMS_Signer_Title) {
				log.Debug("PrepareSignerTitleLinesToPrint } empty");
				return ['', ''];
			}
			else {
				var fixed_text = this.model.FMS_Signer_Title;
				if (fixed_text.length <= 34) {
					log.Debug("PrepareSignerTitleLinesToPrint } 1");
					return [fixed_text, ''];
				}
				else {
					var line1 = fixed_text.substring(0, 34);
					var line2 = fixed_text.substring(34, 72);
					log.Debug("PrepareSignerTitleLinesToPrint } 2");
					return [line1, line2];
				}
			}
		}

		controller.PrepareOrgDocumentTypeLinesToPrint = function () {
			log.Debug("PrepareOrgDocumentTypeLinesToPrint {");
			if (!this.model.Organization_DocumentType) {
				log.Debug("PrepareOrgDocumentTypeLinesToPrint } empty");
				return ['', ''];
			}
			else {
				var fixed_text = this.model.Organization_DocumentType;
				if (fixed_text.length <= 21) {
					log.Debug("PrepareOrgDocumentTypeLinesToPrint } 1");
					return [fixed_text, ''];
				}
				else {
					var line1 = fixed_text.substring(0, 21);
					var line2 = fixed_text.substring(21, 46);
					log.Debug("PrepareOrgDocumentTypeLinesToPrint } 2");
					return [line1, line2];
				}
			}
		}

		controller.BuildXamlView = function ()
		{
			var SymbolsLine = function(symbols)
			{
				var res = '';
				for (var i= 0; i<symbols.length; i++)
				{
				    res += '<TableCell><Paragraph><Label Style="{StaticResource SymbolPlacer}" >' + symbols.charAt(i).toUpperCase() + '</Label></Paragraph></TableCell>\r\n                             ';
				}
				return res;
			};
			var SymbolsLineLen = function(symbols,len)
			{
				var fixed_symbols= symbols && symbols.length && "null" != symbols ? symbols : " ";
				while (fixed_symbols.length < len)
					fixed_symbols+= " ";
				fixed_symbols= fixed_symbols.substring(0,len);
				return SymbolsLine(fixed_symbols);
			};
			var SymbolsLineDateLen = function(symbols,index,len)
			{
				var fixed_symbols= symbols && symbols.length ? symbols.split('.')[index] : " ";
				return SymbolsLineLen(fixed_symbols, len);
			};
			var SymbolsLineTextLen = function (symbols, len) {
				var fixed_symbols = symbols && symbols.text ? symbols.text : " ";
				return SymbolsLineLen(fixed_symbols, len);
			};
			var CityOrOther = function (addr) {
				var res = '';
				if (addr.Город && null != addr.Город)
					res += addr.Город;
				if (addr.НаселенныйПункт && null != addr.НаселенныйПункт) {
					if ('' != res)
						res += ' ';
					res += addr.НаселенныйПункт;
				}
				return res;
			};
			var TerritoryWithSreeet = function (addr) {
				var res = '';
				if (addr.Улица && null != addr.Улица)
					res += addr.Улица;
				if (addr.ГородскойРайон && null != addr.ГородскойРайон && '' != addr.ГородскойРайон) {
					if ('' != res)
						res += ' (';
					res += addr.ГородскойРайон + ')';
				}
				return res;
			};
			var content = print_tpl({
				form: this.model
				, helper_Times: helper_Times
				, UnregDate: helper_Times.stringifyDateUTC('r', helper_Times.ParseRussianDate(this.model.unregDate))
				, AddressStay: this.PrepareAddresssToPrint(this.model.StayPlace)
				, HotelName: this.PrepareHotelNameLinesToPrint()
				, HotelAddress: this.PrepareAddresssLinesToPrint(this.model.Hotel_address)
				, FMSSignerTitle: this.PrepareFMSSignerTitleLinesToPrint()
				, OrgDocumentType: this.PrepareOrgDocumentTypeLinesToPrint()
				, FMSDateSign: !this.model.FMS_DateSign ? null : helper_Times.stringifyDateUTC('r', helper_Times.ParseRussianDate(this.model.FMS_DateSign))
				, SymbolsLine: SymbolsLine
				, SymbolsLineLen: SymbolsLineLen
				, SymbolsLineDateLen: SymbolsLineDateLen
				, SymbolsLineTextLen: SymbolsLineTextLen
				, CityOrOther: CityOrOther
				, TerritoryWithSreeet: TerritoryWithSreeet
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		};
		controller.UseCodec(codec());

		log.Debug('Create }');
		return controller;
	}
});
