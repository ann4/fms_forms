define([
	  'forms/fms/base/town/m_town'
],function (TownAddress)
{
	return function ()
	{
		var res =
		{
			uid: null,
			requestId: '',
			supplierInfo: null,
			subdivision: null,
			employee: { ummsId: null },
			date: null, // 'ДД.ММ.ГГГГ'

			number: null,

			regCaseId: null,
			unregDate: null,
			reason_Unreg: {id: 139363, text: 'Убытие из места пребывания'},

			comments: null,

			stayPeriod_dateFrom: null,
			stayPeriod_dateTo: null,

			LastName: '',
			FirstName: '',
			MiddleName: '',
			FirstName_latin: '',
			LastName_latin: '',
			MiddleName_latin: '',
			Sex: { id: '', text: '' },
			Birthday: '',
			Birthplace: TownAddress(),
			Nationality: null,

			person_uid: null, // Уникальный идентификатор
			person_personId: null, // Идентификатор физического лица, связанного с персональными данными

			DocumentType: null,
			DocumentSeries: '',
			DocumentNumber: '',
			Document_issued: null,
			DocumentGivenDate: null,
			DocumentFinishDate: null,
			Document_DocumentStatus: { id: 102877, text: 'Действительный' },
			Document_uid: null,
			Document_entered: null,

			StayPlace: null,

			Hotel_organization_uid: null,
			Hotel_organization_inn: null,
			Hotel_organization_name: null,
			Hotel_address: null,
			Hotel_address_variants: [],
			Organization_Person_uid: null,
			Organization_Person_personId: null,
			Organization_Person_lastName: '',
			Organization_Person_firstName: '',
			Organization_Person_middleName: '',
			Organization_Person_lastName_latin: '',
			Organization_Person_firstName_latin: '',
			Organization_Person_middleName_latin: '',
			Organization_Person_gender: { id: '', text: '' },
			Organization_Person_nationality: null,
			Organization_Person_birthDate: null,
			Organization_Person_birthPlace: TownAddress(),
			Organization_Person_phone: null,
			Organization_Person_Document_uid: null,
			Organization_Person_Document_series: null,
			Organization_Person_Document_number: null,
			Organization_Person_Document_issued: null,
			//Organization_Person_Document_validFrom: null,
			Organization_Person_Document_validTo: null,
			Organization_Person_DocumentType: null,
			Organization_Person_DocumentStatus: { id: 102877, text: 'Действительный' },
			Organization_Person_Document_entered: null,
			Organization_Person_Address: TownAddress(),
			Organization_Document_uid: null,
			Organization_Document_series: null,
			Organization_Document_number: null,
			Organization_Document_issued: null,
			Organization_Document_validTo: null,
			Organization_DocumentType: null,

			FMS_Signer_Name: '',
			FMS_Signer_LastName: '',
			FMS_Signer_Title: '',
			FMS_DateSign: null
		};
		return res;
	};
});