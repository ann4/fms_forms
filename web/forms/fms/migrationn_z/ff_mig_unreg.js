﻿define([],
function () {
	var file_format =
	{
		  FilePrefix: 'UnregCs'
		, FileExtension: 'xml'
		, Description: 'ФМС. Убытие ИГ или ЛБГ'
	};
	return file_format;
});