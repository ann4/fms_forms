define(function ()
{
	var helper_res =
	{
		GetBoxSel: function (options)
		{
			return !options.boxid || '' == options.boxid ? '#cpw-msgbox' : '#' + options.boxid;
		}

		, Create_OnOpen_ForMsgBoxWithController: function (options)
		{
			var box_sel = this.GetBoxSel(options);
			return function ()
			{
				options.controller.Edit(box_sel);
			}
		}

		, Create_OnClose_ForMsgBoxWithController: function (options)
		{
			var box_sel = this.GetBoxSel(options);
			return function ()
			{
				options.controller.Destroy();
				options.controller = null;
				$(box_sel).dialog('destroy');
			}
		}

		, PrepareDiv: function (options)
		{
			var box_sel = this.GetBoxSel(options);
			var box = $(box_sel);
			if (0 == box.length)
			{
				$('body').prepend('<div id="' + box_sel.replace('#', '') + '" class="cpw-msgbox" style="display:none">Здесь будет всплывающая форма</div>');
				box = $(box_sel);
			}
			return box;
		}

		, ShowModal: function (options)
		{
			var box = this.PrepareDiv(options);

			if (options.html)
			{
				box.html(options.html);
				delete options.html;
			}
			else if (options.controller)
			{
				options.open = this.Create_OnOpen_ForMsgBoxWithController(options);
				options.close = this.Create_OnClose_ForMsgBoxWithController(options);
			}

			if (!options.modal)
				options.modal = true;
			if (!options.modal)
				options.resizable = false;

			var buttons = (!options.buttons) ? ['OK'] : options.buttons;

			var on_close = options.onclose ? options.onclose : function (bname) { };
			delete options.onclose;

			options.buttons = {};
			for (var i = 0; i < buttons.length; i++)
			{
				var button_name = buttons[i];
				options.buttons[button_name] =
					(function (bname, onclose) { return function () { onclose(bname); $(this).dialog("close"); } })
				(button_name, on_close);
			}

			box.dialog(options);
		}
	};
	return helper_res;
});
