define(function ()
{
	return function (model)
	{
		var controller =
		{
			model: model
			, InitializeControls: function () { alert('unimplemented InitializeControls'); }
			, DestroyControls: function () { alert('unimplemented DestroyControls'); }
			, DataLoadToControls: function () { alert('unimplemented DataLoadToControls'); }
			, DataSaveFromControls: function () { alert('unimplemented DataSaveFromControls'); }
			, Validate: function () { return true; }
			, template: null
			, width: 400
			, height: 400
			, title: 'Unimplementer dialog title'

			, dlg_div: null

			, isOpen: false

			, OnKeyDown_CloseFormOnCtrlEnter: function (e)
			{
				if (controller.isOpen && e.ctrlKey && e.keyCode == $.ui.keyCode.ENTER)
				{
					e.preventDefault();
					controller.SaveAndClose();
				}
			}

			, OnAfterSave: function () { }

			, SaveAndClose: function ()
			{
				if (this.dlg_div.dialog('isOpen') && this.Validate())
				{
					this.DataSaveFromControls();
					if (this.OnAfterSave)
						this.OnAfterSave();
					this.dlg_div.dialog('close');
					window.cpw_modal_form_level= window.cpw_modal_form_level-1;
				}
			}

			, PrepareHtml: function()
			{
				return this.template("sdlfkj");
			}

			, ShowModal: function (e)
			{
				e.preventDefault();
				window.cpw_modal_form_level= (!window.cpw_modal_form_level) ? 0 : window.cpw_modal_form_level+1;
				this.id_div_modal_form= 'cpw-modal-form' + window.cpw_modal_form_level;
				var dlg_div = controller.dlg_div = $('<div id="' + this.id_div_modal_form + '"></div>')
				dlg_div.appendTo('body');
				dlg_div.html(this.PrepareHtml());
				if (this.AfterRender)
					this.AfterRender();
				var self= this;
				self.isOpen = true;
				dlg_div.dialog
				({
					title: controller.title,
					width: controller.width,
					height: controller.height,
					modal: true,
					buttons:
					{
						'Сохранить': function ()
						{
							self.SaveAndClose();
						},
						'Отменить': function ()
						{
							dlg_div.dialog('close');
						}
					},
					close: function ()
					{
						self.isOpen = false;
						self.DestroyControls();
						dlg_div.empty();
						e.target.focus();
						dlg_div.dialog('destroy').remove();
					}
				});

				self.InitializeControls();
				self.DataLoadToControls();
			}

			, CloseOnCtrlEnterForSelect2: function (id)
			{
				$('#s2id_' + id + ' .select2-focusser').keydown(controller.OnKeyDown_CloseFormOnCtrlEnter);
			}

			, DestroySelect2: function (id)
			{
				var sel = $(id);
				sel.select2('close');
				sel.select2('destroy');
			}
		};
		return controller;
	}
});
