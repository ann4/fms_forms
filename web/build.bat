call :profile_js
call :fms_js
call :fms_lib_in_txt
exit /B

rem -----------------------------------------------------------
:profile_js
del built\profile.js
del ..\win\FormsHtml\js\profile.js
node optimizers\r.js -o conf\build-profile.js
copy built\profile.js ..\win\FormsHtml\js\profile.js
copy built\profile.js ..\zweb\js\profile.js
exit /B

rem -----------------------------------------------------------
:fms_js
del built\fms.js
del ..\win\FormsHtml\Extensions\fms.js
node optimizers\r.js -o conf\build-fms.js
mkdir ..\win\FormsHtml\Extensions
copy built\fms.js ..\win\FormsHtml\Extensions\fms.js
mkdir ..\zweb\Extensions
copy built\fms.js ..\zweb\Extensions\fms.js
exit /B

rem -----------------------------------------------------------
:fms_lib_in_txt
del built\fms.lib.in.txt
copy forms\fms\wbt.lib.txt built\fms.lib.in.txt
copy /B built\fms.lib.in.txt + forms\fms\questionary\tests\cases\in.lib.txt built\fms.lib.in.txt
copy /B built\fms.lib.in.txt + forms\fms\migrationn\tests\cases\in.lib.txt built\fms.lib.in.txt
copy /B built\fms.lib.in.txt + forms\fms\migrationn_z\tests\cases\in.lib.txt built\fms.lib.in.txt
exit /B