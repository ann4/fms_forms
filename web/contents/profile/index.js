define
(
	[
		  'contents/collector'
		, 'txt!contents/profile/extensions.json.txt'
		, 'txt!forms/fms/profile/tests/contents/current_colaborator.json.txt'
	],
	function (collect)
	{
		return collect([
		  'extensions'
		, 'check_responsible_officier'
		], Array.prototype.slice.call(arguments,1));
	}
);