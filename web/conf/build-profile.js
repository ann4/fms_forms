// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/fms/extension out=..\built\fms.js wrap=true
({
    baseUrl: "..",
    include: ['forms/profile/profile'],
    name: "optimizers/almond",
    out: "..\\built\\profile.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/text'
      }
    }
})