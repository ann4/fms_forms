navigate to ""
web document completed
start play lines from file "..\in.lib.txt"
start store lines as "test_fields_1"
stored lines:
  set_value_id test-test-form-edit 1

start store lines as "test_fields_2"
stored lines:
  set_value_id test-test-form-edit 2

start store lines as "test_current_test_form"
stored lines:

  click_id btn_test_forms_CreateNew

  focus_id test-test-form-edit
  shot_check_png ..\shots\new.png

  play_stored_lines test_fields_1

  click_id btn_test_forms_SaveForm

  click_id btn_test_forms_Edit

  check_stored_lines test_fields_1

  play_stored_lines test_fields_2

  click_id btn_test_forms_SaveForm

  click_id btn_test_forms_Edit

  check_stored_lines test_fields_2

  play_stored_lines test_fields_1

  click_id btn_test_forms_CancelEdit

  click_id btn_test_forms_Edit

  check_stored_lines test_fields_2


stop play lines from file "..\in.lib.txt"
set value "test.min" for id cpw-form-select
start play stored lines "test_current_test_form"
id "btn_test_forms_CreateNew" is clicked
id "test-test-form-edit" is focused
shot "..\shots\new.png"
pre check screenshot
start play stored lines "test_fields_1"
set value "1" for id test-test-form-edit
stop play stored lines "test_fields_1"
id "btn_test_forms_SaveForm" is clicked
id "btn_test_forms_Edit" is clicked
start check stored lines "test_fields_1"
stop check stored lines "test_fields_1"
start play stored lines "test_fields_2"
set value "2" for id test-test-form-edit
stop play stored lines "test_fields_2"
id "btn_test_forms_SaveForm" is clicked
id "btn_test_forms_Edit" is clicked
start check stored lines "test_fields_2"
stop check stored lines "test_fields_2"
start play stored lines "test_fields_1"
set value "1" for id test-test-form-edit
stop play stored lines "test_fields_1"
id "btn_test_forms_CancelEdit" is clicked
id "btn_test_forms_Edit" is clicked
start check stored lines "test_fields_2"
stop check stored lines "test_fields_2"
stop play stored lines "test_current_test_form"
exit
