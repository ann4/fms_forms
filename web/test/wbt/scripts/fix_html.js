if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var fso = new ActiveXObject("Scripting.FileSystemObject");
var shell= new ActiveXObject("WScript.Shell");

ie_version= WScript.Arguments.Item(1);

function fix_ie_version(line)
{
  return line.replace
  (
  "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=10\" />",
  "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=" + ie_version + "\" />"
  );
}

var fixers=
[
  fix_ie_version
];

function fixFile(fname)
{
  var fname_new= fname + '.new';
  var fname_old= fname + '.old';
  var f= fso.OpenTextFile(fname, 1);
  var new_f= fso.CreateTextFile(fname_new);
  while (!f.AtEndOfStream)
  {
    var line= f.ReadLine();
    for (var i= 0; i<fixers.length; i++)
    {
      var fixer= fixers[i];
      line= fixer(line);
    }
    new_f.WriteLine(line);
  }
  f.Close();
  new_f.Close();
  fso.MoveFile(fname,fname_old);
  fso.MoveFile(fname_new,fname);
  fso.DeleteFile(fname_old);
}

fixFile(WScript.Arguments.Item(0));
