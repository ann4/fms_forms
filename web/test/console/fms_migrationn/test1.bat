@set codec_path=forms\fms\migrationn\x_migrationn
@set xml_path=%~dp0..\data\fms\migrationn\migcase_101.xml 

@pushd %~dp0
@call "%~dp0..\scripts\tests.bat" read_write %codec_path% %xml_path%                  > %~dp0migcase_101.etalon.xml
@call "%~dp0..\scripts\tests.bat" read_write %codec_path% %~dp0migcase_101.etalon.xml > %~dp0migcase_101.result.xml
@fc %~dp0migcase_101.result.xml %~dp0migcase_101.etalon.xml
@popd