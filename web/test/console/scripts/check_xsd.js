
if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var varg= WScript.Arguments;

// Create a schema cache and add books.xsd to it.check_xsd.js
var xs = new ActiveXObject("MSXML2.XMLSchemaCache.6.0");
xs.add(varg.Item(0), varg.Item(1));

// Create an XML DOMDocument object.
var xd = new ActiveXObject("MSXML2.DOMDocument.6.0");

// Assign the schema cache to the DOMDocument's
// schemas collection.
xd.schemas = xs;

// Load books.xml as the DOM document.
xd.async = false;
xd.validateOnParse = true;
xd.resolveExternals = true;
xd.load(varg.Item(2));

// Return validation results in message to the user.
if (xd.parseError.errorCode == 0)
{
  WScript.Echo("Validation succeeded!");
}
else
{
  WScript.Echo("Validation failed on " + 
  "\n=====================" +
  "\nReason: " + xd.parseError.reason +
  "\nSource: " + xd.parseError.srcText +
  "\nLine: " + xd.parseError.line + "\n");
}
