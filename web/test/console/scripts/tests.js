
if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var fso = new ActiveXObject("Scripting.FileSystemObject");
var shell= new ActiveXObject("WScript.Shell");

function ProcessException(e)
{
  WScript.Echo(e);
  WScript.Echo(e.description);
  for (var prop in e)
  {
    WScript.Echo(prop + ":" + e[prop]);
  }
  WScript.Echo("}");
}

function WSHInclude(path)
{
  try
  {
    if (!fso)
      WScript.Echo('fso is null');
    var objStream = new ActiveXObject("ADODB.Stream");
    if (!objStream)
      WScript.Echo('can not create objStream');
    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.LoadFromFile(path);
    try
    {
      var txt= objStream.ReadText();
      if (!txt)
        WScript.Echo('ReadText for file returns null');
      try
      {
        var res= eval(txt);
      }
      catch (e)
      {
        WScript.Echo('eval throw exception!');
        throw e;
      }
      return res;
    }
    finally
    {
      objStream.Close();
      delete objStream;
    }
  }
  catch (e)
  {
    WScript.Echo("can not include path \"" + path + "\"");
    WScript.Echo("shell.CurrentDirectory=\"" + shell.CurrentDirectory + "\"");
    ProcessException(e);
    throw e;
  }
}

//WSHInclude('..\\..\\..\\dummy\\js\\vendor\\require.js');

var varg= WScript.Arguments;

function ReadAllText(fpath,funicode)
{
  var objStream = new ActiveXObject("ADODB.Stream");
  objStream.CharSet = "utf-8";
  objStream.Open();
  objStream.LoadFromFile(fpath);
  try
  {
    return objStream.ReadText();
  }
  finally
  {
    objStream.Close();
  }
}

function WriteAllText(fpath,text)
{
  var file= fs.CreateTextFile(fpath, true)
  try
  {
    file.Write(text);
  }
  finally
  {
    file.Close();
  }
}

function define()
{
  var arguments_count= arguments.length;
  if (1>arguments_count)
  {
    WScript.Echo("ERROR: define without arguments!");
  }
  else
  {
    var arg0= arguments[0];
    if( Object.prototype.toString.call( arg0 ) === '[object Function]' )
    {
      var res= arg0();
      return res;
    }
    else if( Object.prototype.toString.call( arg0 ) === '[object Array]' )
    {
      var resolved= [];
      for (var i=0; i<arg0.length; i++)
      {
        var dep= arg0[i];
        var include_res= null;
        if (0===dep.indexOf('txt!'))
        {
          include_res= '';
        }
        else
        {
          include_res= WSHInclude(baseUrl + dep.replace("/","\\") + ".js");
        }
        resolved.push(include_res);
      }
      return arguments[1].apply(null,resolved);
    }
  }
}

var baseUrl= "..\\..\\..\\"

function require(deps,func)
{
  var resolved= [];
  for (var i=0; i<deps.length; i++)
  {
    var dep= deps[i];
    var include_res= WSHInclude(baseUrl + dep.replace("/","\\") + ".js");
    resolved.push(include_res);
  }
  return func.apply(null,resolved);
}

function ReadWrite(codec_path,fpath_in,fpath_out)
{
  require([codec_path],function(create_codec)
  {
    var xml_string= ReadAllText(fpath_in,1);
    var codec= create_codec();
    var data, xml_string2;
    codec.formatted= true;
    try
    {
      data= codec.Decode(xml_string);
    }
    catch (e)
    {
      WScript.Echo("can not decode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    try
    {
      xml_string2= codec.Encode(data);
    }
    catch (e)
    {
      WScript.Echo("can not encode..");
      WScript.Echo("codec_path=" + codec_path);
      ProcessException(e);
      throw e;
    }
    //fso.GetStandardStream(1).Write(xml_string2);
    var objStream = new ActiveXObject("ADODB.Stream");
    objStream.CharSet = "utf-8";
    objStream.Open();
    objStream.WriteText(xml_string2);
    objStream.SaveToFile(fpath_out,2);
    objStream.Close();
  });
}

function BuildText(tpl_path,fpath_in)
{
  require([tpl_path],function(tpl)
  {
    var json_content= ReadAllText(fpath_in,1);
  }
  );
}

if (varg.Count() > 0)
{
  var cmd= varg.Item(0);
  switch (cmd)
  {
    case 'read_write': ReadWrite(varg.Item(1),varg.Item(2),varg.Item(3)); break;
    case 'build_text': BuildText(varg.Item(1),varg.Item(2)); break;
    default: WScript.Echo("unknown cmd=" + cmd);
  }
}
