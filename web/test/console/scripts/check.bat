@SET WEB_PROJ_PATH=D:\server\domains\cpw_forms\web

@SET SCHEME=http://umms.fms.gov.ru/replication/hotel/form5

@SET FILE_SCHEME=%WEB_PROJ_PATH%\forms\fms\schemes\hotel-form5.xsd

rem @call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\test\console\data\fms\questionary\min.xml
rem @call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\test\console\data\fms\questionary\max.xml
rem @call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\test\console\data\fms\questionary\max_atext.xml

rem @call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\test\console\data\fms\questionary\max.etalon.xml
rem @call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\test\console\data\fms\questionary\max_atext.etalon.xml
rem @call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\test\console\data\fms\questionary\min.etalon.xml

@SET FILE_SCHEME=%WEB_PROJ_PATH%\forms\fms\schemes\migration-by-staying.xsd
@SET SCHEME=http://umms.fms.gov.ru/replication/migration/staying

rem @call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\test\console\data\fms\migrationn\migcase_101.xml
@call check_xsd.bat %SCHEME% %FILE_SCHEME% %WEB_PROJ_PATH%\contents\fms\migration.xml 