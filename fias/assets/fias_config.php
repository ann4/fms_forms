<?

global $db_host;
global $db_user;
global $db_password;
global $db_name;
global $max_return_portion;
global $max_return_portion_next;
global $fias_path_max_depth;

/*$db_host= '192.168.0.193';*/
$db_host= 'localhost';
$db_user= 'fias';
$db_password= 'fiaspass';
/*$db_name= 'cpw_dict_new';*/
$db_name= 'cpw_dict_fias';

$fias_path_max_depth= 9;
$max_return_portion= 100;
$max_return_portion_next= 10;
