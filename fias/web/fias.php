<?php

require_once '..\assets\fias_config.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

$action= !isset($_GET['action']) ? 'SEARCH' : $_GET['action'];

function Error($txt)
{
	header("404 Not Found");
	echo $txt;
	exit;
}

function mysqli_stmt_get_result_without_mysqlnd( $Statement )
{
	$RESULT = array();
	$Statement->store_result();
	for ( $i = 0; $i < $Statement->num_rows; $i++ )
	{
		$Metadata = $Statement->result_metadata();
		$PARAMS = array();
		while ( $Field = $Metadata->fetch_field() )
		{
			$PARAMS[] = &$RESULT[ $i ][ $Field->name ];
		}
		call_user_func_array( array( $Statement, 'bind_result' ), $PARAMS );
		$Statement->fetch();
	}
	return $RESULT;
}

function execute_query($txt_query,$parameters)
{
	global $db_host;
	global $db_user;
	global $db_password;
	global $db_name;

	$db_link = mysqli_connect($db_host, $db_user, $db_password, $db_name);

	if (!$db_link)
		Error('Connection error: ' . mysql_error());

	mysqli_set_charset ($db_link, 'utf8');
	$stmt= mysqli_stmt_init($db_link);

	$query_result= array();
	if (mysqli_stmt_prepare($stmt, $txt_query))
	{
		$parameters_len= count($parameters);
		if ($parameters_len>1)
		{
			switch (count($parameters))
			{
				case 0: break;
				case 1: break;
				case 2: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1]); break;
				case 3: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2]); break;
				case 4: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3]); break;
				case 5: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4]); break;
				case 6: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5]); break;
				case 7: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6]); break;
				case 10: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8], $parameters[9]); break;
				case 11: mysqli_stmt_bind_param($stmt, $parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4], $parameters[5], $parameters[6], $parameters[7], $parameters[8], $parameters[9], $parameters[10]); break;
			}
		}
		if (!mysqli_stmt_execute($stmt))
			Error('can not execute query: '.$txt_query);

		/*$result = mysqli_stmt_get_result($stmt);
		while ($row =  mysqli_fetch_object($result))*/
		$result = mysqli_stmt_get_result_without_mysqlnd($stmt);
		while ($row = array_shift($result))
		{
			$query_result[]= $row;
		}
	}
	mysqli_close($db_link);
	return $query_result;
}

function responce($json_result)
{
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	$fn = !isset($_GET['callback']) ? '' : $_GET['callback'];
	if (empty($fn))
	{
		echo $json_result;
	}
	else
	{
		echo $fn.'('.$json_result.')';
	}
}

global $fias_fields;
$fias_fields= array
(
   'AOID'
 , 'AOGUID'
 , 'AOLEVEL'
 , 'IsTerminal'
 , 'SHORTNAME'
 , 'FORMALNAME'
 , 'FullPath'
 , 'STARTDATE'
 , 'ENDDATE'
 , 'Variants'
 , 'VariantsInfo'
);

global $fias_fields_code;
$fias_fields_code= array
(
   'REGIONCODE'
 , 'AREACODE'
 , 'CITYCODE'
 , 'PLACECODE'
 , 'PLANCODE'
 , 'EXTRCODE'
 , 'STREETCODE'
 , 'SEXTCODE'
);

function escapeJsonString($value)
{ # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}

function responce_fias_objects($fias_objects)
{
	global $fias_fields, $fias_fields_code;
	$result_text= "[\r\n";
	$i_fias_object= 0;
	foreach ($fias_objects as $afias_object)
	{
		$fias_object= (object)$afias_object;
		$result_text.= (0==$i_fias_object ? ' ' : ',')."[\r\n";
		$i_fias_field= 0;
		foreach ($fias_fields as $fias_field)
		{
			$result_text.= (0==$i_fias_field ? '  "' : ',"').escapeJsonString($fias_object->$fias_field).'"';
			$i_fias_field++;
		}
		$result_text.= "\r\n  ";
		foreach ($fias_fields_code as $fias_field)
		{
			$result_text.= ',"'.(!isset($fias_object->$fias_field)?'':escapeJsonString($fias_object->$fias_field)).'"';
		}
		$result_text.= "\r\n";
		$result_text.= " ]\r\n";
		$i_fias_object++;
	}
	$result_text.= "]";
	responce($result_text);
}

function Search()
{
	global $fias_fields, $fias_fields_code, $max_return_portion, $max_return_portion_next;
	$return_portion= $max_return_portion;
	$term= !isset($_GET['term']) ? '' : $_GET['term'];
	$txt_query= "select\r\n";
	$i= 0;
	foreach ($fias_fields as $fias_field)
		$txt_query.= ((0==$i++)?'  ':', ').$fias_field."\r\n";
	foreach ($fias_fields_code as $fias_field_code)
		$txt_query.= ', '.$fias_field_code."\r\n";

    $words = str_replace(' ', ' +', $term);
	if (''!=$term)
	{
		if (isset($_GET['min_aolevel']) && 0 < $_GET['min_aolevel'])
		{
			$txt_query.= ', match(FORMALNAME, Variants) against(\'+'.$words.'*\' IN BOOLEAN MODE) as relevant ';
		} else {
			$txt_query.= ', match(FORMALNAME, FullPath, Variants) against(\'>"'.$term.'" <(+'.$words.'*)\' IN BOOLEAN MODE) as relevant';
		}
	}
	$txt_query.= ' from fias_addrobj where 1=1 ';

	if (''==$term)
	{
		$params= array('');
	}
	else
	{
		if (isset($_GET['min_aolevel']) && 0 < $_GET['min_aolevel'])
		{
			$txt_query.= ' and match(FORMALNAME, Variants) against(\'+'.$words.'*\' IN BOOLEAN MODE) ';
		} else {
			$txt_query.= ' and match(FORMALNAME, FullPath, Variants) against(\'>"'.$term.'" <(+'.$words.'*)\' IN BOOLEAN MODE) ';
		}
	}

	if (isset($_GET['min_aolevel']))
	{
		$min_aolevel= $_GET['min_aolevel'];
		$return_portion= 0==$min_aolevel ? $return_portion : $max_return_portion_next;
		if (65==$min_aolevel && '65'==$min_aolevel)
			$min_aolevel= 6;
		$txt_query.= ' and AOLEVEL>? ';
		$params[0].= 'i';
		$params[]= $min_aolevel;
	}

	foreach ($fias_fields_code as $code_name)
	{
		if (isset($_GET[$code_name]))
		{
			$code= $_GET[$code_name];
			$txt_query.= "and $code_name=? ";
			$params[0].= 's';
			$params[]= $code;
		}
	}
	$txt_query.= " order by ";
	if (''!=$term)
	{
		$txt_query.= "relevant DESC, ";
	}
	$txt_query.= "aolevel ASC limit $return_portion;";
	responce_fias_objects(execute_query($txt_query,$params));
}

global $fias_path_max_depth;

function SelectPath()
{
	$txt_query= '
select
 t1.AOID AOID1,t1.AOGUID AOGUID1,t1.AOLEVEL AOLEVEL1,t1.IsTerminal IsTerminal1,t1.SHORTNAME SHORTNAME1
,t1.FORMALNAME FORMALNAME1,t1.FullPath FullPath1,t1.REGIONCODE REGIONCODE1,t1.AREACODE AREACODE1,t1.CITYCODE CITYCODE1
,t1.PLACECODE PLACECODE1,t1.PLANCODE PLANCODE1,t1.EXTRCODE EXTRCODE1,t1.STREETCODE STREETCODE1,t1.SEXTCODE SEXTCODE1

,t2.AOID AOID2,t2.AOGUID AOGUID2,t2.AOLEVEL AOLEVEL2,t2.IsTerminal IsTerminal2,t2.SHORTNAME SHORTNAME2
,t2.FORMALNAME FORMALNAME2,t2.FullPath FullPath2,t2.REGIONCODE REGIONCODE2,t2.AREACODE AREACODE2,t2.CITYCODE CITYCODE2
,t2.PLACECODE PLACECODE2,t2.PLANCODE PLANCODE2,t2.EXTRCODE EXTRCODE2,t2.STREETCODE STREETCODE2,t2.SEXTCODE SEXTCODE2

,t3.AOID AOID3,t3.AOGUID AOGUID3,t3.AOLEVEL AOLEVEL3,t3.IsTerminal IsTerminal3,t3.SHORTNAME SHORTNAME3
,t3.FORMALNAME FORMALNAME3,t3.FullPath FullPath3,t3.REGIONCODE REGIONCODE3,t3.AREACODE AREACODE3,t3.CITYCODE CITYCODE3
,t3.PLACECODE PLACECODE3,t3.PLANCODE PLANCODE3,t3.EXTRCODE EXTRCODE3,t3.STREETCODE STREETCODE3,t3.SEXTCODE SEXTCODE3

,t4.AOID AOID4,t4.AOGUID AOGUID4,t4.AOLEVEL AOLEVEL4,t4.IsTerminal IsTerminal4,t4.SHORTNAME SHORTNAME4
,t4.FORMALNAME FORMALNAME4,t4.FullPath FullPath4,t4.REGIONCODE REGIONCODE4,t4.AREACODE AREACODE4,t4.CITYCODE CITYCODE4
,t4.PLACECODE PLACECODE4,t4.PLANCODE PLANCODE4,t4.EXTRCODE EXTRCODE4,t4.STREETCODE STREETCODE4,t4.SEXTCODE SEXTCODE4

,t5.AOID AOID5,t5.AOGUID AOGUID5,t5.AOLEVEL AOLEVEL5,t5.IsTerminal IsTerminal5,t5.SHORTNAME SHORTNAME5
,t5.FORMALNAME FORMALNAME5,t5.FullPath FullPath5,t5.REGIONCODE REGIONCODE5,t5.AREACODE AREACODE5,t5.CITYCODE CITYCODE5
,t5.PLACECODE PLACECODE5,t5.PLANCODE PLANCODE5,t5.EXTRCODE EXTRCODE5,t5.STREETCODE STREETCODE5,t5.SEXTCODE SEXTCODE5

,t6.AOID AOID6,t6.AOGUID AOGUID6,t6.AOLEVEL AOLEVEL6,t6.IsTerminal IsTerminal6,t6.SHORTNAME SHORTNAME6
,t6.FORMALNAME FORMALNAME6,t6.FullPath FullPath6,t6.REGIONCODE REGIONCODE6,t6.AREACODE AREACODE6,t6.CITYCODE CITYCODE6
,t6.PLACECODE PLACECODE6,t6.PLANCODE PLANCODE6,t6.EXTRCODE EXTRCODE6,t6.STREETCODE STREETCODE6,t6.SEXTCODE SEXTCODE6

,t7.AOID AOID7,t7.AOGUID AOGUID7,t7.AOLEVEL AOLEVEL7,t7.IsTerminal IsTerminal7,t7.SHORTNAME SHORTNAME7
,t7.FORMALNAME FORMALNAME7,t7.FullPath FullPath7,t7.REGIONCODE REGIONCODE7,t7.AREACODE AREACODE7,t7.CITYCODE CITYCODE7
,t7.PLACECODE PLACECODE7,t7.PLANCODE PLANCODE7,t7.EXTRCODE EXTRCODE7,t7.STREETCODE STREETCODE7,t7.SEXTCODE SEXTCODE7

,t8.AOID AOID8,t8.AOGUID AOGUID8,t8.AOLEVEL AOLEVEL8,t8.IsTerminal IsTerminal8,t8.SHORTNAME SHORTNAME8
,t8.FORMALNAME FORMALNAME8,t8.FullPath FullPath8,t8.REGIONCODE REGIONCODE8,t8.AREACODE AREACODE8,t8.CITYCODE CITYCODE8
,t8.PLACECODE PLACECODE8,t8.PLANCODE PLANCODE8,t8.EXTRCODE EXTRCODE8,t8.STREETCODE STREETCODE8,t8.SEXTCODE SEXTCODE8

,t9.AOID AOID9,t9.AOGUID AOGUID9,t9.AOLEVEL AOLEVEL9,t9.IsTerminal IsTerminal9,t9.SHORTNAME SHORTNAME9
,t9.FORMALNAME FORMALNAME9,t9.FullPath FullPath9,t9.REGIONCODE REGIONCODE9,t9.AREACODE AREACODE9,t9.CITYCODE CITYCODE9
,t9.PLACECODE PLACECODE9,t9.PLANCODE PLANCODE9,t9.EXTRCODE EXTRCODE9,t9.STREETCODE STREETCODE9,t9.SEXTCODE SEXTCODE9

from      fias_addrobj t1
left join fias_addrobj t2 on t1.PARENTGUID=t2.AOGUID
left join fias_addrobj t3 on t2.PARENTGUID=t3.AOGUID
left join fias_addrobj t4 on t3.PARENTGUID=t4.AOGUID
left join fias_addrobj t5 on t4.PARENTGUID=t5.AOGUID
left join fias_addrobj t6 on t5.PARENTGUID=t6.AOGUID
left join fias_addrobj t7 on t6.PARENTGUID=t7.AOGUID
left join fias_addrobj t8 on t7.PARENTGUID=t8.AOGUID
left join fias_addrobj t9 on t8.PARENTGUID=t9.AOGUID
where t1.AO';
	$AOID= isset($_GET['AOID']) ? $_GET['AOID'] : 'd7f34217-26f9-4d16-bf8c-37af2453c830';
	$sql_result= execute_query($txt_query.'ID=?',array('s',strtolower($AOID)));
	if (0==count($sql_result))
	{
		$sql_result= execute_query($txt_query.'GUID=?',array('s',strtolower($AOID)));
		if (0==count($sql_result))
		{
			header("404 Not Found");
			exit;
		}
	}

	$sql_result= (object)$sql_result[0];

	global $fias_path_max_depth, $fias_fields, $fias_fields_code;
	$result_text= "[\r\n";
	$i_fias_object= 0;
	for ($i= 1; $i<=$fias_path_max_depth; $i++)
	{
		$fname= 'AOID'.$i;
		if (isset($sql_result->$fname) && null!=$sql_result->$fname && ''!=$sql_result->$fname)
		{
			$result_text.= (0==$i_fias_object ? ' ' : ',')."[\r\n";
			$i_fias_field= 0;
			foreach ($fias_fields as $fias_field)
			{
				$fname= $fias_field.$i;
				$result_text.= (0==$i_fias_field ? '  "' : ',"').escapeJsonString($sql_result->$fname).'"';
				$i_fias_field++;
			}
			$result_text.= "\r\n  ";
			foreach ($fias_fields_code as $fias_field)
			{
				$fname= $fias_field.$i;
				$result_text.= ',"'.(!isset($sql_result->$fname)?'':escapeJsonString($sql_result->$fname)).'"';
			}
			$result_text.= "\r\n";
			$result_text.= " ]\r\n";
			$i_fias_object++;
		}
	}
	$result_text.= "]";
	responce($result_text);
}

function SelectPathNew()
{
	$txt_query= '
select
 t1.AOID AOID1,t1.AOGUID AOGUID1,t1.AOLEVEL AOLEVEL1,t1.IsTerminal IsTerminal1,t1.SHORTNAME SHORTNAME1
,t1.FORMALNAME FORMALNAME1,t1.FullPath FullPath1,t1.REGIONCODE REGIONCODE1,t1.AREACODE AREACODE1,t1.CITYCODE CITYCODE1
,t1.PLACECODE PLACECODE1,t1.PLANCODE PLANCODE1,t1.EXTRCODE EXTRCODE1,t1.STREETCODE STREETCODE1,t1.SEXTCODE SEXTCODE1

,t2.AOID AOID2,t2.AOGUID AOGUID2,t2.AOLEVEL AOLEVEL2,t2.IsTerminal IsTerminal2,t2.SHORTNAME SHORTNAME2
,t2.FORMALNAME FORMALNAME2,t2.FullPath FullPath2,t2.REGIONCODE REGIONCODE2,t2.AREACODE AREACODE2,t2.CITYCODE CITYCODE2
,t2.PLACECODE PLACECODE2,t2.PLANCODE PLANCODE2,t2.EXTRCODE EXTRCODE2,t2.STREETCODE STREETCODE2,t2.SEXTCODE SEXTCODE2

,t3.AOID AOID3,t3.AOGUID AOGUID3,t3.AOLEVEL AOLEVEL3,t3.IsTerminal IsTerminal3,t3.SHORTNAME SHORTNAME3
,t3.FORMALNAME FORMALNAME3,t3.FullPath FullPath3,t3.REGIONCODE REGIONCODE3,t3.AREACODE AREACODE3,t3.CITYCODE CITYCODE3
,t3.PLACECODE PLACECODE3,t3.PLANCODE PLANCODE3,t3.EXTRCODE EXTRCODE3,t3.STREETCODE STREETCODE3,t3.SEXTCODE SEXTCODE3

,t4.AOID AOID4,t4.AOGUID AOGUID4,t4.AOLEVEL AOLEVEL4,t4.IsTerminal IsTerminal4,t4.SHORTNAME SHORTNAME4
,t4.FORMALNAME FORMALNAME4,t4.FullPath FullPath4,t4.REGIONCODE REGIONCODE4,t4.AREACODE AREACODE4,t4.CITYCODE CITYCODE4
,t4.PLACECODE PLACECODE4,t4.PLANCODE PLANCODE4,t4.EXTRCODE EXTRCODE4,t4.STREETCODE STREETCODE4,t4.SEXTCODE SEXTCODE4

,t5.AOID AOID5,t5.AOGUID AOGUID5,t5.AOLEVEL AOLEVEL5,t5.IsTerminal IsTerminal5,t5.SHORTNAME SHORTNAME5
,t5.FORMALNAME FORMALNAME5,t5.FullPath FullPath5,t5.REGIONCODE REGIONCODE5,t5.AREACODE AREACODE5,t5.CITYCODE CITYCODE5
,t5.PLACECODE PLACECODE5,t5.PLANCODE PLANCODE5,t5.EXTRCODE EXTRCODE5,t5.STREETCODE STREETCODE5,t5.SEXTCODE SEXTCODE5

,t6.AOID AOID6,t6.AOGUID AOGUID6,t6.AOLEVEL AOLEVEL6,t6.IsTerminal IsTerminal6,t6.SHORTNAME SHORTNAME6
,t6.FORMALNAME FORMALNAME6,t6.FullPath FullPath6,t6.REGIONCODE REGIONCODE6,t6.AREACODE AREACODE6,t6.CITYCODE CITYCODE6
,t6.PLACECODE PLACECODE6,t6.PLANCODE PLANCODE6,t6.EXTRCODE EXTRCODE6,t6.STREETCODE STREETCODE6,t6.SEXTCODE SEXTCODE6

,t7.AOID AOID7,t7.AOGUID AOGUID7,t7.AOLEVEL AOLEVEL7,t7.IsTerminal IsTerminal7,t7.SHORTNAME SHORTNAME7
,t7.FORMALNAME FORMALNAME7,t7.FullPath FullPath7,t7.REGIONCODE REGIONCODE7,t7.AREACODE AREACODE7,t7.CITYCODE CITYCODE7
,t7.PLACECODE PLACECODE7,t7.PLANCODE PLANCODE7,t7.EXTRCODE EXTRCODE7,t7.STREETCODE STREETCODE7,t7.SEXTCODE SEXTCODE7

,t8.AOID AOID8,t8.AOGUID AOGUID8,t8.AOLEVEL AOLEVEL8,t8.IsTerminal IsTerminal8,t8.SHORTNAME SHORTNAME8
,t8.FORMALNAME FORMALNAME8,t8.FullPath FullPath8,t8.REGIONCODE REGIONCODE8,t8.AREACODE AREACODE8,t8.CITYCODE CITYCODE8
,t8.PLACECODE PLACECODE8,t8.PLANCODE PLANCODE8,t8.EXTRCODE EXTRCODE8,t8.STREETCODE STREETCODE8,t8.SEXTCODE SEXTCODE8

,t9.AOID AOID9,t9.AOGUID AOGUID9,t9.AOLEVEL AOLEVEL9,t9.IsTerminal IsTerminal9,t9.SHORTNAME SHORTNAME9
,t9.FORMALNAME FORMALNAME9,t9.FullPath FullPath9,t9.REGIONCODE REGIONCODE9,t9.AREACODE AREACODE9,t9.CITYCODE CITYCODE9
,t9.PLACECODE PLACECODE9,t9.PLANCODE PLANCODE9,t9.EXTRCODE EXTRCODE9,t9.STREETCODE STREETCODE9,t9.SEXTCODE SEXTCODE9

from      fias_addrobj t1
left join fias_addrobj t2 on t1.PARENTGUID=t2.AOGUID
left join fias_addrobj t3 on t2.PARENTGUID=t3.AOGUID
left join fias_addrobj t4 on t3.PARENTGUID=t4.AOGUID
left join fias_addrobj t5 on t4.PARENTGUID=t5.AOGUID
left join fias_addrobj t6 on t5.PARENTGUID=t6.AOGUID
left join fias_addrobj t7 on t6.PARENTGUID=t7.AOGUID
left join fias_addrobj t8 on t7.PARENTGUID=t8.AOGUID
left join fias_addrobj t9 on t8.PARENTGUID=t9.AOGUID
where ';
	$old_aoid= !isset($_GET['old_variant']) ? null : $_GET['old_variant'];
	$sql_result= execute_query($txt_query.'t1.AOID=?',array('s',strtolower($old_aoid)));
	if (0==count($sql_result))
	{
		$sql_result= execute_query($txt_query.' match(t1.VariantsInfo) against(\'+'.$old_aoid.'*\' IN BOOLEAN MODE) ',array(''));
		if (0==count($sql_result))
		{
			header("404 Not Found");
			exit;
		}
	}

	$sql_result= (object)$sql_result[0];

	global $fias_path_max_depth, $fias_fields, $fias_fields_code;
	$result_text= "[\r\n";
	$i_fias_object= 0;
	for ($i= 1; $i<=$fias_path_max_depth; $i++)
	{
		$fname= 'AOID'.$i;
		if (isset($sql_result->$fname) && null!=$sql_result->$fname && ''!=$sql_result->$fname)
		{
			$result_text.= (0==$i_fias_object ? ' ' : ',')."[\r\n";
			$i_fias_field= 0;
			foreach ($fias_fields as $fias_field)
			{
				$fname= $fias_field.$i;
				$result_text.= (0==$i_fias_field ? '  "' : ',"').escapeJsonString($sql_result->$fname).'"';
				$i_fias_field++;
			}
			$result_text.= "\r\n  ";
			foreach ($fias_fields_code as $fias_field)
			{
				$fname= $fias_field.$i;
				$result_text.= ',"'.(!isset($sql_result->$fname)?'':escapeJsonString($sql_result->$fname)).'"';
			}
			$result_text.= "\r\n";
			$result_text.= " ]\r\n";
			$i_fias_object++;
		}
	}
	$result_text.= "]";
	responce($result_text);
}

switch ($action)
{
	case 'SEARCH': Search(); break;
	case 'PATH':   SelectPath(); break;
	case 'PATHQ':  BuildPathQuery(); break;
	case 'PATHNEW': SelectPathNew(); break;
}


