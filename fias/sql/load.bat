@echo %time% create table
@call run_mysql.bat < 00_create.sql

@echo %time% load csv
@call run_mysql.bat < 01_load.sql

@echo %time% filter actual
@call run_mysql.bat < 02_filter_actual.sql

@echo %time% build Variants
@call run_mysql.bat < 03_build_Variants.sql

@echo %time% filter actual
@call run_mysql.bat < 04_delete_unactual.sql

@echo %time% add id indexes
@call run_mysql.bat < 05_add_id_indexes.sql

@echo %time% build Variants
@call run_mysql.bat < 06_update_Variants.sql

@echo %time% add FullPath, IsTerminal
@call run_mysql.bat < 07_add_extra_fields.sql

@echo %time% build full path
@call run_mysql.bat < 08_build_FullPath.sql

@echo %time% build is terminal
@call run_mysql.bat < 09_build_IsTerminal.sql

@echo %time% build indexes to search
@call run_mysql.bat < 10_add_indexes.sql

@echo %time% drop actual
@call run_mysql.bat < 11_drop_unnecessary_fields.sql

@echo %time% add fulltext index
@call run_mysql.bat < 12_add_fulltext_index.sql

@echo %time% rename table to fias_addrobj
@call run_mysql.bat < 13_move_table.sql

@echo %time%

echo set names cp866; select AOID, formalname from fias_addrobj limit 10; | run_mysql.bat