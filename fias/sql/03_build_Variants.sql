select now() as ' ', 'build variants' as ' ';

SET SESSION group_concat_max_len = 15000;
drop table if exists fias_temp;
create table fias_temp(
  AOGUID varchar(36) default null,
  Variants text default null,
  VariantsInfo text default null
)  ENGINE=MyISAM DEFAULT CHARSET=utf8;

insert into fias_temp (AOGUID, Variants, VariantsInfo)
  select AOGUID, 
    group_concat(distinct SHORTNAME, ' ', FORMALNAME SEPARATOR '&'),
    concat('[ ', group_concat(
      JSON_OBJECT(
        'id',AOID,
        'text',concat(SHORTNAME, ' ', FORMALNAME),
        'startdate',DATE_FORMAT(STARTDATE, '%Y-%m-%d'),
        'enddate',DATE_FORMAT(ENDDATE, '%Y-%m-%d'),
        'actstatus',ACTSTATUS
      )
    ), ' ]')
  from fias_new
  where IsActualPPO=0
  group by AOGUID
;