select now() as ' ', 'add fulltext index' as ' ';
alter table fias_new add fulltext index  iFullNamePath (FORMALNAME, FullPath, Variants);
alter table fias_new add fulltext index  iFullName (FORMALNAME, Variants);
alter table fias_new add fulltext index  iVariantsInfo (VariantsInfo);