select now() as ' ', 'add index for PARENTGUID' as ' ';
alter table fias_new add index  iPARENTGUID (PARENTGUID);

select now() as ' ', 'add index for AOGUID' as ' ';
alter table fias_new add unique index iAOGUID (AOGUID);

select now() as ' ', 'add index for AOID' as ' ';
alter table fias_new add unique index  iAOID (AOID);

