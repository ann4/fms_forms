del addr_obj_dbf\*.csv
del addr_obj.csv
rem copy empty_addr_obj.csv addr_obj.csv

for %%f in (addr_obj_dbf\*.dbf) do call :convert_file %%f

rem call :convert_file addr_obj_dbf\ADDROB01.DBF 

:convert_file
rem call dbf --trim b --noconv --csv addr_obj_dbf\%~n1_cp866.csv %1
call dbf addr_obj_dbf\%~n1_cp866.csv %1
rem call scripts\skip_first_line.bat addr_obj_dbf\%~n1_cp866.csv
call %RIT_DEVTOOLS%\iconv\iconv.exe -f cp866 -t utf-8 addr_obj_dbf\%~n1_cp866.csv >> addr_obj.csv
exit /B