select now() as ' ', 'update Variants' as ' ';
update fias_new fn 
	inner join fias_temp ft on fn.AOGUID=ft.AOGUID and fn.IsActualPPO=1
set fn.Variants=ft.Variants, fn.VariantsInfo=ft.VariantsInfo;

select now() as ' ', 'drop fias_temp' as ' ';
drop table if exists fias_temp;