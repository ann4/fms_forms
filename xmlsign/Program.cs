﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace xmlsign
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (0 == args.Length)
                {
                    Usage();
                }
                else
                {
                    string cmd;
                    string [] parsed_args= ParseArguments(out cmd, args);
                    if (string.IsNullOrEmpty(cmd))
                    {
                        Usage();
                    }
                    else
                    {
                        switch (cmd)
                        {
                            case "--sign": SignXml(parsed_args[0], parsed_args[1], parsed_args[2]); break;
                            case "--check": VerifyXmlFile_AndReportTheResult(parsed_args[0], parsed_args[1]); break;
                            case "--dnnormal": NormalizeDN(parsed_args[0]); break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType().FullName);
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        static void NormalizeDN(string fname)
        {
            using (StreamReader r= new StreamReader(fname, Encoding.UTF8))
            {
                for (string dnnames= r.ReadLine(); null!=dnnames; dnnames= r.ReadLine())
                {
                    string normalized_dnnames= DNFixer.NormalizeIssuerName(dnnames);
                    Console.Out.WriteLine(normalized_dnnames);
                }
            }
        }

        static string[] ParseArguments(out string cmd, string[] args)
        {
            cmd= null;
            List<string> arguments= new List<string>();
            foreach (string arg in args)
            {
                if      ("--Gost3411"==arg) { use_XmlDsigGost3411UrlObsolete= true; }
                else if ("--Gost3410"==arg) { use_XmlDsigGost3410UrlObsolete= true; }
                else if (arg.StartsWith("--Uri="))
                {
                    UriToSign= arg.Substring("--Uri=".Length);
                }
                else if (!arg.StartsWith("--"))
                {
                    arguments.Add(arg);
                }
                else
                {
                    cmd= arg;
                }
            }
            return arguments.ToArray();
        }

        static string UriToSign= "";

        static void Usage()
        {
            Console.WriteLine("usage:");
            Console.WriteLine("  xmlsign.exe [options] --sign  <path_xml_in> <path_xml_out> <path_to_cert>");
            Console.WriteLine("  xmlsign.exe [options] --check <path_xml> <path_to_cert>");
            Console.WriteLine("options:");
            Console.WriteLine("  --Gost3411");
            Console.WriteLine("  --Gost3410");
            Console.WriteLine("  --Uri=<uri to sign>");
        }

        static void SignXml(string path_to_xml_file_in, string path_to_xml_file_out, string path_to_cert)
        {
            string extension= Path.GetExtension(path_to_cert);
            if (".cer" != extension.ToLower())
            {
                SignXml(path_to_xml_file_in, path_to_xml_file_out, new X509Certificate2(path_to_cert));
            }
            else
            {
                X509Certificate2 cert_from_file= new X509Certificate2(path_to_cert);
                X509Certificate2 cert_to_sign= GetCertificateWithPrivateKey(cert_from_file.Thumbprint);
                SignXml(path_to_xml_file_in, path_to_xml_file_out, cert_to_sign);
            }
            Console.WriteLine("ok signing!");
        }

        static void SignXml(string path_to_xml_file_in, string path_to_xml_file_out, X509Certificate2 cert)
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(path_to_xml_file_in);

            SignXml(xmlDoc, cert);

            xmlDoc.Save(path_to_xml_file_out);
        }

        static bool use_XmlDsigGost3411UrlObsolete= false;
        static bool use_XmlDsigExcC14NTransform= false;
        static bool use_XmlDsigGost3410UrlObsolete= false;

        public class AsymmetricAlgorithmForCert : AsymmetricAlgorithm
        {
            X509Certificate2 cert;
            public AsymmetricAlgorithmForCert(X509Certificate2 cert)
            {
                this.cert= cert;
                KeySizeValue= 512;
                LegalKeySizesValue= new KeySizes [] { new KeySizes(512,512,0) };

                Console.WriteLine("AsymmetricAlgorithmForCert.AsymmetricAlgorithmForCert()");
            }

            public override string KeyExchangeAlgorithm
            {
                get
                {
                    Console.WriteLine("AsymmetricAlgorithmForCert.KeyExchangeAlgorithm");
                    return "";
                }
            }
            public override string SignatureAlgorithm
            {
                get
                {
                    Console.WriteLine("AsymmetricAlgorithmForCert.SignatureAlgorithm ");
                    return "";
                }
            }

            protected override void Dispose(bool disposing)
            {
                Console.WriteLine("AsymmetricAlgorithmForCert.Dispose");
            }

            public override void FromXmlString(string xmlString)
            {
                Console.WriteLine("AsymmetricAlgorithmForCert.FromXmlString");
            }

            public override string ToXmlString(bool includePrivateParameters)
            {
                Console.WriteLine("AsymmetricAlgorithmForCert.ToXmlString");
                return "";
            }
        }

        public static void SignXml(XmlDocument xmlDoc, X509Certificate2 cert)
        {
            SignedXml signedXml = new SignedXml(xmlDoc);

            if (null==cert)
                Console.WriteLine("(null==cert)");
            Console.WriteLine("cert.SignatureAlgorithm: {0}",null==cert.SignatureAlgorithm ? "" : cert.SignatureAlgorithm.Value);
            Console.WriteLine("cert.HasPrivateKey: {0}",cert.HasPrivateKey);

            try
            {
                signedXml.SigningKey = cert.PrivateKey;
            }
            catch (Exception ex)
            {
                Console.Write("can not get PrivateKey: ");
                Console.Write(ex);
                Console.WriteLine();

                if ("1.3.14.3.2.29" == cert.SignatureAlgorithm.Value)
                {
                    throw ex;
                }
                else
                {
                    signedXml.SigningKey = new AsymmetricAlgorithmForCert(cert);
                }
            }

            Console.WriteLine("signedXml.SigningKey.KeySize={0}",signedXml.SigningKey.KeySize);
            foreach (KeySizes ks in signedXml.SigningKey.LegalKeySizes)
            {
                Console.WriteLine("MinSize= {0}, MaxSize= {1}, SkipSize= {2}",ks.MinSize,ks.MaxSize,ks.SkipSize);
            }

            /**/
            /*
t=CryptoPro.Sharpei.Gost3410CryptoServiceProvider
t=CryptoPro.Sharpei.Gost3410
t=System.Security.Cryptography.AsymmetricAlgorithm
t=System.Object
             * */
            /**/

            Reference reference = new Reference();
            safe_use_XmlDsigGost3411UrlObsolete(reference);
            reference.Uri = UriToSign;

            if (!use_XmlDsigExcC14NTransform)
            {
                XmlDsigEnvelopedSignatureTransform env = new XmlDsigEnvelopedSignatureTransform();
                reference.AddTransform(env);
            }
            else
            {
                XmlDsigExcC14NTransform c14 = new XmlDsigExcC14NTransform();
                reference.AddTransform(c14);
                Console.WriteLine("using XmlDsigExcC14NTransform");
            }

            signedXml.AddReference(reference);

            /**/
            KeyInfo keyInfo = new KeyInfo();
            //KeyInfoX509Data kdata = new KeyInfoX509Data(cert);
            KeyInfoX509Data kdata = new KeyInfoX509Data();
            kdata.AddIssuerSerial(DNFixer.NormalizeIssuerName(cert.IssuerName.Name), cert.SerialNumber);
            //kdata.AddIssuerSerial(cert.IssuerName.Name, cert.SerialNumber);
            keyInfo.AddClause(kdata);
            signedXml.KeyInfo = keyInfo;
            /**/

            safe_prepare_SignedInfo(signedXml.SignedInfo);

            TraceSignedXml(signedXml);

            signedXml.ComputeSignature();

            XmlElement xmlDigitalSignature = signedXml.GetXml();
            xmlDigitalSignature.Prefix= "ds";

            string sxmlDigitalSignature= xmlDigitalSignature.OuterXml;

            XmlNode sigNode= xmlDoc.ImportNode(xmlDigitalSignature, true);

            XmlNode sigNodeValue= sigNode["SignatureValue"];
            if (null != sigNodeValue)
                sigNodeValue.InnerText= SignatureValueFixer.FixSignature(sigNodeValue.InnerText);

            XmlNode uidNode= xmlDoc.DocumentElement["uid"];
            xmlDoc.DocumentElement.InsertAfter(sigNode,uidNode);
        }

        

        static void safe_prepare_SignedInfo(SignedInfo sinfo)
        {
            if (use_XmlDsigExcC14NTransform)
                sinfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;
            if (use_XmlDsigGost3410UrlObsolete)
            {
                /*System.Reflection.Assembly Sharpei_Xml= System.Reflection.Assembly.LoadFrom("CryptoPro.Sharpei.Xml.dll");
                Type t= Sharpei_Xml.GetType("CryptoPro.Sharpei.Xml.CPSignedXml");
                System.Reflection.FieldInfo fi= t.GetField("XmlDsigGost3410UrlObsolete");
                string value= (string)fi.GetValue(null);*/
                string value= "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";
                sinfo.SignatureMethod= value;
            }
        }

        static void safe_use_XmlDsigGost3411UrlObsolete(Reference reference)
        {
            if (use_XmlDsigGost3411UrlObsolete)
            {
                /*System.Reflection.Assembly Sharpei_Xml= System.Reflection.Assembly.LoadFrom("CryptoPro.Sharpei.Xml.dll");
                Type t= Sharpei_Xml.GetType("CryptoPro.Sharpei.Xml.CPSignedXml");
                System.Reflection.FieldInfo fi= t.GetField("XmlDsigGost3411UrlObsolete");
                string value= (string)fi.GetValue(null);*/
                string value= "http://www.w3.org/2001/04/xmldsig-more#gostr3411";
                reference.DigestMethod = value;
            }
        }

        public static void VerifyXmlFile_AndReportTheResult(string path_to_xml_file, string path_to_cert)
        {
            X509Certificate2 cert= new X509Certificate2(path_to_cert);
            if (VerifyXmlFile(path_to_xml_file,cert))
            {
                Console.WriteLine("verification of the signature is successefull");
            }
            else
            {
                Console.WriteLine("verification of the signature is failed");
            }
        }

        public static bool VerifyXmlFile(string path_to_xml_file, X509Certificate2 cert)
        {
            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.PreserveWhitespace = true;
            xmlDocument.Load(path_to_xml_file);

            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("ds:Signature");
            XmlElement signatureElement= (XmlElement)nodeList[0];

            return VerifyXmlDocument(xmlDocument,signatureElement,cert);
        }

        public static bool VerifyXmlDocument(XmlDocument xmlDocument, XmlElement signatureElement, X509Certificate2 cert)
        {
            SignedXml signedXml = new SignedXml(xmlDocument);

            signedXml.LoadXml(signatureElement);

            TraceSignedXml(signedXml);

            return signedXml.CheckSignature(cert,true);
        }

        static void TraceSignedXml(SignedXml signedXml)
        {
            Console.WriteLine("CanonicalizationMethod " + signedXml.SignedInfo.CanonicalizationMethod);
            Console.WriteLine("SignatureMethod " + signedXml.SignedInfo.SignatureMethod);
            Console.WriteLine("CanonicalizationMethodObject " + signedXml.SignedInfo.CanonicalizationMethodObject.GetType().FullName);

            foreach (Reference reference in signedXml.SignedInfo.References)
            {
                Console.WriteLine("  reference Uri:\"{0}\"",reference.Uri);
                Console.WriteLine("     DigestMethod:\"{0}\"",reference.DigestMethod);
                foreach (Transform transform in reference.TransformChain)
                {
                    Console.WriteLine("     transform " + transform.Algorithm);
                    Console.WriteLine("        " + transform.GetType().FullName);
                }
            }
        }

        static public X509Certificate2 GetCertificateWithPrivateKey(string tmb)
        {
            foreach (X509Certificate2 cert in GetPersonalCertificatesWithPrivateKey())
            {
                string ctmb = cert.Thumbprint;
                if (ctmb != null &&
                    ctmb.Equals(tmb, StringComparison.InvariantCultureIgnoreCase))
                {
                    return cert;
                }
            }
            return null;
        }

        static public IList<X509Certificate2> GetPersonalCertificatesWithPrivateKey()
        {
            return GetPersonalCertificates(true, StoreName.My, StoreLocation.CurrentUser, null);
        }

        delegate bool CheckSertificate(X509Certificate2 cert);

        static IList<X509Certificate2> GetPersonalCertificates(bool onlyWithPrivateKey, StoreName storeName, StoreLocation storeLocation, CheckSertificate check)
        {
            X509Store store = new X509Store(storeName, storeLocation);
            IList<X509Certificate2> certificates = new List<X509Certificate2>();
            store.Open(OpenFlags.ReadOnly);
            try
            {
                foreach (X509Certificate2 certificate in store.Certificates)
                {
                    if ((!onlyWithPrivateKey || certificate.HasPrivateKey) && certificate.NotAfter > DateTime.Now)
                    {
                        if (null == check || check(certificate))
                            certificates.Add(certificate);
                    }
                }
            }
            finally
            {
                store.Close();
            }
            return certificates;
        }
    }
}
