﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xmlsign
{
    public class SignatureValueFixer
    {
        public static string FixSignature(string txt)
        {
            int line_len= 73;
            StringBuilder sb= new StringBuilder();
            for (int pos= 0; pos<txt.Length; pos+= line_len)
            {
                sb.Append("\r\n    ");
                int to_end= txt.Length-pos;
                sb.Append(txt.Substring(pos,to_end > line_len ? line_len : to_end));
            }
            sb.Append("\r\n");
            return sb.ToString();
        }
    }

    public class DNFixer
    {
        static void ParseIssuerNamePart(string txt, out string name, out string value)
        {
            int pos= txt.IndexOf('=');
            name= txt.Substring(0,pos).Trim();
            value= txt.Substring(pos+1).Trim();
        }

        static Func<string,string> FixName(string fixed_name, Func<string,string> value_fixer)
        {
            return (s) => {return string.Format("{0}={1}",fixed_name,value_fixer(s));};
        }

        static void AddFixer(Dictionary<string, Func<string, string>> vfn, string right_name, string alter_name, Func<string,string> value_fixer)
        {
            vfn.Add(alter_name, FixName(right_name,value_fixer));
            vfn.Add(right_name, FixName(right_name,value_fixer));
        }

        static string DoNotFix(string txt)
        {
            return txt;
        }

        static string FixQuotes(string txt)
        {
            return !(txt.StartsWith("\"") && txt.EndsWith("\"") && txt.Length>1) ? txt 
                : string.Format("\"{0}\"",txt.Substring(1,txt.Length-2).Replace("\"\"","\\\""));
        }

        static string FixInnOgrnNumber(string txt)
        {
            if (txt.StartsWith("#"))
            {
                return txt;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("#12");
                sb.Append(txt.Length.ToString("X2"));
                foreach (char c in txt.ToCharArray())
                {
                    int ic= c;
                    sb.Append(ic.ToString("X2"));
                }
                return sb.ToString();
            }
        }

        static Dictionary<string,Func<string,string>> _ValueFixersByNames= null;
        static Dictionary<string, Func<string, string>> ValueFixersByNames
        {
            get
            {
                if (null == _ValueFixersByNames)
                {
                    Dictionary<string, Func<string, string>> vfn= new Dictionary<string,Func<string,string>>();

                    AddFixer(vfn,"2.5.4.3",                 "CN",   DoNotFix); // InfoTrust
                    AddFixer(vfn,"2.5.4.11",                "OU",   DoNotFix); // Удостоверяющий центр
                    AddFixer(vfn,"2.5.4.10",                "O",    FixQuotes); // "ООО НПП ""Ижинформпроект""
                    AddFixer(vfn,"2.5.4.7",                 "L",    DoNotFix); // Ижевск
                    AddFixer(vfn,"2.5.4.8",                 "S",    DoNotFix); // 18 Удмуртская Республика
                    AddFixer(vfn,"2.5.4.6",                 "C",    DoNotFix); // RU
                    AddFixer(vfn,"1.2.840.113549.1.9.1",    "E",    DoNotFix); // pki@infotrust.ru
                    AddFixer(vfn,"1.2.643.3.131.1.1",       "ИНН",  FixInnOgrnNumber); // 001831014533
                    AddFixer(vfn,"1.2.643.100.1",           "ОГРН", FixInnOgrnNumber); // 1021801161140 

                    _ValueFixersByNames= vfn;
                }
                return _ValueFixersByNames;
            }
        }

        static string NormalizeIssuerNamePart(string name, string value)
        {
            Func<string,string> fixer;
            if (!ValueFixersByNames.TryGetValue(name, out fixer))
            {
                return string.Empty;
            }
            else
            {
                return fixer(value);
            }
        }

        static string NormalizeIssuerNamePart(string txt)
        {
            string name, value;
            ParseIssuerNamePart(txt,out name, out value);
            return NormalizeIssuerNamePart(name, value);
        }

        public static string NormalizeIssuerName(string txt)
        {
            string [] parts= txt.Split(',');
            StringBuilder sb= new StringBuilder();
            foreach (string part in parts)
            {
                string normalized_part= NormalizeIssuerNamePart(part);
                if (!string.IsNullOrEmpty(normalized_part))
                {
                    if (0!=sb.Length)
                        sb.Append(", ");
                    sb.Append(NormalizeIssuerNamePart(part));
                }
            }
            return sb.ToString();
        }
    }
}
